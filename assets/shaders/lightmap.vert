#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout (location = 0) in vec3 vp;
layout (location = 1) in vec2 vn;
layout (location = 2) in vec2 vt;

// out vec3 light_vector;
// out vec3 normal_vector;
// out vec3 halfway_vector;

out vec2 textureCoordinates;

void main() {
  textureCoordinates = vt;
  gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vp, 1.0);

  // vec4 vertexViewSpace = viewMatrix * modelMatrix * vec4(vp, 1.0);
  // vec3 normal = normalize(vn);

  // normal_vector = (inverse(transpose(viewMatrix * modelMatrix)) * vec4(normal, 0.0)).xyz;
}
