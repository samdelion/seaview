#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout (location = 0) in vec3 vp;
layout (location = 2) in vec2 vt;

out vec2 textureCoordinates;

void main() {
  textureCoordinates = vt;
  gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vp, 1.0);
}
