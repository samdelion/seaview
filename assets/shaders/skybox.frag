// Taken from: http://gamedev.stackexchange.com/questions/60313/implementing-a-skybox-with-glsl-version-330

#version 330

uniform samplerCube cubeMapTexture;

smooth in vec3 eyeDir;

out vec4 fragColor;

void main()
{
  fragColor = texture(cubeMapTexture, eyeDir); 
}