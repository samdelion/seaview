#version 330

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform vec2 startCoords;
uniform vec2 endCoords;

out vec2 texCoords;

void main()
{
    vec4 start = vec4(startCoords, 0.0, 1.0);
    vec4 end = vec4(endCoords, 0.0, 1.0);

    float xOffset = abs(start.x - end.x);
    float yOffset = abs(start.y - end.y);

    gl_Position = start;
    texCoords = vec2(0.0, 0.0);
    EmitVertex();

    gl_Position = start + vec4(xOffset, 0.0, 0.0, 0.0);
    texCoords = vec2(1.0, 0.0);
    EmitVertex();

    gl_Position = start + vec4(0.0, -yOffset, 0.0, 0.0);
    texCoords = vec2(0.0, 1.0);
    EmitVertex();

    gl_Position = end;
    texCoords = vec2(1.0, 1.0);
    EmitVertex();


    EndPrimitive();
}