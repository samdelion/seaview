#version 330

in vec2 texCoords;

out vec4 fragColor;

uniform float transparencyFactor;

uniform sampler2D basicTexture;

void main()
{
    vec4 texel = texture(basicTexture, texCoords);
    texel.w = transparencyFactor * texel.w;
    fragColor = texel;
}