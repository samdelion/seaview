#version 330

in vec2 textureCoordinates;

out vec4 frag_colour;

uniform sampler2D basicTexture;

void main () {
  vec4 texel = texture(basicTexture, textureCoordinates);
  /*frag_colour = vec4(textureCoordinates, 0.0f, 1.0f);*/
  frag_colour = texel; 
}
