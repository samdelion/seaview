#version 330

in vec2 textureCoordinates;

out vec4 frag_colour;

uniform sampler2D diffuseTexture;
uniform sampler2D lightmapTexture;

void main () {
  vec4 diffuseTexel = texture(diffuseTexture, textureCoordinates);
  vec4 lightmapTexel = texture(lightmapTexture, textureCoordinates);

  vec4 emissive_color = vec4(0.0, 1.0, 0.0, 1.0);
  vec4 ambient_color = vec4(1.0, 1.0, 1.0, 1.0);
  vec4 diffuse_color = vec4(1.0, 1.0, 1.0, 1.0);
  vec4 specular_color = vec4(0.0, 0.0, 1.0, 1.0);

  float emissive_contribution = 0.02;
  float ambient_contribution = 0.28;
  float diffuse_contribution = 0.70;
  float specular_contribution = 0.00;

  vec4 c = diffuseTexel * lightmapTexel;
  frag_colour = emissive_color * emissive_contribution +
              ambient_color * ambient_contribution * diffuseTexel +
              diffuse_color * diffuse_contribution * c; 
}
