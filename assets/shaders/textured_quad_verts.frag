#version 330

smooth in vec2 texCoord;

out vec4 fragColor;

uniform float transparencyFactor;

uniform sampler2D basicTexture;

void main()
{
    vec4 texel = texture(basicTexture, texCoord);
    texel.w = transparencyFactor * texel.w;
    fragColor = texel;
    // fragColor = vec4(1.0, 0.0, 0.0, 1.0);
}