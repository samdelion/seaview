all music sourced from the 'xKito Music' user on youtube with full permission, can be contacted at xkitomusic@gmail.com for enquiries.

all music files must be named musx where x is in the range 0-6
e.g
mus0
mus1
mus2
mus3
mus4
mus5
mus6

if you would like to use your own music convert it to a .wave file using:
http://audio.online-convert.com/convert-to-wav

then put it in the sound assets folder and rename it to the correct conventions.

for a list of sfxy files and there corresponding triggers go to sfx.txt