#ifndef SEA_CHARACTER_H
#define SEA_CHARACTER_H

#include "math/Globals.h"
#include "math/Vec3.h"
#include "math/Quat.h"
#include "math/Plane.h"
#include "game/test/PlayerWorldCollision.h"
using namespace sea_math;

namespace sea
{
    class Character
    {
    private:
        struct RespawnPt
        {
            sea_math::Vec3  location,
                            velocity,
                            acceleration,
                            lcn;
            sea_math::Quat  orientation;
        };

        const unsigned int JUMP_MODE        = 1;        // Single jumping

        const sea_math::Vec3  SPAWN_POINT   = sea_math::Vec3(-15.4f, 10.0f, -12.8f);

        const bool  SLOPE_JUMPING           = true;
        const bool  INERTIA_JUMPING         = true;
        const bool  FALL_JUMPING            = false;
        const bool  WALL_JUMPING            = false;
        const bool  GOD_FLIGHT              = false;
        const bool  SCALAR_INERTIA_AIR      = true;
        const bool  SCALAR_INERTIA_LAND     = false;

        const float JUMP_STRENGTH           = 95.0f;
        const float RUN_ACCEL               = 600.0f;
        const float RUN_DECEL               = 1200.0f;
        const float RUN_SPEED_MAX           = 150.0f;

        // For best air accel, you need to accelerate at the angle given by acos((AIR_SPEED_SOFT_MAX - deltaTime * AIR_ACCEL)/|velocity|).
        // Source air strafing works as this angle is very close to 90 degrees, thus A & D are effective.
        // Therefore for the same feeling, you can aim to achieve: AIR_SPEED_SOFT_MAX - deltaTime * AIR_ACCEL == 0. 
        // Unlike Source 66FPS physics update, this does depend on the rendering update, and thus, the game running at 60fps.
        const float AIR_ACCEL = (12.0f * RUN_SPEED_MAX);

        const float STEEPEST_FOOTHOLD       = SEA_PI / 10.0f;
        const float RADIUS                  = 1.0f;
        const float JUMP_PADDING            = COLLIDE_PADDING;
        const float TIME_TO_LIVE            = 0.0f;

        unsigned int    m_score,                        // Current score for this character
                        m_jumps,                        // Number of jumps used since last time on a surface
                        m_jumpMode;                     // 0 = infinite, 1 = single, 2 = double jumping

        bool            m_slopeJump,                    // Jump direction is affected by surface gradient
                        m_inertiaJump,                  // Jumping combines with motion in jump direction
                        m_fallJump,                     // The first jump can occur while airborne
                        m_wallJump,                     // Jump from steep surfaces
                        m_godFlight,                    // Ignore gravity
                        m_scalarInertiaAir,             // Inertia preserves magnitude only, not direction, airborne
                        m_scalarInertiaLand,            // Inertia preserves magnitude only, not direction, grounded
                        m_isGrounded;                   // Touch a surface that isn't too steep to stand on

        float           m_jumpStrength,                 
                        m_runAccel,                     // Acceleration from user input when running at less than m_runSpeedMax
                        m_runDecel,                     // Deceleration strength when no input is applied, down to m_coastSpeedMax
                        m_runSpeedMax,                  // Maximum running speed the character can accelerate to under user input
                        m_airAccel,
                        m_steep,                        // Surfaces with normals greater than this are considered walls. 0 == flat ground.
                        m_radius,                       // Character radius
                        m_timeToLive;                   // Time before character dies, determined per-level

        sea_math::Vec3  m_location,
                        m_velocity,
                        m_acceleration,
                        m_lcn;                          // Last Collision Normal - Normal of the surface last collided with

        RespawnPt       m_respawnPt;
        sea_math::Quat  m_orientation;

    public:

        /**
        *   Create a Character object.
        *
        */
        Character();

        /**
        *   Set character's score, restricted to between 0 and 100 points.
        *
        *   @param  newScore    unsigned int, intended new score
        */
        void SetScore(unsigned int newScore);

        /**
        *   Adjust character's score, restricted to between 0 and 100 points.
        *
        *   @param  newScore    unsigned int, amount by which to adjust score
        */
        void AdjustScore(unsigned int extraScore);

        /**
        *   Retrieve character's current score
        *
        *   @return unsigned int, character score
        */
        unsigned int GetScore() const;

        /**
        *   Retrieve the character's radius
        *
        *   @return float, character radius
        */
        float GetRadius() const;

        /**
        *   Set character's time to live, restricted to between 0 and 100 seconds.
        *
        *   @param  newTime float, intended new time to live
        */
        void SetTimeToLive(float newTime);

        /**
        *   Adjust character's time to live, restricted to between 0 and 100 seconds.
        *   
        *   @param  extraTime   float, amount by which to adjust time to live
        */
        void AdjustTimeToLive(float extraTime);

        /**
        *   Retrieve remaining time to live
        *
        *   @return float, time to live in seconds
        */
        float GetTimeToLive() const;

        /**
        *   Set the last collision normal hit by the character sphere
        *
        *   @param  lcn         const Vec3 reference, last collision normal
        */
        void SetLCN(const sea_math::Vec3 &lcn);

        /**
        *   Retrieve last collision normal the character sphere collided with
        *
        *   @return Vec3, last collision normal
        */
        sea_math::Vec3 GetLCN() const;

        /**
        *   Set character's orientation
        *
        *   @param  orientation const Quat reference, intended orientation
        */
        void SetOrientation(const sea_math::Quat& orientation);

        /**
        *   Adjust character's orientation
        *
        *   @param  orientation const Quat reference, intended orientation
        */
        void Reorient(float sensitivity, int deltaX, int deltaY, float deltaTime);

        /**
        *   Retrieve character's orientation
        *
        *   @return Quat, character orientation
        */
        sea_math::Quat GetOrientation() const;

        /**
        *   Set character's location
        *
        *   @param  loca        const Vec3 reference, intended location
        */
        void SetLocation(const sea_math::Vec3& loca);

        /**
        *   Retrieve character's location
        *
        *   @return             Vec3, character location
        */
        sea_math::Vec3 GetLocation() const;

        /**
        *   Stop character motion
        */
        void StopMoving();

        /**
        *   Set character's velocity
        *
        *   @param  loca        const Vec3 reference, intended velocity
        */
        void SetVelocity(const sea_math::Vec3& velo);

        /**
        *   Retrieve character's velocity
        *
        *   @return             Vec3, character velocity
        */
        sea_math::Vec3 GetVelocity() const;

        /**
        *   Stop character acceleration
        */
        void StopAccelerating();

        /**
        *   Set character's acceleration
        *
        *   @param  loca        const Vec3 reference, intended acceleration
        */
        void SetAccel(const Vec3& accel);

        /**
        *   Retrieve character's acceleration
        *
        *   @return Vec3, character acceleration
        */
        sea_math::Vec3 GetAccel() const;

        /**
        *   Reduce character acceleration according to the character's running deceleration value,
        *   only if the character is grounded. While this function does not affect velocity, 
        *   deltaTime is used to ensure acceleration is not produced in the wrong direction.
        *
        *   @param  deltaTime   float, time over which deceleration would apply
        *   @return bool, true running deceleration was applied
        */
        bool ApplyRunningDecelForce(float deltaTime);

        /**
        *   Recalculate velocity according to the character's current acceleration, given
        *   a deltaTime, restricted by terminal velocity and air max velocity.
        *
        *   @param  deltaTime       float, time over which to accelerate
        *   @param  terminalVelo    float, maximum velocity magnitude
        *   @param  airMaxVelo      float, maximum airborne velocity magnitude in direction of motion only 
        */
        void RefreshVelocity(float deltaTime, float terminalVelo, float airMaxVelo);

        /**
        *   Set the character's grounded state
        *
        *   @param  grounded        bool, intended grounded state
        */
        void SetGrounded(bool grounded);

        /**
        *   Retrieve character's grounded state. The character is assumed to be grounded when
        *   within JUMP_PADDING distance of a surface with a normal that isn't too steep.
        *
        *   @return bool, true character is grounded
        */
        bool IsGrounded() const;

        /*
        * Retrieve a letter score determined by the character's point score
        *
        *   @return char, letter score a, b, c, d, e or f
        */
        char GetLetterScore() const;

        /**
        *   Retrieve the direction in which the character is facing. This is like 
        *   getting the character's local Z.
        *
        *   @return Vec3, direction aligned with character vision
        */
        sea_math::Vec3 Forward() const;

        /**
        *   Retrieve the character's rightwards direction. This is like getting 
        *   the character's local X.
        *
        *   @return Vec3, direction aligned with character's right
        */
        sea_math::Vec3 Right() const;

        /**
        *   Retrieve the direction in which the character is facing. This is like
        *   getting the character's local Y.
        *
        *   @return Vec3, direction aligned with character's up
        */
        sea_math::Vec3 Up() const;

        /**
        *   Restrict character's speed to a given speed if airborne, or
        *   the character's own running maximum if grounded.
        *
        *   @param  speedLim    float, speed to limit to
        *   @return bool, true if there was a collision
        */
        bool CapSpeed(float speedLim);

        /**
        *   Shift the character's position, ignoring collision. The amount to shift
        *   by scales with the world's gravity.
        *
        *   @param  x           int, amount to shift right, horizontally
        *   @param  y           int, amount to shift up
        *   @param  z           int, amount to shift forward, horizontally
        *   @return bool, true if there was a collision
        */
        void Displace(const int x, const int y, const int z);

        /**
        *   Set the character's last collision normal and jump count, depending on 
        *   the surface touched. If the surface was the ground, jumps count is reset.
        *   Otherwise jumps count is only reset if wall jumping is enabled.
        *
        *   @param  normal      const Vec3 reference, normal of the surface the character has touched
        */
        void TouchDown(const Vec3 &normal);

        /**
        *   Report whether the character is touching any surface.
        *
        *   @param  tris        const reference to vector of const Triangles, the world mesh
        *   @return bool, true if within JUMP_PADDING distace of any triangle in the world mesh
        */
        bool IsTouchingSurface(const std::vector<const Triangle> &tris) const;

        /**
        *   Add acceleration in the horizontal component of where the character is looking.
        *   Adjusts for changed gravity direction.
        *
        *   @param  normal      const Vec3 reference, acceleration force to add
        */
        void AddInputForce(const sea_math::Vec3 &inputForce);

        /**
        *   Report the current "down"
        *
        *   @return Vec3, direction of down
        */
        sea_math::Vec3 TheEnemysGate() const;

        /**
        *   Add acceleration force that isn't from user input. This allows different
        *   speed limits to be applied.
        *
        *   @param  force       const Vec3 reference, acceleration to add
        */
        void AddExternalForce(const Vec3 &force);

        /**
        *   Report whether character's LCN represents a wall
        *
        *   @return bool, true if character's LCN is steeper than the character's steepest foothold
        */        bool IsLCNTooSteep() const;

        /**
        *   Directly change character's velocity to jump if the right conditions for a jump are met.
        *   Jumping can only occur if the character is grounded, or is not grounded but fall jumping
        *   is on and the character has not yet jumped the number of permitted jumps.
        *
        *   @param  tris        const reference to vector of const Triangles, the world mesh
        *   @return bool, true if jumping is permitted
        */
        bool Jump(const std::vector<const Triangle> &tris);

        /**
        *   Set the respawn point to the character's current location, velocity and orientation.
        */
        void SetRespawnHere();

        /**
        *   Reset the respawn point to the original point
        */        
        void ResetRespawn();

        /**
        *   Relocate the character to the last set respawn point
        */
        void Respawn();

        /**
        *   Relocate the character to the original respawn point
        */
        void RespawnToZero();

        /**
        *   Cycle through jump modes in this order: infinite, single, double.
        */
        void CycleJumpMode();

        /**
        *   Return the current jump mode setting
        *
        *   @return unsigned int, 0 if infinite, 1 if single, 2 if double jumping
        */
        unsigned int GetJumpMode() const;

        /**
        *   Enable/disable the gradient-defined jumps effect
        */
        void ToggleSlopeJump();

        /**
        *   Return the current gradient-defined jumps setting
        *
        *   @return bool, true if gradient-defined jumping is on
        */
        bool GetSlopeJump() const;

        /**
        *   Enable/disable the inertia-influenced jumps effect
        */
        void ToggleInertiaJump();

        /**
        *   Return the current inertia-influenced jumps setting
        *
        *   @return bool, true if inertia-influenced jumping is on
        */
        bool GetInertiaJump() const;

        /**
        *   Enable/disable the ability to jump when falling. This applies only
        *   to the first jump if multiple jumping is permitted.
        */
        void ToggleFallJump();

        /**
        *   Return the fall jumping setting
        *
        *   @return bool, true if fall jumping is on
        */
        bool GetFallJump() const;

        /**
        *   Enable/disable the ability to jump from walls
        */
        void ToggleWallJump();

        /**
        *   Return the wall jumping setting
        *
        *   @return bool, true if wall jumping is on
        */
        bool GetWallJump() const;

        /**
        *   Enable/disable the ability to ignore gravity
        */
        void ToggleGodFlight();


        /**
        *   Return the god mode flight setting
        *
        *   @return bool, true if god mode flight is on
        */
        bool GetGodFlight() const;

        /**
        *   Enable/disable the scalar air inertia effect
        */
        void ToggleScalarAirInertia();

        /**
        *   Return the scalar air inertia setting
        *
        *   @return bool, true if scalar air inertia is on
        */
        bool GetScalarAirInertia() const;

        /**
        *   Enable/disabled the scalar land inertia effect
        */
        void ToggleScalarLandInertia();

        /**
        *   Return the scalar land inertia setting
        *
        *   @return bool, true if scalar land inertia is on
        */
        bool GetScalarLandInertia() const;
    };
}

#endif // SEA_CHARACTER_H
