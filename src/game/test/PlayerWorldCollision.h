#ifndef SEA_PLAYER_WORLD_COLLISION_H
#define SEA_PLAYER_WORLD_COLLISION_H

#include <vector>
#include <iostream>
#include <iomanip>
#include "math/Vec3.h"
#include "math/Plane.h"

#define MAX_RECURSIONS  10      // Don't recurse more than this many times. Increase if velocity feels wrong (but it's not likely to)
#define COLLIDE_PADDING	3.0f	// Without padding, float errors will push us through walls.
#define	CUTOFF_SPEED	0.05f	// The higher this is, the earlier recursion is ended.
                                // Sharp concave corners are the main culprits.

struct Triangle
{
  sea_math::Vec3	cornerA,
          cornerB,
          cornerC;
};

/**
*	Report whether two points are on the same side as a given line.
*
*   @param  p1	const Vec3 reference, first point to check
*	@param	p2	const Vec3 reference, second point to check
*	@param	a	const Vec3 reference, pivotal point to define line
*	@param	b	const Vec3 reference, second point to define line
*   @return		bool, true if p1 and p2 are both on the same side of line AB
*/
bool AreSameSide(const sea_math::Vec3 &p1, const sea_math::Vec3 &p2, const sea_math::Vec3 &a,
                 const sea_math::Vec3 &b);

/**
*	Report whether a given point is inside a given triangle.
*
*   @param  tri const Triangle reference, triangle to check inside
*   @param  p	const Vec3 reference, point to check
*   @return		bool, true if p is inside tri
*/
bool CheckTriHit(const Triangle &tri, const sea_math::Vec3 &p);

/**
*	Calculate output velocity from a moving sphere colliding at a given point.
*
*	@param	posi		const Vec3 reference, centre of sphere
*	@param	velo		const Vec3 reference, sphere velocity
*	@param	time		const float, travel time remaining
*   @param  hitPoint	const Vec3 reference, point of collision between sphere and sliding plane
*   @return				Vec3, velocity resulting from collision
*/
sea_math::Vec3 DeflectVelocity(const sea_math::Vec3 &posi, const sea_math::Vec3 &velo,
                               const float time, const sea_math::Vec3 &hitPoint);

/**
*	Find the lowest positive solution for a quadratic defined by ax^2 + bx + c = 0.
*
*	@param		a		const float, a coefficient
*	@param		b		const float, b coefficient
*	@param		c		const float, c coefficient
*	@param[out]	root	float reference, lowest positive root if exists
*	@return				bool, true if positive root found
*/
bool getLowestRoot(const float a, const float b, const float c, float& root);

/**
*	Calculate the time and point of a unit sphere's collision against a given edge.
*
*	@param		posi	const Vec3 reference, sphere centre
*	@param		velo	const Vec3 reference, sphere velocity
*	@param		v0		const Vec3 reference, edge point
*	@param		v1		const Vec3 reference, edge point
*	@param[out]	time	float reference, time to collision
*	@param[out]	collPt	float reference, point of collision
*	@return				bool, true if future potential collision detected
*/
bool IntersectEdge(const sea_math::Vec3 &posi, const sea_math::Vec3 &velo,
                   const sea_math::Vec3 &v0, const sea_math::Vec3 &v1, float& time,
                   sea_math::Vec3& collPt);

/**
*	Calculate the time taken for a unit sphere to collide against a given vertex.
*
*	@param		posi	const Vec3 reference, sphere centre
*	@param		velo	const Vec3 reference, sphere velocity
*	@param		vert	const Vec3 reference, vertex
*	@param[out]	time	float reference, time to collision
*	@return				bool, true if future potential collision detected
*/
bool IntersectVertex(const sea_math::Vec3 &posi, const sea_math::Vec3 &velo,
                     const sea_math::Vec3 &vert, float& time);

/**
*	Determine the type, time and point of intersection that will occur with a given plane.
*
*	@param		posi	const Vec3 reference, sphere centre
*	@param		velo	const Vec3 reference, sphere velocity
*   @param      plane   const Plane reference, plane to collide against
*   @param[out] t0      float reference, first moment of intersecting plane
*   @param[out] t1      float reference, last moment of intersecting plane
*	@param[out]	collPt	Vec3 reference, point of collision with plane
*	@return				int, 0 if moving away from plane, 1 is approaching and clear,
*                       2 if approaching and already intersecting
*/
int IntersectPlane(const sea_math::Vec3 &posi, const sea_math::Vec3 &velo, const sea_math::Plane &plane,
                   float &t0, float &t1, sea_math::Vec3 &collPt);
/**
*	Given the status, time, and point of an edge collision and a vert collision,
*   output the one that occurs first.
*
*	@param		hitV    const bool, whether vertex collision occurred
*	@param		hitE    const bool, whether edge collision occurred
*	@param		vTime   const float, time of vertex collision
*	@param		eTime   const float, time of edge collision
*	@param		vCollPt const Vec3 reference, point of vertex collision
*	@param		eCollPt const Vec3 reference, point of edge collision
*	@param[out]	minTime	float reference, time of earliest collision
*	@param[out]	collPt	Vec3 reference, point of earliest collision
*	@return				bool, true if there was a collision
*/
bool ConsolidateHits(const bool hitV, const bool hitE, const float vTime, const float eTime,
           const sea_math::Vec3 &vCollPt, const sea_math::Vec3 &eCollPt, float& minTime,
                     sea_math::Vec3 &collPt);

/**
*	Report whether a moving sphere will hit any edge between 3 given points.
*
*	@param		tri     const Triangle reference, tri with the 3 edges to check
*	@param		posi    const Vec3 reference, sphere centre
*	@param		velo    const Vec3 reference, sphere velocity
*	@param[out]	minTime	float reference, time of earliest collision
*	@param[out]	collPt	Vec3 reference, point of earliest collision
*	@return				bool, true if there was a collision
*/
bool Check3EdgeHits(const Triangle &tri, const sea_math::Vec3 &posi, const sea_math::Vec3 &velo,
                    float &minTime, sea_math::Vec3& collPt);

/**
*	Report whether a moving sphere will hit any of 3 given points.
*
*	@param		tri     const Triangle reference, tri with the 3 vertices to check
*	@param		posi    const Vec3 reference, sphere centre
*	@param		velo    const Vec3 reference, sphere velocity
*	@param[out]	minTime	float reference, time of earliest collision
*	@param[out]	collPt	Vec3 reference, point of earliest collision
*	@return				bool, true if there was a collision
*/
bool Check3VertHits(const Triangle &tri, const sea_math::Vec3 &posi, const sea_math::Vec3 &velo,
          float& minTime, sea_math::Vec3& collPt);

/**
*	Report the time and position a unit sphere with given position and velocity
*   will collide with a given triangle in any way.
*
*	@param		tri     const Triangle reference, triangle to collide with
*	@param		posi    const Vec3 reference, sphere centre
*	@param		velo    const Vec3 reference, sphere velocity
*	@param[out]	minTime	float reference, time of earliest collision
*	@param[out]	collPt	Vec3 reference, point of earliest collision
*	@return				bool, true if there was a collision
*/
bool GetCollisionData(const Triangle &tri, const sea_math::Vec3 &posi, const sea_math::Vec3 &velo,
                      float &minTime, sea_math::Vec3 &collPt);

/**
*	Given a set of Triangles, and a sphere with location and velocity,
*   move through a recursive set of collision steps and update sphere values.
*
*	@param		tris        const vector of const Triangles reference, set of tris to collide with
*	@param      time        float, amount of time to move sphere through
*	@param[out]	posi        Vec3 reference, sphere centre
*	@param[out]	velo        Vec3 reference, sphere velocity
*	@param[out]	lastNormal	Vec3 reference, normal of the final collision
*   @param[out] grounded    bool reference, whether the ground was encountered
*	@return                 int, number of collisions encountered recursively

*/
int MoveWithCollisions(const std::vector<const Triangle> &tris, float timeRem, sea_math::Vec3 &posi,
                       sea_math::Vec3 &velo, sea_math::Vec3 &lastNormal, bool &grounded);

#endif
