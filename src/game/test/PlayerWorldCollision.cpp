/******************************************************************************
Spherical collision detection, brute force triangle checking.
The "sphere" is a cube, but the algorithm is running as a sphere. Whatever
model can be put on top of it later.
Ellipsoidal CD is easily added later by converting vertex points, velocity,
and sphere centre into ellipsoid space.
WASD controls. LCTRL to stop. Left mouse button to move scene.

Disclaimers!!
Yes the commenting, const'ing, code compartmentalising, variable naming,
ie... everything... is horrible. I made Shay's look good. Also, timing is
not dealt with properly, just hacky for now. That really depends on our
engine's game loop anyway, I believe.

@Dan Campbell

@todo
Walls bug - radius needs to be applied for this too + needs last 2 LCNs
Jumping needs to ignore wall LCNs if wall jumping off
Limit mouse motion
Combine with octrees
const checking
Cleanup .cpp file
Ellipsoid space still
Take jump off keyrepeat
Hardcode global to disable switches when playing
Stand still on sloped ground
Bunny hopping, hold jump bunny hopping

Character class actually doing the work.


Dash
Dynamic jumping (not important)
Two last god modes (not important)
Smoothen mouse motion (can't find a way to!)
******************************************************************************/
#include "game/test/PlayerWorldCollision.h"
#include "game/test/Character.h"

// Hacky global ahoy
int g_testRecurs = 0;

bool AreSameSide(const Vec3& p1, const Vec3& p2, const Vec3& a, const Vec3& b)
{
  // With a common pivot, Cross product line 1 and line 2.
  // Then, still using the common pivot, Cross product line 1 and line 3.
  // Then Dot product the 2 results. If this returns a positive, then line 2 and line 3 both end
  // on the same side of line 1. Lots of operations but the good is that it doesn't depend on
  // float-precise sitting on the tri being checked. Note to self - Faster ways doable

  Vec3	cp1 = Cross(b - a, p1 - a),
      cp2 = Cross(b - a, p2 - a);

  return Dot(cp1, cp2) >= 0.0f;
}

bool CheckTriHit(const Triangle &tri, const Vec3 &p)
{
  // If a point is on the triangle side of all 3 edges of a triangle, then it is inside the tri.
    Vec3 a = tri.cornerA,
         b = tri.cornerB,
         c = tri.cornerC;

  return AreSameSide(p, a, b, c) && AreSameSide(p, b, a, c) && AreSameSide(p, c, a, b);
}

sea_math::Vec3 DeflectVelocity(const Vec3 &posi, const Vec3 &velo, const float timeSurplus, const Vec3 &hitPoint)
{
    // @todo This is returning slight inaccuracies (at about the 5th d.p.) and I'm not sure why.


  // The sliding plane is determined by the knowledge that a line from a sphere's point of impact
  // to that sphere's centre is perpendicular to the collision's sliding plane.
  Vec3    N = Normalize(posi - hitPoint);

  // The resulting destination if there had been no collision.
  Vec3	oldDest = hitPoint + timeSurplus * velo;

  // Resulting destination point on plane is old destination + (normal * distance from plane).
  // Distance from plane is determined by plane equation.
  float	planeConst = Dot(N, hitPoint);
  float	distance = Dot(oldDest, N) - planeConst;
  Vec3	planeDest = oldDest - N * distance;

  // Simply speed = distance / time
  return	(planeDest - hitPoint) * (1.0f/timeSurplus);
}



bool getLowestRoot(const float a, const float b, const float c, float& root)
{
  float determinant = b * b - 4.0f * a * c;

  if (determinant < 0.0f)
    return false;

  float	root1 = (-b - sqrt(determinant)) / (2.0f * a),
      root2 = (-b + sqrt(determinant)) / (2.0f * a);

  if ((root1 < root2) && (root1 > 0.0f))
  {
    root = root1;
    return true;
  }
  else if ((root2 < root1) && (root2 > 0.0f))
  {
    root = root2;
    return true;
  }
  else
  {
      return false;
  }
}


// We reduce to a quadratic like with vertices,
// but first check against an infinite line then shrink to segment only, like with planes.
bool IntersectEdge(const Vec3 &posi, const Vec3 &velo, const Vec3 &v0, const Vec3 &v1, float &time, Vec3 &collPt)
{
  Vec3 edge = v1 - v0;
  Vec3 centreToVert = v0 - posi;

  float a = Dot(edge, edge) * -1.0f * Dot(velo, velo) + Dot(edge, velo) * Dot(edge, velo);
  float b = Dot(edge, edge) * 2.0f * Dot(velo, centreToVert) - 2.0f * Dot(edge, velo) * Dot(edge, centreToVert);
  float c = Dot(edge, edge) * (1 - Dot(centreToVert, centreToVert)) + Dot(edge, centreToVert) * Dot(edge, centreToVert);

  float intersectTime;
  if (getLowestRoot(a, b, c, intersectTime))
  {
    // So we have the time it crosses an infinite edge, but is that in our segment?
    // Parameterise the line as px = p0 + q * edge. Then we need q to be 0 < q < 1 to ensure it's on our segment.

    float q = (Dot(edge, velo) * intersectTime - Dot(edge, centreToVert)) / Dot(edge, edge);

    if (q > 0.0f && q < 1.0f)
    {
      // Hit the segment!
      collPt = v0 + q * edge;
      time = intersectTime;
      return true;
    }
    else
    {
        return false;
    }
  }
  else
  {
      return false;	// Doesn't hit the edge at all.
  }
}




/*


Reduce a vertex intersection to a quadratic as follows:
v = velocity, t = time, P0 = sphere centre, vert = vertex to intersect

We know a vert hits a sphere at "radius" distance from the sphere's centre.
x^2 + y^2 = r^2
From the dot product rule we know that this is when:
(v * t + P0 - vert).(v * t + P0 - vert) = 1^2
v^2*t^2 + (P0-vert)*v*t + (P0-vert).(P0-vert) = 1

A = v.v
B = 2*v*(P0-vert)
C = (P0-vert).(P0-vert) - 1
Then throw that at the quadratic solver.
*/
bool IntersectVertex(const Vec3 &posi, const Vec3 &velo, const Vec3 &vert, float& time)
{
  float	a = Dot(velo, velo),
            b = 2.0f * (Dot(velo, posi - vert)),
            c = Dot(posi - vert, posi - vert) - 1.0f;

  return getLowestRoot(a, b, c, time);
}

int IntersectPlane(const Vec3 &posi, const Vec3 &velo, const Plane &plane,
                   float &t0, float &t1, Vec3 &collPt)
{
  // If not approaching, definite bail.
  // Else, if intersecting, report as much. Can't be a tri hit.

  bool	isApproaching = false,
      isIntersecting = false;

  float	dist = plane.DistanceToPoint(posi),
            denominator = Dot(plane.Normal(), velo);        // Page 12


  //cout << "Dist: sphere centre to plane = " << dist << endl;
  //cout << "denom * dist = " << denominator * dist << endl;

  if (denominator * dist < 0.0f)
  {
    isApproaching = true;
    // cout << "Approaching plane" << endl;
    // Sphere is moving towards plane. Find when and where it will hit.
    // There will be two results, one for each side of the plane.

    if (dist <= 1.0f && dist >= -1.0f) // The sphere radius. Abs() this.
    {	// We're closer than the sphere radius, so already intersecting.
      // Still need to check verts and edges, just not tris.
      isIntersecting = true;

      //std::cout << "Already intersecting plane." << std::endl;

      t0 = t1 = 0.0f;		// Really, t0 has passed but t1 could still happen. But no need to care.
      collPt = posi;		// Note that this is not valid, since it's not a valid collision

      //cout << "Intersecting plane" << endl;
    }
    else
    {	// Approaching yes, intersecting no. Find t0, t1, and collPt.
      t0 = (1.0f - dist) / denominator;
      t1 = (-1.0f - dist) / denominator;

            if (t0 > t1)
            {   // Just make t0 the first of the two moments
                float temp = t0;
                t0 = t1;
                t1 = temp;
            }

      // Simple vector shenanigans.
      // If approach from "behind" the plane, add the normal, don't subtract it.
      collPt = posi - ((dist > 0.0f) ? plane.Normal() : (-plane.Normal())) + t0 * velo;
      //cout << "Plane intersection point is " << collisionPt.x << "," << collisionPt.y << "," << collisionPt.z << endl;
    }
  }
  else
  {
    // Not approaching so nothing matters at all.
  }

    if (!isApproaching)
    {
        return 0;
    }
    else if (!isIntersecting)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}


// Just takes a bunch of vert-hit and edge-hit data and returns the best result.
bool ConsolidateHits(const bool hitV, const bool hitE, const float vTime, const float eTime,
           const Vec3 &vCollPt, const Vec3 &eCollPt, float &minTime, Vec3 &collPt)
{
  if (hitV && hitE)
  {
        if ((vTime < eTime) && (vTime < minTime))
    {
      minTime = vTime;
      collPt = vCollPt;
      //std::cout << "HIT VERT (edge in there too)" << std::endl;
    } // Make 2 planes level if need be.
        else if (eTime < minTime)
    {
      minTime = eTime;
      collPt = eCollPt;
      //std::cout << "HIT EDGE (vert in there too)" << std::endl;
    }
    return true;
  }
    else if ((hitV) && (vTime < minTime))
  {
    minTime = vTime;
    collPt = vCollPt;
    //std::cout << "HIT VERT only" << std::endl;
    return true;
  }
    else if ((hitE) && (eTime < minTime))
  {
    minTime = eTime;
    collPt = eCollPt;
    //std::cout << "HIT EDGE only" << std::endl;
    return true;
  }
  else
  {
    return false;
  }
}

bool Check3EdgeHits(const Triangle &tri, const Vec3 &posi, const Vec3 &velo, float& minTime, Vec3& collPt)
{
  float	e0Time,
      e1Time,
      e2Time;

  Vec3	e0CollPt,		// Unlike with verts, each edge has infinite possible collision points.
      e1CollPt,
      e2CollPt;

  bool	e0Hit = IntersectEdge(posi, velo, tri.cornerA, tri.cornerB, e0Time, e0CollPt),
            e1Hit = IntersectEdge(posi, velo, tri.cornerB, tri.cornerC, e1Time, e1CollPt),
            e2Hit = IntersectEdge(posi, velo, tri.cornerC, tri.cornerA, e2Time, e2CollPt);

  if (e0Hit)
  {	// Compare #0 and #1.
    if (e1Hit && e1Time < e0Time)
    {
      minTime = e1Time;
      collPt = e1CollPt;
    }
    else
    {
      minTime = e0Time;
      collPt = e0CollPt;
    }

    // Compare the winner to #2.
    if (e2Hit && e2Time < minTime)
    {
      minTime = e2Time;
      collPt = e2CollPt;
    }
  }
  else if (e1Hit)
  {	// Compare #1 and #2.
    if (e2Hit && e2Time < e1Time)
    {
      minTime = e2Time;
      collPt = e2CollPt;
    }
    else
    {
      minTime = e1Time;
      collPt = e1CollPt;
    }
  }
  else if (e2Hit)
  {	// Just check #2.
    minTime = e2Time;
    collPt = e2CollPt;
  }
  else
  {
    // Empty reminder that it's possible to hit no edges.
  }

  if (e0Hit || e1Hit || e2Hit)
  {
    // cout << eMinTime << " TO EDGE." << endl;
  }

  return (e0Hit || e1Hit || e2Hit);
}

bool Check3VertHits(const Triangle &tri, const Vec3 &posi, const Vec3 &velo, float& minTime, Vec3& collPt)
{
  // But if the upcoming collision is not inside the tri, we still can't set a minTime.
  // The sphere might still scrape an edge or vertex first, so we have to check both. Verts are easier so they'll go first.
  // To check a sphere's intersection against verts, we just need to know when the distance == radius.
  // Since the radius is 1, we'll check when distance^2 is 1.

  float	v0Time,
      v1Time,
      v2Time;

    bool	v0Hit = IntersectVertex(posi, velo, tri.cornerA, v0Time),
      v1Hit = IntersectVertex(posi, velo, tri.cornerB, v1Time),
            v2Hit = IntersectVertex(posi, velo, tri.cornerC, v2Time);

  // Check all 3 verts, and get the minimum time from those that report a hit. Time for if-plantations.
  // The collision point is simply the vert itself.
  if (v0Hit)
  {	// Compare #0 and #1.
    if (v1Hit && v1Time < v0Time)
    {
      minTime = v1Time;
            collPt = tri.cornerB;  // Corner B to match v1Time, and so on.
        }
    else
    {
            minTime = v0Time;
            collPt = tri.cornerA;
    }

    // Compare the winner to #2.
        if (v2Hit && v2Time < minTime)
    {
            minTime = v2Time;
            collPt = tri.cornerC;
    }
  }
  else if (v1Hit)
  {	// Compare #1 and #2.
    if (v2Hit && v2Time < v1Time)
    {
            minTime = v2Time;
            collPt = tri.cornerC;
    }
    else
    {
            minTime = v1Time;
            collPt = tri.cornerB;
    }
  }
  else if (v2Hit)
  {	// Just check #2.
        minTime = v2Time;
    collPt = tri.cornerC;
  }
  else
  {
    // Just a reminder that it's possible no vertex was hit at all,
    // so vMinTime might still be empty.
  }

  if (v0Hit || v1Hit || v2Hit)
  {
    // cout << vMinTime << " TO VERT." << endl;
  }

  return (v0Hit || v1Hit || v2Hit);
}




bool GetCollisionData(const Triangle &tri, const Vec3 &posi, const Vec3 &velo,
                      float &minTime, Vec3 &collPt)
{
    Vec3    pt0 = tri.cornerA,
            pt1 = tri.cornerB,
            pt2 = tri.cornerC;

    bool hitDetected = false;
  float	t0,	// The first and last moment the object can possibly
      t1; // be intersecting. Found from plane intersection.

  // Define any point p's distance from a plane as dist = N.p + d, where N is the plane normal
  // and d is the plane constant. (This code should be inside IntersectPlane().)
  Vec3 N = Normalize(Cross(pt1 - pt0, pt2 - pt0));
  float d = 1.0f * Dot(N, pt0);  // mistake?

    Plane plane = Plane();
    plane.Init(pt0, pt1, pt2);
    int planeIntersectStat = IntersectPlane(posi, velo, plane, t0, t1, collPt);

    // Moving away. Could still hit edge or vert when travelling at a sharp angle out of a plane, but not face.
    // OR
    // Moving towards, but already intersecting plane, therefore face hit not possible but check verts and edges.
    if (planeIntersectStat == 0)
    {
        //std::cout << "Moving away or perp." << std::endl;
        bool	hitVert, hitEdge;
        float	vertTime, edgeTime;
        Vec3	vertCollPt, edgeCollPt;

        hitVert = Check3VertHits(tri, posi, velo, vertTime, vertCollPt);
        hitEdge = Check3EdgeHits(tri, posi, velo, edgeTime, edgeCollPt);

        return ConsolidateHits(hitVert, hitEdge, vertTime, edgeTime, vertCollPt, edgeCollPt, minTime, collPt);
    }
    else if (planeIntersectStat == 2)
  {
        //std::cout << "Intersect." << std::endl;
    bool	hitVert, hitEdge;
    float	vertTime, edgeTime;
    Vec3	vertCollPt, edgeCollPt;

    hitVert = Check3VertHits(tri, posi, velo, vertTime, vertCollPt);
        hitEdge = Check3EdgeHits(tri, posi, velo, edgeTime, edgeCollPt);

    return ConsolidateHits(hitVert, hitEdge, vertTime, edgeTime, vertCollPt, edgeCollPt, minTime, collPt);
  }
  else // Moving towards and not intersecting, so could hit a face, edge, or vert.
  {	// Check everything.
        //std::cout << "Clean hit possible." << std::endl;
        //!Check time range here too, to avoid a heap of checks.
    /*if (t0 >= timeDelta)
    {
    return false;
    }
    */
        //std::cout << "Plane hit" << std::endl;
    // The plane is definitely going to get hit. But plane-hit does not mean tri-hit,
    // so we don't know the minimum time yet even though we have t0.
    // Is this first collision point that's on the plane at t0, also inside our tri?

    // cout << "collPt: " << collPt.x << ", " << collPt.y << ", " << collPt.z << endl;

    if (CheckTriHit(tri, collPt))
    {
      //std::cout << "TriHit was true" << std::endl;
      // If yes, then we're already done. We don't have to check edges or verts.
      // We keep our collision point and save our minimum time.
      minTime = t0 < t1 ? t0 : t1;
      hitDetected = true;
      // cout << minTime << " TO TRI." << endl;
      //std::cout << "Hit TRI" << std::endl;
      return true;
    }
    else
    {
      // At this point, we know there wasn't a clean inner-tri hit
            //std::cout << "TriHit was false" << std::endl;
      bool	hitVert, hitEdge;
      float	vertTime, edgeTime;
      Vec3	vertCollPt, edgeCollPt;

      hitVert = Check3VertHits(tri, posi, velo, vertTime, vertCollPt);
      hitEdge = Check3EdgeHits(tri, posi, velo, edgeTime, edgeCollPt);

      return ConsolidateHits(hitVert, hitEdge, vertTime, edgeTime, vertCollPt, edgeCollPt, minTime, collPt);
    }
  }
}


// Only 2 things need to be known about any collision: the time it took to get there, and
// precisely where the point of collision was. From position and velocity, everything else
// can be determined.

// We have to check for a collision against the plane, then the vertices, then the edges in order to ensure
// we find the lowest value for time. This works by checking every triangle, brute force unless something
// faster is needed. Once all tris are checked, we'll have the time and location of the soonest collision of
// all tris. Once we have that, we can then enter the phase of motion handling (stopping and sliding).

// If there was a collision, it is likely to have happened before the end of this frame's window of
// time, meaning we have some time left to deal with still. So, the time remaining is updated, and the
// velocity and position are updated, and with those 3 values this function is recursively called again.
// This continues until either all the time is used up, or until velocity becomes small enough to end the recursion.

// Main collision handling recursive function. COLLIDE_PADDING is used for the collision skin thickness.
// Outputs a new position, velocity, and last normal hit.
int MoveWithCollisions(const std::vector<const Triangle> &tris, float timeRem, sea_math::Vec3 &posi,
                       sea_math::Vec3 &velo, sea_math::Vec3 &lastNormal, bool &grounded)
{
    std::setprecision(5);
    //std::cout << "--- ENTER ---" << std::endl;
    //std::cout << "ENTER --- MWC, V: (" << velo.x << "," << velo.y << "," << velo.z << ")" << std::endl;
    //    std::cout << ")  P: (" << posi.x << "," << posi.y << "," << posi.z << ")" << std::endl;

    int count = 0;
  g_testRecurs++;

  if (g_testRecurs > 300)
  {
      //std::cout << "Deep recursion: " << g_testRecurs << std::endl;
  }

    /*
    BEFORE doing anything with this velocity, disable any and every component of it that is pointing towards a
    face, edge, or vertex that's too close.
    */




  bool	hitDetected = false;
    float	pureCollTime = 90;// timeRem;	//	999	// How much time until a pure collision, ie, a collision with zero padding.
    Vec3	pureCollPt;						// Point in world space of the pure collision.

    int trisGettingHit = 0;

  // BEGIN TRI LOOP
  for (unsigned int i = 0; i < tris.size(); i++)
  {
    float	triCollTime = 999.0f;
    Vec3	triCollPt;

        bool triWillEverGetHit = GetCollisionData(tris[i], posi, velo, triCollTime, triCollPt);

        if (triWillEverGetHit) { trisGettingHit++; }

        if (triWillEverGetHit && (triCollTime < pureCollTime))
    {
      pureCollTime = triCollTime;
      pureCollPt = triCollPt;
    }

        hitDetected = hitDetected || triWillEverGetHit;

    // So now we have whether a hit happened, when it happens first, and where, for this triangle.
    // Keep hitDetected's state, and keep repeating until we've checked every triangle.
  }	// END TRI LOOP.

  // Now we have the best time and place from all the tris, so we can move the character.
  // However, move only as far as COLLIDE_PADDING distance from the triangle we're hitting. <-- Different to moving COLLIDE_PADDING distance back in the direction of motion!

  if (hitDetected)
  {
        //std::cout << "Got a hit. X collision pt: = " << pureCollPt.x;
        //std::cout << " But time and velocity says: " << (posi + (pureCollTime * velo)).x << std::endl;

    // If we detected a possible hit, factor in the required collision padding then check whether we will hit that
    // padding within the time that is remaining.
    // Variables of time, position, and collPts prepended with "pure" are accounting for zero padding.

        float realCollTime = pureCollTime - COLLIDE_PADDING / Magnitude(velo);  // time (to travel across padding) = dist/speed

        //std::cout << "MWC HITPOINT ---->  X: " << pureCollPt.x << ", Y: " << pureCollPt.y << ", Z: " << pureCollPt.z << std::endl;
        //std::cout << "pureCollTime: " << pureCollTime;
        //std::cout << " realCollTime: " << realCollTime;

        if (realCollTime <= timeRem)
        {
            count++;
            //std::cout << "  Found a hit this frame. ";
            //std::cout << "Found hit. \nCollPt was (" << pureCollPt.x << "," << pureCollPt.y << "," << pureCollPt.z << ")" << std::endl;
            Vec3 pos2 = posi + (pureCollTime *velo);
            //std::cout << "Pos2Pt was (" << pos2.x << "," << pos2.y << "," << pos2.z << ")" << std::endl;
            Vec3 thing = Normalize(pos2 - pureCollPt);
            //std::cout << "Normal was (" << thing.x << "," << thing.y << "," << thing.z << ")" << std::endl;
            if (realCollTime < 0.0f)
            {
                //Vec3 thing = Normalize(posi - pureCollPt);

                //std::cout << "Found hit inside padding. Normal was (" << thing.x << "," << thing.y << "," << thing.z << ")" << std::endl;
                realCollTime = 0.0f;
            }
            //std::cout << std::endl;

            Vec3 realPosi = posi + velo * realCollTime;
            Vec3 purePosi = posi + velo * pureCollTime;  // but it's possible that realCollTime < timeRem < pureCollTime
            float pureTimeSurplus = timeRem - pureCollTime;  // that doesn't matter though. pureCollTime simply exists, whether it's within timeRem or not.
        //std::cout << "Z VELO BEFORE DEFLECT: [" << velo.z;
        //std::cout << "Velo was (" << velo.x << "," << velo.y << "," << velo.z << ")" << std::endl;

        velo = DeflectVelocity(purePosi, velo, pureTimeSurplus, pureCollPt);
        //std::cout << "] AFTER DEFLECT: [" << velo.z << "]" << std::endl;
        //std::cout << "Velo became (" << velo.x << "," << velo.y << "," << velo.z << ")" << std::endl;

        //lastGroundedNormal = lastNormal;
           lastNormal = Normalize(purePosi - pureCollPt);

           Vec3 up = Vec3(0.0f, 1.0f, 0.0f);
           float angle = acos(Dot(lastNormal, up) / (Magnitude(lastNormal) * Magnitude(up)));

           if (angle <= SEA_PI / 10.0f)
            {
                   grounded = grounded || true;
              }

 //           std::cout << "In MWC LCN: " << lastNormal.x << "," << lastNormal.y << "," << lastNormal.z << std::endl;
   //         std::cout << "In MWC, centre posi: " << realPosi.x << "," << realPosi.y << "," << realPosi.z << std::endl;

            timeRem -= realCollTime;
            posi = realPosi;
            //posi = realPosi + Normalize(lastNormal) * 0.01f;

            if ((Magnitude(velo) > CUTOFF_SPEED) && (g_testRecurs < MAX_RECURSIONS))
            {   // Check to see if new velocity too low to bother with, or if we've already recursed enough.
                count += MoveWithCollisions(tris, timeRem, posi, velo, lastNormal, grounded);
            }
            else
            {
                //velo = Vec3();
                //posi = realPosi;
                //posi = realPosi + Normalize(lastNormal) * 0.01f;
                timeRem = 0.0f;
                // Unwind the recursion. If velocity magnitude was too small, retain its value but don't check
                // for collisions, and thus, don't move any further.
                // Update last collision normal here since this was the last collision in the recursion.

            }
        }
        else
    {
      posi += velo * timeRem;
      timeRem = 0.0f;
      }
  }
  else	// No hit detected so move as usual.
  {
        //std::cout << "\tNO HIT!" << std::endl;
    posi += velo * timeRem;
    timeRem = 0.0f;
  }

    //std::cout << "EXIT --- MWC" << std::endl;
    //std::cout << "EXIT (last hit occurred) --- MWC, V: (" << velo.x << "," << velo.y << "," << velo.z << std::endl;

  g_testRecurs--;
  return count;
}
