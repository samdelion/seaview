#ifndef SEA_TEST_APPLICATION_H
#define SEA_TEST_APPLICATION_H

#include "engine/interfaces/IApplication.h"
#include "engine/EntityFactory.h"
#include "engine/render/Skybox.h"

#include "math/Vec3.h"
#include "engine/util/ShaderManager.h"
#include "game/test/PlayerWorldCollision.h"
#include "game/test/Universe.h"
#include "game/test/Character.h"
#include "engine/sound/JSound.h"
#include "engine/render/UIDrawer.h"
#include "engine/systems/RenderingSystem.h"
#include "engine/systems/PowerupSystem.h"
#include "engine/core/EntityManager.h"
#include "engine/physics/Octree.h"

#define INIT_MOUSE_SENSITIVITY 0.25f  // 1.0f = 100%.

/**
 * Game application class.
 */
class TestApplication : public sea::IApplication
{
public:
    /**
     * Default constructor.
     */
    TestApplication();
    /**
     * Free all memory associated with application.
     */
    ~TestApplication();
    /**
     * Callback function called by engine when application is switched to.
     * This includes the first time the engine is started up (called before
     * VInit).
     *
     * Used to blit load screen before loading any assets.
     */
    virtual void VOnSwitchTo();
    /**
     *  Called by the engine after the window is created.
     *
     *  Initialize application-specific data.
     *
     *  @return     bool, true if initialization was successful, false otherwise.
     */
    virtual bool VInit();
    /**
     *  Return the name of the application.
     *
     *  Will be displayed in the title bar of the window.
     *
     *  @return     const char *, application title string.
     */
    virtual const char *VGetApplicationTitle();
    /**
     *  Return the path to the application options file
     *  which will be used to initialize application/engine settings.
     *
     *  Not currently used.
     *
     *  @return     const char *, path to application options file string.
     */
    virtual const char *VGetApplicationOptionsPath();
    /**
     *  Update method called each frame.
     *
     *  The update method is passed the time the last frame took to render.
     *
     *  @param  deltaTime   float, time last frame took to render.
     */
    virtual void VUpdate(float deltaTime);
    /**
     *  Render method called each frame.
     *
     *  Render method is called after the update method.
     */
    virtual void VRender();
    /**
     *  Method called whenever the window is resized.
     *
     *  Method is passed the new window width and height.
     *
     *  @param  width   unsigned int, new window width.
     *  @param  height  unsigned int, new window height.
     */
    virtual void VResize(unsigned int width, unsigned int height);
    /**
     *  Return the version of OpenGL to use (major).
     *
     *  @return     unsigned int, major version of OpenGL to use.
     */
    virtual unsigned int VGetOpenGLMajorVersion();
    /**
     *  Return the version of OpenGL to use (minor).
     *
     *  @return     unsigned int, minor version of OpenGL to use.
     */
    virtual unsigned int VGetOpenGLMinorVersion();
    /**
     *  Callback function called by engine when key down
     *  event occurs.
     *
     *  @param  key     SDL_Keycode, enum representing key pressed.
     */
    virtual void VKeyDown(SDL_Keycode key);
    /**
     *  Callback function called by engine when key up
     *  event occurs.
     *
     *  @param  key     SDL_Keycode, enum representing key pressed.
     */
    virtual void VKeyUp(SDL_Keycode key) {}
    /**
     *  Callback function called by engine when mouse button down
     *  event occurs.
     *
     *  @param  button  Uint8, enum representing which mouse button
     *                  was pressed.
     */
    virtual void VMouseButtonDown(Uint8 button) {}

private:
    sea::EntityFactory m_entityFactory;
    sea::Skybox m_skybox;

    JSound m_js;
    sea::RenderingSystem     *m_pRenderingSystem;
    sea::PowerupSystem *m_pPowerupSystem;
    sea::EntityManager   m_entityManager;

    std::vector<const Triangle> m_triangles;
    sea::Character m_bruce = sea::Character();
    sea_math::Vec3 m_velState;                       // Store velocity when paused

    sea::UIDrawer m_uiDrawer;
    sea::Texture m_uiTexture;

    bool m_isInExitState;
    float m_airDrawTop;
    float m_airDisplayTop;
    float m_scoreDrawTop;
    float m_scoreDisplayTop;
    float m_mouseSensitivity = INIT_MOUSE_SENSITIVITY;
    bool m_initialTimerToggle;
    sea::Octree<const Triangle *> *m_pLevelGeometryOctree;
};

#endif  // SEA_TEST_APPLICATION_H
