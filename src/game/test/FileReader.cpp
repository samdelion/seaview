#include "FileReader.h"
#include <cstring>
#include <sstream>


FileReader::FileReader(std::string filePath)
{
    setNewFile(filePath);

}

void FileReader::setNewFile(std::string filePath)
{

    std::ifstream theFile;
    theFile.open(filePath.c_str());
    std::string tempString = "";

    if(theFile.is_open())
    {
        while (getline(theFile, tempString))
        {
            m_allLines.push_back(tempString);
        }

        theFile.close();
    }
    else
    {
        std::cout << "Error: Opening " << filePath << std::endl;
        std::cout << strerror(errno) << std::endl;
    }
}

void FileReader::getTokenContents(std::vector<std::string> & tokenizedValues, int & linecount, char delim)
{

    linecount = m_allLines.size();
    tokenizedValues = tokenize(delim);
}

void FileReader::getUntokenContents(std::vector<std::string> & buff)
{
    buff = m_allLines;
}


std::vector<std::string> FileReader::tokenize(char delim)
{
    std::vector<std::string> allValues;
    std::vector<std::string> tempVec;
    std::vector<std::string> sectempVec;

    for(std::string s: m_allLines)
    {

        sectempVec = allValues;
        allValues.clear();
        tempVec = tokenizeLine(s, delim);
        joinVectors(sectempVec, tempVec, allValues);
        sectempVec.clear();
        tempVec.clear();
    }

    return allValues;
}

void FileReader::joinVectors(std::vector<std::string> & vecA, std::vector<std::string> & vecB,
                             std::vector<std::string> & outputVec)

{
    outputVec.reserve(vecA.size() + vecB.size());
    outputVec.insert(outputVec.end(), vecA.begin(), vecA.end());
    outputVec.insert(outputVec.end(), vecB.begin(), vecB.end());
}



std::vector<std::string> FileReader::tokenizeLine(std::string &s, char delim)
{
    std::vector<std::string> elements;
    split(s, delim, elements);
    return elements;
}


void FileReader::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }

}


std::vector<std::string> FileReader::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
