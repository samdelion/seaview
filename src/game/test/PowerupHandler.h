#include <vector>
#include <string>
#include <map>

#include "engine/EntityFactory.h"
#include "engine/core/EntityManager.h"
#include "FileReader.h"

class PowerupHandler
{
public:
    void CreatePowerups(std::string & filePath, sea::EntityFactory & eFact);
private:
    void CreatePowerupTransform(const std::vector<std::string> &pos,
                                const std::vector<std::string> &scale,
                                sea::EntityFactory & eFact,
                                sea::Entity & puEntity);
    void CreatePowerupRender(const std::string &mesh,
                             const std::string &texture,
                             const std::string &shader,
                             sea::EntityFactory &eFact,
                             sea::Entity &puEntity);
    void CreatePowerupPC(const std::string &radiusX,
                         const std::string &radiusY,
                         const std::string &radiusZ,
                         const std::string &type,
                         sea::EntityFactory &eFact,
                         sea::Entity & puEntity);
};
