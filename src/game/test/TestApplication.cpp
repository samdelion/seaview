#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cmath>

#include <GL/glew.h>

#include "math/Globals.h"
#include "math/Mat4.h"
#include "math/Quat.h"

#include "engine/Defines.h"
#include "engine/util/ObjFile.h"
#include "engine/util/BMPFile.h"
#include "engine/render/Mesh.h"
#include "engine/render/Texture.h"
#include "engine/render/RenderInstance.h"
#include "engine/Engine.h"
#include "game/test/TestApplication.h"
#include "game/shay/ShayApplication.h"
#include "engine/components/MeshComponentManager.h"
#include "engine/components/RenderComponentManager.h"
#include "engine/components/TransformComponentManager.h"
#include "engine/core/EntityManager.h"
#include "engine/systems/RenderingSystem.h"
#include "engine/render/RenderInstanceNew.h"
#include "engine/util/ShaderManager.h"

#include "game/test/PlayerWorldCollision.h"
#include "game/test/Universe.h"
#include "game/test/Character.h"
#include "game/test/PowerupHandler.h"

#include "engine/render/Camera.h"

#include "engine/render/UIDrawer.h"

#define DEV_KEYS    true

static sea::Texture crosshair;
static sea::Texture texture1, pauseTexture, startTexture, splashTexture, blackBar, airUIback, airUIbar, scoreUIback, scoreUIbar, gameoverUI, scoreA, scoreB, scoreC, scoreD, scoreE, scoreF;

//Game Level Entity
sea::Entity gameLevel01;

float quitTime = 0.0f;

bool dead = false;				// Variable used to ensure that death sound only plays once.
bool startScreen = true;		// Starts true. First press of esc, sets to false, then false.
bool tutorial = false;
bool quitting = false;			// Game status, true when plays has clicked exit.
int bye = 0;					// Used to ensure that goodbye sound only plays once.

sea::Texture currentTutorialTexture = pauseTexture;
std::vector<sea::Texture *> tutorialTextures;
int tutorialTextureIndex = 0;

TestApplication::TestApplication()
{
    m_pLevelGeometryOctree = nullptr;
}

TestApplication::~TestApplication()
{
    if (m_pLevelGeometryOctree != nullptr)
    {
        delete m_pLevelGeometryOctree;
    }
}

void TestApplication::VOnSwitchTo()
{
    // Initialize UI loader
    m_uiDrawer.Init();
    unsigned int imgWidth, imgHeight;
    GLuint textureObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/loadingFinal.bmp",
        imgWidth, imgHeight);
    sea::Texture texture = sea::Texture(textureObject, imgWidth, imgHeight);

    // Draw loading screen
    m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, 1.0f, -1.0f, texture, 1.0f);

    m_initialTimerToggle = false;
}

bool TestApplication::VInit()
{
    sea::ShaderManager shaderManager;
    // Load shaders
    shaderManager.CreateProgram("TexturedFlat",
                                PATH_TO_ASSETS "shaders/anton.vert",
                                PATH_TO_ASSETS "shaders/anton.frag");
    shaderManager.CreateProgram("TexturedLightmap",
                                PATH_TO_ASSETS "shaders/lightmap.vert",
                                PATH_TO_ASSETS "shaders/lightmap.frag");

    // Initialize UI loader
    unsigned int imgWidth, imgHeight;
    GLuint textureObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/pillars1tex.bmp",
        imgWidth, imgHeight);
    texture1 = sea::Texture(textureObject, imgWidth, imgHeight);

    //air ui
    GLuint airUIbackObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/airUIback.bmp",
        imgWidth, imgHeight);
    airUIback = sea::Texture(airUIbackObject, imgWidth, imgHeight);

    GLuint airUIbarObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/airUIbar.bmp",
        imgWidth, imgHeight);
    airUIbar = sea::Texture(airUIbarObject, imgWidth, imgHeight);

    GLuint blackBarObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/blackbar.bmp",
        imgWidth, imgHeight);
    blackBar = sea::Texture(blackBarObject, imgWidth, imgHeight);

    //score ui
    GLuint scoreUIbackObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreUIback.bmp",
        imgWidth, imgHeight);
    scoreUIback = sea::Texture(scoreUIbackObject, imgWidth, imgHeight);

    GLuint scoreUIbarObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreUIbar.bmp",
        imgWidth, imgHeight);
    scoreUIbar = sea::Texture(scoreUIbarObject, imgWidth, imgHeight);

    //game over ui
    GLuint gameoverUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/gameoverUI.bmp",
        imgWidth, imgHeight);
    gameoverUI = sea::Texture(gameoverUIObject, imgWidth, imgHeight);

    textureObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/crosshair.bmp",
        imgWidth, imgHeight);
    crosshair = sea::Texture(textureObject, imgWidth, imgHeight);

    //score at game over A B C D E or F
    GLuint scoreAUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreA.bmp",
        imgWidth, imgHeight);
    scoreA = sea::Texture(scoreAUIObject, imgWidth, imgHeight);

    GLuint scoreBUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreB.bmp",
        imgWidth, imgHeight);
    scoreB = sea::Texture(scoreBUIObject, imgWidth, imgHeight);

    GLuint scoreCUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreC.bmp",
        imgWidth, imgHeight);
    scoreC = sea::Texture(scoreCUIObject, imgWidth, imgHeight);

    GLuint scoreDUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreD.bmp",
        imgWidth, imgHeight);
    scoreD = sea::Texture(scoreDUIObject, imgWidth, imgHeight);

    GLuint scoreEUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreE.bmp",
        imgWidth, imgHeight);
    scoreE = sea::Texture(scoreEUIObject, imgWidth, imgHeight);

    GLuint scoreFUIObject = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/scoreF.bmp",
        imgWidth, imgHeight);
    scoreF = sea::Texture(scoreFUIObject, imgWidth, imgHeight);

    //others
    GLuint pauseTextObj = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/pauseFinal.bmp",
        imgWidth, imgHeight);
    pauseTexture = sea::Texture(pauseTextObj, imgWidth, imgHeight);

    GLuint startScreenObj = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/loadedFinal.bmp",
        imgWidth, imgHeight);
    startTexture = sea::Texture(startScreenObj, imgWidth, imgHeight);

    GLuint endScreenObj = sea::Texture::CreateTextureObject(
        PATH_TO_ASSETS "textures/splashScreenFinal.bmp",
        imgWidth, imgHeight);
    splashTexture = sea::Texture(endScreenObj, imgWidth, imgHeight);

    // Start playing music and load SFX
    m_js.ShuffleMusic();
    m_js.LoadSfx();
    m_js.InitSpeedMode(true); // toggle speed mode on at the start - change false to toggle off at start

    // End sound test
    m_pRenderingSystem = new sea::RenderingSystem();
    m_pRenderingSystem->Init(
        m_entityFactory.GetTransformComponentManager(),
        m_entityFactory.GetRenderComponentManager()
        );


    sea::PowerupSystem::TypeCallbackMap callbackMap;
    std::string scoreType = "score";
    auto scoreFunc = [&] ()
        {
            m_bruce.AdjustScore(2);
            m_js.PlaySfxP(2);
        };
    std::string airType = "air";
    auto airFunc = [&] ()
        {
            m_bruce.AdjustTimeToLive(10.0f);
            m_js.PlaySfxP(3);
        };
    std::string gameOverType = "gameOver";
    auto gameOverFunc = [&] ()
        {
            //end goal bonus
            m_bruce.SetScore(m_bruce.GetScore() + ((int)(m_bruce.GetTimeToLive() / 2.0f) + 5));
            m_bruce.SetTimeToLive(0.0f);
            m_js.PlaySfxP(4);
        };
    std::string tutorialType = "tutorial";
    tutorialTextures.push_back(&splashTexture);
    tutorialTextures.push_back(&pauseTexture);
    auto tutorialFunc = [&] ()
        {
            TestApplication::SetPaused(true);
            tutorial = true;
            currentTutorialTexture = *tutorialTextures[tutorialTextureIndex];
            ++tutorialTextureIndex;
        };

    callbackMap.insert(std::pair<std::string, std::function<void (void)>>(scoreType, scoreFunc));
    callbackMap.insert(std::pair<std::string, std::function<void (void)>>(airType, airFunc));
    callbackMap.insert(std::pair<std::string, std::function<void (void)>>(gameOverType, gameOverFunc));
    callbackMap.insert(std::pair<std::string, std::function<void (void)>>(tutorialType, tutorialFunc));
    m_pPowerupSystem = new sea::PowerupSystem();
    m_pPowerupSystem->Init(
        m_entityFactory.GetTransformComponentManager(),
        m_entityFactory.GetRenderComponentManager(),
        m_entityFactory.GetPowerupComponentManager(),
        &m_bruce,
        callbackMap
        );

    sea::g_engine->AddSystem(m_pRenderingSystem);

    // Create gameLevel01 Entity
    //The below should perhaps be a Map<std::string, Entity> gameLevel - Scalabliity increase.
    sea::g_engine->AddSystem(m_pPowerupSystem);

    gameLevel01 = m_entityFactory.CreateEntity();

    // Build Render Component to add to gameLevel01 entity
    std::map<std::string, std::vector<std::string>> renderComponentInfo;

    renderComponentInfo["mesh"].push_back("meshes/test2.obj");

    //Loading Textures
    renderComponentInfo["texture"].push_back("textures/slide1.bmp");
    renderComponentInfo["texture"].push_back("textures/uturnslide1.bmp");
    renderComponentInfo["texture"].push_back("textures/circleslide1.bmp");
    renderComponentInfo["texture"].push_back("textures/wallTEXTURE2.bmp");
    renderComponentInfo["texture"].push_back("textures/startingplatformTEXTURE.bmp");
    renderComponentInfo["texture"].push_back("textures/pillarTEXTURE.bmp");
    renderComponentInfo["texture"].push_back("textures/finishingplatformTEXTURE.bmp");
    renderComponentInfo["texture"].push_back("textures/airTEXTURE.bmp");
    renderComponentInfo["texture"].push_back("textures/starTEXTURE.bmp");


    // componentInfo["shader"].push_back("TexturedFlat");
    renderComponentInfo["texture"].push_back("textures/lightmapWall.bmp");
    renderComponentInfo["texture"].push_back("textures/lightmapPillar.bmp");
    renderComponentInfo["texture"].push_back("textures/lightmapFloor.bmp");

    // Lightmaps
    renderComponentInfo["lightmapTexture"].push_back("x.midPillars_Plane.001 textures/lightmapPillar.bmp");
    renderComponentInfo["lightmapTexture"].push_back("x.walls_Plane.002 textures/lightmapWall.bmp");
    renderComponentInfo["lightmapTexture"].push_back("x.floor_Plane textures/lightmapFloor.bmp");

    //Texturing slide1
    renderComponentInfo["textureMap"].push_back("slide1.005_Plane.001 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.003_Plane.001 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.003_Plane.000 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.005_Plane.000 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.001_Plane.005 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.004_Plane.008 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.002_Plane.006 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.005_Plane.009 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.003_Plane.007 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.002_Plane.000 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.004_Plane.000 textures/slide1.bmp");
    renderComponentInfo["textureMap"].push_back("slide1.first_slide1.005_Plane.002 textures/slide1.bmp");

    //Texturing uturn1
    renderComponentInfo["textureMap"].push_back("uturn1.001_Plane.001 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.002_Plane.000 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.001_Plane.000 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1_Plane.011 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.004_Plane.015 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.005_Plane.016 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.002_Plane.013 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.001_Plane.012 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.003_Plane.014 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.004_Plane.000 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.004_Plane.001 textures/uturnslide1.bmp");
    renderComponentInfo["textureMap"].push_back("uturn1.005_Plane.000 textures/uturnslide1.bmp");

    //Texturing circle1
    renderComponentInfo["textureMap"].push_back("circle1_Plane.000 textures/circleslide1.bmp");
    renderComponentInfo["textureMap"].push_back("circle1_Plane.018 textures/circleslide1.bmp");
    renderComponentInfo["textureMap"].push_back("circle1_Plane.001 textures/circleslide1.bmp");
    renderComponentInfo["textureMap"].push_back("circle1_Plane.002 textures/circleslide1.bmp");
    renderComponentInfo["textureMap"].push_back("circle1_Plane.003 textures/circleslide1.bmp");
    renderComponentInfo["textureMap"].push_back("circle1_Plane.004 textures/circleslide1.bmp");

    //Texturing spiral circle1_Plane.000
    renderComponentInfo["textureMap"].push_back("circle1_Plane.000 textures/circleslide1.bmp");

    //Texturing pillars
    renderComponentInfo["textureMap"].push_back("Cylinder.000 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.001 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.002 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.003 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.004 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.005 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.006_Cylinder.008 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.007_Cylinder.009 textures/pillarTEXTURE.bmp");
    renderComponentInfo["textureMap"].push_back("Cylinder.008_Cylinder.010 textures/pillarTEXTURE.bmp");

    //Texturing stars for score
    //renderComponentInfo["textureMap"].push_back("star_Circle textures/starTEXTURE.bmp");

    //Texturing air tanks
    //renderComponentInfo["textureMap"].push_back("air_Cylinder.006 textures/airTEXTURE.bmp");

    //Texturing starting platform startingplat_Plane
    renderComponentInfo["textureMap"].push_back("startingplat_Plane textures/startingplatformTEXTURE.bmp");

    //Texturing finishing platform and sign
    renderComponentInfo["textureMap"].push_back("finishplatform_Plane.024 textures/finishingplatformTEXTURE.bmp");

    //Texturing walls
    renderComponentInfo["textureMap"].push_back("wallsnew_Plane.003 textures/wallTEXTURE2.bmp");

    // Shaders
    renderComponentInfo["shader"].push_back("TexturedFlat");

    m_entityFactory.AddComponent(gameLevel01, "RenderComponent", renderComponentInfo);

    // Build Transform Component to add to gameLevel01 entity
    std::map<std::string, std::vector<std::string>> transformComponentInfo;
    transformComponentInfo["position"].push_back("0 0 0");
    transformComponentInfo["scale"].push_back("1 1 1");
    m_entityFactory.AddComponent(gameLevel01, "TransformComponent", transformComponentInfo);

    // Build Level Details Component to add to gameLevel01 entity
    std::map<std::string, std::vector<std::string>> levelDetailsComponentInfo;
    levelDetailsComponentInfo["minY"].push_back("-1000");
    levelDetailsComponentInfo["time"].push_back("80");
    m_entityFactory.AddComponent(gameLevel01, "LevelDetailsComponent", levelDetailsComponentInfo);

    // Load powerups
    PowerupHandler pHandler;
    std::string path = PATH_TO_ASSETS "properties/powerupTest.txt";

    pHandler.CreatePowerups(path, m_entityFactory);

    glEnable(GL_DEPTH_TEST); // enable depth-testing
    glDepthFunc(GL_LESS);
    glClearDepth(1.0f);
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);

    // glEnable(GL_CULL_FACE);
    // glCullFace(GL_BACK);
    // glFrontFace(GL_CCW);

    // Generate object to world transform
    sea_math::Mat4 modelMatrix = sea_math::Mat4(1.0f);
    modelMatrix[3].x = 0.0f;
    modelMatrix[3].y = 0.0f;
    modelMatrix[3].z = 0.0f;

    sea::ObjFile objFile;
    std::ifstream objStream(PATH_TO_ASSETS "meshes/test2.obj");

    if (objFile.Init(objStream) == false)
    {
        std::cerr << "Could not load Wavefront OBJ file." << std::endl;
    }

    // Grab 3 indices at a time, corresponding to 3 verts and 1 tri.
    for (int i = 0; i < objFile.GetPackedArrayIndicesContiguous().size(); i += 3)
    {
        Vec4	vert4D;
        Vec3	corner0, corner1, corner2;
        float	x, y, z;
        int		index;

        index = objFile.GetPackedArrayIndicesContiguous()[i + 0];
        x = objFile.GetPackedArray()[index * 8 + 0];
        y = objFile.GetPackedArray()[index * 8 + 1];
        z = objFile.GetPackedArray()[index * 8 + 2];
        vert4D = modelMatrix * Vec4(x, y, z, 1.0f);		// Use a 4D vector to convert each point to world coords.
        corner0 = Vec3(vert4D.x, vert4D.y, vert4D.z);	// Then put the result back into a 3D vector.

        index = objFile.GetPackedArrayIndicesContiguous()[i + 1];
        x = objFile.GetPackedArray()[index * 8 + 0];
        y = objFile.GetPackedArray()[index * 8 + 1];
        z = objFile.GetPackedArray()[index * 8 + 2];
        vert4D = modelMatrix * Vec4(x, y, z, 1.0f);
        corner1 = Vec3(vert4D.x, vert4D.y, vert4D.z);

        index = objFile.GetPackedArrayIndicesContiguous()[i + 2];
        x = objFile.GetPackedArray()[index * 8 + 0];
        y = objFile.GetPackedArray()[index * 8 + 1];
        z = objFile.GetPackedArray()[index * 8 + 2];
        vert4D = modelMatrix * Vec4(x, y, z, 1.0f);
        corner2 = Vec3(vert4D.x, vert4D.y, vert4D.z);

        const Triangle nextTri = { corner0, corner1, corner2 };
        m_triangles.push_back(nextTri);
    }

    m_pLevelGeometryOctree =
        new sea::Octree<const Triangle *>(sea_math::Vec3(0.0f, 0.0f, 0.0f), 10000);

    for (unsigned int i = 0; i < m_triangles.size(); ++i)
    {
        sea::Octree<const Triangle *>::Object object;
        const Triangle *tri = &m_triangles[i];
        sea_math::Vec3 centre =
            (m_triangles[i].cornerA + m_triangles[i].cornerB + m_triangles[i].cornerC) * (1/3.0f);
        float distCornerA = sea_math::Magnitude(m_triangles[i].cornerA - centre);
        float distCornerB = sea_math::Magnitude(m_triangles[i].cornerB - centre);
        float distCornerC = sea_math::Magnitude(m_triangles[i].cornerC - centre);
        float radius = fmax(fmax(distCornerA, distCornerB), distCornerC);

        object = sea::Octree<const Triangle *>::Object(tri, centre, radius);
        m_pLevelGeometryOctree->Insert(object);
    }

    // Load skybox
    shaderManager.CreateProgram("Skybox",
                                PATH_TO_ASSETS "shaders/skybox.vert",
                                PATH_TO_ASSETS "shaders/skybox.frag");
    m_pRenderingSystem->InitSkybox("Skybox",
                                   PATH_TO_ASSETS "textures/spaceSKYBOX.bmp",
                                   PATH_TO_ASSETS "textures/spaceSKYBOX.bmp",
                                   PATH_TO_ASSETS "textures/spaceSKYBOX.bmp",
                                   PATH_TO_ASSETS "textures/spaceSKYBOX.bmp",
                                   PATH_TO_ASSETS "textures/spaceSKYBOX.bmp",
                                   PATH_TO_ASSETS "textures/spaceSKYBOX.bmp"
        );

    m_pRenderingSystem->RegisterShaderForCameraUpdate("TexturedFlat");

    // Make sure shaders get latest version of projection and view matrices
    m_pRenderingSystem->RegisterShaderForCameraUpdate("TexturedFlat");
    m_pRenderingSystem->RegisterShaderForCameraUpdate("TexturedLightmap");
    m_pRenderingSystem->RegisterShaderForCameraUpdate("Skybox");

    // Output test controls
    std::cout << "\n\tCONTROLS\n\n";
    std::cout << "WASD:\tMove\n";
    std::cout << "Mouse:\tLook\n";
    std::cout << "F1-F6:\tJump switches\n";
    std::cout << "F7-F9:\tGod mode switches\n";
    std::cout << "F10-F11:\tScalar inertia switches\n";
    std::cout << "SPACE:\tJump\n";
    std::cout << "F:\tSave respawn point\n";
    std::cout << "R:\tRespawn to saved point\n";
    std::cout << "Q:\tRespawn to origin\n";
    std::cout << "LCTRL:\tFreeze position\n";
    std::cout << "TAB:\tCharacter status readout\n";
    std::cout << "NUMPAD:\tCollision-free displacement\n";
    std::cout << "X:\tQuit\n";
    std::cout << "\nUSEFUL KEYS: [F1]/[J] (cycle single/double/infinite jumps).";
    std::cout << "\nUSEFUL KEYS: [F5](jump from falls). [F6](jump from walls).";
    std::cout << "\nUSEFUL KEYS: [R](respawn). [F](save new spawn point). [Q](respawn to origin).";
    std::cout << "\nUSEFUL KEYS: [F7](gravity). [LCTRL](freeze)." << std::endl;

    shaderManager.CreateProgram("GeometryShader",
                                PATH_TO_ASSETS "shaders/lightmap.vert",
                                PATH_TO_ASSETS "shaders/lightmap.frag");

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    return true;
}

void TestApplication::VRender()
{
    float crosshairXOffset = (64.0f/sea::g_engine->GetWindowWidth()) / 2.0f;
    float crosshairYOffset = (64.0f/sea::g_engine->GetWindowHeight()) / 2.0f;

    if (TestApplication::GetPaused() && startScreen == true)
    {
        m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, 1.0f, -1.0f, startTexture, 1.0f);
    }

    //drawing skybox

    if (TestApplication::GetPaused() && tutorial == false)
    {
        m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, 1.0f, -1.0f, pauseTexture, 1.0f);
    }

    if (quitting)
    {
        m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, 1.0f, -1.0f, splashTexture, 1.0f);
    }

    if (tutorial == true)
    {
        m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, 1.0f, -1.0f, currentTutorialTexture, 1.0f);
    }

    //prevents game over from appearing on first spawn
    if(!m_initialTimerToggle)
    {
        m_bruce.SetTimeToLive(80.0f);
        m_initialTimerToggle = true;
    }

    if(m_bruce.GetTimeToLive() <= 0.0f)
    {
        //determining letter score for game over screen
        switch(m_bruce.GetLetterScore())
        {
        case 'a':
            m_uiDrawer.DrawTexturedQuad(0.20f, 0.25f, 0.60f, -0.25f, scoreA, 1.0f);
            break;
        case 'b':
            m_uiDrawer.DrawTexturedQuad(0.20f, 0.25f, 0.60f, -0.25f, scoreB, 1.0f);
            break;
        case 'c':
            m_uiDrawer.DrawTexturedQuad(0.20f, 0.25f, 0.60f, -0.25f, scoreC, 1.0f);
            break;
        case 'd':
            m_uiDrawer.DrawTexturedQuad(0.20f, 0.25f, 0.60f, -0.25f, scoreD, 1.0f);
            break;
        case 'e':
            m_uiDrawer.DrawTexturedQuad(0.20f, 0.25f, 0.60f, -0.25f, scoreE, 1.0f);
            break;
        case 'f':
            m_uiDrawer.DrawTexturedQuad(0.20f, 0.25f, 0.60f, -0.25f, scoreF, 1.0f);
            break;
        }
        m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, 1.0f, -1.0f, gameoverUI, 1.0f);
    }

    //drawing crosshair
    m_uiDrawer.DrawTexturedQuad(0.0f - crosshairXOffset,
                                0.0f + crosshairYOffset,
                                0.0f + crosshairXOffset,
                                0.0f - crosshairYOffset, crosshair, 1.0f);

    //determining y value of air bar
    m_airDrawTop = (m_bruce.GetTimeToLive() / 50.0f) -1.15f;
    if (m_airDisplayTop != m_airDrawTop)
    {
        m_airDisplayTop += (m_airDrawTop - m_airDisplayTop) * 0.12f;
    }


    m_scoreDrawTop = ((float)m_bruce.GetScore() / 50) - 1.15;
    //determining y value of score bar - score will be max of 100
    if (m_scoreDisplayTop != m_scoreDrawTop)
    {
        m_scoreDisplayTop += (m_scoreDrawTop - m_scoreDisplayTop) * 0.25f;
    }


    //drawing air ui
    m_uiDrawer.DrawTexturedQuad(-1.0f, 0.85f, -0.95f, m_airDisplayTop, blackBar, 1.0f);
    m_uiDrawer.DrawTexturedQuad(-1.0f, 0.85f, -0.95f, -1.0f, airUIbar, 1.0f);
    m_uiDrawer.DrawTexturedQuad(-1.0f, 1.0f, -0.85f, -1.0f, airUIback, 1.0f);

    //drawing score ui
    m_uiDrawer.DrawTexturedQuad(0.95f, 0.85f, 1.0f, m_scoreDisplayTop, blackBar, 1.0f);
    m_uiDrawer.DrawTexturedQuad(0.95f, 0.85f, 1.0f, -1.0f, scoreUIbar, 1.0f);
    m_uiDrawer.DrawTexturedQuad(0.85f, 1.0f, 1.0f, -1.0f, scoreUIback, 1.0f);
}

void TestApplication::VUpdate(float deltaTime)
{
    std::vector<sea::Octree<const Triangle *>::Object> potentialCollisions =
        m_pLevelGeometryOctree->SphereOctreeQuery(m_bruce.GetLocation(),
                                                  m_bruce.GetRadius() * 100.0f);

    //Check if music is playing - play next if not
    if (Mix_PlayingMusic() == 0)
    {
        m_js.NextMusic();
    }

    //intital screen
    if (startScreen)
    {
        TestApplication::SetPaused(true);
    }
    if (quitting)
    {
        //count down to 9, close.
        quitTime += deltaTime;

        if (quitTime > 2.0f)
        {
            m_js.SetVolume(0);
        }
        if (quitTime > 2.5f && bye == 0)
        {
            m_js.PlaySfxP(5);
            bye++;
        }
        if (quitTime > 9.0f)
        {
            exit(0);
        }
    }

    // Mouse input
    // Collect relative state even when paused so it doesn't snap to a new orientation when unpausing.
    int deltaMouseX, deltaMouseY;
    SDL_GetRelativeMouseState(&deltaMouseX, &deltaMouseY);

    if (TestApplication::GetPaused() || quitting)   // If paused, nothing to move.
    {
        return;
    }

    m_bruce.Reorient(m_mouseSensitivity, deltaMouseX, deltaMouseY, deltaTime);


    sea::Camera &cam = m_pRenderingSystem->GetCamera();

    // Keyboard input
    const unsigned char *keys = SDL_GetKeyboardState(NULL);

    if (keys[SDL_SCANCODE_LCTRL])   // If frozen, keep mouselook working, but nothing else to move.
    {
        m_bruce.StopMoving();
        cam.SetOrientation(m_bruce.GetOrientation());
        return;
    }

    // Not paused, and not frozen, so factor in the 3 forces now.
    // Input, gravity, running deceleration.
    // Reset acceleration to start things off.
    m_bruce.StopAccelerating();

    // INPUT FORCE - The only part the player has a say in.
    // Combine several input presses at once, but don't combine their strength.
    Vec3 playerInput = Vec3();
    bool gotInput = false;

    if (keys[SDL_SCANCODE_W])
    {
        playerInput.z += 1.0f;
        gotInput = true;
    }
    if (keys[SDL_SCANCODE_S])
    {
        playerInput.z -= 1.0f;
        gotInput = true;
    }
    if (keys[SDL_SCANCODE_D])
    {
        playerInput.x += 1.0f;
        gotInput = true;
    }
    if (keys[SDL_SCANCODE_A])
    {
        playerInput.x -= 1.0f;
        gotInput = true;
    }
    playerInput = Normalize(playerInput);
    m_bruce.AddInputForce(playerInput);

    // GRAVITY FORCE - With a bit of work this could be any direction.
    if (!m_bruce.GetGodFlight())
    {
        m_bruce.AddExternalForce(Vec3(0.0f, GRAVITY, 0.0f));
    }

    // DECELERATION FORCE - No fancy static/kinetic friction here.
    // Just pretend acceleration to push back the component of acceleration that is along the ground.
    if (!gotInput)
    {
        m_bruce.ApplyRunningDecelForce(deltaTime);
    }

    m_bruce.RefreshVelocity(deltaTime, TERMINAL_VELOCITY, AIR_SPEED_SOFT_MAX);

    std::vector<const Triangle> potentialTriangleCollisions;
    for (unsigned int i = 0; i < potentialCollisions.size(); ++i)
    {
        potentialTriangleCollisions.push_back(*(potentialCollisions[i].contents));
    }

    sea_math::Vec3  loc         = m_bruce.GetLocation(),
        vel         = m_bruce.GetVelocity(),
        lcn         = m_bruce.GetLCN();
    bool            grounded    = false;

    // Finally we have the real velocity to work with. Move, then cleanup.
    // int hitsInsideFrame = MoveWithCollisions(m_triangles, deltaTime, loc, vel, lcn, grounded);
    int hitsInsideFrame = MoveWithCollisions(potentialTriangleCollisions, deltaTime, loc, vel, lcn, grounded);

    m_bruce.SetLocation(loc);
    m_bruce.SetVelocity(vel);
    m_bruce.SetLCN(lcn);
    m_bruce.SetGrounded(grounded);


    if (hitsInsideFrame > 0)
    {
        //std::cout << hitsInsideFrame << " hits inside last frame.--------------------" << std::endl;
    }

    // Changing volume according to speed
    m_js.SpeedMode(sea_math::Magnitude(m_bruce.GetVelocity()));

    // Update time to live.
    m_bruce.AdjustTimeToLive(-deltaTime);
    if (m_bruce.GetTimeToLive() < 0.0f && !dead)
    {
        dead = true;
        m_js.PlaySfxP(6);
    }

    // Check that we're still on the map (and forget that any hits happened if we aren't).
    sea::Instance gl01 = m_entityFactory.GetLevelDetailsComponentManager()->VGetInstanceForEntity(gameLevel01);
    if (m_bruce.GetLocation().y < m_entityFactory.GetLevelDetailsComponentManager()->GetMinY(gl01))
    {
        m_bruce.ResetRespawn();
        m_bruce.Respawn();
        m_js.PlaySfxP(1);
    }
    else if (hitsInsideFrame >= 1)
    {
        //std::cout << "Hits/Recursions: " << hitsInsideFrame << std::endl;
        m_bruce.TouchDown(lcn);
    }

    cam.SetLocation(m_bruce.GetLocation());
    cam.SetOrientation(m_bruce.GetOrientation());

}

void TestApplication::VResize(unsigned int width, unsigned int height)
{
    glViewport(0, 0, width, height);

    m_pRenderingSystem->GetCamera().SetAspectRatio((float)width / (float)height);
}

const char *TestApplication::VGetApplicationTitle()
{
    return "Star Sliders";
}

const char *TestApplication::VGetApplicationOptionsPath()
{
    return "";
}

unsigned int TestApplication::VGetOpenGLMajorVersion()
{
    return 3;
}

unsigned int TestApplication::VGetOpenGLMinorVersion()
{
    return 3;
}

void TestApplication::VKeyDown(SDL_Keycode key)
{
    sea::Camera &cam = m_pRenderingSystem->GetCamera();

    switch (key)
    {
        // [0] to [9] for sound controls
    case SDLK_1:
        m_js.PrintControls();
        break;
    case SDLK_7:
        m_js.ShuffleMusic();
        break;
    case SDLK_9:
        m_js.NextMusic();
        break;
    case SDLK_8:
        m_js.PreviousMusic();
        break;
    case SDLK_0:
        m_js.PauseResumeMusic();
        break;
    case SDLK_MINUS:
        m_js.VolumeDown(4);
        break;
    case SDLK_EQUALS:
        m_js.VolumeUp(4);
        break;
    case SDLK_6:
        m_js.ToggleSpeedMode();
        break;
    case SDLK_5:
        m_js.ToggleSfx();
        break;
        //FOR TESTING SCORE
        /*
          case SDLK_2:
          m_bruce.SetScore(m_bruce.GetScore() - 5);
          break;
          case SDLK_3:
          m_bruce.SetScore(m_bruce.GetScore() + 5);
          break;
        */
        // [F1] cycles through multi-jump modes
    case SDLK_F1:
    case SDLK_j:        //(J for JUMP yo)
        std::cout << "[F1] Multi-jump mode";
        m_bruce.CycleJumpMode();
        break;

        // [F2] to [F6] toggle jump features
    case SDLK_F2:
        m_bruce.ToggleSlopeJump();
        std::cout << "[F2] Slope jumping: " << (m_bruce.GetSlopeJump() ? "ON" : "OFF") << std::endl;
        break;
    case SDLK_F3:
        m_bruce.ToggleInertiaJump();
        std::cout << "[F3] Inertia jumping: " << (m_bruce.GetInertiaJump()? "ON":"OFF") << std::endl;
        break;
    case SDLK_F5:
        m_bruce.ToggleFallJump();
        std::cout << "[F5] Fall jumping: " << (m_bruce.GetFallJump()? "ON" : "OFF") << std::endl;
        break;
    case SDLK_F6:
        m_bruce.ToggleWallJump();
        std::cout << "[F6] Wall jumping: " << (m_bruce.GetWallJump()? "ON" : "OFF") << std::endl;
        break;
        // [F7] to [F9] toggle god modes
    case SDLK_F7:
        m_bruce.ToggleGodFlight();
        std::cout << "[F7] God mode - Fly: " << (m_bruce.GetGodFlight()? "ON" : "OFF") << std::endl;
        break;
        // [F10] & [F11] toggle inertia modes
    case SDLK_F10:
        m_bruce.ToggleScalarAirInertia();
        std::cout << "[F10] Scalar inertia - Air: " << (m_bruce.GetScalarAirInertia()? "ON" : "OFF") << std::endl;
        break;
    case SDLK_F11:
        m_bruce.ToggleScalarLandInertia();
        std::cout << "[F11] Scalar inertia - Land: " << (m_bruce.GetScalarLandInertia()? "ON" : "OFF") << std::endl;
        break;
    case SDLK_SPACE:
        if (m_bruce.Jump(m_triangles))
        {
            m_js.PlaySfxP(0);
        }
        break;

        // [TAB] to display a readout of states, position and direction
    case SDLK_TAB:
        std::cout << std::endl;
        std::cout << "Location: [" << m_bruce.GetLocation().x << "," << m_bruce.GetLocation().y << "," << m_bruce.GetLocation().z << "]" << std::endl;
        std::cout << "Orientation: [" << cam.Forward().x << "," << cam.Forward().y << "," << cam.Forward().z << "]" << std::endl;
        std::cout << std::endl;
        std::cout << "[N]/[M] Mouse sensivity: " << m_mouseSensitivity*100.0f << "%" << std::endl;
        std::cout << std::endl;
        std::cout << "[F1]/[J] Multi-jump mode: " << m_bruce.GetJumpMode() << std::endl;
        std::cout << "[F2] Jumping - Considers surface gradient: " << (m_bruce.GetSlopeJump() ? "ON" : "OFF") << std::endl;
        std::cout << "[F3] Jumping - Preserves vertical inertia: " << (m_bruce.GetInertiaJump() ? "ON" : "OFF") << std::endl;
        std::cout << "[F5] Jumping - From a fall: " << (m_bruce.GetFallJump() ? "ON" : "OFF") << std::endl;
        std::cout << "[F6] Jumping - While wall sliding: " << (m_bruce.GetWallJump() ? "ON" : "OFF") << std::endl;
        std::cout << "[F7] God mode - Fly: " << (m_bruce.GetGodFlight() ? "ON" : "OFF") << std::endl;
        std::cout << "[F10] Scalar inertia - Air: " << (m_bruce.GetScalarAirInertia() ? "ON" : "OFF") << std::endl;
        std::cout << "[F11] Scalar inertia - Land: " << (m_bruce.GetScalarLandInertia() ? "ON" : "OFF") << std::endl;
        std::cout << "Reminder: QRF = respawn controls. T = extend time. 1 = print sound controls." << std::endl;
        break;

        // [NUMPAD] to move with collision-free displacement
        // Based on gravity as it's an easy reflection of the level's scale


    case SDLK_KP_8:
        m_bruce.Displace(0, 0, -GRAVITY);
        break;
    case SDLK_KP_2:
        m_bruce.Displace(0, 0, GRAVITY);
        break;
    case SDLK_KP_6:
        m_bruce.Displace(-GRAVITY, 0, 0);
        break;
    case SDLK_KP_4:
        m_bruce.Displace(GRAVITY, 0, 0);
        break;
    case SDLK_KP_9:
        m_bruce.Displace(0, -GRAVITY, 0);
        break;
    case SDLK_KP_3:
        m_bruce.Displace(0, GRAVITY, 0);
        break;
    // [R][F][Q][T] for respawn controls
    case SDLK_r:
        m_bruce.ResetRespawn();
        m_bruce.Respawn();
        if(m_bruce.GetTimeToLive() <= 0.0f)
        {
            sea::Instance gl01 = m_entityFactory.GetLevelDetailsComponentManager()->VGetInstanceForEntity(gameLevel01);
            m_bruce.SetScore(5);
            m_bruce.SetTimeToLive(m_entityFactory.GetLevelDetailsComponentManager()->GetLevelTime(gl01));

            // Delete all powerups present previously
            sea::PowerupComponentManager *powerupManager =
                m_entityFactory.GetPowerupComponentManager();
            for (unsigned int i = 0; i < powerupManager->VGetNumInstances(); ++i)
            {
                sea::Instance powerUp = sea::Instance::MakeInstance(i);
                sea::Entity e = powerupManager->VGetEntityForInstance(powerUp);

                sea::TransformComponentManager *transformManager =
                    m_entityFactory.GetTransformComponentManager();
                sea::Instance transform = transformManager->VGetInstanceForEntity(e);

                sea::RenderComponentManager *renderManager =
                    m_entityFactory.GetRenderComponentManager();
                sea::Instance render = transformManager->VGetInstanceForEntity(e);

                powerupManager->VRemoveInstance(powerUp);
                transformManager->VRemoveInstance(transform);
                renderManager->VRemoveInstance(render);
            }

            PowerupHandler pHandler;
            std::string path = PATH_TO_ASSETS "properties/powerupTest.txt";
            pHandler.CreatePowerups(path, m_entityFactory);
            tutorialTextureIndex = 0;
        }
        m_js.PlaySfxP(1);
        dead = 0;
        std::cout << "[R] Respawned" << std::endl;
        break;
    case SDLK_f:
        m_bruce.SetRespawnHere();
        std::cout << "[F] Updated respawn values" << std::endl;
        break;
    case SDLK_q:
        m_bruce.RespawnToZero();
        std::cout << "[Q] Respawned to original starting point" << std::endl;
        break;
    case SDLK_t:
        if(m_bruce.GetTimeToLive() >= 0.1f)
        {
            m_bruce.AdjustTimeToLive(10.0f);
            std::cout << "[T] Time extended by 10 seconds" << std::endl;
        }
        else
        {
            std::cout << "[T] Cannot extend time if time is 0" << std::endl;
        }
        break;
    case SDLK_n:
        m_mouseSensitivity -= 0.025f;
        if (m_mouseSensitivity < 0.025f)
        {
            m_mouseSensitivity = 0.025f;
        }
        std::cout << "[N] Mouse sensitivity: " << m_mouseSensitivity * 100 << "%" << std::endl;
        break;
    case SDLK_m:
        m_mouseSensitivity += 0.025f;
        if (m_mouseSensitivity > 1.0f)
        {
            m_mouseSensitivity = 1.0f;   
        }
        std::cout << "[M] Mouse sensitivity: " << m_mouseSensitivity * 100 << "%" << std::endl;
        break;

        // Pause menu
    case SDLK_ESCAPE:
        /* Goal : Application calls renderer to create and translate and spin box that has the menu screen on it
           If pause is already true then resume else pause.
        */
        startScreen = false;
        if (quitting)
        {
            return;
        }
        if (TestApplication::GetPaused())
        {
            //unpause
            //std::cout << "System pause state is :" << TestApplication::GetPaused() << std::endl;
            //wait.. do animation
            //count down sound...
            m_bruce.SetVelocity(m_velState);
            m_velState = Vec3();
            TestApplication::SetPaused(false);
        }
        else
        {
            //pause
            //std::cout << "System pause state is :" << TestApplication::GetPaused() << std::endl;
            TestApplication::SetPaused(true);
            m_velState = m_bruce.GetVelocity();
            m_bruce.StopMoving();
            //animation and wait
        }

        if (tutorial == true)
        {
            tutorial = false;
        }
        break;

        // Quit
    case SDLK_x:
        if(TestApplication::GetPaused())
        {
            //Ensure that if paused, pause screen will be removed, but code in VUpdate will ensure that it stays paused during exit sequence.
            TestApplication::SetPaused(false);
        }
        quitting = true;
        break;
    }
}
