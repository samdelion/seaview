#define TERMINAL_VELOCITY	1000.0f             // Absolute maximum speed ever possible. This will affect your horizontal speed when you start falling very fast.
#define AIR_SPEED_SOFT_MAX  325.0f              // Maximum air speed characters can accelerate to in a direction. Projecting onto this = core of surf mechanic.
                                                // In TF2, this is 10x less than most characters' max running speeds. I gave a slight bump up due to our 60FPS ticks.
#define GRAVITY				-250.0f             // Should make this a vector.
#define BOTTOM_OF_UNIVERSE  "-1000"
