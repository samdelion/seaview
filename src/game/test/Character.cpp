#include "game/test/Character.h"

sea::Character::Character() :   m_score(0),
                                m_jumps(0),
                                m_jumpMode(JUMP_MODE),
                                m_slopeJump(SLOPE_JUMPING),
                                m_inertiaJump(INERTIA_JUMPING),
                              //m_dynamicJump(DYNAMIC_JUMPING),
                                m_fallJump(FALL_JUMPING),
                                m_wallJump(WALL_JUMPING),
                                m_godFlight(GOD_FLIGHT),
                              //m_godGhost(GOD_GHOST),
                              //m_godSpiderman(GOD_SPIDERMAN),
                                m_scalarInertiaAir(SCALAR_INERTIA_AIR),
                                m_scalarInertiaLand(SCALAR_INERTIA_LAND),
                                m_isGrounded(false),
                              //m_surfaceIsForward(SURFACE_IS_FORWARD),
                                m_jumpStrength(JUMP_STRENGTH),
                                m_runAccel(RUN_ACCEL),
                                m_runDecel(RUN_DECEL),
                                m_runSpeedMax(RUN_SPEED_MAX),
                              //m_coastSpeedMax(COAST_SPEED_MAX),
                                m_airAccel(AIR_ACCEL),
                              //m_airDecel(AIR_DECEL),
                                m_steep(STEEPEST_FOOTHOLD),
                                m_radius(RADIUS),
                              //m_height(HEIGHT),
                                m_timeToLive(TIME_TO_LIVE),
                                m_location(SPAWN_POINT),
                                m_velocity(sea_math::Vec3()),
                                m_acceleration(sea_math::Vec3()),
                                m_lcn(sea_math::Vec3(0.0f, 1.0f, 0.0f)),
                              //m_lgcn(sea_math::Vec3(0.0f, 1.0f, 0.0f)),
                                m_orientation(sea_math::Quat(0.0f, -1.0f, 0.0f, 0.0f)),
                                m_respawnPt(RespawnPt()) {}



char sea::Character::GetLetterScore() const
{
    char letterScore;

    if (m_score < 25)
    {
        letterScore = 'f';
    }
    else if (m_score < 40)
    {
        letterScore = 'e';
    }
    else if (m_score < 60)
    {
        letterScore = 'd';
    }
    else if (m_score < 75)
    {
        letterScore = 'c';
    }
    else if (m_score < 90)
    {
        letterScore = 'b';
    }
    else
    {
        letterScore = 'a';
    }

    return letterScore;
}

sea_math::Vec3 sea::Character::Forward() const
{
    sea_math::Vec4 fwd = sea_math::Normalize(sea_math::CastMat4(m_orientation)
                       * sea_math::Vec4(0.0f, 0.0f, -1.0f));

    return (sea_math::Vec3(fwd.x, fwd.y, fwd.z));
}
sea_math::Vec3 sea::Character::Right() const
{
    sea_math::Vec4 right = sea_math::Normalize(sea_math::CastMat4(m_orientation)
                         * sea_math::Vec4(1.0f, 0.0f, 0.0f));

    return (sea_math::Vec3(right.x, right.y, right.z));
}
sea_math::Vec3 sea::Character::Up() const
{
    sea_math::Vec4 up = sea_math::Normalize(sea_math::CastMat4(m_orientation)
                      * sea_math::Vec4(0.0f, 1.0f, 0.0f));

    return (sea_math::Vec3(up.x, up.y, up.z));
}

bool sea::Character::CapSpeed(float speedlim)
{
    if (IsGrounded() && (Magnitude(m_velocity) > m_runSpeedMax) && m_jumps == 0)
    {
        m_velocity = Normalize(m_velocity) * m_runSpeedMax;
    }

    if (Magnitude(m_velocity) > speedlim)
    {
        m_velocity = Normalize(m_velocity) * speedlim;
        return true;
    }
    return false;
}

void sea::Character::Displace(const int x, const int y, const int z)
{
    Vec3 down = TheEnemysGate();        // "The enemy's gate is down." - Bean

    Vec3 fwd = Forward();
    Vec3 fwdVer = ProjectAontoB(Forward(), down);
    Vec3 fwdHoz = fwd - fwdVer;

    Vec3 right = Right();
    Vec3 rightVer = ProjectAontoB(Right(), down);
    Vec3 rightHoz = right - rightVer;

    Vec3 up = -down;

    m_location += x * rightHoz + y * up + z * fwdHoz;
}

void sea::Character::TouchDown(const Vec3 &normal)
{
    m_lcn = Normalize(normal);

    // Only reset jump count if the surface touched wasn't too steep, or wall jumping is on.
    if (!IsLCNTooSteep() || m_wallJump)
    {
        m_jumps = 0;
    }
}

bool sea::Character::IsGrounded() const
{
    return m_isGrounded;
}

bool sea::Character::IsTouchingSurface(const std::vector<const Triangle> &tris) const



// Why check along a line? Why not just check for any triangle within collision range?
// Because there are occasions when the character can travel parallel to a wall within collision range, without
// any collision ever happening. Allowing a jump from such a surface would not appear unusual, since the
// character will still definitely be within COLLISION_PADDING distance from that wall, however the algorithm
// will not have registered any hit, so anything you want to do before jumping, such as particle effects
// to assert that you landed, or recording the landing point, is impossible. It's personal preference, but this
// implementation guarantees that if you are "grounded", it's because you're still in contact with a surface you hit,
// rather than a confusing "in contact" with a surface you might never hit.

{
    // Check along a line rather than check for any triangle within collision range. 
    // This is because there are occasions when the character can travel parallel to a wall within
    // collision range, without having collided.

    for (unsigned int i = 0; i < tris.size(); i++)
    {
        Plane plane = Plane();
        plane.Init(tris[i].cornerA, tris[i].cornerB, tris[i].cornerC);

        float dist = plane.DistToPlaneAlongLine(m_location, -m_lcn);

        if ((dist <= RADIUS + JUMP_PADDING) && (dist > 0.0f))
        {
            // If this triangle's plane was close enough, check the triangle itself.
            Vec3 planePoint = m_location + -Normalize(m_lcn) * dist;
            if (CheckTriHit(tris[i], planePoint))
            {
                return true;
            }
        }
    }

    return false;
}

void sea::Character::AddInputForce(const Vec3 &inputForce)
{
    // To increase acceleration in only in the "horizontal" component of the character's direction,
    // remove the downwards component from the direction's forward and rightward components.

    Vec3 down = TheEnemysGate();        // "The enemy's gate is down." - Bean

    Vec3 fwd = Forward();
    Vec3 fwdVer = ProjectAontoB(Forward(), down);
    Vec3 fwdHoz = fwd - fwdVer;

    Vec3 right = Right();
    Vec3 rightVer = ProjectAontoB(Right(), down);
    Vec3 rightHoz = right - rightVer;

    // Now add the x (right) and z (forward) components of our input vector.
    if (m_isGrounded)
    {
        m_acceleration += (Normalize(rightHoz) * inputForce.x) * m_runAccel;
        m_acceleration += (Normalize(fwdHoz) * inputForce.z) * m_runAccel;
    }
    else
    {
        m_acceleration += (Normalize(rightHoz) * inputForce.x) * m_airAccel;
        m_acceleration += (Normalize(fwdHoz) * inputForce.z) * m_airAccel;

    }
}

// Add force that isn't from player input. Ie., input limits aren't applied.
void sea::Character::AddExternalForce(const Vec3 &force)
{
    m_acceleration += force;
}

Vec3 sea::Character::TheEnemysGate() const          // "The enemy's gate is down." - Bean
{
    // This was planned to allow gravity in any direction.
    return Vec3(0.0f, -1.0f, 0.0f);
}

bool sea::Character::IsLCNTooSteep() const
{
    Vec3 up = Vec3(0.0f, 1.0f, 0.0f);

    float angle = acos(Dot(m_lcn, up) / (Magnitude(m_lcn) * Magnitude(up)));
    return angle > m_steep;
}

bool sea::Character::Jump(const std::vector<const Triangle> &tris)
{
    Vec3 jumpDirection = Vec3(0.0f, 1.0f, 0.0f);
    bool doJump = false;

    // We can jump if touching a surface and either it isn't too steep, or we have wall jumping.
    // Note, wall jumping + inertia jumping - slope jumping = rapid jumping up cliffs.
    if (IsTouchingSurface(tris) && (!IsLCNTooSteep() || m_wallJump))
    {
        // Since we're jumping off a surface, factor in the slope jumping feature.
        if (m_slopeJump)
        {
            jumpDirection = Normalize(m_lcn);
        }

        doJump = true;

        // Reset the number of jumps as well.
        m_jumps = 1;
    }
    else
    {	// Else, we're airborne. So we can only air jump if we haven't jumped yet (ie., falling)
        // and fall jumping is on,
        // or we've jumped only once before and double jumping is on,
        // or we've jumped at least once before and infinite jumping is on.
        if (	((m_jumps == 0) && m_fallJump)
               ||	((m_jumps == 1) && (m_jumpMode == 2))
               ||	((m_jumps >= 1) && (m_jumpMode == 0)))
         {
             doJump = true;
             m_jumps++; 
         }
    }

    // If we're jumping, now factor in inertia jumping.
    if (doJump)
    {
        if (m_inertiaJump)
        {
            // Retaining inertia is the actually easier than ignoring it.
            m_velocity += m_jumpStrength * jumpDirection;
        }
        else
        {
            // To jump without using inertia in the direction of the jump, remove all velocity in
            // that direction, then add the fixed jump velocity in that direction.
            float projectionSize = Dot(m_velocity, jumpDirection);
            m_velocity -= projectionSize * jumpDirection;
            m_velocity += m_jumpStrength * jumpDirection;
        }
    }

    return doJump;
}

void sea::Character::SetRespawnHere()
{
    m_respawnPt.location = m_location;
    m_respawnPt.velocity = m_velocity;
    m_respawnPt.acceleration = m_acceleration;
    m_respawnPt.lcn = Vec3(0.0f, 1.0f, 0.0f);
    m_respawnPt.orientation = m_orientation;
}

void sea::Character::ResetRespawn()
{
    m_respawnPt = RespawnPt();
    m_respawnPt.orientation = Quat(0.0f, -1.0f, 0.0f, 0.0f);
    m_respawnPt.location = SPAWN_POINT;
}

void sea::Character::Respawn()
{
    m_location = m_respawnPt.location;
    m_velocity = m_respawnPt.velocity;
    m_acceleration = m_respawnPt.acceleration;
    m_lcn = Vec3(0.0f, 1.0f, 0.0f);
    m_orientation = m_respawnPt.orientation;
}

void sea::Character::RespawnToZero()
{
    m_location = SPAWN_POINT;
    m_velocity = sea_math::Vec3();
    m_acceleration = sea_math::Vec3();
    m_lcn = sea_math::Vec3(0.0f, 1.0f, 0.0f);
    m_orientation = Quat();
}

float sea::Character::GetRadius() const
{
    return m_radius;
}

sea_math::Vec3 sea::Character::GetLocation() const
{
    return m_location;
}

unsigned int sea::Character::GetScore() const
{
    return m_score;
}

void sea::Character::SetScore(unsigned int newScore)
{
    if (newScore < 5)
    {
        m_score = 5;
    }
    else if (newScore > 100)
    {
        m_score = 100;
    }
    else
    {
        m_score = newScore;
    }
}

void sea::Character::AdjustScore(unsigned int extraScore)
{
    SetScore(m_score + extraScore);
}

float sea::Character::GetTimeToLive() const
{
    return m_timeToLive;
}

void sea::Character::SetTimeToLive(float newTime)
{
    if (newTime < 0.0f)     
    {
        m_timeToLive = 0.0f;
    }
    else if (newTime > 100.0f)
    {
        m_timeToLive = 100.0f;
    }
    else
    {
        m_timeToLive = newTime;
    }
}

void sea::Character::AdjustTimeToLive(float extraTime)
{    
    SetTimeToLive(m_timeToLive + extraTime);   
}


void sea::Character::SetOrientation(const sea_math::Quat &orientation)
{
    m_orientation = orientation;
}

sea_math::Quat sea::Character::GetOrientation() const
{
    return m_orientation;
}

void sea::Character::StopMoving()
{
    m_velocity = sea_math::Vec3();
}

sea_math::Vec3 sea::Character::GetVelocity() const
{
    return m_velocity;
}

void sea::Character::SetVelocity(const Vec3 &velo)
{
    m_velocity = velo;
}


void sea::Character::StopAccelerating()
{
    m_acceleration = sea_math::Vec3();
}

sea_math::Vec3 sea::Character::GetAccel() const
{
    return m_acceleration;
}

void sea::Character::SetAccel(const Vec3 &accel)
{
    m_acceleration = accel;
}

sea_math::Vec3 sea::Character::GetLCN() const
{
    return m_lcn;
}

void sea::Character::SetLCN(const Vec3& lcn)
{
    m_lcn = sea_math::Normalize(lcn);
}

void sea::Character::Reorient(float sensitivity, int deltaX, int deltaY, float deltaTime)
{
    sea_math::Quat yaw      = sea_math::Quat();
    sea_math::Quat pitch    = sea_math::Quat();
    sea_math::Quat temp     = sea_math::Quat();

    /* The orientation Quaternion is composed of (yaw * pitch), therefore
    * the correct order is yaw * orientation * pitch,
    * ie. yaw * (yaw * pitch) * pitch
    */
    yaw.Offset(sea_math::Vec3(0.0f, 1.0f, 0.0f), sensitivity * -deltaX * deltaTime);
    pitch.Offset(sea_math::Vec3(1.0f, 0.0f, 0.0f), sensitivity * -deltaY * deltaTime);
    temp = sea_math::Normalize(yaw * m_orientation * pitch);

    // Keep mouselook from going past the vertical
    sea_math::Vec4 up4  = sea_math::Normalize(sea_math::CastMat4(temp) 
                        * sea_math::Vec4(0.0f, 1.0f, 0.0f));
    sea_math::Vec3 up3 = Vec3(up4.x, up4.y, up4.z);
    float vertIncidence = AngleBetween(Vec3(0.0f, 1.0f, 0.0f), up3);

    // If the view upwards passes the vertical, remove the pitch offset
    if (vertIncidence >= SEA_PI / 2.0f)
    {
        pitch = sea_math::Quat();
    }

    // Apply the yaw and pitch offsets
    m_orientation = sea_math::Normalize(yaw * m_orientation * pitch);
}

bool sea::Character::ApplyRunningDecelForce(float deltaTime)
{
    if (m_isGrounded)
    {
        Vec3 veloIntoGround = ProjectAontoB(m_velocity, -m_lcn);
        Vec3 veloAlongGround = m_velocity - veloIntoGround;

        Vec3 decelOnYourFeet = -sea_math::Normalize(veloAlongGround) * m_runDecel;

        // We don't want running deceleration to accelerate us backwards. So we just ahead and use
        // deltaTime to check the resulting velocity in the direction along the surface. If indeed
        // slow enough, set that component of the velocity to zero.
        if (sea_math::Magnitude(decelOnYourFeet * deltaTime)>=sea_math::Magnitude(veloAlongGround))
        {
            m_velocity -= veloAlongGround;
        }
        else
        {   // Otherwise, we still want to apply our pseudofriction.
            m_acceleration += decelOnYourFeet;
        }
    }

    // Return true if we did apply the pseudo friction.
    return m_isGrounded;
}

void sea::Character::RefreshVelocity(float deltaTime, float terminalVelo, float airMaxVelo)
{
    // Now that all 3 forces are applied, get our velocity to start working with.
    Vec3    inputVelocity = m_acceleration * deltaTime;

    // Adding velocity, with the base motion similar to Source engine.
    if (m_isGrounded)
    {
        m_velocity += inputVelocity;

        if (m_scalarInertiaLand)
        {
            Vec3    currentVeloVer = sea_math::ProjectAontoB(m_velocity, TheEnemysGate()),
                    currentVeloHoz = m_velocity - currentVeloVer;

            m_velocity  = sea_math::Normalize(sea_math::Vec3(Forward().x, 0.0f, Forward().z))
                        * sea_math::Magnitude(currentVeloHoz);

            m_velocity += currentVeloVer;
        }
    }
    else        // Else, airborne.
    {
        // Imitating Source's air input, air acceleration is very high but ignored after very little speed is reached in its direction.
        // As a result, accelerating backwards is super effective, and accelerating sideways does add speed as long as it continues to
        // be sideways. This is why air strafing works. As you align your camera to match your direction of motion, the A & D keys
        // can continue to provide acceleration sideways, avoiding the speed cap in that direction. This allows you to steer your
        // flight path as you go; just keep the mouse aligned to the direction you're moving. Or press F10. :D

        sea_math::Vec3  inputVeloVer = sea_math::ProjectAontoB(inputVelocity, TheEnemysGate()),
                        inputVeloHoz = inputVelocity - inputVeloVer,
                        currentVeloVer = sea_math::ProjectAontoB(m_velocity, TheEnemysGate()),
                        currentVeloHoz = m_velocity - currentVeloVer;

        sea_math::Vec3 projVelocityOntoInput = sea_math::ProjectAontoB(currentVeloHoz,inputVeloHoz);
        float   magProjVelocity = sea_math::Magnitude(projVelocityOntoInput);

        if (sea_math::Dot(inputVeloHoz, currentVeloHoz) < 0.0f)
        {
            magProjVelocity *= -1.0f;
        }

        // If the component of the character's velocity going in the direction of the new acceleration's velocity is already too high,
        // ignore the acceleration. Otherwise, add the new velocity but only up to that speed limit in its direction.

        if (magProjVelocity + sea_math::Magnitude(inputVeloHoz) < airMaxVelo)    // Enough room to add the whole lot
        {
            m_velocity += inputVeloHoz;
        }
        else if (magProjVelocity < airMaxVelo)                // Not enough room to add the whole lot, so just set it to full.
        {
            m_velocity  = m_velocity - projVelocityOntoInput
                        + sea_math::Normalize(inputVeloHoz) * airMaxVelo;
        }
        else
        {
            // Ignore the acceleration completely, just keep the velocity as is.
        }

        // If scalar inertia is on, we want that velocity going the way we are looking, not the way we are flowing.
        if (m_scalarInertiaAir && !m_isGrounded)
        {
            Vec3    currentVeloVer = sea_math::ProjectAontoB(m_velocity, TheEnemysGate()),
                    currentVeloHoz = m_velocity - currentVeloVer;

            m_velocity  = sea_math::Normalize(sea_math::Vec3(Forward().x, 0.0f, Forward().z))
                        * sea_math::Magnitude(currentVeloHoz);

            m_velocity += currentVeloVer;
        }

        m_velocity += inputVeloVer;
    }

    CapSpeed(terminalVelo);
}

void sea::Character::SetGrounded(bool grounded)
{
    m_isGrounded = grounded;
}

void sea::Character::SetLocation(const sea_math::Vec3 &loca)
{
    m_location = loca;
}

void sea::Character::CycleJumpMode()
{
    m_jumpMode++;
    if (m_jumpMode >= 3)
    {
        m_jumpMode = 0;
    }
}

unsigned int sea::Character::GetJumpMode() const
{
    return m_jumpMode;
};

void sea::Character::ToggleSlopeJump()
{
    m_slopeJump = !m_slopeJump;
}
bool sea::Character::GetSlopeJump() const
{
    return m_slopeJump;
}

void sea::Character::ToggleInertiaJump()
{
    m_inertiaJump = !m_inertiaJump;
};

bool sea::Character::GetInertiaJump() const
{
    return m_inertiaJump;
}

void sea::Character::ToggleFallJump()
{
    m_fallJump = !m_fallJump;
}
bool sea::Character::GetFallJump() const
{
    return m_fallJump;
}

void sea::Character::ToggleWallJump()
{
    m_wallJump = !m_wallJump;
}
bool sea::Character::GetWallJump() const
{
    return m_wallJump;
}

void sea::Character::ToggleGodFlight()
{
    m_godFlight = !m_godFlight;
}
bool sea::Character::GetGodFlight() const
{
    return m_godFlight;
}

void sea::Character::ToggleScalarAirInertia()
{
    m_scalarInertiaAir = !m_scalarInertiaAir;
}
bool sea::Character::GetScalarAirInertia() const
{
    return m_scalarInertiaAir;
}

void sea::Character::ToggleScalarLandInertia()
{
    m_scalarInertiaLand = !m_scalarInertiaLand;
}
bool sea::Character::GetScalarLandInertia() const
{
    return m_scalarInertiaLand;
}