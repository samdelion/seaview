#include "PowerupHandler.h"

void PowerupHandler::CreatePowerups(std::string & filePath, sea::EntityFactory & eFact)
{
    FileReader fileHandle(filePath);
    std::vector<std::string> contents;

    int lineCount = 0;
    fileHandle.getTokenContents(contents, lineCount, ',');

    int item = -1;

    for(int i = 0; i < lineCount; i++)
    {
        std::vector<std::string> pos;
        std::vector<std::string> scale;

        sea::Entity puEntity = eFact.CreateEntity();

        pos.push_back(contents[++item]);
        pos.push_back(contents[++item]);
        pos.push_back(contents[++item]);

        scale.push_back(contents[++item]);
        scale.push_back(contents[++item]);
        scale.push_back(contents[++item]);

        CreatePowerupTransform(pos, scale, eFact, puEntity);
        CreatePowerupRender(contents[item + 1], contents[item + 2], contents[item + 3], eFact, puEntity);
        ++item;
        ++item;
        ++item;
        CreatePowerupPC(contents[item + 1], contents[item + 2], contents[item + 3], contents[item + 4], eFact, puEntity);
        ++item;
        ++item;
        ++item;
        ++item;
    }
}



void PowerupHandler::CreatePowerupTransform(const std::vector<std::string> &pos,
                                            const std::vector<std::string> &scale,
                                            sea::EntityFactory & eFact,
                                            sea::Entity & puEntity)
{
    std::map<std::string, std::vector<std::string>> powerupTransformComponentInfo;

    powerupTransformComponentInfo["position"].push_back(pos[0] + " " + pos[1] + " " + pos[2]);
    powerupTransformComponentInfo["scale"].push_back(scale[0] + " " + scale[1] + " " + scale[2]);

    eFact.AddComponent(puEntity, "TransformComponent", powerupTransformComponentInfo);
}



void PowerupHandler::CreatePowerupRender(const std::string &mesh,
                                         const std::string &texture,
                                         const std::string &shader,
                                         sea::EntityFactory &eFact,
                                         sea::Entity &puEntity)
{
    std::map<std::string, std::vector<std::string>> powerupRenderComponentInfo;

    powerupRenderComponentInfo["mesh"].push_back(mesh);
    powerupRenderComponentInfo["texture"].push_back(texture);
    powerupRenderComponentInfo["shader"].push_back(shader);

    eFact.AddComponent(puEntity, "RenderComponent", powerupRenderComponentInfo);
}



void PowerupHandler::CreatePowerupPC(const std::string &radiusX,
                                     const std::string &radiusY,
                                     const std::string &radiusZ,
                                     const std::string &type,
                                     sea::EntityFactory &eFact,
                                     sea::Entity & puEntity)
{
    std::map<std::string, std::vector<std::string>> powerupPowerupComponentInfo;

    powerupPowerupComponentInfo["radiusX"].push_back(radiusX);
    powerupPowerupComponentInfo["radiusY"].push_back(radiusY);
    powerupPowerupComponentInfo["radiusZ"].push_back(radiusZ);
    powerupPowerupComponentInfo["type"].push_back(type);

    eFact.AddComponent(puEntity, "PowerupComponent", powerupPowerupComponentInfo);
}
