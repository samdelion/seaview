#include <string>
#include <vector>
#include <iostream>
#include <fstream>


class FileReader
{
public:
    FileReader() { };
    FileReader(std::string filePath);
    void setNewFile(std::string path);
    void getTokenContents(std::vector<std::string> & tokenizedValues, int & linecount, char delim);
    void getUntokenContents(std::vector<std::string> & buff);
    std::vector<std::string> tokenize(char delim);



private:
    void joinVectors(std::vector<std::string> & vecA, std::vector<std::string> & vecB,
                     std::vector<std::string> & outputVec);
    std::vector<std::string> tokenizeLine(std::string &s, char delim);
    void split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string> split(const std::string &s, char delim);

    std::vector<std::string> m_allLines;
    std::vector<std::string> m_tokenizedLines;
};
