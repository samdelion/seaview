#include <iostream>

#include <GL/glew.h>
#include <SDL2/SDL.h>

#include "engine/Engine.h"
#include "game/test/TestApplication.h"
#include "game/shay/ShayApplication.h"

sea::Engine *sea::g_engine = new sea::Engine();

#undef main

int main(int argc, char **argv)
{
    sea::g_engine->Init(new ShayApplication());
    sea::g_engine->MainLoop();

    delete sea::g_engine;

    return 0;
}
