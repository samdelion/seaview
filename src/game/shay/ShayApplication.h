#ifndef SEA_SHAY_APPLICATION_H
#define SEA_SHAY_APPLICATION_H

#include <vector>

#include "engine/interfaces/IApplication.h"
#include "engine/render/RenderInstance.h"
#include "engine/render/Mesh.h"
#include "engine/render/Texture.h"

class ShayApplication : public sea::IApplication
{
public:
    /**
     * Callback function called by engine when application is switched to.
     * This includes the first time the engine is started up (called before
     * VInit).
     *
     * Useful for blitting a load screen before loading any assets.
     */
    virtual void VOnSwitchTo() {}
    /**
     *  Called by the engine after the window is created.
     *
     *  Initialize application-specific data.
     *
     *  @return     bool, true if initialization was successful, false otherwise.
     */
    virtual bool VInit();
    /**
     *  Return the name of the application.
     *
     *  Will be displayed in the title bar of the window.
     *
     *  @return     const char *, application title string.
     */
    virtual const char *VGetApplicationTitle();
    /**
     *  Return the path to the application options file
     *  which will be used to initialize application/engine settings.
     *
     *  Not currently used.
     *
     *  @return     const char *, path to application options file string.
     */
    virtual const char *VGetApplicationOptionsPath();
    /**
     *  Update method called each frame.
     *
     *  The update method is passed the time the last frame took to render.
     *
     *  @param  deltaTime   float, time last frame took to render.
     */
    virtual void VUpdate(float deltaTime);
    /**
     *  Render method called each frame.
     *
     *  Render method is called after the update method.
     */
    virtual void VRender();
    /**
     *  Method called whenever the window is resized.
     *
     *  Method is passed the new window width and height.
     *
     *  @param  width   unsigned int, new window width.
     *  @param  height  unsigned int, new window height.
     */
    virtual void VResize(unsigned int width, unsigned int height);
    /**
     *  Return the version of OpenGL to use (major).
     *
     *  @return     unsigned int, major version of OpenGL to use.
     */
    virtual unsigned int VGetOpenGLMajorVersion();
    /**
     *  Return the version of OpenGL to use (minor).
     *
     *  @return     unsigned int, minor version of OpenGL to use.
     */
    virtual unsigned int VGetOpenGLMinorVersion();
    /**
     *  Callback function called by engine when key down
     *  event occurs.
     *
     *  @param  key     SDL_Keycode, enum representing key pressed.
     */
    virtual void VKeyDown(SDL_Keycode key);
    /**
     *  Callback function called by engine when key up
     *  event occurs.
     *
     *  @param  key     SDL_Keycode, enum representing key pressed.
     */
    virtual void VKeyUp(SDL_Keycode key);
    /**
     *  Callback function called by engine when mouse button down
     *  event occurs.
     *
     *  @param  button  Uint8, enum representing which mouse button
     *                  was pressed.
     */
    virtual void VMouseButtonDown(Uint8 button);
private:
    void InitializeExtension();
    void DisplayExtension();
    // initializes setting
    void myinit();

    // display functions
    void Display();
    void reshape(int w, int h);
    void keys(unsigned char key, int x, int y);

    // keyboard and mouse functions
    /*
    Commented out by Rikki Kiely - 3/9/15 - Convserion to WASD
    void movementKeys(int key, int x, int y);
    void releaseKey(int key, int x, int y);
    */
    void releaseKeys(unsigned char key, int x, int y);
    void Mouse(int button, int state, int x, int y);
    void mouseMove(int x, int y);

    // calls display functions below to draw the backdrops
    void DrawBackdrop();
    // functions to display display lists (images) and bind them to a texture
    void DisplayAboveWindowBlock ();
    void DisplayBench ();
    void DisplayBricks ();
    void DisplayChancPosts ();
    void DisplayCylinders ();
    void DisplayDoorPaving ();
    void DisplayDoorPosts ();
    void DisplayEntranceSteps ();
    void DisplayExtras ();
    void DisplayGrass ();
    void DisplayLargerTextures ();
    void DisplayLibraryPosts ();
    void DisplayMainPosts ();
    void DisplayPavement ();
    void DisplayPhysSciPosts ();
    void DisplayPurplePosts ();
    void DisplayRedPosts ();
    void DisplayRoof();
    void DisplayStepBricks ();
    void DisplayLights ();
    void DisplayECL ();

    // calls functions to create display lists (below)
    void CreateTextureList();
    // creates display lists
    void DrawGrass ();
    void DrawChancPosts ();
    void DrawDoorPosts ();
    void DrawPurplePosts ();
    void DrawRedPosts ();
    void DrawMainPosts ();
    void DrawAboveWindowBlock ();
    void DrawDoorPaving ();
    void DrawPhysSciPosts ();
    void DrawLibraryPosts ();
    void DrawBricks ();
    void DrawPavement ();
    void DrawExtras ();
    void DrawRoof();
    void DrawEntranceSteps ();
    void DrawLargerTextures ();
    void DrawLights ();
    void DrawBench ();
    void DrawCylinders ();
    void DrawAngledRoofBeam (int listNo, GLdouble x, GLdouble y, GLdouble z, GLdouble beamSize);
    void DrawAngledRoofBeam2 (int listNo, GLdouble x, GLdouble y, GLdouble z, GLdouble beamSize);
    void DrawStepBricks ();
    void DrawMapExit ();
    void DrawECL ();


    void BindBridgeWall(GLint LR);
    void BindBuildingWall();
    void BindWallPosts(GLint LR);

    void IncrementFrameCount();

    // loads images and creates texture
    void CreateTextures();
    // creates bounding boxes for collsion detection
    void CreateBoundingBoxes();
    // creates different plains
    void CreatePlains();

    // deletes image and clears memory
    void DeleteImageFromMemory(unsigned char* tempImage);

    // Initialize our extension to Shay's world
    void Resize(int width, int height);

    static GLuint CreateTextureObject(const char *filePath, unsigned int &imgWidth, unsigned int &imgHeight);

    std::vector<sea::Mesh>              m_meshes;
    std::vector<sea::Texture>           m_textures;
    std::vector<sea::RenderInstance>    m_renderInstances;
};

#endif  // SEA_SHAY_APPLICATION_H
