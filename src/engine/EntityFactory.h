#ifndef SEA_ENTITY_FACTORY_H
#define SEA_ENTITY_FACTORY_H

#include <vector>
#include <string>
#include <map>

#include "engine/core/EntityManager.h"
#include "engine/components/RenderComponentManager.h"
#include "engine/components/TransformComponentManager.h"
#include "engine/components/LevelDetailsComponentManager.h"
#include "engine/components/PowerupComponentManager.h"

namespace sea
{
    /**
     * Entity factory class, responsible for handling the creation of entities,
     * registering their components and providing methods for accessing entity
     * data.
     */
    class EntityFactory
    {
    public:
        /**
         * Create an entity with no components and
         * return a handle to the entity.
         *
         * @return Entity, handle to entity created.
         */
        Entity CreateEntity();

        /**
         * Add a component to the given entity.
         *
         * Component type is one of:
         *  "TransformComponent"
         *  "RenderComponent"
         *
         * Component info is a map that maps a component property to it's value.
         * Eg. "Position", "37.5 100 0.3"
         *
         * @todo Write a list of valid component types and component properties
         *  and value formats.
         *
         * @param e Entity, handle of entity component should be added to.
         * @param componentType const std::string &, component type to add.
         * @param componentInfo const std::map<std::string, std::vector<std::string>> &,
         *  map of component properties to set.
         */
        bool AddComponent(Entity e,
                          const std::string &componentType,
                          const std::map<std::string, std::vector<std::string>> &componentInfo);

        /**
         * Get a reference to the factory's transform component manager.
         *
         * @return TransformComponentManager * const, factory's transform component manager.
         */
        TransformComponentManager *const GetTransformComponentManager();

        /**
         * Get a reference to the factory's render component manager.
         *
         * @return RenderComponentManager * const, factory's render component manager.
         */
        RenderComponentManager *const GetRenderComponentManager();

        /**
         *
         * Get a reference to the factory's level component manager.
         *
         * @return LevelDetailsComponentManager * const, factory's level component manager.
         */
        LevelDetailsComponentManager *const GetLevelDetailsComponentManager();

        /**
         *
         * Get a reference to the factory's powerup component manager.
         *
         * @return PowerupComponentManager * const, factory's power component manager.
         */
        PowerupComponentManager *const GetPowerupComponentManager();

    private:
        // Entity manager
        EntityManager m_entityManager;

        // Component managers
        TransformComponentManager m_transformComponentManager;
        RenderComponentManager m_renderComponentManager;
        LevelDetailsComponentManager m_levelDetailsComponentManager;
        PowerupComponentManager m_powerupComponentManager;
    };
}

#endif
