#ifndef SEA_SKYBOX_H
#define SEA_SKYBOX_H

#include <string>
#include <vector>

#include <GL/glew.h>

namespace sea
{
    /**
     * Skybox class.
     *
     * Manages rendering of Skybox in the scene.
     */
    class Skybox
    {
    public:
        /**
         * Default constructor for Skybox.
         *
         * Skybox won't draw correctly until Init is called.
         */
        Skybox();

        /**
         * Initialize the skybox with the given shader program and skybox
         * images.
         *
         * @pre shader program provided has already been created using shader
         * manager.
         *
         * @param shaderProgramName const std::string &, name of the shader
         * program to use for the skybox. Must have been created before call.
         * @param front const std::string &, path to front image of skybox.
         * @param back const std::string &, path to back image of skybox.
         * @param left const std::string &, path to left image of skybox.
         * @param right const std::string &, path to right image of skybox.
         * @param top const std::string &, path to top image of skybox.
         * @param bottom const std::string &, path to bottom image of skybox.
         * @return bool, TRUE if initialization was successful, FALSE otherwise.
         */
        bool Init(const std::string &shaderProgramName,
                  const std::string &front, const std::string &back,
                  const std::string &left, const std::string &right,
                  const std::string &top, const std::string &bottom);

        /**
         * Draw the skybox.
         *
         * @pre Init must have already been called successfully.
         */
        void Draw() const;
    private:
        GLuint m_vao;
        GLuint m_texture;
        GLuint m_shader;

        std::vector<float> m_quadVerts;
    };
}

#endif // SEA_SKYBOX_H
