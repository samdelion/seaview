#ifndef SEA_TEXTURE_H
#define SEA_TEXTURE_H

#include <GL/glew.h>

#include "engine/util/BMPFile.h"

namespace sea
{
    /**
     * Texture class.
     *
     * Holds all information pertaining to an OpenGL texture.
     */
    class Texture
    {
    public:
        /**
         * Default constructor, creates empty texture object.
         */
        Texture()
        {
            m_textureID = 0;
            m_width = 0;
            m_height = 0;
        }

        /**
         * Creates a texture object with the given textureID, width and height.
         *
         * @param textureID unsigned int, OpenGL texture id.
         * @param width unsigned int, width of the texture.
         * @param height unsigned int, height of the texture.
         */
        Texture(unsigned int textureID, unsigned int width, unsigned int height)
        {
            m_textureID = textureID;
            m_width = width;
            m_height = height;
        }

        /**
         * Frees all memory associated with the texture object.
         * Does not free OpenGL texture object.
         */
        ~Texture()
        {
        }

        /**
         * Get the texture ID of the texture object.
         *
         * @return unsigned int, OpenGl texture object id.
         */
        unsigned int GetTextureID() const
        {
            return m_textureID;
        }

        /**
         * Get the width of the texture in pixels.
         *
         * @return unsigned int, width of the texture object in pixels.
         */
        unsigned int GetWidth() const
        {
            return m_width;
        }

        /**
         * Get the height of the texture in pixels.
         *
         * @return unsigned int, height of the texture object in pixels.
         */
        unsigned int GetHeight() const
        {
            return m_height;
        }

        /**
         * Generate an OpenGL cube map texture object.
         *
         * @param front const std::string &, path to front cubemap image.
         * @param back const std::string &, path to back cubemap image.
         * @param left const std::string &, path to left cubemap image.
         * @param right const std::string &, path to right cubemap image.
         * @param top const std::string &, path to top cubemap image.
         * @param bottom const std::string &, path to bottom cubemap image.
         * @return GLuint, id referring to OpenGL cubemap texture object.
         */
        static GLuint CreateCubeMapTextureObject(const std::string &front, const std::string &back,
                                                 const std::string &left, const std::string &right,
                                                 const std::string &top, const std::string &bottom)
        {
            unsigned char *pixels[6];
            unsigned int width[6];
            unsigned int height[6];

            // Load images in
            FILE *bmpIn = fopen(front.c_str(), "r");
            BMPFile bmpFile;

            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << front.c_str() << std::endl;
            }

            pixels[0] = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4);
            width[0] = bmpFile.GetImageWidth();
            height[0] = bmpFile.GetImageHeight();

            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[0][4*i + 0] = pixel.r;
                pixels[0][4*i + 1] = pixel.g;
                pixels[0][4*i + 2] = pixel.b;
                pixels[0][4*i + 3] = pixel.a;
            }

            fclose(bmpIn);

            bmpIn = fopen(back.c_str(), "r");
            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << back.c_str() << std::endl;
            }

            pixels[1] = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4);
            width[1] = bmpFile.GetImageWidth();
            height[1] = bmpFile.GetImageHeight();

            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[1][4*i + 0] = pixel.r;
                pixels[1][4*i + 1] = pixel.g;
                pixels[1][4*i + 2] = pixel.b;
                pixels[1][4*i + 3] = pixel.a;
            }

            fclose(bmpIn);

            bmpIn = fopen(left.c_str(), "r");
            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << left.c_str() << std::endl;
            }

            pixels[2] = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4);
            width[2] = bmpFile.GetImageWidth();
            height[2] = bmpFile.GetImageHeight();

            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[2][4*i + 0] = pixel.r;
                pixels[2][4*i + 1] = pixel.g;
                pixels[2][4*i + 2] = pixel.b;
                pixels[2][4*i + 3] = pixel.a;
            }

            fclose(bmpIn);

            bmpIn = fopen(right.c_str(), "r");
            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << right.c_str() << std::endl;
            }

            pixels[3] = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4);
            width[3] = bmpFile.GetImageWidth();
            height[3] = bmpFile.GetImageHeight();

            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[3][4*i + 0] = pixel.r;
                pixels[3][4*i + 1] = pixel.g;
                pixels[3][4*i + 2] = pixel.b;
                pixels[3][4*i + 3] = pixel.a;
            }

            fclose(bmpIn);

            bmpIn = fopen(top.c_str(), "r");
            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << top.c_str() << std::endl;
            }

            pixels[4] = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4);
            width[4] = bmpFile.GetImageWidth();
            height[4] = bmpFile.GetImageHeight();

            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[4][4*i + 0] = pixel.r;
                pixels[4][4*i + 1] = pixel.g;
                pixels[4][4*i + 2] = pixel.b;
                pixels[4][4*i + 3] = pixel.a;
            }

            fclose(bmpIn);

            bmpIn = fopen(bottom.c_str(), "r");
            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << bottom.c_str() << std::endl;
            }

            pixels[5] = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4);
            width[5] = bmpFile.GetImageWidth();
            height[5] = bmpFile.GetImageHeight();

            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[5][4*i + 0] = pixel.r;
                pixels[5][4*i + 1] = pixel.g;
                pixels[5][4*i + 2] = pixel.b;
                pixels[5][4*i + 3] = pixel.a;
            }

            fclose(bmpIn);

            GLuint texCube;
            glGenTextures(1, &texCube);

            glBindTexture(GL_TEXTURE_CUBE_MAP, texCube);

            glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0,
                         GL_RGBA,
                         width[0], height[0], 0,
                         GL_RGBA, GL_UNSIGNED_BYTE,
                         pixels[0]);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0,
                         GL_RGBA,
                         width[1], height[1], 0,
                         GL_RGBA, GL_UNSIGNED_BYTE,
                         pixels[1]);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0,
                         GL_RGBA,
                         width[2], height[2], 0,
                         GL_RGBA, GL_UNSIGNED_BYTE,
                         pixels[2]);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0,
                         GL_RGBA,
                         width[3], height[3], 0,
                         GL_RGBA, GL_UNSIGNED_BYTE,
                         pixels[3]);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0,
                         GL_RGBA,
                         width[4], height[4], 0,
                         GL_RGBA, GL_UNSIGNED_BYTE,
                         pixels[4]);
            glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0,
                         GL_RGBA,
                         width[5], height[5], 0,
                         GL_RGBA, GL_UNSIGNED_BYTE,
                         pixels[5]);

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

            for (int i = 0; i < 6; ++i)
            {
                free(pixels[i]);
            }

            return texCube;
        }

        /**
         * Create an OpenGL texture object.
         *
         * @param filePath const std::string &, path to texture image file
         * (must be in BMP RGBA 32-bit format).
         * @param imgWidth unsigned int, width of the image in pixels.
         * @param imgHeight unsigned int, height of the image in pixels.
         * @return GLuint, OpenGL texture object.
         */
        static GLuint CreateTextureObject(const std::string &filePath, unsigned int &imgWidth, unsigned int &imgHeight)
        {
            FILE *bmpIn = fopen(filePath.c_str(), "r");
            sea::BMPFile bmpFile;

            if (bmpFile.Init(bmpIn) == false)
            {
                std::cerr << "Could not load BMP file: " << filePath.c_str() << std::endl;
            }

            // Get bmpFile in format we need (so messy! Do this somewhere else)
            unsigned char *pixels = (unsigned char *)malloc(sizeof(unsigned char) * bmpFile.GetImageWidth() * bmpFile.GetImageHeight() * 4 /* 4 for each RGBA component */);
            for (unsigned int i = 0; i < bmpFile.GetImageHeight() * bmpFile.GetImageWidth(); ++i)
            {
                sea::BMPFile::Pixel pixel = bmpFile.GetImageData()[i];

                pixels[4*i + 0] = pixel.r;
                pixels[4*i + 1] = pixel.g;
                pixels[4*i + 2] = pixel.b;
                pixels[4*i + 3] = pixel.a;
            }

            // Create Texture - Move this to a Renderer call
            GLuint texture = 0;
            glGenTextures(1, &texture);
            glBindTexture(GL_TEXTURE_2D, texture);
            //glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
            glTexImage2D(   GL_TEXTURE_2D, 0,
                            GL_RGBA,
                            bmpFile.GetImageWidth(), bmpFile.GetImageHeight(), 0,
                            GL_RGBA, GL_UNSIGNED_BYTE,
                            pixels);
            glGenerateMipmap(GL_TEXTURE_2D);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            /**
            *  Set maximum anisotropy
            *  @todo   performance?
            */
            GLfloat maxAnisotropy = 0.0f;
            glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);

            glBindTexture(GL_TEXTURE_2D, 0);

            fclose(bmpIn);
            free(pixels);

            imgWidth = bmpFile.GetImageWidth();
            imgHeight = bmpFile.GetImageHeight();

            return texture;
        }
    private:
        unsigned int    m_textureID;

        unsigned int    m_width;
        unsigned int    m_height;
    };
}

#endif  // SEA_TEXTURE_H
