#include "engine/render/UIDrawer.h"
#include "engine/Defines.h"

namespace sea
{
    bool UIDrawer::Init()
    {
        ShaderManager shaderManager;

        // Geometry shader version
        // shaderManager.CreateProgram("TexturedQuad",
        //                             PATH_TO_ASSETS "shaders/textured_quad.vert",
        //                             PATH_TO_ASSETS "shaders/textured_quad.geom",
        //                             PATH_TO_ASSETS "shaders/textured_quad.frag");

        // m_texturedQuadProgram = shaderManager.GetProgram("TexturedQuad");

        // Non-Geometry shader version
        shaderManager.CreateProgram("TexturedQuad",
                                    PATH_TO_ASSETS "shaders/textured_quad_verts.vert",
                                    PATH_TO_ASSETS "shaders/textured_quad_verts.frag"
        );

        m_texturedQuadProgram = shaderManager.GetProgram("TexturedQuad");


        glGenVertexArrays(1, &m_vao);
        glGenBuffers(1, &m_vbo);

        return true;
    }

    void UIDrawer::DrawTexturedQuad(float startX, float startY,
                                    float endX, float endY,
                                    const Texture &texture,
                                    float transparency) 
    {
        m_vertices.clear();
        m_vertices = {startX, startY,
                      0.0f, 0.0f,
                      endX, startY,
                      1.0f, 0.0f,
                      startX, endY,
                      0.0f, 1.0f,
                      endX, endY,
                      1.0f, 1.0f};

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(float), &m_vertices[0], GL_DYNAMIC_DRAW);

        glUseProgram(m_texturedQuadProgram);

        glBindVertexArray(m_vao);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), NULL);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (char*)NULL + 2 * sizeof(float));

        // Associate sampler with texture unit 0
        GLint textureLocation = glGetUniformLocation(m_texturedQuadProgram,
                                                     "basicTexture");
        glUniform1i(textureLocation, 0);

        // Bind quad coordinates to shader
        GLint startCoordsLocation = glGetUniformLocation(m_texturedQuadProgram,
                                                         "startCoords");
        glUniform2f(startCoordsLocation, startX, startY);

        GLint endCoordsLocation = glGetUniformLocation(m_texturedQuadProgram,
                                                       "endCoords");
        glUniform2f(endCoordsLocation, endX, endY);

        GLint transparencyLocation = glGetUniformLocation(m_texturedQuadProgram,
                                                          "transparencyFactor");
        glUniform1f(transparencyLocation, transparency);

        // Bind texture to texture unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture.GetTextureID());

        // Non-geometry shader
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        // Geometry shader
        // glDrawArrays(GL_POINTS, 0, 1);

        glUseProgram(0);
    }
}
