#include "engine/render/Skybox.h"
#include "engine/util/ShaderManager.h"
#include "engine/render/Texture.h"

namespace sea
{
    Skybox::Skybox()
    {
        m_vao = 0;
        m_texture = 0;
        m_shader = 0;
    }

    bool Skybox::Init(const std::string &shaderProgramName,
                      const std::string &front, const std::string &back,
                      const std::string &left, const std::string &right,
                      const std::string &top, const std::string &bottom)
    {
        bool result = false;

        m_quadVerts.push_back(-1.0f); m_quadVerts.push_back(-1.0f);
        m_quadVerts.push_back(1.0f); m_quadVerts.push_back(-1.0f);
        m_quadVerts.push_back(1.0f); m_quadVerts.push_back(1.0f);
        m_quadVerts.push_back(1.0f); m_quadVerts.push_back(1.0f);
        m_quadVerts.push_back(-1.0f); m_quadVerts.push_back(1.0f);
        m_quadVerts.push_back(-1.0f); m_quadVerts.push_back(-1.0f);

        GLuint vbo = 0;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, m_quadVerts.size() * sizeof(float), &m_quadVerts[0], GL_STATIC_DRAW);

        GLuint vao = 0;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glBindVertexArray(0);

        m_vao = vao;

        ShaderManager shaderManager;
        m_shader = shaderManager.GetProgram(shaderProgramName);

        m_texture = Texture::CreateCubeMapTextureObject(front, back,
                                                        left, right,
                                                        top, bottom);

        return result;
    }

    void Skybox::Draw() const
    {
        glDepthMask(GL_FALSE);

        glUseProgram(m_shader);
        glUniform1f(glGetUniformLocation(m_shader, "cubeMapTexture"), 0);
        glBindVertexArray(m_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture);

        // 2 floats per vertex
        glDrawArrays(GL_TRIANGLES, 0, m_quadVerts.size()/2);

        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        glBindVertexArray(0);
        glUseProgram(0);

        glDepthMask(GL_TRUE);
    }
}
