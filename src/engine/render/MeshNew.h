#ifndef SEA_MESH_NEW_H
#define SEA_MESH_NEW_H

#include <vector>
#include <map>
#include <string>
#include <cassert>

#include "engine/render/SubMesh.h"

namespace sea
{
    /**
     * Mesh class.
     *
     * Holds all information relating to the mesh, including it's OpenGL id
     * and any SubMeshes (Meshes are made up of smalled SubMesh objects which
     * index into the larger vertex array provided by the Mesh).
     */
    class MeshNew
    {
    public:
        /**
         *  Default constructor.
         *
         *  Initialize member variables to sensible
         *  default values.
         */
        MeshNew()
        {
            m_vaoID = 0;
        }
        /**
         *  Constructor.
         *
         *  Initialize mesh with given vertex array ID.
         *
         *  @param  vaoID   unsigned int, vertex array ID.
         */
        MeshNew(unsigned int vaoID)
        {
            m_vaoID = vaoID;
        }

        /**
         *  Get the vertex array ID of this mesh.
         *
         *  @return     unsigned int, vertex array ID of this mesh.
         */
        unsigned int GetVertexArrayID() const
        {
            return m_vaoID;
        }
        /**
         *  Set the vertex array ID of this mesh.
         *
         *  @param  vaoID   unsigned int, vertex array ID to set.
         *  @return         bool, true if set was successful, false otherwise.
         */
        bool SetVertexArrayID(unsigned int vaoID)
        {
            m_vaoID = vaoID;

            return true;
        }

        /**
         *  Add a sub-mesh to this mesh.
         *
         *  A sub-mesh is required in order for the mesh to be drawn.
         *  You must also give a name to the mesh. The name must be
         *  unique within the mesh. Otherwise it will replace the
         *  sub-mesh currently existing under that name.
         *
         *  @pre    A sub-mesh within this mesh does not already have
         *          the given name.
         *
         *  @param  subMeshName     const std::string &, name to give subMesh.
         *  @param  subMesh         const SubMesh &, sub-mesh to add.
         */
        void AddSubMesh(const std::string &subMeshName, const SubMesh &subMesh)
        {
            // Index where subMesh is about to be placed
            // Note that this is the size of the array before insertion.
            unsigned int index = m_subMeshes.size();
            m_subMeshes.push_back(subMesh);
            m_subMeshNameToIndexMap[subMeshName] = index;
        }
        /**
         *  Get the number of sub-meshes belonging to this mesh.
         *
         *  @return     unsigned int, number of sub-meshes belonging to this mesh.
         */
        unsigned int GetNumSubMeshes() const
        {
            return m_subMeshes.size();
        }
        /**
         *  Get the index of the sub-mesh with the given name.
         *
         *  @param  subMeshName     const std::string &, name of the sub-mesh.
         *  @return                 unsigned int, sub-mesh index.
         */
        unsigned int GetSubMeshIndex(const std::string &subMeshName)
        {
            return m_subMeshNameToIndexMap[subMeshName];
        }
        /**
         *  Get the sub-mesh at the given index.
         *
         *  @pre    index is a valid index (0 <= index < GetNumSubMeshes()).
         *
         *  @param  index   unsigned int, index.
         *  @return         const SubMesh &, sub-mesh at given index.
         */
        const SubMesh &GetSubMesh(unsigned int index) const
        {
            assert(index < m_subMeshes.size());

            return m_subMeshes[index];
        }
    private:
        typedef std::map<std::string, unsigned int> SubMeshMap;

        // Vertex array id of this mesh
        unsigned int            m_vaoID;

        // Sub-meshes of this mesh.
        std::vector<SubMesh>    m_subMeshes;
        // Map a sub-mesh's name to it's index
        std::map<std::string, unsigned int>     m_subMeshNameToIndexMap;
    };
}

#endif  // SEA_MESH_NEW_H
