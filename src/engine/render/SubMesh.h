#ifndef SEA_SUB_MESH_H

#define SEA_SUB_MESH_H

namespace sea
{
    /**
     * SubMesh class.
     *
     * Used to represent a submesh in a larger mesh (Mesh object).
     * A submesh is made of merely a start index into a larger buffer and
     * the number of indices the submesh is made up of.
     */
    class SubMesh
    {
    public:
        /**
         * Default constructor, create a SubMesh with a start index of 0 and
         * no indices.
         */
        SubMesh()
        {
            m_startIndex = 0;
            m_numIndices = 0;
        }

        /**
         * Create a SubMesh with a given start index and a given number of
         * indices.
         *
         * @param startIndex unsigned int, start index of the subMesh in the
         * larger mesh.
         * @param numIndices unsigned int, number of indices subMesh is made of.
         */
        SubMesh(unsigned int startIndex, unsigned int numIndices)
        {
            m_startIndex = startIndex;
            m_numIndices = numIndices;
        }

        /**
         * Get the start index of the SubMesh.
         *
         * @return unsigned int, start index of the SubMesh.
         */
        unsigned int GetStartIndex() const
        {
            return m_startIndex;
        }

        /**
         * Get the number of indices in the SubMesh.
         *
         * @return unsigned int, number of indices the SubMesh is made up of.
         */
        unsigned int GetNumIndices() const
        {
            return m_numIndices;
        }

        /**
         * Set the start index of the SubMesh.
         *
         * @param startIndex unsigned int, start index of the SubMesh in the
         * larger mesh.
         * @return bool, TRUE if set was successful, FALSE otherwise.
         */
        bool SetStartIndex(unsigned int startIndex)
        {
            bool result = true;

            m_startIndex = startIndex;

            return result;
        }

        /**
         * Set the number of indices the SubMesh is made up of.
         *
         * @param numIndices unsigned int, number of indices SubMesh is made up
         * of.
         * @return bool, TRUE if set was successful, FALSE otherwise.
         */
        bool SetNumIndices(unsigned int numIndices)
        {
            bool result = true;

            m_numIndices = numIndices;

            return result;
        }

        /**
         * SubMesh equivalence operator.
         *
         * Compare one Submesh to another and return TRUE if their number of
         * indices AND start indices are the same or FALSE otherwise.
         *
         * @param other const SubMesh &, const reference to SubMesh object to
         * compare this SubMesh with.
         * @return bool, TRUE if SubMeshes are equal, FALSE otherwise.
         */
        bool operator==(const SubMesh &other) const
        {
            return (this->GetNumIndices() == other.GetNumIndices() &&
                    this->GetStartIndex() == other.GetStartIndex());
        }
    private:
        unsigned int m_startIndex;
        unsigned int m_numIndices;
    };
}

#endif  // SEA_SUB_MESH_H
