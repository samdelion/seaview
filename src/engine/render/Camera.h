#ifndef SEA_CAMERA_H
#define SEA_CAMERA_H

#include "math/Vec3.h"
#include "math/Mat4.h"
#include "math/Quat.h"
#include "math/Globals.h"

namespace sea
{
    /**
     * Camera class.
     *
     * Information pertaining to Camera object used to render the scene.
     * Cameras have a location, orientation, forward an up direction,
     * aspect ratio, near clip, far clip and field of view.
     */
    class Camera
    {
    public:
        /**
         * Camera constructor. Create a Camera object with the given location,
         * orientation, forward and up direction, aspect ratio,
         * near Z clipping plane, far Z clipping plane and field of view.
         *
         * @param location sea_math::Vec3, location of the camera.
         * @param orientation sea_math::Quat, orientation of the camera.
         * @param forward sea_math::Vec3, forward direction of the camera.
         * @param up sea_math::Vec3, up direction of the camera.
         * @param aspectRation float, aspect ratio of the camera.
         * @param nearZClip float, near Z clipping plane of the camera.
         * @param farZClip float, far Z clipping plane of the camera.
         * @param verticalFovDeg, vertical field of view of the camera in
         * degrees.
         */
        Camera(sea_math::Vec3 location = sea_math::Vec3(),
               sea_math::Quat orientation = sea_math::Quat(),
               sea_math::Vec3 forward = sea_math::Vec3(0.0f, 0.0f, -1.0f),
               sea_math::Vec3 up = sea_math::Vec3(0.0f, 1.0f, 0.0f),
               float aspectRatio = 800.0f / 600.0f,
               float nearZClip = 0.01f,
               float farZClip = 20000.0f,
               float verticalFovDeg = 66.6f)
        {
            m_location = location;
            m_orientation = orientation;
            m_forward = forward;
            m_up = up;
            m_aspectRatio = aspectRatio;
            m_nearZClip = nearZClip;
            m_farZClip = farZClip;
            m_verticalFovDeg = verticalFovDeg;
        }

        /**
         * Get the current forward direction of the camera (where the camera
         * is pointing).
         *
         * @return sea_math::Vec3, forward direction of the camera.
         */
        sea_math::Vec3 Forward() const
        {
            sea_math::Vec4 fwd =
                sea_math::Normalize(sea_math::CastMat4(m_orientation) * sea_math::Vec4(m_forward, 0.0f));
            return (sea_math::Vec3(fwd.x, fwd.y, fwd.z));
        }

        /**
         * Get the current right direction of the camera.
         *
         * @return sea_math::Vec3, right direction of the camera.
         */
        sea_math::Vec3 Right() const
        {
            sea_math::Vec4 right =
                sea_math::Normalize(sea_math::CastMat4(m_orientation) *
                                    sea_math::Cross(m_forward, m_up));
            return (sea_math::Vec3(right.x, right.y, right.z));
        }

        /**
         * Get the current up direction of the camera.
         *
         * @return sea_math::Vec3, up direction of the camera.
         */
        sea_math::Vec3 Up() const
        {
            sea_math::Vec4 up =
                sea_math::Normalize(sea_math::CastMat4(m_orientation) * sea_math::Vec4(m_up, 0.0f));
            return (sea_math::Vec3(up.x, up.y, up.z));
        }

        /**
         * Get the current location of the camera.
         *
         * @return const sea_math::Vec3 &, const reference to current camera
         * location.
         */
        const sea_math::Vec3 &GetLocation() const
        {
            return m_location;
        }

        /**
         * Set the current location of the camera.
         *
         * @param location const sea_math::Vec3 &
         */
        void SetLocation(const sea_math::Vec3 &location)
        {
            m_location = location;
        }

        /**
         * Get the current orientation of the camera.
         *
         * @return const sea_math::Quat &, orientation of the camera.
         */
        const sea_math::Quat &GetOrientation() const
        {
            return m_orientation;
        }

        /**
         * Set the orientation of the camera.
         *
         * @param orientation const sea_math::Quat &, desired orientation of
         * the camera.
         */
        void SetOrientation(const sea_math::Quat &orientation)
        {
            m_orientation = orientation;
        }

        /**
         * Set the aspect ratio of the camera.
         *
         * @param aspectRatio float, desired aspect ratio.
         */
        void SetAspectRatio(float aspectRatio)
        {
            m_aspectRatio = aspectRatio;
        }

        /**
         * Generate the view matrix of the camera.
         *
         * @return sea_math::Mat4, current view matrix of the camera.
         */
        sea_math::Mat4 CreateViewMatrix() const
        {
            sea_math::Mat4 translationMatrix = sea_math::Mat4(1.0f);
            translationMatrix[3] = sea_math::Vec4(-GetLocation(), 1.0f);
            sea_math::Mat4 rotationMatrix = sea_math::CastMat4(-GetOrientation());

            return (rotationMatrix * translationMatrix);
        }

        /**
         * Generate the projection matrix of the camera.
         *
         * @return sea_math::Mat4, current projection matrix of the camera.
         */
        sea_math::Mat4 CreateProjectionMatrix() const
        {
            sea_math::Mat4 projectionMatrix = sea_math::Mat4(0.0f);

            float range = tan(sea_math::fDegToRad(m_verticalFovDeg) * 0.5) * m_nearZClip;
            // X Scale
            float Sx = (2.0f * m_nearZClip) / (range * m_aspectRatio + range * m_aspectRatio);
            // Y Scale
            float Sy =  m_nearZClip / range;
            // Z Scale
            float Sz =  -(m_farZClip + m_nearZClip) / (m_farZClip - m_nearZClip);
            float Pz = -(2.0f * m_farZClip * m_nearZClip) / (m_farZClip - m_nearZClip);

            projectionMatrix[0].x = Sx;
            projectionMatrix[1].y = Sy;
            projectionMatrix[2].z = Sz;
            projectionMatrix[2].w = -1.0f;
            projectionMatrix[3].z = Pz;
            projectionMatrix[3].w = 0.0f;

            return projectionMatrix;
        }
    private:
        sea_math::Vec3 m_location;
        sea_math::Quat m_orientation;
        sea_math::Vec3 m_forward;
        sea_math::Vec3 m_up;
        float m_aspectRatio;
        float m_nearZClip;
        float m_farZClip;
        float m_verticalFovDeg;
    };
}

#endif // SEA_CAMERA_H
