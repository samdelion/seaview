#ifndef SEA_RENDER_INSTANCE_H
#define SEA_RENDER_INSTANCE_H

#include <vector>
#include <map>

#include "engine/render/Mesh.h"
#include "engine/render/Texture.h"

namespace sea
{
    class RenderInstance
    {
    public:
        RenderInstance()
        {
            m_mesh = NULL;
        }

        bool AddMesh(Mesh *mesh)
        {
            bool result = false;

            if (mesh != NULL)
            {
                m_mesh = mesh;

                result = true;
            }

            return result;
        }

        bool AddTexture(Texture *texture)
        {
            bool result = false;

            if (texture != NULL)
            {
                m_textures.push_back(texture);

                result = true;
            }

            return result;
        }

        const Mesh *const GetMesh()
        {
            return m_mesh;
        }

        const Texture *const GetTexture(unsigned int index)
        {
            if (index < m_textures.size())
            {
                return m_textures[index];
            }
            else
            {
                return NULL;
            }
        }

        const SubMesh *const GetSubMesh(unsigned int index)
        {
            if (m_mesh != NULL)
            {
                return &(m_mesh->GetSubMesh(index));
            }
            else
            {
                return NULL;
            }
        }

        void MapTextureToSubMesh(unsigned int textureIndex, unsigned int subMeshIndex)
        {
            m_subMeshToTextureMap[subMeshIndex] = textureIndex;
        }

        unsigned int GetTextureIndexForSubMesh(unsigned int subMeshIndex)
        {
            return m_subMeshToTextureMap[subMeshIndex];
        }
    private:
        Mesh                    *m_mesh;
        std::vector<Texture *>  m_textures;

        std::map<unsigned int, unsigned int>    m_subMeshToTextureMap;
    };
}

#endif  // SEA_RENDER_INSTANCE_H
