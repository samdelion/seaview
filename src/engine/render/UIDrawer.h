#ifndef SEA_UI_DRAWER_H
#define SEA_UI_DRAWER_H

#include <vector>

#include "engine/util/ShaderManager.h"
#include "engine/render/Texture.h"

namespace sea
{
    /**
     * UIDrawer class.
     *
     * Draws UI elements on the screen.
     *
     * Some methods take screen-space coordinates. The screen in OpenGL is set
     * out as follows:
     * (-1, 1) - top left
     * (1, 1) - top right
     * (0, 0) - dead centre
     * (-1, -1) - bottom left
     * (1, -1) - bottom right
     *
     * i.e.
     *  (-1, 1)--------------- (1, 1)
     *         |             |
     *         |      +(0, 0)|
     *         |             |
     * (-1, -1)--------------- (1, -1)
     */
    class UIDrawer
    {
    public:
        /**
         * Initialize the UI Drawer class.
         *
         * This method must be called before the UI Drawer
         * can be used.
         */
        bool Init();

        /**
         * Draw a textured quad (rectangle) on the screen.
         *
         * Coordinates are given in screen space.
         *
         * @param startX float, x value to start drawing quad from.
         * @param startY float, y value to start drawing quad from.
         * @param endX float, x value at which to stop drawing quad.
         * @param endY float, y value at which to stop drawing quad.
         * @param texture const Texture &, texture object to draw on quad.
         * @param transparency float, between 0 (fully transparent)
         * and 1 (fully opaque), how transparent UI element should be.
         * Note that this value is multiplied by the transparency value of
         * the texture.
         */
        void DrawTexturedQuad(float startX, float startY,
                              float endX, float endY,
                              const Texture &texture,
                              float transparency);
    private:
        GLuint m_vao, m_vbo;
        GLuint m_texturedQuadProgram;
        std::vector<float> m_vertices;
    };
}

#endif // SEA_UI_DRAWER_H
