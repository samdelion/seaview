#ifndef SEA_MESH_H
#define SEA_MESH_H

#include <vector>

#include "engine/render/SubMesh.h"

namespace sea
{
    /**
     *  Class
     */
    class Mesh
    {
    public:
        Mesh()
        {
            m_vboID = 0;
            m_iboID = 0;
            m_stride = 0;
            m_vertexOffset = 0;
            m_normalOffset = 0;
            m_texCoordOffset = 0;
        }

        ~Mesh()
        {
        }

        Mesh(unsigned int vertexBufferID, unsigned int dataStride,
            unsigned int vertexOffset, unsigned int normalOffset, unsigned int texCoordOffset,
            unsigned int indexBufferID)
        {
            m_vboID = vertexBufferID;
            m_iboID = indexBufferID;

            m_stride = dataStride;
            m_vertexOffset = vertexOffset;
            m_normalOffset = normalOffset;
            m_texCoordOffset = texCoordOffset;
        }

        void AddSubMesh(unsigned int startIndex, unsigned int numIndices)
        {
            m_subMeshes.push_back(SubMesh(startIndex, numIndices));
        }

        unsigned int GetVertexBufferID() const
        {
            return m_vboID;
        }

        unsigned int GetIndexBufferID() const
        {
            return m_iboID;
        }

        unsigned int GetBufferDataStride() const
        {
            return m_stride;
        }

        unsigned int GetVertexOffset() const
        {
            return m_vertexOffset;
        }

        unsigned int GetNormalOffset() const
        {
            return m_normalOffset;
        }

        unsigned int GetTexCoordOffset() const
        {
            return m_texCoordOffset;
        }

        unsigned int GetNumSubMeshes() const
        {
            return m_subMeshes.size();
        }

        const SubMesh &GetSubMesh(unsigned int index) const
        {
            return m_subMeshes[index];
        }
    private:
        unsigned int    m_vboID;
        unsigned int    m_iboID;

        unsigned int    m_stride;
        unsigned int    m_vertexOffset;
        unsigned int    m_normalOffset;
        unsigned int    m_texCoordOffset;

        std::vector<SubMesh> m_subMeshes;
    };
}

#endif  // SEA_MESH_H
