#ifndef SEA_RENDER_INSTANCE_NEW_H
#define SEA_RENDER_INSTANCE_NEW_H

#include <vector>
#include <map>

#include "engine/render/MeshNew.h"
#include "engine/render/Texture.h"

namespace sea
{
    /**
     * Render instance class.
     *
     * Contains all information required to render an object in the scene.
     * Used by the Rendering System object to render the scene.
     */
    class RenderInstanceNew
    {
    public:
        /**
         * Add a mesh to the render instance.
         *
         * Only one mesh may be present in the render instance at any one time.
         *
         * @param mesh const MeshNew &, const reference to Mesh object to use.
         */
        void AddMesh(const MeshNew &mesh)
        {
            m_mesh = mesh;
        }

        /**
         * Add a texture object to the render instance.
         *
         * These texture objects are keyed by a string.
         *
         * @param textureName const std::string &, string to key texture with.
         * @param texture const Texture &, texture object to add to render
         * instance.
         */
        void AddTexture(const std::string &textureName, const Texture &texture)
        {
            m_textures[textureName] = texture;
        }

        /**
         * Get the mesh the render instance is currently using.
         *
         * @return const MeshNew &, mesh the render instance is currently using.
         */
        const MeshNew &GetMesh()
        {
            return m_mesh;
        }

        /**
         * Associate a diffuse texture (accessed by it's string key) to a
         * SubMesh in the larger Mesh.
         *
         * @param textureName const std::string &, texture to map to SubMesh.
         * @param subMeshIndex unsigned int, index of the subMesh in the Mesh
         * object that you want to map the given texture to.
         */
        void MapDiffuseTextureToSubMesh(const std::string &textureName,
                                        unsigned int subMeshIndex)
        {
            m_subMeshToDiffuseTextureMap[subMeshIndex] = textureName;
        }

        /**
         * Get the diffuse texture object currently associated with the
         * SubMesh given by it's index in the larger Mesh object.
         *
         * @param subMeshIndex unsigned int, the index of the SubMesh in the
         * larger Mesh object.
         * @return const Texture &, texture object currently associated with
         * given SubMesh.
         */
        const Texture &GetDiffuseTextureForSubMesh(unsigned int subMeshIndex)
        {
            return m_textures[m_subMeshToDiffuseTextureMap[subMeshIndex]];
        }

        /**
         * Not currently used.
         */
        void MapLightmapTextureToSubMesh(const std::string &textureName,
                                         unsigned int subMeshIndex)
        {
            m_subMeshToLightmapTextureMap[subMeshIndex] = textureName;
        }

        /**
         * Not currently used.
         */
        const Texture &GetLightmapTextureForSubMesh(unsigned int subMeshIndex)
        {
            return m_textures[m_subMeshToLightmapTextureMap[subMeshIndex]];
        }

        /**
         * Get the shader program the RenderInstance will use when it is
         * rendered.
         *
         * @param unsigned int, OpenGL shader program object id.
         */
        unsigned int GetShaderProgram()
        {
            return m_shaderProgram;
        }

        /**
         * Set the shader program that will be used when this RenderInstance
         * is rendered.
         *
         * @param shaderProgram unsigned int, OpenGL shader program object id.
         * @return bool, TRUE if the set was successful, FALSE otherwise.
         */
        bool SetShaderProgram(unsigned int shaderProgram)
        {
            m_shaderProgram = shaderProgram;

            return true;
        }
    private:
        MeshNew m_mesh;
        unsigned int m_shaderProgram;

        // List of textures mapped to their name
        std::map<std::string, Texture>  m_textures;
        // Map sub-mesh index to texture name
        std::map<unsigned int, std::string> m_subMeshToDiffuseTextureMap;
        std::map<unsigned int, std::string> m_subMeshToLightmapTextureMap;
    };
}

#endif  // SEA_RENDER_INSTANCE_NEW_H
