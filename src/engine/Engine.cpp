#include <iostream>

#include <GL/glew.h>
#include "engine/Engine.h"

namespace sea
{
    Engine::Engine()
    {
        m_pApplication = NULL;
        m_shouldQuit = false;
        m_isPaused = false;
    }

    bool Engine::Init(IApplication *application)
    {
        bool result = false;

        m_pApplication = application;

        /** @todo   Look in playerOptions.xml for width and height */
        m_width = 1400;
        m_height = 800;
        int xpos = 100;
        int ypos = 100;

        //jules testing
        SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

        Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048);

        //jules testing

        SDL_SetRelativeMouseMode(SDL_TRUE);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

        window = SDL_CreateWindow(
            m_pApplication->VGetApplicationTitle(),
            xpos, ypos,
            m_width, m_height,
            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE |  SDL_WINDOW_INPUT_GRABBED
            );

        int width, height;
        SDL_GetWindowSize(window, &width, &height);
        m_width = width;
        m_height = height;

        // Get OpenGL version information from the application
        GLuint majorVersion = m_pApplication->VGetOpenGLMajorVersion();
        GLuint minorVersion = m_pApplication->VGetOpenGLMinorVersion();

        // If requesting a modern OpenGL context
        if ((majorVersion >= 4) || (majorVersion >= 3 && minorVersion >= 3))
        {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, majorVersion);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minorVersion);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        }
        // Else requesting legacy OpenGL context
        else
        {
#ifdef __APPLE__
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, majorVersion);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minorVersion);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
#else
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
#endif
        }

        SDL_GLContext openGLContext = SDL_GL_CreateContext(window);

        std::cout << SDL_GetError() << std::endl;

        glewExperimental = GL_TRUE;
        GLenum err = glewInit();

        if (err != GLEW_OK)
        {
            std::cerr << "Failed to initialize GLEW: " << std::endl;
            std::cerr <<  glewGetErrorString(err) << std::endl;
        }

        const GLubyte *renderer = glGetString(GL_RENDERER);
        const GLubyte *version = glGetString(GL_VERSION);
        std::cout << "Renderer: " << renderer << " Version: " << version << std::endl;

        int depthSize = 0;
        SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &depthSize);

        std::cout << "Depth size: " << depthSize << std::endl;

        glEnable(GL_DEPTH_TEST); // enable depth-testing
        glDepthFunc(GL_LESS);
        glClearDepth(1.0f);
        glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
        glViewport(0, 0, m_width, m_height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Switch to application
        m_pApplication->VOnSwitchTo();
        // Allow application to draw
        SDL_GL_SwapWindow(window);

        // Initialize application
        result = m_pApplication->VInit();
        m_pApplication->VResize(m_width, m_height);

        return result;
    }

    void Engine::MainLoop()
    {
        /** @todo Move all SDL stuff to a Window class */
        SDL_Event   event;

        while (!m_shouldQuit)
        {
            static double   prevSeconds = SDL_GetTicks() / 1000.0f;
            double      currSeconds = SDL_GetTicks() / 1000.0f;
            double      elapsedSeconds = currSeconds - prevSeconds;
            prevSeconds = currSeconds;

            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                case SDL_WINDOWEVENT:
                    switch (event.window.event)
                    {
                    case SDL_WINDOWEVENT_SIZE_CHANGED:
                        m_width = event.window.data1;
                        m_height = event.window.data2;
                        // Call Resize callback
                        m_pApplication->VResize(m_width, m_height);
                        break;
                    case SDL_WINDOWEVENT_CLOSE:
                        event.type = SDL_QUIT;
                        SDL_PushEvent(&event);
                        break;
                    }
                    break;
                case SDL_KEYDOWN:
                    m_pApplication->VKeyDown(event.key.keysym.sym);
                    break;
                case SDL_KEYUP:
                    m_pApplication->VKeyUp(event.key.keysym.sym);
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    m_pApplication->VMouseButtonDown(event.button.button);
                    break;
                case SDL_QUIT:
                    RequestExit();
                    break;
                }
            }


            if (m_isPaused == false)
            {
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                // Update systems
                for (int i = 0; i < m_systems.size(); ++i)
                {
                    if (m_systems[i] != NULL)
                    {
                        m_systems[i]->VUpdate(elapsedSeconds);
                    }
                }

                // Call update callback
                m_pApplication->VUpdate(elapsedSeconds);
                // Call render callback
                m_pApplication->VRender();

                SDL_GL_SwapWindow(window);
            }
        }

        Cleanup();
    }

    bool Engine::SwitchApplication(IApplication *application)
    {
        bool result = true;

        delete m_pApplication;

        Cleanup();

        Init(application);

        return result;
    }

    void Engine::AddSystem(ISystem *system)
    {
        m_systems.push_back(system);
    }

    void Engine::RequestExit()
    {
        m_shouldQuit = true;
    }

    void Engine::RequestPause()
    {
        m_isPaused = true;
        SDL_GL_SwapWindow(window);
    }

    void Engine::RequestUnPause()
    {
        m_isPaused = false;
    }

    void Engine::Cleanup()
    {
        // Remove all registered systems
        m_systems.clear();

        SDL_DestroyWindow(window);
        Mix_CloseAudio();
        SDL_Quit();
    }

    unsigned int Engine::GetWindowWidth() const
    {
        int width;
        SDL_GetWindowSize(window, &width, nullptr);

        return width;
    }

    unsigned int Engine::GetWindowHeight() const
    {
        int height;
        SDL_GetWindowSize(window, nullptr, &height);

        return height;
    }
}
