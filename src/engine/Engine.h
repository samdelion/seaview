#ifndef SEA_ENGINE_H
#define SEA_ENGINE_H

#include <vector>

#include <SDL2/SDL.h>

#include "engine/interfaces/IApplication.h"
#include "engine/interfaces/ISystem.h"
#include <SDL_mixer.h>

namespace sea
{
    /**
     *  Engine class.
     *
     *  Infinite loop that processes events
     *  and calls functions in the Application currently running
     *  when appropriate.
     */
    class Engine
    {
    public:
        /**
         *  Default constructor.
         *
         *  Give safe default values to engine variables.
         */
        Engine();
        /**
         *  Initialize engine.
         *
         *  @param  application     IApplication *, pointer to application
         *                          to initialize engine with. This is the
         *                          application that will be run by the engine
         *                          initially.
         *  @return                 bool, true if initialization was successful, false otherwise.
         *  @todo                   Use smart pointers here.
         */
        bool Init(IApplication *application);
        /**
         *  Infinite loop that processes events
         *  and calls appropriate functions in application.
         */
        void MainLoop();
        /**
         *  Switch application engine is currently executing.
         *
         *  Cleans up previous application.
         *
         *  @return     bool, returns true if switch was successful, false otherwise.
         *  @todo       Remove this.
         */
        bool SwitchApplication(IApplication *application);
        /**
         * Add a system to the Engine.
         *
         * The Engine updates each of it's systems every tick.
         *
         * @param system ISystem *, pointer to system to register with engine.
         */
        void AddSystem(ISystem *system);

        /**
         * Exits the application, frees any memory associated with the engine
         * and closes the window.
         */
        void RequestExit();

        /**
         * Request the engine to pause. Will swap the current framebuffer one
         * last time and pause execution of update and render callbacks, as well
         * as system updates. Will still handle input.
         */
        void RequestPause();

        /**
         * Request the engine to unpause and continue updating systems and
         * executing callbacks.
         */
        void RequestUnPause();

        /**
         * Get the width of the window the engine is currently
         * rendering to.
         *
         * @return unsigned int, width of the window in pixels.
         */
        unsigned int GetWindowWidth() const;

        /**
         * Get the height of the window the engine is currently
         * rendering to.
         *
         * @return unsigned int, height of the window in pixels.
         */
        unsigned int GetWindowHeight() const;
    private:
        /**
         * Method to clean up after the engine and application after a
         * application switch. Should also free any memory used by
         * external libraries etc.
         */
        void Cleanup();

        IApplication    *m_pApplication;
        unsigned int    m_width;
        unsigned int    m_height;
        SDL_Window      *window;

        std::vector<ISystem *> m_systems;

        bool m_shouldQuit;
        bool m_isPaused;
    };

    /** Global engine pointer */
    extern Engine *g_engine;
}

#endif  // SEA_ENGINE_H
