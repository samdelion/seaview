#ifndef SEA_RENDERING_SYSTEM_H
#define SEA_RENDERING_SYSTEM_H

#include <vector>
#include <string>

#include "engine/interfaces/ISystem.h"
#include "engine/components/TransformComponentManager.h"
#include "engine/components/RenderComponentManager.h"
#include "engine/util/ShaderManager.h"
#include "engine/render/Camera.h"
#include "engine/render/Skybox.h"

namespace sea
{
    /**
     * Rendering System.
     *
     * Responsible for rendering all entities in the game with a
     * valid render component. Also renders the skybox and holds the camera
     * the scene is rendered from.
     */
    class RenderingSystem : public ISystem
    {
    public:
        /**
         * Initialize the Rendering system.
         *
         * Requires access to the transform components and render components
         * of the game.
         *
         * @param transformManager TransformComponentManager *, pointer to
         * transform components of the game.
         * @param renderManager RenderComponentManager *, pointer to render
         * components of the game.
         */
        void Init(TransformComponentManager *transformManager,
                  RenderComponentManager *renderManager);
        /**
         * Initialize the skybox with the given shader program and skybox
         * images.
         *
         * @pre shader program provided has already been created using shader
         * manager.
         *
         * @param shaderProgramName const std::string &, name of the shader
         * program to use for the skybox. Must have been created before call.
         * @param front const std::string &, path to front image of skybox.
         * @param back const std::string &, path to back image of skybox.
         * @param left const std::string &, path to left image of skybox.
         * @param right const std::string &, path to right image of skybox.
         * @param top const std::string &, path to top image of skybox.
         * @param bottom const std::string &, path to bottom image of skybox.
         */
        void InitSkybox(const std::string &shaderProgramName,
                        const std::string &front, const std::string &back,
                        const std::string &left, const std::string &right,
                        const std::string &top, const std::string &bottom);
        /**
         * Called by Engine every update tick, all rendering happens in this
         * method.
         *
         * @param deltaTime float, time last frame took to render in
         * milliseconds.
         */
        virtual void VUpdate(float deltaTime);

        /**
         * Every VUpdate call, will update given shader
         * with the latest view and projection matrices.
         *
         * The name of the uniforms in the shader MUST be
         * "viewMatrix" for view matrix and
         * "projectionMatrix" for projection matrix.
         *
         * @param shaderProgramName const std::string &, name of shader program
         * as registered with shader manager.
         */
        void RegisterShaderForCameraUpdate(const std::string &shaderProgramName);

        /**
         * Get the camera the scene is rendered from.
         *
         * @return const Camera &, const reference to Camera object.
         */
        const Camera &GetCamera() const;
        /**
         * Get the camera the scene is rendered from.
         *
         * @return Camera &, reference to Camera object.
         */
        Camera &GetCamera();
    private:
        TransformComponentManager   *m_pTransformManager;
        RenderComponentManager      *m_pRenderManager;
        ShaderManager m_shaderManager;
        Camera m_camera;
        Skybox m_skybox;

        std::vector<std::string> m_registeredShaders;
    };
}

#endif  // SEA_RENDERING_SYSTEM_H
