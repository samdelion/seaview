#include <iostream>

#include <GL/glew.h>

#include "engine/core/Instance.h"
#include "engine/systems/RenderingSystem.h"
#include "engine/render/RenderInstanceNew.h"

namespace sea
{
    void RenderingSystem::Init(TransformComponentManager *transformManager, RenderComponentManager *renderManager)
    {
        m_pTransformManager = transformManager;
        m_pRenderManager = renderManager;
    }

    void RenderingSystem::InitSkybox(const std::string &shaderProgramName,
                                     const std::string &front, const std::string &back,
                                     const std::string &left, const std::string &right,
                                     const std::string &top, const std::string &bottom)
    {
        m_skybox.Init(shaderProgramName,
                      front, back,
                      left, right,
                      top, bottom);
    }

    void RenderingSystem::VUpdate(float deltaTime)
    {
        m_skybox.Draw();
        // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Give shaders latest versions of view and projection matrices
        sea_math::Mat4 viewMatrix = m_camera.CreateViewMatrix();
        sea_math::Mat4 projectionMatrix = m_camera.CreateProjectionMatrix();
        for (unsigned int i = 0; i < m_registeredShaders.size(); ++i)
        {
            GLuint program = m_shaderManager.GetProgram(m_registeredShaders[i]);
            glUseProgram(program);

            GLint viewMatrixLocation =
                glGetUniformLocation(program, "viewMatrix");
            glUniformMatrix4fv(viewMatrixLocation,
                               1,
                               GL_FALSE,
                               (float *)&viewMatrix[0][0]
            );

            GLint projectionMatrixLocation =
                glGetUniformLocation(program, "projectionMatrix");
            glUniformMatrix4fv(projectionMatrixLocation,
                               1,
                               GL_FALSE,
                               (float*)&projectionMatrix[0][0]
            );

            glUseProgram(0);
        }

        // For each render component
        for (unsigned int i = 0; i < m_pRenderManager->VGetNumInstances(); ++i)
        {
            Instance instance = Instance::MakeInstance(i);
            bool isVisible = m_pRenderManager->GetVisFlag(instance);

            if (isVisible)
            {
                    RenderInstanceNew renderInstance =
                        m_pRenderManager->GetRenderInstance(instance);

                    Entity e = m_pRenderManager->VGetEntityForInstance(instance);

                    GLuint program = renderInstance.GetShaderProgram();
                    glUseProgram(program);

                    sea_math::Mat4 modelMatrix = sea_math::Mat4(1.0f);
                    sea_math::Vec3 scale = m_pTransformManager->GetScale(m_pTransformManager->VGetInstanceForEntity(e));
                    sea_math::Vec3 pos = m_pTransformManager->GetPosition(m_pTransformManager->VGetInstanceForEntity(e));
                    modelMatrix[0].x = scale.x;
                    modelMatrix[1].y = scale.y;
                    modelMatrix[2].z = scale.z;
                    modelMatrix[3].x = pos.x;
                    modelMatrix[3].y = pos.y;
                    modelMatrix[3].z = pos.z;

                    // Bind transform to shader
                    GLint modelMatrixLocation = glGetUniformLocation(program, "modelMatrix");

                    glUseProgram(program);
                    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, (float *)&modelMatrix[0][0]);

                    sea::MeshNew mesh = renderInstance.GetMesh();

                    glBindVertexArray(mesh.GetVertexArrayID());

                    for (unsigned int subMeshIndex = 0; subMeshIndex < mesh.GetNumSubMeshes(); ++subMeshIndex)
                    {
                        sea::SubMesh subMesh = mesh.GetSubMesh(subMeshIndex);
                        sea::Texture diffuseTexture =
                            renderInstance.GetDiffuseTextureForSubMesh(subMeshIndex);
                        sea::Texture lightmapTexture =
                            renderInstance.GetLightmapTextureForSubMesh(subMeshIndex);

                        GLuint diffuseLocation = glGetUniformLocation(program, "diffuseTexture");
                        glUniform1i(diffuseLocation, 0);

                        GLuint lightmapLocation = glGetUniformLocation(program, "lightmapTexture");
                        glUniform1i(lightmapLocation, 1);

                        // Bind diffuse texture
                        glActiveTexture(GL_TEXTURE0);
                        glBindTexture(GL_TEXTURE_2D, diffuseTexture.GetTextureID());

                        // Bind lightmap texture
                        glActiveTexture(GL_TEXTURE1);
                        glBindTexture(GL_TEXTURE_2D, lightmapTexture.GetTextureID());

                        glDrawElements(GL_TRIANGLES, subMesh.GetNumIndices(), GL_UNSIGNED_INT, (unsigned int *)NULL + subMesh.GetStartIndex());
                        glBindTexture(GL_TEXTURE_2D, 0);
                    }

                    glBindVertexArray(0);
                }

                glUseProgram(0);
            }
    }

    const Camera &RenderingSystem::GetCamera() const
    {
        return m_camera;
    }

    Camera &RenderingSystem::GetCamera()
    {
        return m_camera;
    }

    void RenderingSystem::RegisterShaderForCameraUpdate(const std::string &shaderProgramName)
    {
        m_registeredShaders.push_back(shaderProgramName);
    }
}
