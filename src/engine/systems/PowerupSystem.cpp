#include "PowerupSystem.h"
#include "engine/physics/Collision.h"

void sea::PowerupSystem::Init(TransformComponentManager * transManager, RenderComponentManager * rendManager, PowerupComponentManager * powerManager, const Character * const bruce, const TypeCallbackMap &typeCallbacks)
{
    m_pTransformManager = transManager;
    m_pRenderManager = rendManager;
    m_pPowerupManager = powerManager;
    m_bruce = bruce;
    m_typeCallbacks = typeCallbacks;
}

void sea::PowerupSystem::VUpdate(float deltaTime)
{

    Sphere bruceSphere;

    bruceSphere.setSpherePosition(m_bruce->GetLocation());
    bruceSphere.setSphereRadius(m_bruce->GetRadius());

    // std::cout << m_pPowerupManager->VGetNumInstances() << std::endl;

    for (unsigned int i = 0; i < m_pPowerupManager->VGetNumInstances(); ++i)
    {
        Instance powerUp = Instance::MakeInstance(i);
        float radiusX = m_pPowerupManager->GetRadiusX(powerUp);
        float radiusY = m_pPowerupManager->GetRadiusY(powerUp);
        float radiusZ = m_pPowerupManager->GetRadiusZ(powerUp);
        std::string powerUpType = m_pPowerupManager->GetPowerupType(powerUp);
        Entity tempEnt = m_pPowerupManager->VGetEntityForInstance(powerUp);

        Instance transConfig = m_pTransformManager->VGetInstanceForEntity(tempEnt);

        Vec3 tempPosition = m_pTransformManager->GetPosition(transConfig);

        // Sphere tempSphere;
        // tempSphere.setSpherePosition(tempPosition);
        // tempSphere.setSphereRadius(tempRadius);

        // bool colResult = tempSphere.isCollided(bruceSphere);
        bool colResult =
            sea_phys::AABBSphereCollision(tempPosition,
                                          radiusX, radiusY, radiusZ,
                                          bruceSphere.getSpherePosition(),
                                          bruceSphere.getSphereRadius());

        if(colResult == true)
        {
            Instance tempRend = m_pRenderManager->VGetInstanceForEntity(tempEnt);
            m_pRenderManager->SetVisFlagInstance(tempRend, false);

            // Check if powerup type exists
            TypeCallbackMap::const_iterator it = m_typeCallbacks.find(powerUpType);
            if (it == m_typeCallbacks.end())
            {
                std::cout << "Given powerup type '" << powerUpType << "' not found in callback map" << std::endl;
                assert(false);
            }
            else
            {
                // Call callback function for powerup
                it->second();
            }

            // Remove powerup instance when collided
            m_pPowerupManager->VRemoveInstance(powerUp);
            m_pTransformManager->VRemoveInstance(transConfig);
            m_pRenderManager->VRemoveInstance(tempRend);
        }
    }
}
