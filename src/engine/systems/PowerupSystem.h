#ifndef SEA_POWERUP_SYSTEM_H
#define SEA_POWERUP_SYSTEM_H

#include <map>
#include <string>
#include <functional>

#include "engine/interfaces/ISystem.h"
#include "engine/components/TransformComponentManager.h"
#include "engine/components/RenderComponentManager.h"
#include "engine/components/PowerupComponentManager.h"
#include "game/test/Character.h"
#include "math/Sphere.h"

namespace sea
{
    class PowerupSystem : public ISystem
    {
    public:
        /** Maps powerup type to callback function. */
        typedef std::map<std::string, std::function<void (void)>> TypeCallbackMap;

        /**
         * Initialize the powerup manager.
         *
         * The powerup manager requires access to the transform manager,
         * the render manager, the powerup manager, the character and a map
         * of powerup types to callback functions.
         *
         * @param transManager TransformComponentManager *, pointer to transform
         * component manager.
         * @param rendManager RenderComponentManager *, pointer to render
         * component manager.
         * @param powerManager PowerupComponentManager *, pointer to powerup
         * component manager.
         * @param bruce const Character *const bruce, character object.
         * @param typeCallbacks const TypeCallbackMap &, map of powerup type to
         * callback function. Provides the functionality of the powerups.
         */
        void Init(TransformComponentManager * transManager,
                  RenderComponentManager * rendManager,
                  PowerupComponentManager * powerManager,
                  const Character* const bruce,
                  const TypeCallbackMap &typeCallbacks);
        /**
         * Update method called by the engine each update tick.
         *
         * @param deltaTime float, time the last frame took to complete in
         * milliseconds.
         */
        virtual void VUpdate(float deltaTime);
    private:
        TransformComponentManager * m_pTransformManager;
        RenderComponentManager * m_pRenderManager;
        PowerupComponentManager * m_pPowerupManager;
        const Character * m_bruce;
        TypeCallbackMap m_typeCallbacks;
    };
}

#endif
