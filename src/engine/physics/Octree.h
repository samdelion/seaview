/**
 * Credit for much of the implementation goes to David Geier, found at:
 * https://geidav.wordpress.com/2014/08/18/advanced-octrees-2-node-representations/.
 *
 * @author David Geier
 * @modified Samuel Evans-Powell
 * @date 29/09/2015
 */
#ifndef SEA_OCTREE_H
#define SEA_OCTREE_H

#include <cstdint>
#include <vector>
#include <unordered_map>

#include "math/Vec3.h"

namespace sea
{
    /**
     * Hashed (Linear) Octree class.
     *
     * Location codes are used to access and organize octree elements.
     * Each of the 8 octants at each level of the octree are given a location
     * code from 0 (0b000) to 8 (0b111). The location codes are given in Morton
     * order,
     * i.e.: https://geidav.files.wordpress.com/2014/08/octant_numbers.png?w=800
     *
     * Location codes at each level are concatenated to form a location code
     * specific to that node. These location codes are used to access the
     * octree node from a hashmap. The root node is given a location code of 1.
     *
     * Further information can be found at the credit link given at the beginning
     * of this file.
     */
    template<typename T>
    class Octree
    {
    public:
        struct Object;
        struct OctreeNode;

        /**
         * Construct an octree with it's center at the origin
         * and with the given half-size (distance from center to
         * edge of octree AABB).
         *
         * @param origin sea_math::Vec3, origin of the octree.
         * @param halfSize float, distance from center to edge of octree.
         */
        Octree(sea_math::Vec3 origin, float halfSize);

        /**
         * Insert an object into the octree.
         *
         * @param object const Object &, object to insert into the octree.
         */
        void Insert(const Object &object);

        /**
         * Get a const pointer to the octree node identified by the
         * given location code.
         *
         * @param locationCode uint32_t, location code of the node to get.
         * @return OctreeNode *, pointer to node if it exists or nullptr
         * if node does not exist.
         */
        const OctreeNode *LookupNode(uint32_t locationCode) const;

        /**
         * Get a pointer to the octree node identified by the
         * given location code.
         *
         * @param locationCode uint32_t, location code of the node to get.
         * @return OctreeNode *, pointer to node if it exists or nullptr
         * if node does not exist.
         */
        OctreeNode *LookupNode(uint32_t locationCode);

        /**
         * Set the maximum number of objects that can exist in an octree node
         * before the octree recurses and eight new octree nodes are generated
         * to contain the objects.
         *
         * Defaults to 1 object per node.
         *
         * Octree will only continue recursing while tree depth is less than
         * MAX_TREE_DEPTH.
         *
         * @param maxObjectsPerNode unsigned int, max number of octree objects
         * per octree node.
         */
        void SetMaxObjectsPerNode(unsigned int maxObjectsPerNode);

        /**
         * Get the maximum number of objects that can exist in an octree node
         * before the octree recurses.
         *
         * @return unsigned int, maximum number of octree objects per node.
         */
        unsigned int GetMaxObjectsPerNode() const;

        /**
         * Get a list of Octree objects which intersect with a sphere with a
         * center point at centerSphere and radius of radiusSphere.
         *
         * @param centerSphere sea_math::Vec3, location of query sphere center.
         * @param radiusSphere float, radius of query sphere.
         */
        std::vector<Object> SphereOctreeQuery(const sea_math::Vec3 &centerSphere,
                                              float radiusSphere) const;
    private:
        /**
         * Find which of the given node's octants to insert the object into.
         * Based on the objects position relative to the octree node.
         *
         * @param parentNode const OctreeNode &, parent node,
         * where object is to be inserted.
         * @param object const Object &, octree object to be inserted.
         * @return uint32_t, locational code of the node which the object
         * should be inserted into. Does not guarantee that the node exists.
         */
        uint32_t GetNodeToInsertInto(const OctreeNode &parentNode, const Object &object) const;

        /**
         * Insert given octree object into given octree node.
         * Recursive method, will traverse all child nodes and find the correct
         * node to insert the object into.
         *
         * @pre Octree node must exist.
         *
         * @param node const pointer to OctreeNode, node to insert object into.
         * @param object const Object &, octree object to insert.
         */
        void InsertIntoNode(OctreeNode *const node, const Object &object);

        /**
         * Create a child octree node from the parent's data and the child's location
         * code.
         *
         * This method will initialize all properties of the child node to their
         * correct values relative to the parent. Child node must have a parent.
         *
         */
        OctreeNode DeriveChildFromParent(const OctreeNode *const parent, uint32_t childLocationCode) const;

        /**
         * Get the depth of the given node.
         *
         * A depth of 0 indicates the root node.
         *
         * @param node const OctreeNode *const, node to find depth of.
         * @return unsigned int, depth of node.
         */
        unsigned int GetNodeDepth(const OctreeNode *const node) const;

        /**
         * Given an octree node and a sphere, this method returns
         * (via the objectList parameter) a list of octree objects that intersect
         * with the given sphere.
         *
         * @param node const pointer to const Octree node, node to start
         * query at. Will recursively check all child nodes.
         * @param centerSphere sea_math::Vec3, center of the query sphere.
         * @param radiusSphere float, radius of the query sphere.
         * @param objectList const pointer to std::vector of Object, object
         * array to return results of query in.
         */
        void SphereOctreeQuery(const OctreeNode *const node,
                               const sea_math::Vec3 &centerSphere,
                               float radiusSphere,
                               std::vector<Object> *const objectList) const;

        /**
         * This value is related to the number of bits available in the location
         * code. The maximum depth from 32 bits is 10.
         */
        const unsigned int MAX_TREE_DEPTH = 10;
        unsigned int m_maxObjectsPerNode;
        std::unordered_map<uint32_t, OctreeNode> m_nodes;
    };

    /**
     * Octree object.
     *
     * Any object to be inserted into the octree.
     */
    template<typename T>
    struct Octree<T>::Object
    {
        Object(T _contents, sea_math::Vec3 _position, float _radius = 1.0f)
        : contents(_contents), position(_position), radius(_radius)
        {}

        Object()
        {}

        T contents;
        sea_math::Vec3 position;
        float radius;

        bool operator==(const Object &other) const
        {
            return (contents == other.contents &&
                    position == other.position);
        }
    };
}

#include "Octree.hpp"

#endif // SEA_OCTREE_H
