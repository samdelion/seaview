#ifndef SEA_COLLISION_H
#define SEA_COLLISION_H

#include "math/Vec3.h"

namespace sea_phys
{
    /**
     * Note: Touching is NOT considered collision.
     */
    bool SphereSphereCollision(sea_math::Vec3 centre1, float radius1,
                               sea_math::Vec3 centre2, float radius2);

    /**
     * Note: Touching is NOT considered collision.
     * Credit: James Arvo, in book:
     * A. Glassner, Graphics Gems. Academic Press, 1990, pp. 335-336.
     */
    bool AABBSphereCollision(sea_math::Vec3 centreAABB,
                             float radiusX, float radiusY, float radiusZ,
                             sea_math::Vec3 centreSphere,
                             float radiusSphere);

    /**
     * Note: Touching IS considered intersection.
     * Credit: http://www.paulnettle.com/pub/FluidStudios/CollisionDetection/Fluid_Studios_Generic_Collision_Detection_for_Games_Using_Ellipsoids.pdf
     * Credit: http://www.lighthouse3d.com/tutorials/maths/ray-sphere-intersection/
     *
     * @param rayDir sea_math::Vec3, NORMALIZED ray direction vector.
     */
    bool RaySphereClosestIntersection(sea_math::Vec3 centreSphere,
                                      float radiusSphere,
                                      sea_math::Vec3 rayOrigin,
                                      sea_math::Vec3 rayDir,
                                      sea_math::Vec3 *closestIntersection);

    /**
     * Assumes rayDir and planeNormal are normalized vectors.
     *
     * In the case of infinite intersection points (ray parallel to and on plane)
     * will return ray origin as intersection point.
     *
     * If the ray and the plane are nearly parallel
     * (i.e. their intersection point is very far away [close to inf]),
     * function will NOT return an intersection.
     *
     * Credit: http://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection
     */
    bool RayPlaneIntersection(sea_math::Vec3 rayOrigin, sea_math::Vec3 rayDir,
                              sea_math::Vec3 planeOrigin, sea_math::Vec3 planeNormal,
                              sea_math::Vec3 *const intersectionPoint);

    /**
     * Function requires point to be in the plane of the triangle
     *
     * Credit: William Sherif,
     * http://math.stackexchange.com/questions/4322/check-whether-a-point-is-within-a-3d-triangle
     */
    bool IsPointInsideTriangle(sea_math::Vec3 point,
                               sea_math::Vec3 a,
                               sea_math::Vec3 b,
                               sea_math::Vec3 c);

    /**
     * Find the point within the given triangle that is closest
     * to the given point.
     *
     * Given point must not lie within triangle.
     *
     * Credit: Paul Nettle,
     * http://www.paulnettle.com/pub/FluidStudios/CollisionDetection/Fluid_Studios_Generic_Collision_Detection_for_Games_Using_Ellipsoids.pdf
     */
    sea_math::Vec3 ClosestPointWithinTriangleToPoint(sea_math::Vec3 point,
                                                     sea_math::Vec3 a,
                                                     sea_math::Vec3 b,
                                                     sea_math::Vec3 c);

    /**
     * Find the point on the given line that is closest to the given point.
     *
     * Credit: Paul Nettle,
     * http://www.paulnettle.com/pub/FluidStudios/CollisionDetection/Fluid_Studios_Generic_Collision_Detection_for_Games_Using_Ellipsoids.pdf
     */
    sea_math::Vec3 ClosestPointOnLineToPoint(sea_math::Vec3 point,
                                             sea_math::Vec3 a,
                                             sea_math::Vec3 b);
}

#endif // SEA_COLLISION_H
