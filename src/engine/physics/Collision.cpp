#include <iostream>

#include <cmath>

#include "Collision.h"

namespace sea_phys
{
    bool SphereSphereCollision(sea_math::Vec3 centre1, float radius1,
                               sea_math::Vec3 centre2, float radius2)
    {
        sea_math::Vec3 centre2ToCentre1 = centre2 - centre1;

        return (sea_math::Magnitude(centre2ToCentre1) < (radius1 + radius2));
    }

    bool AABBSphereCollision(sea_math::Vec3 centreAABB,
                             float radiusX, float radiusY, float radiusZ,
                             sea_math::Vec3 centreSphere,
                             float radiusSphere)
    {
        bool result = false;

        float minSquaredDist = 0.0f;
        sea_math::Vec3 AABBmin =
            centreAABB - sea_math::Vec3(radiusX, radiusY, radiusZ);
        sea_math::Vec3 AABBmax =
            centreAABB + sea_math::Vec3(radiusX, radiusY, radiusZ);

        if (centreSphere.x > AABBmax.x)
        {
            minSquaredDist +=
                (centreSphere.x - AABBmax.x) * (centreSphere.x - AABBmax.x);
        }
        else if (centreSphere.x < AABBmin.x)
        {
            minSquaredDist +=
                (centreSphere.x - AABBmin.x) * (centreSphere.x - AABBmin.x);
        }

        if (centreSphere.y > AABBmax.y)
        {
            minSquaredDist +=
                (centreSphere.y - AABBmax.y) * (centreSphere.y - AABBmax.y);
        }
        else if (centreSphere.y < AABBmin.y)
        {
            minSquaredDist +=
                (centreSphere.y - AABBmin.y) * (centreSphere.y - AABBmin.y);
        }

        if (centreSphere.z > AABBmax.z)
        {
            minSquaredDist +=
                (centreSphere.z - AABBmax.z) * (centreSphere.z - AABBmax.z);
        }
        else if (centreSphere.z < AABBmin.z)
        {
            minSquaredDist +=
                (centreSphere.z - AABBmin.z) * (centreSphere.z - AABBmin.z);
        }

        if (minSquaredDist < (radiusSphere * radiusSphere))
        {
            result = true;
        }

        return result;
    }

    bool RaySphereClosestIntersection(sea_math::Vec3 centreSphere,
                                      float radiusSphere,
                                      sea_math::Vec3 rayOrigin,
                                      sea_math::Vec3 rayDir,
                                      sea_math::Vec3 *closestIntersection)
    {
        sea_math::Vec3 originToSphere = centreSphere - rayOrigin;

        // Projection of origin to sphere centre vector onto ray direction
        float projCentreRay = sea_math::Dot(originToSphere, rayDir);

        // Sphere behind ray origin
        // Will only intersect if ray origin is inside sphere
        if (projCentreRay < 0)
        {
            float distFromRayToSphere = sea_math::Magnitude(originToSphere);
            // Ray origin not inside or touching sphere
            if (distFromRayToSphere > radiusSphere)
            {
                // No possible intersection
                return false;
            }
            // Ray origin touching sphere
            else if (distFromRayToSphere == radiusSphere)
            {
                // intersection at ray origin
                if (closestIntersection != nullptr)
                {
                    *closestIntersection = rayOrigin;
                }

                return true;
            }
            // Ray origin inside sphere
            else
            {
                // Closest point between sphere center and ray
                sea_math::Vec3 projCentreRayVector = projCentreRay * rayDir;

                // Magnitude of shortest path from sphere centre to ray
                float magShortestPath = sea_math::Magnitude(
                    projCentreRayVector - centreSphere
                );

                // Distance to first intersection (along ray)
                // from closest point of sphere centre to ray.
                float distToFirst = sqrt(radiusSphere*radiusSphere -
                                         magShortestPath*magShortestPath);

                // Distance from ray to closest intersection
                float distRayToClosest =
                    distToFirst - sea_math::Magnitude(projCentreRayVector - rayOrigin);

                if (closestIntersection != nullptr)
                {
                    *closestIntersection = rayOrigin + distRayToClosest * rayDir;
                }

                return true;
            }
        }
        // Sphere in front of/on top of ray origin
        else
        {
            // Closest point between sphere center and ray
            sea_math::Vec3 projCentreRayVector = projCentreRay * rayDir;

            // Magnitude of shortest path from sphere centre to ray
            float magShortestPath = sea_math::Magnitude(
                projCentreRayVector - originToSphere
                );
            if (magShortestPath > radiusSphere)
            {
                // No intersection
                return false;
            }
            else
            {
                // Distance to first intersection (along ray)
                // from closest point of sphere centre to ray.
                float distToFirst = sqrt(radiusSphere*radiusSphere -
                                         magShortestPath*magShortestPath);

                // Distance from ray to closestIntersection
                // float distRayToClosest = -1.0f;
                // Ray origin outside sphere
                if (sea_math::Magnitude(originToSphere) > radiusSphere)
                {
                    if (closestIntersection != nullptr)
                    {
                        *closestIntersection = rayOrigin +
                            projCentreRayVector - distToFirst * rayDir;
                    }
                    // distRayToClosest =
                    //     sea_math::Magnitude(projCentreRayVector - rayOrigin) - distToFirst;
                }
                // Ray origin inside sphere
                else
                {
                    if (closestIntersection != nullptr)
                    {
                        *closestIntersection = rayOrigin +
                            projCentreRayVector + distToFirst * rayDir;
                    }
                }

                return true;
            }
        }
    }

    bool RayPlaneIntersection(sea_math::Vec3 rayOrigin, sea_math::Vec3 rayDir,
                              sea_math::Vec3 planeOrigin, sea_math::Vec3 planeNormal,
                              sea_math::Vec3 *const intersectionPoint)
    {
        bool result = false;

        float numer = -sea_math::Dot(rayOrigin - planeOrigin, planeNormal);
        float denom = sea_math::Dot(rayDir, planeNormal);
        // If ray is parallel, denom = 0
        // Either ray is always intersecting plane (ray origin on plane) or
        // never intersecting plane (ray origin away from plane)
        if (denom == 0)
        {
            // Is ray on plane?
            if (sea_math::Dot((rayOrigin - planeOrigin), planeNormal) == 0)
            {
                result = true;
                *intersectionPoint = rayOrigin;
            }
            else
            {
                result = false;
            }
        }
        else
        {
            // Prevent from getting hits on intersection points large distances
            // away
            if (fabs(denom) > 1e-6)
            {
                float k = numer / (float)denom;
                result = true;
                *intersectionPoint = rayOrigin + k * rayDir;
            }
            else
            {
                result = false;
            }
        }

        return result;
    }

    bool IsPointInsideTriangle(sea_math::Vec3 point,
                               sea_math::Vec3 a,
                               sea_math::Vec3 b,
                               sea_math::Vec3 c)
    {
        bool result = false;

        float areaTri =
            0.5f * sea_math::Magnitude(b - a) * sea_math::Magnitude(c - a);

        // Compute barycentric coordinates of point
        float alpha =
            sea_math::Magnitude(b - point) * sea_math::Magnitude(c - point) / (2*areaTri);
        float beta =
            sea_math::Magnitude(c - point) * sea_math::Magnitude(a - point) / (2*areaTri);
        float gamma = 1 - alpha - beta;

        if (alpha >= 0 && alpha <= 1 &&
            beta >= 0 && beta <= 1 &&
            gamma >=0 && gamma <= 1)
        {
            result = true;
        }

        return result;
    }

    sea_math::Vec3 ClosestPointWithinTriangleToPoint(sea_math::Vec3 point,
                                                     sea_math::Vec3 a,
                                                     sea_math::Vec3 b,
                                                     sea_math::Vec3 c)
    {
        // Find closest point on each edge of the triangle
        sea_math::Vec3 Rab = ClosestPointOnLineToPoint(point, a, b);
        sea_math::Vec3 Rbc = ClosestPointOnLineToPoint(point, b, c);
        sea_math::Vec3 Rca = ClosestPointOnLineToPoint(point, c, a);

        float distToRab = sea_math::Magnitude(Rab - point);
        float distToRbc = sea_math::Magnitude(Rbc - point);
        float distToRca = sea_math::Magnitude(Rca - point);

        // Find closest of potential points
        float minDist = fmin(fmin(distToRab, distToRbc), distToRca);

        if (minDist == distToRab)
        {
            return Rab;
        }
        else if (minDist == distToRbc)
        {
            return Rbc;
        }
        else
        {
            return Rca;
        }
    }

    sea_math::Vec3 ClosestPointOnLineToPoint(sea_math::Vec3 point,
                                             sea_math::Vec3 a,
                                             sea_math::Vec3 b)
    {
        // Project vector from one vertex of line to point onto normalized
        // vector from one vertex to another.
        // This gives the distance along line AB where the
        // closest point is found.
        sea_math::Vec3 vertexToPoint = point - a;
        sea_math::Vec3 lineDir = sea_math::Normalize(b - a);
        float lineLength = sea_math::Magnitude(b - a);
        float dist = sea_math::Dot(vertexToPoint, lineDir);

        // Check if dist is greater than line length (closest point is far vertex)
        // or less than 0 (closest point is near vertex).
        if (dist < 0) return a;
        if (dist > lineLength) return b;

        // Else return point that lies between a and b
        return (a + (dist * lineDir));
    }
}
