/**
 * Credit for much of the implementation goes to David Geier, found at:
 * https://geidav.wordpress.com/2014/08/18/advanced-octrees-2-node-representations/.
 *
 * @author David Geier
 * @modified Samuel Evans-Powell
 * @date 29/09/2015
 */
#include <cassert>
#include <vector>
#include <bitset>

#include "engine/physics/Collision.h"

namespace sea
{
    /**
     * Octree node.
     */
    template<typename T>
    struct Octree<T>::OctreeNode
    {
        /**
         * Location code of this node.
         *
         * Location code is made up of a sentinel bit and groups of 3 bits
         * uniquely identifying this node and each of it's parents.
         */
        uint32_t locationCode;
        /** Bit mask used to record which children exist */
        uint8_t childExists;
        /** Center of the AABB containing the octant. */
        sea_math::Vec3 center;
        /** Distace from the center of the AABB to any side of the AABB */
        float halfSize;
        /**
         * Octree objects stored in the node.
         */
        std::vector<Object> objects;
    };

    template<typename T>
    Octree<T>::Octree(sea_math::Vec3 origin, float halfSize)
    {
        // Create octree root node
        m_nodes[1] = OctreeNode();

        m_nodes[1].locationCode = 1;
        m_nodes[1].childExists = 0;
        m_nodes[1].center = origin;
        m_nodes[1].halfSize = halfSize;

        m_maxObjectsPerNode = 1;
    }

    template<typename T>
    uint32_t Octree<T>::GetNodeToInsertInto(const OctreeNode &parentNode, const Object &object) const
    {
        // Location code relative to parent
        uint32_t relativeLocation = 0;

        if (object.position.x > parentNode.center.x)
        {
            // relativeLocation |= 0b001;
            relativeLocation |= 1;
        }
        if (object.position.y > parentNode.center.y)
        {
            // relativeLocation |= 0b100;
            relativeLocation |= 4;
        }
        // Z is flipped because OpenGL world z is away from camera
        if (object.position.z < parentNode.center.z)
        {
            // relativeLocation |= 0b010;
            relativeLocation |= 2;
        }

        // If object is straddling any boundary, object should
        // be inserted in parent node.
        if (object.position.x == parentNode.center.x ||
            object.position.y == parentNode.center.y ||
            object.position.z == parentNode.center.z)
        {
            return parentNode.locationCode;
        }

        return ((parentNode.locationCode << 3) + relativeLocation);
    }

    template<typename T>
    const typename Octree<T>::OctreeNode *Octree<T>::LookupNode(uint32_t locationCode) const
    {
        const auto iter = m_nodes.find(locationCode);
        return (iter == m_nodes.end() ? nullptr : &(iter->second));
    }

    template<typename T>
    typename Octree<T>::OctreeNode *Octree<T>::LookupNode(uint32_t locationCode)
    {
        auto iter = m_nodes.find(locationCode);
        return (iter == m_nodes.end() ? nullptr : &(iter->second));
    }

    template<typename T>
    void Octree<T>::SetMaxObjectsPerNode(unsigned int maxObjectsPerNode)
    {
        m_maxObjectsPerNode = maxObjectsPerNode;
    }

    template<typename T>
    unsigned int Octree<T>::GetMaxObjectsPerNode() const
    {
        return m_maxObjectsPerNode;
    }

    template<typename T>
    void Octree<T>::Insert(const Object &object)
    {
        // Call recursive insert method, specifying root node as starting point
        InsertIntoNode(LookupNode(1), object);
    }

    template<typename T>
    void Octree<T>::InsertIntoNode(OctreeNode *const node, const Object &object)
    {
        assert(node);

        // Insert object into this node
        node->objects.push_back(object);

        // Does this node have any children the object could be inserted into?
        if(node->childExists)
        {
            // Remove object from this node
            Object objectToPushDown = node->objects.back();
            // Get node to insert into
            uint32_t locationCode = GetNodeToInsertInto(*node, objectToPushDown);

            // If we're not inserting into this node
            if (locationCode != node->locationCode)
            {
                node->objects.pop_back();

                OctreeNode *childNode = LookupNode(locationCode);

                // Create child if it does not already exist
                if (!childNode)
                {
                    m_nodes[locationCode] = DeriveChildFromParent(node, locationCode);
                    // Parent is always one level "shallower" than child, therefore
                    // shift 3 to left before subtracting to get relative location code
                    uint32_t relativeLocationCode = locationCode - (node->locationCode << 3);
                    node->childExists |= (1 << relativeLocationCode);

                    childNode = LookupNode(locationCode);
                }

                // Recursively insert object into child node
                InsertIntoNode(childNode, objectToPushDown);
            }
        }
       // No children
        else
        {
            // Are there too many objects in this node?
            // Are we less than the maximum depth for the tree?
            if (node->objects.size() > m_maxObjectsPerNode && GetNodeDepth(node) < MAX_TREE_DEPTH)
            {
                const unsigned int numObjectsInNode = node->objects.size();
                for (unsigned int i = 0; i < numObjectsInNode; ++i)
                {
                    Object objectToPushDown = node->objects.back();

                    // Get node to insert into
                    uint32_t locationCode = GetNodeToInsertInto(*node, objectToPushDown);

                    // If not inserting into this node
                    if (locationCode != node->locationCode)
                    {
                        // Remove object from this node
                        node->objects.pop_back();

                        // Parent is always one level "shallower" than child, therefore
                        // shift 3 to left before subtracting to get relative location code
                        uint32_t relativeLocationCode = locationCode - (node->locationCode << 3);

                        // OctreeNode *childNode = LookupNode(locationCode);
                        OctreeNode *childNode = nullptr;


                        // Create child if it does not already exist
                        if (!((1 << relativeLocationCode) & node->childExists))
                        {
                            m_nodes[locationCode] = DeriveChildFromParent(node, locationCode);
                            node->childExists |= (1 << relativeLocationCode);
                            childNode = LookupNode(locationCode);
                        }
                        else
                        {
                            childNode = LookupNode(locationCode);
                        }

                        // Recursively insert object into child node
                        InsertIntoNode(childNode, objectToPushDown);
                    }
                }
            }
        }
    }

    template<typename T>
    typename Octree<T>::OctreeNode Octree<T>::DeriveChildFromParent(const OctreeNode *const parent, uint32_t childLocationCode) const
    {
        // Child must have parent
        assert(parent && LookupNode(childLocationCode >> 3));

        uint32_t relativeLocationCode = childLocationCode - (parent->locationCode << 3);

        OctreeNode child = OctreeNode();
        child.locationCode = childLocationCode;
        child.halfSize = parent->halfSize / 2.0f;
        child.center = sea_math::Vec3(0.0f, 0.0f, 0.0f);
        child.childExists = 0;

        // Is child's center x greater than paren't center x?
        // if (relativeLocationCode & (0b001))
        if (relativeLocationCode & (1))
        {
            child.center.x = parent->center.x + child.halfSize;
        }
        else
        {
            child.center.x = parent->center.x - child.halfSize;
        }

        // if (relativeLocationCode & (0b100))
        if (relativeLocationCode & (4))
        {
            child.center.y = parent->center.y + child.halfSize;
        }
        else
        {
            child.center.y = parent->center.y - child.halfSize;
        }

        // Flipped for z
        // if (relativeLocationCode & (0b010))
        if (relativeLocationCode & (2))
        {
            child.center.z = parent->center.z - child.halfSize;
        }
        else
        {
            child.center.z = parent->center.z + child.halfSize;
        }

        return (child);
    }

    template<typename T>
    unsigned int Octree<T>::GetNodeDepth(const OctreeNode *const node) const
    {
        // At least sentinel bit must be set
        assert(node->locationCode);

        unsigned int depth = 0;
        for (uint32_t locCode = node->locationCode; locCode != 1; locCode >>= 3)
        {
            ++depth;
        }

        return depth;
    }

    template<typename T>
    std::vector<typename Octree<T>::Object> Octree<T>::SphereOctreeQuery(
        const sea_math::Vec3 &centerSphere,
        float radiusSphere) const
    {
        std::vector<Object> objectList;

        // Start query at root node
        SphereOctreeQuery(LookupNode(1), centerSphere, radiusSphere, &objectList);

        return objectList;
    }

    template<typename T>
    void Octree<T>::SphereOctreeQuery(const OctreeNode *const node,
                                      const sea_math::Vec3 &centerSphere,
                                      float radiusSphere,
                                      std::vector<typename Octree<T>::Object> *const objectList) const
    {
        assert(node);
        assert(objectList);

        bool doesIntersect =
            sea_phys::AABBSphereCollision(node->center,
                                          node->halfSize, node->halfSize, node->halfSize,
                                          centerSphere,
                                          radiusSphere);

        if (doesIntersect)
        {
            for (unsigned int i = 0; i < node->objects.size(); ++i)
            {
                Object object = node->objects[i];

                if (sea_phys::SphereSphereCollision(centerSphere, radiusSphere,
                                                    object.position, object.radius))
                {
                    objectList->push_back(object);
                }
            }

            uint32_t locationCode = node->locationCode;

            for (unsigned int i = 0; i < 8; ++i)
            {
                if ((1 << i) & node->childExists)
                {
                    SphereOctreeQuery(LookupNode((locationCode << 3) + i),
                                      centerSphere, radiusSphere,
                                      objectList);
                }
            }
        }
    }
}
