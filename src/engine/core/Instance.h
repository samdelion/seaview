/**
 *  This code was only slightly modified from this source:
 *  http://bitsquid.blogspot.com.au/2014/08/building-data-oriented-entity-system.html?m=1
 *
 *  @author Niklas
 *  @author Samuel Evans-Powell (modified)
 *  @date   23/08/2015
 */
#ifndef SEA_INSTANCE_H
#define SEA_INSTANCE_H

namespace sea
{
    /**
     *  The index of an Entity does not map directly to an index
     *  into the array of components. Instead, a layer of indirection
     *  is used.
     *
     *  This instance type essentially represents the real index into
     *  the component array. It is a way of referring to a specific
     *  component instance.
     */
    struct Instance
    {
        /**
         *  Convenience method, create an Instance from an array index.
         *
         *  @param  index   unsigned int, index into array.
         *  @return         Instance, Instance handle.
         */
        static Instance MakeInstance(int index)
        {
            Instance instance;
            instance.index = index;

            return instance;
        }

        /**
         *  Equivalence operator.
         *
         *  Compare two Instances and return TRUE if they are equal or FALSE otherwise.
         *
         *  @param  other   const Instance reference, other Instance to compare to this Instance.
         *  @return         bool, TRUE if two Instances are equal, FALSE otherwise.
         */
        bool operator==(const Instance &other) const
        {
            return (index == other.index);
        }

        int index;
    };
}

#endif  // SEA_INSTANCE_H
