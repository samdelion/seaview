#ifndef SEA_DEFINES_H
#define SEA_DEFINES_H

// Expands to the users asset directory (set in top-level CMakeLists.txt)
#define PATH_TO_ASSETS CMAKE_ASSET_DIR
// Convenience define for Shay's assets (they are a mess so I wanted to keep them seperate)
#define PATH_TO_ASSETS_SHAY PATH_TO_ASSETS"shay/"

#endif  // SEA_DEFINES_H
