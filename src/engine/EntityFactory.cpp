#include <iostream>
#include <sstream>

#include <GL/glew.h>

#include "engine/Defines.h"
#include "engine/util/ObjFile.h"
#include "engine/util/BMPFile.h"
#include "engine/EntityFactory.h"
#include "engine/util/ShaderManager.h"
#include "engine/render/Texture.h"
#include "engine/util/MeshManager.h"
#include "engine/util/TextureManager.h"

namespace sea
{
    Entity EntityFactory::CreateEntity()
    {
        return m_entityManager.Create();
    }

    bool EntityFactory::AddComponent(Entity e,
                                     const std::string &componentType,
                                     const std::map<std::string, std::vector<std::string>> &componentInfo)
    {
        bool result = false;

        if (componentType == "TransformComponent")
        {
            // Create component
            Instance i = m_transformComponentManager.VCreateComponentForEntity(e);

            // Read position
            std::stringstream inputLine = std::stringstream(componentInfo.at("position")[0]);
            float x, y, z;

            inputLine >> x;
            inputLine >> y;
            inputLine >> z;

            // Read scale
            inputLine = std::stringstream(componentInfo.at("scale")[0]);

            float xScale, yScale, zScale;

            inputLine >> xScale;
            inputLine >> yScale;
            inputLine >> zScale;

            m_transformComponentManager.SetPosition(i, sea_math::Vec3(x, y, z));
            m_transformComponentManager.SetScale(i, sea_math::Vec3(xScale, yScale, zScale));
        }
        else if (componentType == "RenderComponent")
        {
            ShaderManager shaderManager;

            Instance i = m_renderComponentManager.VCreateComponentForEntity(e);

            RenderInstanceNew renderInstance;

            // Construct mesh path
            std::stringstream meshPath;
            meshPath << PATH_TO_ASSETS << componentInfo.at("mesh")[0];

            // Get Mesh
            MeshManager meshManager;
            MeshNew mesh = meshManager.GetMesh(meshPath.str());

            renderInstance.AddMesh(mesh);

            TextureManager textureManager;
            // Add all textures to the render instance
            const std::map<std::string, std::vector<std::string>>::const_iterator itTexture =
                componentInfo.find("texture");
            if (itTexture != componentInfo.end())
            {
                for (unsigned int i = 0; i < componentInfo.at("texture").size(); ++i)
                {
                    std::stringstream texturePath;
                    texturePath << PATH_TO_ASSETS << componentInfo.at("texture")[i];

                    Texture texture = textureManager.GetTexture(texturePath.str());
                    renderInstance.AddTexture(componentInfo.at("texture")[i], texture);
                }
            }

            // Map textures to sub meshes
            // Parse texture map data
            // If no texture map info specified
            const std::map<std::string, std::vector<std::string>>::const_iterator it =
                componentInfo.find("textureMap");
            if (it == componentInfo.end())
            {
                // Assume that we'll map first texture to
                // first submesh.
                // std::string subMeshName = objFile.GetSubMeshNames()[0];
                // unsigned int subMeshIndex = newMesh.GetSubMeshIndex(suMeshName);
                std::string textureToUse;

                const std::map<std::string, std::vector<std::string>>::const_iterator iter =
                    componentInfo.find("texture");
                if (iter != componentInfo.end())
                {
                    textureToUse = componentInfo.at("texture")[0];
                    renderInstance.MapDiffuseTextureToSubMesh(textureToUse, 0);
                }
            }
            else
            {
                for (unsigned int i = 0; i < componentInfo.at("textureMap").size(); ++i)
                {
                    std::stringstream inputLine =
                        std::stringstream(componentInfo.at("textureMap")[i]);
                    std::string subMeshName, textureToUse;
                    inputLine >> subMeshName >> std::ws;
                    inputLine >> textureToUse;

                    unsigned int subMeshIndex = mesh.GetSubMeshIndex(subMeshName);
                    renderInstance.MapDiffuseTextureToSubMesh(textureToUse, subMeshIndex);
                }

            }

            const std::map<std::string, std::vector<std::string>>::const_iterator itLightmap =
                componentInfo.find("lightmapTexture");
            if (itLightmap != componentInfo.end())
            {
                for (unsigned int i = 0; i < componentInfo.at("lightmapTexture").size(); ++i)
                {
                    std::stringstream inputLine =
                        std::stringstream(componentInfo.at("lightmapTexture")[i]);
                    std::string subMeshName, textureToUse;
                    inputLine >> subMeshName >> std::ws;
                    inputLine >> textureToUse;

                    unsigned int subMeshIndex = mesh.GetSubMeshIndex(subMeshName);
                    renderInstance.MapLightmapTextureToSubMesh(textureToUse, subMeshIndex);
                }
            }

            renderInstance.SetShaderProgram(
                shaderManager.GetProgram(componentInfo.at("shader")[0])
            );
            m_renderComponentManager.SetRenderInstance(i, renderInstance);
        }
        else if (componentType == "LevelDetailsComponent")
        {
            //Create instance of component
            Instance i = m_levelDetailsComponentManager.VCreateComponentForEntity(e);

            //Set level information
            std::string time = componentInfo.at("time")[0];
            std::string minY = componentInfo.at("minY")[0];

            m_levelDetailsComponentManager.SetLevelTime(i, time);
            m_levelDetailsComponentManager.SetMinY(i, minY);
        }
        else if (componentType == "PowerupComponent")
        {
            Instance i = m_powerupComponentManager.VCreateComponentForEntity(e);

            std::stringstream inputLine =
                std::stringstream(componentInfo.at("radiusX")[0]);
            float radiusX;
            inputLine >> radiusX;

            inputLine =
                std::stringstream(componentInfo.at("radiusY")[0]);
            float radiusY;
            inputLine >> radiusY;

            inputLine =
                std::stringstream(componentInfo.at("radiusZ")[0]);
            float radiusZ;
            inputLine >> radiusZ;

            std::string type = componentInfo.at("type")[0];

            m_powerupComponentManager.SetRadiusX(i, radiusX);
            m_powerupComponentManager.SetRadiusY(i, radiusY);
            m_powerupComponentManager.SetRadiusZ(i, radiusZ);
            m_powerupComponentManager.SetPowerupType(i, type);
        }
        else
        {
            std::cout << "Error: Component " << componentType << " is not a valid component type." << std::endl;
            std::cout << "Program Exiting." << std::endl;
            exit(1);
        }

        return result;
    }

    TransformComponentManager *const EntityFactory::GetTransformComponentManager()
    {
        return &m_transformComponentManager;
    }

    RenderComponentManager *const EntityFactory::GetRenderComponentManager()
    {
        return &m_renderComponentManager;
    }

    LevelDetailsComponentManager *const EntityFactory::GetLevelDetailsComponentManager()
    {
        return &m_levelDetailsComponentManager;
    }

    PowerupComponentManager *const EntityFactory::GetPowerupComponentManager()
    {
        return &m_powerupComponentManager;
    }
}
