#include <cassert>

#include "engine/components/TextureComponentManager.h"

namespace sea
{
    TextureComponentManager::TextureComponentManager()
    {
    }

    TextureComponentManager::~TextureComponentManager()
    {
    }

    unsigned int TextureComponentManager::VGetNumInstances() const
    {
        // Arrays should be the same size, so just return size
        // of one of the arrays to get the number of active
        // instances.
        return m_data.entity.size();
    }

    Instance TextureComponentManager::VCreateComponentForEntity(Entity entity)
    {
        // Index to new component instance will be put into
        unsigned int newIndex = m_data.entity.size();

        m_data.entity.push_back(entity);
        m_data.textureID.push_back(0);

        // Put new entry into the map (mapping index array to Entity)
        m_map[entity.id] = newIndex;

        // Return instance to caller
        return Instance::MakeInstance(newIndex);
    }

    Instance TextureComponentManager::VGetInstanceForEntity(Entity entity) const
    {
        int index = -1;
        EntityMap::const_iterator it = m_map.find(entity.id);

        if (it != m_map.end())
        {
            index = it->second;
        }

        return Instance::MakeInstance(index);
    }

    Entity TextureComponentManager::VGetEntityForInstance(Instance i) const
    {
        Entity e;

        const int index = i.index;

        if (index < VGetNumInstances() && index >= 0)
        {
            e = m_data.entity[index];
        }

        return e;
    }

    bool TextureComponentManager::VRemoveInstance(Instance i)
    {
        bool result = false;

        // Keep data array tightly packed by swapping last element
        // with element we want to remove and reducing the number
        // of active instances.
        const int index = i.index;
        const unsigned int lastIndex = VGetNumInstances() - 1;

        // Make sure we are trying to delete a valid instance
        if (index < VGetNumInstances() && index >= 0)
        {
            // Get the Entity at the index to destroy
            Entity entityToDestroy = m_data.entity[index];
            // Get the entity at the end of the array
            Entity lastEntity = m_data.entity[lastIndex];

            // Move last entity's data
            m_data.entity[index] = m_data.entity[lastIndex];
            m_data.textureID[index] = m_data.textureID[lastIndex];

            // Update map entry for the swapped entity
            m_map[lastEntity.id] = index;
            // Remove the map entry for the destroyed entity
            m_map.erase(entityToDestroy.id);

            // Destroy component at end of array
            m_data.entity.pop_back();
            m_data.textureID.pop_back();

            result = true;
        }

        return result;
    }

    unsigned int TextureComponentManager::GetTextureID(Instance i) const
    {
        assert(i.index < VGetNumInstances() && i.index >= 0 &&
            "TextureComponentManager: Tried to get texture ID of invalid instance");

        return m_data.textureID[i.index];
    }

    bool TextureComponentManager::SetTextureID(Instance i, unsigned int textureID)
    {
        bool result = false;

        // Make sure we're trying to set a valid instance
        if (i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.textureID[i.index] = textureID;
            result = true;
        }

        return result;
    }
}
