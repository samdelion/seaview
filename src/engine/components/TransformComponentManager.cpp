#include <cassert>

/**
 *  This code was only slightly modified from this source:
 *  http://bitsquid.blogspot.com.au/2014/08/building-data-oriented-entity-system.html?m=1
 *
 *  @author Niklas
 *  @author Samuel Evans-Powell (modified)
 *  @date   16/09/2015
 */
#include <cstdlib>
#include <iostream>

#include "engine/components/TransformComponentManager.h"

namespace sea
{
    TransformComponentManager::TransformComponentManager()
    {
        m_data.instancesActive = 0;
        m_data.instancesAllocated = 0;
        m_data.buffer = NULL;
        m_data.entity = NULL;
        m_data.position = NULL;
        m_data.orientation = NULL;
        m_data.scale = NULL;
    }

    TransformComponentManager::~TransformComponentManager()
    {
        if (m_data.buffer != NULL)
        {
            free(m_data.buffer);
        }
    }

    unsigned int TransformComponentManager::VGetNumInstances() const
    {
        return m_data.instancesActive;
    }

    Instance TransformComponentManager::VCreateComponentForEntity(Entity entity)
    {
        // Do we have enough room to hold another component?
        if (m_data.instancesActive >= m_data.instancesAllocated)
        {
            // No room for another component, create room
            const unsigned int numInstancesToAllocate = m_data.instancesAllocated == 0 ? 2 : m_data.instancesAllocated * 2;

            Allocate(numInstancesToAllocate);
        }

        // Index to put new component instance into
        unsigned int newIndex = m_data.instancesActive;

        // Default values
        m_data.entity[newIndex] = entity;
        m_data.position[newIndex] = sea_math::Vec3();
        m_data.orientation[newIndex] = sea_math::Quat();
        m_data.scale[newIndex] = sea_math::Vec3();

        // Put new entry into the map (mapping index array to Entity)
        m_map[entity.id] = newIndex;

        // Make sure we keep track of number of active instances
        ++m_data.instancesActive;

        // Return instance to caller
        return Instance::MakeInstance(newIndex);
    }

    Instance TransformComponentManager::VGetInstanceForEntity(Entity entity) const
    {
        int index = -1;
        EntityMap::const_iterator it = m_map.find(entity.id);

        if (it != m_map.end())
        {
            index = it->second;
        }

        return Instance::MakeInstance(index);
    }

    Entity TransformComponentManager::VGetEntityForInstance(Instance i) const
    {
        Entity e;

        const int index = i.index;

        if (index < VGetNumInstances() && index >= 0)
        {
            e = m_data.entity[index];
        }

        return e;
    }

    bool TransformComponentManager::VRemoveInstance(Instance i)
    {
        bool result = false;

        // Keep data array tightly packed by swapping last element
        // with element we want to remove and reducing the number
        // of active instances.
        const int index = i.index;
        const unsigned int lastIndex = m_data.instancesActive - 1;

        if (index < m_data.instancesActive && index >= 0)
        {
            // Get the Entity at the index to destroy
            Entity entityToDestroy = m_data.entity[index];
            // Get the entity at the end of the array
            Entity lastEntity = m_data.entity[lastIndex];

            // Move last entity's data
            m_data.entity[index] = m_data.entity[lastIndex];
            m_data.position[index] = m_data.position[lastIndex];
            m_data.orientation[index] = m_data.orientation[lastIndex];
            m_data.scale[index] = m_data.scale[lastIndex];

            // Update map entry for the swapped entity
            m_map[lastEntity.id] = index;
            // Remove the map entry for the destroyed entity
            m_map.erase(entityToDestroy.id);

            --m_data.instancesActive;

            result = true;
        }

        return result;
    }

    bool TransformComponentManager::Allocate(unsigned int numInstances)
    {
        bool result = false;

        // Only try to allocate more space, not less space.
        if (numInstances > m_data.instancesActive)
        {
            InstanceData newData;
            // Size of new data
            const unsigned int size = numInstances * (sizeof(Entity) + sizeof(sea_math::Vec3) + sizeof(sea_math::Quat) + sizeof(sea_math::Vec3));
            // Allocate memory for new data
            newData.buffer = malloc(size);
            memset(newData.buffer, 0, size);

            // Still the same number of active instances
            newData.instancesActive = m_data.instancesActive;
            // New number of allocated instances
            newData.instancesAllocated = numInstances;

            // Entity data
            newData.entity = (Entity *)newData.buffer;
            // Position data located after all entity data
            newData.position = (sea_math::Vec3 *)(newData.entity + numInstances);
            // Orientation data located after all position data
            newData.orientation = (sea_math::Quat *)(newData.position + numInstances);
            // Scale data located after all orientation data
            newData.scale = (sea_math::Vec3 *)(newData.orientation + numInstances);

            // Copy old data to new buffer (if we had any old data)
            if (m_data.buffer != NULL)
            {
                memcpy(newData.entity, m_data.entity, m_data.instancesActive * sizeof(Entity));
                memcpy(newData.position, m_data.position, m_data.instancesActive * sizeof(sea_math::Vec3));
                memcpy(newData.orientation, m_data.orientation, m_data.instancesActive * sizeof(sea_math::Quat));
                memcpy(newData.scale, m_data.scale, m_data.instancesActive * sizeof(sea_math::Vec3));

                // Free old data (if we had any old data)
                free(m_data.buffer);
            }

            // Substitute new data
            m_data = newData;

            result = true;
        }

        return result;
    }

    const sea_math::Vec3 &TransformComponentManager::GetPosition(Instance i) const
    {
        assert(i.index < m_data.instancesActive && i.index >= 0 &&
            "TransformComponentManager: Tried to get position of invalid instance");

        return m_data.position[i.index];
    }

    const sea_math::Quat &TransformComponentManager::GetOrientation(Instance i) const
    {
        assert(i.index < m_data.instancesActive && i.index >= 0 &&
            "TransformComponentManager: Tried to get orientation of invalid instance");

        return m_data.orientation[i.index];
    }

    const sea_math::Vec3 &TransformComponentManager::GetScale(Instance i) const
    {
        assert(i.index < m_data.instancesActive && i.index >= 0 &&
            "TransformComponentManager: Tried to get scale of invalid instance");

        return m_data.scale[i.index];
    }

    bool TransformComponentManager::SetPosition(Instance i, const sea_math::Vec3 &position)
    {
        bool result = false;

        if (i.index < m_data.instancesActive)
        {
            m_data.position[i.index] = position;
            result = true;
        }

        return result;
    }

    bool TransformComponentManager::SetOrientation(Instance i, const sea_math::Quat &orientation)
    {
        bool result = false;

        if (i.index < m_data.instancesActive)
        {
            m_data.orientation[i.index] = orientation;
            result = true;
        }

        return result;
    }

    bool TransformComponentManager::SetScale(Instance i, const sea_math::Vec3 &scale)
    {
        bool result = false;

        if (i.index < m_data.instancesActive)
        {
            m_data.scale[i.index] = scale;
            result = true;
        }

        return result;
    }
}
