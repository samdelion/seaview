#ifndef SEA_MESH_COMPONENT_H
#define SEA_MESH_COMPONENT_H

#include <unordered_map>
#include <vector>

#include "engine/interfaces/IComponentManager.h"
#include "engine/render/SubMesh.h"

namespace sea
{
    /**
     *  Mesh Component Manager class.
     *
     *  Used to abstract away the representation of components
     *  in the system and provide a method of obtaining and setting the:
     *   - vertex array buffer id and
     *   - sub meshes
     *  of any mesh components in the system.
     */
    class MeshComponentManager : public IComponentManager
    {
    public:
        /**
         *  Default constructor, initialize member variables to sensible
         *  values.
         */
        MeshComponentManager();
        /**
         *  Destructor, free any dynamically allocated memory associated
         *  with this class.
         */
        ~MeshComponentManager();
        /**
         *  Return the number of components that
         *  currently exist within the component manager.
         *
         *  @return     unsigned int, number of component instances
         *              the component manager is managing.
         */
        virtual unsigned int VGetNumInstances() const;
        /**
         *  Create a component for the given entity and
         *  return a component instance which can be used to refer to
         *  that component.
         *
         *  @pre    Entity does not already have a component of this type.
         *
         *  @param  entity  Entity, entity to create component for.
         *                  The component will be associated with this entity.
         *  @return         Instance, the new component instance created.
         */
        virtual Instance VCreateComponentForEntity(Entity entity);
        /**
         *  Get the component instance for the given entity.
         *
         *  Returned instance index will be -1 if given entity does not have a component
         *  of this type.
         *
         *  @param  entity  Entity, get component instance belonging to this entity.
         *  @return         Instance, the component instance belonging to that entity.
         */
        virtual Instance VGetInstanceForEntity(Entity entity) const;
        virtual Entity VGetEntityForInstance(Instance i) const;
        /**
         *  Remove a component instance from the manager.
         *
         *  @post   Any component instance handles currently stored are invalidated
         *          once this function is called.
         *
         *  @param  i   Instance, the component instance to remove.
         *  @return     bool, true if the remove was successful, false otherwise.
         */
        virtual bool VRemoveInstance(Instance i);

        /**
         *  Get the vertex array ID of a given component instance.
         *
         *  @param  i   Instance, component instance to get vertex array ID of.
         *  @return     unsigned int, vertex array id.
         */
        unsigned int GetVertexArrayID(Instance i) const;
        /**
         *  Get a sub-mesh of a given component instance. The sub-mesh
         *  retrieved is given by the sub-mesh index provided.
         *
         *  @param  i           Instance, component instance to get sub-mesh of.
         *  @param  subMesh     unsigned int, sub-mesh index.
         *  @return             const SubMesh &, const reference to sub-mesh.
         */
        const SubMesh &GetSubMesh(Instance i, unsigned int subMesh) const;

        /**
         *  Set the vertex array id of the given component instance.
         *
         *  @param  i       Instance, component instance to set vertex array ID of.
         *  @param  vaoID   unsigned int, vertex array ID value to set.
         *  @return         bool, TRUE if set was successful, FALSE otherwise.
         */
        bool SetVertexArrayID(Instance i, unsigned int vaoID);
        /**
         *  Add a sub-mesh to the given component instance.
         *
         *  @param  i           Instance, component instance to add sub-mesh to.
         *  @param  subMesh     const SubMesh &, const reference to sub-mesh to add.
         *  @return             bool, TRUE if add was successful, FALSE otherwise.
         */
        bool AddSubMesh(Instance i, const SubMesh &subMesh);
    private:
        // Map entity to array index
        typedef std::unordered_map<uint32_t, unsigned int> EntityMap;

        /**
         *  All data belonging to the component manager.
         */
        struct InstanceData {
            // Parallel arrays
            // Entity data
            std::vector<Entity>                 entity;
            // Vertex array ID data
            std::vector<unsigned int>           vaoID;
            // SubMesh data
            std::vector<std::vector<SubMesh>>   subMesh;
        };

        // Component manager data
        InstanceData    m_data;
        EntityMap       m_map;
    };
}

#endif  // SEA_MESH_COMPONENT_H
