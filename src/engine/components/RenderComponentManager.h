#ifndef SEA_RENDER_COMPONENT_MANAGER_H
#define SEA_RENDER_COMPONENT_MANAGER_H

#include <vector>
#include <unordered_map>

#include "engine/interfaces/IComponentManager.h"
#include "engine/render/RenderInstanceNew.h"

namespace sea
{
    class RenderComponentManager : public IComponentManager
    {
    public:
        /**
         *  Return the number of components that
         *  currently exist within the component manager.
         *
         *  @return     unsigned int, number of component instances
         *              the component manager is managing.
         */
        virtual unsigned int VGetNumInstances() const;
        /**
         *  Create a component for the given entity and
         *  return a component instance which can be used to refer to
         *  that component.
         *
         *  @param  entity  Entity, entity to create component for.
         *                  The component will be associated with this entity.
         *  @return         Instance, the new component instance created.
         */
        virtual Instance VCreateComponentForEntity(Entity entity);
        /**
         *  Get the component instance for the given entity.
         *
         *  Returned instance index will be -1 if given entity does not have a component
         *  of this type.
         *
         *  @param  entity  Entity, get component instance belonging to this entity.
         *  @return         Instance, the component instance belonging to that entity.
         */
        virtual Instance VGetInstanceForEntity(Entity entity) const;
        /**
         * Get the entity associated with the given component instance.
         *
         * @param i Instance, instance to find entity for.
         * @return Entity, entity associated with the given
         */
        virtual Entity VGetEntityForInstance(Instance i) const;
        /**
         *  Remove a component instance from the manager.
         *
         *  @param  i   Instance, the component instance to remove.
         *  @return     bool, true if the remove was successful, false otherwise.
         */
        virtual bool VRemoveInstance(Instance i);

        /**
         * Get the render instance of the given component instance.
         *
         * @pre Component instance exists and instance handle is valid.
         *
         * @param i Instance, the component instance to get the
         * render instance of.
         * @return const RenderInstanceNew &, render instance of given
         * component instance.
         */
        const RenderInstanceNew &GetRenderInstance(Instance i) const;
        /**
         * Set the render instance of the given component instance.
         *
         * @param i Instance, the component instance to set the
         * render instance of.
         * @param renderInstance const RenderInstanceNew &, render instance
         * to set.
         * @return bool, TRUE if set was successful, FALSE otherwise.
         */
        bool SetRenderInstance(Instance i, const RenderInstanceNew &renderInstance);

        /**
         * Get the visibilty flag of the given component instance.
         *
         * @pre Component instance exists and instance handle is valid.
         *
         * @param i Instance, the component instance to get the visibility
         * flag of.
         * @return const bool &, visibility flag (TRUE for visible, FALSE for not).
         */
        const bool &GetVisFlag(Instance i) const;
        /**
         * Set the visibility flag of the given component instance.
         *
         * @param i Instance, the component instance to set the visibility flag
         * of.
         * @param const bool &, boolean value to set for the components
         * visibility.
         */
        bool SetVisFlagInstance(Instance i, const bool &);

    private:
        // Map entity to array index
        typedef std::unordered_map<uint32_t, unsigned int> EntityMap;

        /**
         *  All data belonging to the component manager.
         */
        struct InstanceData {
            // Parallel arrays
            // Entity data
            std::vector<Entity>                 entity;
            // Render instance data
            std::vector<RenderInstanceNew>         renderInstance;
            std::vector<bool> visibilityFlag;
        };

        // Component manager data
        InstanceData    m_data;
        EntityMap       m_map;
    };
}

#endif  // SEA_RENDER_COMPONENT_MANAGER_H
