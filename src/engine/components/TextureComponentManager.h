#ifndef SEA_TEXTURE_COMPONENT_MANAGER
#define SEA_TEXTURE_COMPONENT_MANAGER

#include <unordered_map>
#include <vector>

#include "engine/interfaces/IComponentManager.h"

namespace sea
{
    class TextureComponentManager : public IComponentManager
    {
    public:
        /**
         *  Default constructor, initialize member variables to sensible
         *  values.
         */
        TextureComponentManager();
        /**
         *  Destructor, free any dynamically allocated memory associated
         *  with this class.
         */
        ~TextureComponentManager();
        /**
         *  Return the number of components that
         *  currently exist within the component manager.
         *
         *  @return     unsigned int, number of component instances
         *              the component manager is managing.
         */
        virtual unsigned int VGetNumInstances() const;
        /**
         *  Create a component for the given entity and
         *  return a component instance which can be used to refer to
         *  that component.
         *
         *  @pre    Entity does not already have a component of this type.
         *
         *  @param  entity  Entity, entity to create component for.
         *                  The component will be associated with this entity.
         *  @return         Instance, the new component instance created.
         */
        virtual Instance VCreateComponentForEntity(Entity entity);
        /**
         *  Get the component instance for the given entity.
         *
         *  Returned instance index will be -1 if given entity does not have a component
         *  of this type.
         *
         *  @param  entity  Entity, get component instance belonging to this entity.
         *  @return         Instance, the component instance belonging to that entity.
         */
        virtual Instance VGetInstanceForEntity(Entity entity) const;
        /**
         * Get the entity associated with the given component instance.
         *
         * @param i Instance, instance to find entity for.
         * @return Entity, entity associated with the given
         */
        virtual Entity VGetEntityForInstance(Instance i) const;
        /**
         *  Remove a component instance from the manager.
         *
         *  @post   Any component instance handles currently stored are invalidated
         *          once this function is called.
         *
         *  @param  i   Instance, the component instance to remove.
         *  @return     bool, true if the remove was successful, false otherwise.
         */
        virtual bool VRemoveInstance(Instance i);

        /**
         *  Get the texture ID of the given component instance.
         *
         *  @pre    Component instance exists, instance handle is valid
         *          and has a valid texture ID.
         *
         *  @param  i   Instance, the component instance to get the texture ID of.
         *  @return     unsigned int, texture ID of the given component instance.
         */
        unsigned int GetTextureID(Instance i) const;

        /**
         *  Set the texture ID of the given component instance.
         *
         *  @pre    Component instance exists and instance handle is valid.
         *
         *  @param  i               Instance, the component instance to remove.
         *  @param  unsigned int    textureID, new texture ID to set.
         *  @return                 bool, true if set was successful, false otherwise.
         */
        bool SetTextureID(Instance i, unsigned int textureID);
    private:
        // Map entity to array index
        typedef std::unordered_map<uint32_t, unsigned int> EntityMap;

        /**
         *  All data belonging to the component manager.
         */
        struct InstanceData {
            // Parallel arrays
            // Entity data
            std::vector<Entity>                 entity;
            // Texture ID data
            std::vector<unsigned int>           textureID;
        };

        // Component manager data
        InstanceData    m_data;
        EntityMap       m_map;
    };
}

#endif  // SEA_TEXTURE_COMPONENT_MANAGER
