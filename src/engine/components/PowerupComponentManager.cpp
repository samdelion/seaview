#include <cassert>

#include "PowerupComponentManager.h"

namespace sea
{


    unsigned int PowerupComponentManager::VGetNumInstances() const
    {
        return m_data.entity.size();
    }


    Instance PowerupComponentManager::VCreateComponentForEntity(Entity entity)
    {
        unsigned int newIndex = m_data.entity.size();

        m_data.entity.push_back(entity);
        m_data.radiusX.push_back(0);
        m_data.radiusY.push_back(0);
        m_data.radiusZ.push_back(0);
        m_data.type.push_back("");

        m_map[entity.id] = newIndex;

        return Instance::MakeInstance(newIndex);
    }


    Instance PowerupComponentManager::VGetInstanceForEntity(Entity entity) const
    {
        int index = -1;
        EntityMap::const_iterator it = m_map.find(entity.id);

        if (it != m_map.end())
        {
            index = it->second;
        }

        return Instance::MakeInstance(index);
    }


    Entity PowerupComponentManager::VGetEntityForInstance(Instance i) const
    {
        Entity e;

        const int index = i.index;

        if (index < VGetNumInstances() && index >= 0)
        {
            e = m_data.entity[index];
        }

        return e;

    }


    bool PowerupComponentManager::VRemoveInstance(Instance i)
    {
        bool result = false;

        const int index = i.index;
        const unsigned int lastIndex = VGetNumInstances() - 1;


        if (index < VGetNumInstances() && index >= 0)
        {

            Entity entityToDestroy = m_data.entity[index];

            Entity lastEntity = m_data.entity[lastIndex];

            m_data.entity[index] = m_data.entity[lastIndex];
            m_data.radiusX[index] = m_data.radiusX[lastIndex];
            m_data.radiusY[index] = m_data.radiusY[lastIndex];
            m_data.radiusZ[index] = m_data.radiusZ[lastIndex];
            m_data.type[index] = m_data.type[lastIndex];


            m_map[lastEntity.id] = index;

            m_map.erase(entityToDestroy.id);


            m_data.entity.pop_back();
            m_data.radiusX.pop_back();
            m_data.radiusY.pop_back();
            m_data.radiusZ.pop_back();
            m_data.type.pop_back();

            result = true;
        }

        return result;
    }


    const float PowerupComponentManager::GetRadiusX(Instance i) const
    {
        assert (i.index < VGetNumInstances() && i.index >= 0 &&
                "PowerupComponentManager: Tried to get x radius of invalid instance");

        return m_data.radiusX[i.index];
    }

    bool PowerupComponentManager::SetRadiusX(Instance i, const float radius)
    {
        bool result = false;

        if(i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.radiusX[i.index] = radius;
            result = true;
        }

        return result;
    }

    const float PowerupComponentManager::GetRadiusY(Instance i) const
    {
        assert (i.index < VGetNumInstances() && i.index >= 0 &&
                "PowerupComponentManager: Tried to get y radius of invalid instance");

        return m_data.radiusY[i.index];
    }

    bool PowerupComponentManager::SetRadiusY(Instance i, const float radius)
    {
        bool result = false;

        if(i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.radiusY[i.index] = radius;
            result = true;
        }

        return result;
    }

    const float PowerupComponentManager::GetRadiusZ(Instance i) const
    {
        assert (i.index < VGetNumInstances() && i.index >= 0 &&
                "PowerupComponentManager: Tried to get z radius of invalid instance");

        return m_data.radiusZ[i.index];
    }

    bool PowerupComponentManager::SetRadiusZ(Instance i, const float radius)
    {
        bool result = false;

        if(i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.radiusZ[i.index] = radius;
            result = true;
        }

        return result;
    }

    const std::string &PowerupComponentManager::GetPowerupType(Instance i) const
    {
        assert (i.index < VGetNumInstances() && i.index >= 0 &&
                "PowerupComponentManager: Tried to get type of invalid instance");

        return m_data.type[i.index];
    }

    bool PowerupComponentManager::SetPowerupType(Instance i, const std::string &type)
    {
        bool result = false;

        if(i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.type[i.index] = type;
            result = true;
        }

        return result;
    }
}
