/**
 *  This code was only slightly modified from this source:
 *  http://bitsquid.blogspot.com.au/2014/08/building-data-oriented-entity-system.html?m=1
 *
 *  @author Niklas
 *  @author Samuel Evans-Powell (modified)
 *  @date   16/09/2015
 */
#ifndef SEA_TRANSFORM_COMPONENT_MANAGER
#define SEA_TRANSFORM_COMPONENT_MANAGER

#include <unordered_map>

#include "engine/interfaces/IComponentManager.h"
#include "math/Vec3.h"
#include "math/Quat.h"

namespace sea
{
    /**
     *  Transform Component Manager class.
     *
     *  Used to abstract away the representation of components
     *  in the system and provide a method of obtaining and setting the:
     *   - position,
     *   - orientation and
     *   - scale
     *  of any transform components in the system.
     */
    class TransformComponentManager : public IComponentManager
    {
    public:
        /**
         *  Default constructor, initialize members to sensible values.
         */
        TransformComponentManager();
        /**
         *  Free any memory associated with the manager.
         */
        ~TransformComponentManager();
        /**
         *  Return the number of components that
         *  currently exist within the component manager.
         *
         *  @return     unsigned int, number of component instances
         *              the component manager is managing.
         */
        virtual unsigned int VGetNumInstances() const;
        /**
         *  Create a component for the given entity and
         *  return a component instance which can be used to refer to
         *  that component.
         *
         *  @pre    Entity does not already have a component of this type.
         *
         *  @param  entity  Entity, entity to create component for.
         *                  The component will be associated with this entity.
         *  @return         Instance, the new component instance created.
         */
        virtual Instance VCreateComponentForEntity(Entity entity);
        /**
         *  Get the component instance for the given entity.
         *
         *  Returned instance index will be -1 if given entity does not have a component
         *  of this type.
         *
         *  @param  entity  Entity, get component instance belonging to this entity.
         *  @return         Instance, the component instance belonging to that entity.
         */
        virtual Instance VGetInstanceForEntity(Entity entity) const;
        /**
         * Get the entity associated with the given instance.
         *
         * @param i Instance, component instance to get owning entity of.
         * @return Entity, owning entity.
         */
        virtual Entity VGetEntityForInstance(Instance i) const;
        /**
         *  Remove a component instance from the manager.
         *
         *  @post   Any component instance handles currently stored are invalidated
         *          once this function is called.
         *
         *  @param  i   Instance, the component instance to remove.
         *  @return     bool, true if the remove was successful, false otherwise.
         */
        virtual bool VRemoveInstance(Instance i);

        /**
         *  Get the position of the given component instance.
         *
         *  @pre    Valid Instance handle given.
         *
         *  @param  i   Instance, component instance to get position of.
         *  @return     const Vec3 &, position of the component instance.
         */
        const sea_math::Vec3 &GetPosition(Instance i) const;
        /**
         *  Get the orientation of the given component instance.
         *
         *  @pre    Valid Instance handle given.
         *
         *  @param  i   Instance, component instance to get the orientation of.
         *  @return     const Quat &, quaternion representing orientation of the component instance.
         */
        const sea_math::Quat &GetOrientation(Instance i) const;
        /**
         *  Get the scale of the given component instance.
         *
         *  @pre    Valid Instance handle given.
         *
         *  @param  i   Instance, component instance to get the scale of.
         *  @return     const Vec3 &, x,y and z scale of the component instance.
         */
        const sea_math::Vec3 &GetScale(Instance i) const;
        /**
         *  Set the position of the component instance.
         *
         *  @param  i           Instance, component instance to set the position of.
         *  @param  position    const Vec3 &, new position to set.
         *  @return             bool, true if set was successful, false otherwise.
         */
        bool SetPosition(Instance i, const sea_math::Vec3 &position);
        /**
         *  Set the orientation of the component instance.
         *
         *  @param  i               Instance, component instance to set the orientation of.
         *  @param  orientation     const Quat &, new orientation to set.
         *  @return                 bool, true if set was successful, false otherwise.
         */
        bool SetOrientation(Instance i, const sea_math::Quat &orientation);
        /**
         *  Set the scale of the component instance.
         *
         *  @param  i               Instance, component instance to set the scale of.
         *  @param  scale           const Vec3 &, new scale to set.
         *  @return                 bool, true if set was successful, false otherwise.
         */
        bool SetScale(Instance i, const sea_math::Vec3 &scale);
    private:
        /**
         *  Allocate memory for the given number of instances.
         *
         *  @param  numInstances    number of instances to allocate memory for.
         *  @return                 bool, true if allocation was successful, false otherwise.
         */
        bool Allocate(unsigned int numInstances);

        // Map entity to array index
        typedef std::unordered_map<uint32_t, unsigned int> EntityMap;

         /*  Data is arranged in the following manner:
         *      - all entity data followed by
         *      - all position data followed by
         *      - all orientation data followed by
         *      - all scale data
         *   Data is arranged in this way to increase cache locality.
         */
        struct InstanceData {
            // Number of active/alive/used instances
            unsigned int    instancesActive;
            // Number of allocated instances
            unsigned int    instancesAllocated;
            // Instance data (one big buffer)
            void            *buffer;

            // Offset to Entity data in buffer
            Entity          *entity;
            // Offset to position data in buffer
            sea_math::Vec3  *position;
            // Offset to orientation data in buffer
            sea_math::Quat  *orientation;
            // Offset to scale data in buffer
            sea_math::Vec3  *scale;
        };

        // Component data
        InstanceData    m_data;
        EntityMap       m_map;
    };
}

#endif  // SEA_TRANSFORM_COMPONENT_MANAGER
