#include <cassert>

#include "engine/components/MeshComponentManager.h"

namespace sea
{
    MeshComponentManager::MeshComponentManager()
    {
    }

    MeshComponentManager::~MeshComponentManager()
    {
    }

    unsigned int MeshComponentManager::VGetNumInstances() const
    {
        // Arrays should be the same size, so just return size
        // of one of the arrays to get the number of active
        // instances.
        return m_data.entity.size();
    }

    Instance MeshComponentManager::VCreateComponentForEntity(Entity entity)
    {
        // Index to new component instance will be put into
        unsigned int newIndex = m_data.entity.size();

        m_data.entity.push_back(entity);
        m_data.vaoID.push_back(0);
        m_data.subMesh.push_back(std::vector<SubMesh>());

        // Put new entry into the map (mapping index array to Entity)
        m_map[entity.id] = newIndex;

        // Return instance to caller
        return Instance::MakeInstance(newIndex);
    }

    Instance MeshComponentManager::VGetInstanceForEntity(Entity entity) const
    {
        int index = -1;
        EntityMap::const_iterator it = m_map.find(entity.id);

        if (it != m_map.end())
        {
            index = it->second;
        }

        return Instance::MakeInstance(index);
    }

    Entity MeshComponentManager::VGetEntityForInstance(Instance i) const
    {
        Entity e;

        const int index = i.index;

        if (index < VGetNumInstances() && index >= 0)
        {
            e = m_data.entity[index];
        }

        return e;
    }

    bool MeshComponentManager::VRemoveInstance(Instance i)
    {
        bool result = false;

        // Keep data array tightly packed by swapping last element
        // with element we want to remove and reducing the number
        // of active instances.
        const int index = i.index;
        const unsigned int lastIndex = VGetNumInstances() - 1;

        // Make sure we are trying to delete a valid instance
        if (index < VGetNumInstances() && index >= 0)
        {
            // Get the Entity at the index to destroy
            Entity entityToDestroy = m_data.entity[index];
            // Get the entity at the end of the array
            Entity lastEntity = m_data.entity[lastIndex];

            // Move last entity's data 
            m_data.entity[index] = m_data.entity[lastIndex];
            m_data.vaoID[index] = m_data.vaoID[lastIndex];
            m_data.subMesh[index] = m_data.subMesh[lastIndex];

            // Update map entry for the swapped entity
            m_map[lastEntity.id] = index;
            // Remove the map entry for the destroyed entity
            m_map.erase(entityToDestroy.id);
            
            // Destroy component at end of array
            m_data.entity.pop_back();
            m_data.vaoID.pop_back();
            m_data.subMesh.pop_back();

            result = true;
        }

        return result;
    }

    unsigned int MeshComponentManager::GetVertexArrayID(Instance i) const
    {
        assert(i.index < VGetNumInstances() && i.index >= 0 &&
            "MeshComponentManager: Tried to get vaoID of invalid instance");
        
        return m_data.vaoID[i.index];
    }

    const SubMesh &MeshComponentManager::GetSubMesh(Instance i, unsigned int subMesh) const
    {
        assert(i.index < VGetNumInstances() && i.index >= 0 &&
            "MeshComponentManager: Tried to get subMesh of invalid instance");
        assert(subMesh < m_data.subMesh[i.index].size() &&
            "MeshComponentManager: Tried to get invalid subMesh");
        
        return m_data.subMesh[i.index][subMesh];
    }

    bool MeshComponentManager::SetVertexArrayID(Instance i, unsigned int vaoID)
    {
        bool result = false;

        // Make sure we're trying to set a valid instance
        if (i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.vaoID[i.index] = vaoID;
            result = true;
        }

        return result;
    }

    bool MeshComponentManager::AddSubMesh(Instance i, const SubMesh &subMesh)
    {
        bool result = false;

        // Make sure we're trying to set a valid instance
        if (i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.subMesh[i.index].push_back(subMesh);
            result = true;
        }

        return result;
    }
}
