/**
*  This code was only slightly modified from this source:
*  http://bitsquid.blogspot.com.au/2014/08/building-data-oriented-entity-system.html?m=1
*
*  @author Niklas
*  @author Samuel Evans-Powell (modified)
*  @author Rikki Kiely (modified)
*  @date   10/10/2015
*/
#include <cassert>
#include <stdlib.h>

#include <iostream> //testing

#include "engine/components/LevelDetailsComponentManager.h"

namespace sea
{
	LevelDetailsComponentManager::LevelDetailsComponentManager()
	{
	}


	LevelDetailsComponentManager::~LevelDetailsComponentManager()
	{
	}

	unsigned int LevelDetailsComponentManager::VGetNumInstances() const
	{
		return m_data.entity.size();
	}

	Instance LevelDetailsComponentManager::VCreateComponentForEntity(Entity entity)
	{
		// Index to new component instance will be put into
		unsigned int newIndex = m_data.entity.size();

		m_data.entity.push_back(entity);
		m_data.levelTime.push_back(0);
		m_data.minY.push_back(0);

		// Put new entry into the map (mapping index array to Entity)
		m_map[entity.id] = newIndex;

		// Return instance to caller
		return Instance::MakeInstance(newIndex);
	}

	Instance LevelDetailsComponentManager::VGetInstanceForEntity(Entity entity) const
	{
		int index = -1;
		EntityMap::const_iterator it = m_map.find(entity.id);

		if (it != m_map.end())
		{
			index = it->second;
		}

		return Instance::MakeInstance(index);
	}

	Entity LevelDetailsComponentManager::VGetEntityForInstance(Instance i) const
	{
		Entity e;

		const int index = i.index;

		if (index < VGetNumInstances() && index >= 0)
		{
			e = m_data.entity[index];
		}

		return e;
	}

	bool LevelDetailsComponentManager::VRemoveInstance(Instance i)
	{
		bool result = false;

		// Keep data array tightly packed by swapping last element
		// with element we want to remove and reducing the number
		// of active instances.
		const int index = i.index;
		const unsigned int lastIndex = VGetNumInstances() - 1;

		// Make sure we are trying to delete a valid instance
		if (index < VGetNumInstances() && index >= 0)
		{
			// Get the Entity at the index to destroy
			Entity entityToDestroy = m_data.entity[index];
			// Get the entity at the end of the array
			Entity lastEntity = m_data.entity[lastIndex];

			// Move last entity's data 
			m_data.entity[index] = m_data.entity[lastIndex];
			m_data.levelTime[index] = m_data.levelTime[lastIndex];
			m_data.minY[index] = m_data.minY[lastIndex];

			// Update map entry for the swapped entity
			m_map[lastEntity.id] = index;
			// Remove the map entry for the destroyed entity
			m_map.erase(entityToDestroy.id);

			// Destroy component at end of array
			m_data.entity.pop_back();
			m_data.levelTime.pop_back();
			m_data.minY.pop_back();

			result = true;
		}
		return result;
	}

	const unsigned int &LevelDetailsComponentManager::GetLevelTime(Instance i) const
	{
		assert(i.index < VGetNumInstances() && i.index >= 0 &&
			"LevelDetailsComponentManager: Tried to get levelTime of invalid isntance.");

		return m_data.levelTime[i.index];
	}

	const int &LevelDetailsComponentManager::GetMinY(Instance i) const
	{
		assert(i.index < VGetNumInstances() && i.index >= 0 &&
			"LevelDetailsComponentManager: Tried to get minY of invalid isntance.");

		return m_data.minY[i.index];
	}

	bool LevelDetailsComponentManager::SetMinY(Instance i, const std::string &minYvalue)
	{
		bool result = false;

		// Make sure we're trying to set a valid instance
		if (i.index < VGetNumInstances() && i.index >= 0)
		{
			m_data.minY[i.index] = atoi(minYvalue.c_str());
			//printf("Y Set: %d\n", m_data.minY[i.index]);
			result = true;
		}

		return result;
	}

	bool LevelDetailsComponentManager::SetLevelTime(Instance i, const std::string &levelTimeValue)
	{
		bool result = false;

		// Make sure we're trying to set a valid instance
		if (i.index < VGetNumInstances() && i.index >= 0)
		{
			m_data.levelTime[i.index] = atoi(levelTimeValue.c_str());
			//printf("Time set to: %d\n", m_data.levelTime[i.index]);
			result = true;
		}

		return result;
	}

}

