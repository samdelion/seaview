#ifndef SEA_POWERUP_COMPONENT_MANAGER
#define SEA_POWERUP_COMPONENT_MANAGER

#include <vector>
#include <string>
#include <unordered_map>

#include "engine/interfaces/IComponentManager.h"
#include "math/Vec3.h"


namespace sea
{
    class PowerupComponentManager : public IComponentManager
    {
    public:

        /**
         *  Return the number of components that
         *  currently exist within the component manager.
         *
         *  @return     unsigned int, number of component instances
         *              the component manager is managing.
         */
        virtual unsigned int VGetNumInstances() const;

        /**
         *  Create a component for the given entity and
         *  return a component instance which can be used to refer to
         *  that component.
         *
         *  @pre    Entity does not already have a component of this type.
         *
         *  @param  entity  Entity, entity to create component for.
         *                  The component will be associated with this entity.
         *  @return         Instance, the new component instance created.
         */
        virtual Instance VCreateComponentForEntity(Entity entity);

        /**
         *  Get the component instance for the given entity.
         * pt
         *  Returned instance index will be -1 if given entity does not have a component
         *  of this type.
         *
         *  @param  entity  Entity, get component instance belonging to this entity.
         *  @return         Instance, the component instance belonging to that entity.
         */
        virtual Instance VGetInstanceForEntity(Entity entity) const;

        /**
         * Get the entity associated with the given instance.
         *
         * @param i Instance, component instance to get owning entity of.
         * @return Entity, owning entity.
         */
        virtual Entity VGetEntityForInstance(Instance i) const;

        /**
         *  Remove a component instance from the manager.
         *
         *  @post   Any component instance handles currently stored are invalidated
         *          once this function is called.
         *
         *  @param  i   I ptnstance, the component instance to remove.
         *  @return     bool, true if the remove was successful, false otherwise.
         */
        virtual bool VRemoveInstance(Instance i);

        /**
         * Get the x radius of the AABB of the powerup.
         *
         * @param i Instance, powerup instance to get the x radius of.
         * @return const float, x radius of the AABB of the powerup.
         */
        const float GetRadiusX(Instance i) const;
        /**
         * Set the x radius of the AABB of the powerup.
         *
         * @param i Instance, powerup instance to set the x radius of.
         * @param radius const float, new x radius to set.
         */
        bool SetRadiusX(Instance i, const float radius);

        /**
         * Get the y radius of the AABB of the powerup.
         *
         * @param i Instance, powerup instance to get the y radius of.
         * @return const float, y radius of the AABB of the powerup.
         */
        const float GetRadiusY(Instance i) const;
        /**
         * Set the y radius of the AABB of the powerup.
         *
         * @param i Instance, powerup instance to set the y radius of.
         * @param radius const float, new y radius to set.
         */
        bool SetRadiusY(Instance i, const float radius);

        /**
         * Get the z radius of the AABB of the powerup.
         *
         * @param i Instance, powerup instance to get the z radius of.
         * @return const float, y radius of the AABB of the powerup.
         */
        const float GetRadiusZ(Instance i) const;
        /**
         * Set the z radius of the AABB of the powerup.
         *
         * @param i Instance, powerup instance to set the z radius of.
         * @param radius const float, new z radius to set.
         */
        bool SetRadiusZ(Instance i, const float radius);

        /**
         * Get the type of the powerup.
         *
         * @param i Instance, powerup instance to get the powerup type of.
         * @return const std::string &, power up type as a string.
         */
        const std::string &GetPowerupType(Instance i) const;
        /**
         * Set the powerup type of the powerup.
         *
         * @param i Instance, powerup instance to set the powerup type of.
         * @param type const std::string &, new type of powerup to set.
         * @return bool, true if set was successful, false otherwise.
         */
        bool SetPowerupType(Instance i, const std::string &type);

    private:

        typedef std::unordered_map<uint32_t, unsigned int> EntityMap;

        struct InstanceData
        {
            std::vector<Entity> entity;
            std::vector<float> radiusX;
            std::vector<float> radiusY;
            std::vector<float> radiusZ;
            std::vector<std::string> type;
        };

        InstanceData m_data;
        EntityMap m_map;
    };
}

#endif
