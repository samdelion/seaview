#include <cassert>

#include "engine/components/RenderComponentManager.h"

namespace sea
{
    unsigned int RenderComponentManager::VGetNumInstances() const
    {
        // Arrays should be the same size, so just return size
        // of one of the arrays to get the number of active
        // instances.
        return m_data.entity.size();
    }

    Instance RenderComponentManager::VCreateComponentForEntity(Entity entity)
    {
        // Index to new component instance will be put into
        unsigned int newIndex = m_data.entity.size();

        m_data.entity.push_back(entity);
        m_data.renderInstance.push_back(RenderInstanceNew());
        m_data.visibilityFlag.push_back(true);

        // Put new entry into the map (mapping index array to Entity)
        m_map[entity.id] = newIndex;

        // Return instance to caller
        return Instance::MakeInstance(newIndex);
    }

    Instance RenderComponentManager::VGetInstanceForEntity(Entity entity) const
    {
        int index = -1;
        EntityMap::const_iterator it = m_map.find(entity.id);

        if (it != m_map.end())
        {
            index = it->second;
        }

        return Instance::MakeInstance(index);
    }

    Entity RenderComponentManager::VGetEntityForInstance(Instance i) const
    {
        Entity e;

        const int index = i.index;

        if (index < VGetNumInstances() && index >= 0)
        {
            e = m_data.entity[index];
        }

        return e;
    }

    bool RenderComponentManager::VRemoveInstance(Instance i)
    {
        bool result = false;

        // Keep data array tightly packed by swapping last element
        // with element we want to remove and reducing the number
        // of active instances.
        const int index = i.index;
        const unsigned int lastIndex = VGetNumInstances() - 1;

        // Make sure we are trying to delete a valid instance
        if (index < VGetNumInstances() && index >= 0)
        {
            // Get the Entity at the index to destroy
            Entity entityToDestroy = m_data.entity[index];
            // Get the entity at the end of the array
            Entity lastEntity = m_data.entity[lastIndex];

            // Move last entity's data
            m_data.entity[index] = m_data.entity[lastIndex];
            m_data.renderInstance[index] = m_data.renderInstance[lastIndex];
            m_data.visibilityFlag[index] = m_data.visibilityFlag[lastIndex];

            // Update map entry for the swapped entity
            m_map[lastEntity.id] = index;
            // Remove the map entry for the destroyed entity
            m_map.erase(entityToDestroy.id);

            // Destroy component at end of array
            m_data.entity.pop_back();
            m_data.renderInstance.pop_back();
            m_data.visibilityFlag.pop_back();

            result = true;
        }

        return result;
    }

    const RenderInstanceNew &RenderComponentManager::GetRenderInstance(Instance i) const
    {
        assert(i.index < VGetNumInstances() && i.index >= 0 &&
               "RenderComponentManager: Tried to get renderInstance of invalid instance");

        return m_data.renderInstance[i.index];
    }

    bool RenderComponentManager::SetRenderInstance(Instance i, const RenderInstanceNew &renderInstance)
    {
        bool result = false;

        // Make sure we're trying to set a valid instance
        if (i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.renderInstance[i.index] = renderInstance;
            result = true;
        }

        return result;
    }

    const bool &RenderComponentManager::GetVisFlag(Instance i) const
    {
        return m_data.visibilityFlag[i.index];
    }


    bool RenderComponentManager::SetVisFlagInstance(Instance i, const bool &theFlag)
    {
        bool result = false;

        if (i.index < VGetNumInstances() && i.index >= 0)
        {
            m_data.visibilityFlag[i.index] = theFlag;
            result = true;
        }

        return result;
    }
}
