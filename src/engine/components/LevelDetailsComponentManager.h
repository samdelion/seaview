#ifndef LEVEL_DETAILS_MANAGER_H
#define LEVEL_DETAILS_MANAGER_H

#include <unordered_map>
#include <vector>

#include "engine/interfaces/IComponentManager.h"

namespace sea
{
  /**
  *  Level Details Manager class.
  *
  *  Used to abstract away the representation of components
  *  in the system and provide a method of obtaining and setting the:
  *   - level base time
  *   - minimum Y value before respawn
  *  of any level details component
  */
  class LevelDetailsComponentManager : public IComponentManager
  {
  public:
    LevelDetailsComponentManager();
    ~LevelDetailsComponentManager();
    virtual unsigned int VGetNumInstances() const;
    virtual Instance VCreateComponentForEntity(Entity entity);
    virtual Instance VGetInstanceForEntity(Entity entity) const;
    virtual Entity VGetEntityForInstance(Instance i) const;
    virtual bool VRemoveInstance(Instance i);

    const unsigned int &GetLevelTime(Instance i) const;
    const int &GetMinY(Instance i) const;
    bool SetMinY(Instance i, const std::string &);
    bool SetLevelTime(Instance i, const std::string &);

  private:
    // Map entity to array index
    typedef std::unordered_map<uint32_t, unsigned int> EntityMap;

    /**
    *  All data belonging to the component manager.
    */
    struct InstanceData
    {
      //Related entity
      std::vector<Entity>				entity;

      //Base time the level starts with
      std::vector<unsigned>		levelTime;

      //The minimum Y value of a level before respawn
      std::vector<int>				minY;
    };

    // Component manager data
    InstanceData    m_data;
    EntityMap       m_map;
  };
}
#endif // !LEVEL_DETAILS_MANAGER_H
