#ifndef SEA_IAPPLICATION_H
#define SEA_IAPPLICATION_H

/** @todo Uncouple SDL2 */
#include <SDL2/SDL.h>

namespace sea
{
    /**
     *  Application interface.
     *
     *  Defines the interface which applications (i.e. games)
     *  must implement in order to be run by the engine.
     */
    class IApplication
    {
    public:
        /**
         * Virtual destructor so inheriting classes destructor is properly
         * called.
         */
        virtual ~IApplication() {}

        /**
         * Callback function called by engine when application is switched to.
         * This includes the first time the engine is started up (called before
         * VInit).
         *
         * Useful for blitting a load screen before loading any assets.
         */
        virtual void VOnSwitchTo() = 0;
        /**
         *  Called by the engine after the window is created.
         *
         *  Initialize application-specific data.
         *
         *  @return     bool, true if initialization was successful, false otherwise.
         */
        virtual bool VInit() = 0;
        /**
         *  Return the name of the application.
         *
         *  Will be displayed in the title bar of the window.
         *
         *  @return     const char *, application title string.
         */
        virtual const char *VGetApplicationTitle() = 0;
        /**
         *  Return the path to the application options file
         *  which will be used to initialize application/engine settings.
         *
         *  Not currently used.
         *
         *  @return     const char *, path to application options file string.
         */
        virtual const char *VGetApplicationOptionsPath() = 0;
        /**
         *  Update method called each frame.
         *
         *  The update method is passed the time the last frame took to render.
         *
         *  @param  deltaTime   float, time last frame took to render.
         */
        virtual void VUpdate(float deltaTime) = 0;
        /**
         *  Render method called each frame.
         *
         *  Render method is called after the update method.
         */
        virtual void VRender() = 0;
        /**
         *  Method called whenever the window is resized.
         *
         *  Method is passed the new window width and height.
         *
         *  @param  width   unsigned int, new window width.
         *  @param  height  unsigned int, new window height.
         */
        virtual void VResize(unsigned int width, unsigned int height) = 0;
        /**
         *  Return the version of OpenGL to use (major).
         *
         *  @return     unsigned int, major version of OpenGL to use.
         */
        virtual unsigned int VGetOpenGLMajorVersion() = 0;
        /**
         *  Return the version of OpenGL to use (minor).
         *
         *  @return     unsigned int, minor version of OpenGL to use.
         */
        virtual unsigned int VGetOpenGLMinorVersion() = 0;
        /**
         *  Callback function called by engine when key down
         *  event occurs.
         *
         *  @param  key     SDL_Keycode, enum representing key pressed.
         */
        virtual void VKeyDown(SDL_Keycode key) = 0;
        /**
         *  Callback function called by engine when key up
         *  event occurs.
         *
         *  @param  key     SDL_Keycode, enum representing key pressed.
         */
        virtual void VKeyUp(SDL_Keycode key) = 0;
        /**
         *  Callback function called by engine when mouse button down
         *  event occurs.
         *
         *  @param  button  Uint8, enum representing which mouse button
         *                  was pressed.
         */
        virtual void VMouseButtonDown(Uint8 button) = 0;

        /**
         *	Getter - returns current state of "m_paused".
         */
        bool GetPaused(){ return m_paused; }

        /**
         *	Setter - Sets "m_paused"
         *
         *	@param	paused	bool represnting the current state of the application.
         */
        void SetPaused(bool paused){ m_paused = paused; }

    protected:
        /**
         *	Member varaible to depict the play state of the game, paused or playing
         */
        bool m_paused = false;
    };
}

#endif  // SEA_IAPPLICATION_H
