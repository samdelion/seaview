#ifndef SEA_ICOMPONENT_MANAGER_H
#define SEA_ICOMPONENT_MANAGER_H

#include "engine/core/Entity.h"
#include "engine/core/Instance.h"

namespace sea
{
    /**
     *  A component manager abstracts the
     *  representation of components in the engine.
     *
     *  Any concrete manager class will:
     *   - use Instances as a method of refering to specific component instances
     *   - provide getter and setter methods for all the components'
     *  data members.
     */
    class IComponentManager
    {
    public:
        /**
         *  Virtual destructor to ensure that sub-class destructor is called.
         */
        virtual ~IComponentManager() { };

        /**
         *  Return the number of components that
         *  currently exist within the component manager.
         *
         *  @return     unsigned int, number of component instances
         *              the component manager is managing.
         */
        virtual unsigned int VGetNumInstances() const = 0;
        /**
         *  Create a component for the given entity and
         *  return a component instance which can be used to refer to
         *  that component.
         *
         *  @param  entity  Entity, entity to create component for.
         *                  The component will be associated with this entity.
         *  @return         Instance, the new component instance created.
         */
        virtual Instance VCreateComponentForEntity(Entity entity) = 0;
        /**
         *  Get the component instance for the given entity.
         *
         *  Returned instance index will be -1 if given entity does not have a component
         *  of this type.
         *
         *  @param  entity  Entity, get component instance belonging to this entity.
         *  @return         Instance, the component instance belonging to that entity.
         */
        virtual Instance VGetInstanceForEntity(Entity entity) const = 0;
        /**
         * Get the entity associated with the given instance.
         *
         * @param i Instance, component instance to get owning entity of.
         * @return Entity, owning entity.
         */
        virtual Entity VGetEntityForInstance(Instance i) const = 0;
        /**
         *  Remove a component instance from the manager.
         *
         *  @param  i   Instance, the component instance to remove.
         *  @return     bool, true if the remove was successful, false otherwise.
         */
        virtual bool VRemoveInstance(Instance i) = 0;
        /**
         *  Get the entity owning the given component instance.
         *
         *  @param  i   Instance, the component instance to get the owning entity of.
         *  @return     Entity, entity owning the given component instance.
         */
        //virtual Entity VGetEntityForInstance(Instance i) = 0;
    };
}

#endif  // SEA_ICOMPONENT_MANAGER_H
