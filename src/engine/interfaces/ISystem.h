#ifndef SEA_ISYSTEM_H
#define SEA_ISYSTEM_H

namespace sea
{
    /**
     * System interface.
     *
     * Inherited by all classes that need to be updated by the engine every
     * frame.
     */
    class ISystem
    {
    public:
        /**
         * Virtual destructor so inheriting classes destructor is properly
         * called.
         */
        virtual ~ISystem() { };
        /**
         * Update method, called each tick by the Engine when the system is
         * registered with the Engine.
         *
         * @param deltaTime float, time last frame took to complete in
         * milliseconds.
         */
        virtual void VUpdate(float deltaTime) = 0;
    };
}

#endif  // ISYSTEM_H
