//JSound.h
#ifndef SEA_JSOUND_H
#define SEA_JSOUND_H

#include <SDL_mixer.h>
#include <stdio.h>
#include <string>

 /**
     *  sound class.
     *
     *  used to load, play and free music and sound effects (Sfx)

   *	@todo add looping  1 music, add looping shuffle
     */
class JSound
{

public:
  /**
   *  Default constructor.
   *
   *  Gives safe variables to the sound object's variables.
   */
  JSound();

  /**
   *  Calls FreeMusic() to free memory holding current music
   *  allocate a random integer within a range to m_CurrentMusicNum
   *  calls LoadMusic() to load new music into memory
   *
   *  @todo
   */
  void ShuffleMusic();

  /**
   *  Calls FreeMusic() to free memory holding current music
   *  +1 to m_CurrentMusicNum to load the next music
   *	Checks that m_CurrentMusicNum is in integer range
   *  Calls LoadMusic() to load new music into memory
   *
   *  @todo
   */
  void NextMusic();

  /**
   *  Calls FreeMusic() to free memory holding current music
   *  -1 to m_CurrentMusicNum to load the next music
   *	Checks that m_CurrentMusicNum is in integer range
   *  Calls LoadMusic() to load new music into memory
   *
   *  @todo
   */
  void PreviousMusic();

  /**
   *  Deallocates m_gMusic memory
   *	set m_gMusic to NULL
   *
   *  @todo
   */
  void FreeMusicP();

  /**
   *  if music is playing pause music
   *	if music is paused resume music
   *
   *  @todo
   */
  void PauseResumeMusic();

  /**
   *  Calls FreeSfx() to free memory holding all current Sfx
   *  loads Sfx into all m_gSfx variables
   *
   *  @todo
   */
  void LoadSfx();

  /**
   *  Public function use to play Sfx
   *	assigns num to m_gSfxNum
   *	Calls PlaySfx();
   *
   *  @param  num         used to change m_gSfxNum variable for
   *                          loading specific Sfx
   *
   *  @todo
   */
  void PlaySfxP(int num);

  /**
   *  calls FreeMusic() and FreeSfx() functions;
   *
   *  @todo
   */
  void FreeAll();

  /**
   *  decrease the music volume by num
   *
   *  @param  num         amount to decrease volume by
   *
   *  @todo
   */
  void VolumeDown(int num);

  /**
   *  increase the music volume by num
   *
   *  @param  num         amount to increase volume by
   *
   *  @todo
   */
  void VolumeUp(int num);

  /**
   *  set the volume to num
   *	of num is outside range move into range
   *
   *  @param  num         amount to set volume too
   *
   *  @todo
   */
  void SetVolume(int num);

  /**
  * returns int value of current music volume
  *
  *	@return integer num current music volume
  */
  int GetVolume();

  /**
   *  toggles m_SpeedMode boolean for use in SpeedMode function
   *
   *
   *  @todo
   */
  void ToggleSpeedMode();

  /**
   *  set's m_SpeedMode boolean to on for use in SpeedMode function
   *
   *  @param  on          TRUE = speedmode on false = speedmode off
   *
   *  @todo
   */
  void InitSpeedMode(bool on);

  /**
   *  returns m_SpeedMode variable
   *
   *  @return boolean m_SpeedMode
   *  @todo
   */
  bool GetSpeedMode();

  /**
   *  increases volume of music the higher bruce's magnitude gets
   *
   *  @param  speed         current magnitude of bruce
   *
   *  @todo
   */
  void SpeedMode(float speed);

  /**
   *  toggles sfx by freeing Mix_Chunk or loading Mix_Chunk depending on m_ToggleSfx
   *
   *
   *  @todo
   */
  void ToggleSfx();

  void PrintControls();

private:

  /**
   *  switch using m_CurrentMusicNum
   *	case load music into memory using variable m_gMusic
   *  calls PlayMusic();
   *
   *  @todo
   */
  void LoadMusic();

  /**
   *  free memory m_gMusic
   *  set m_gMusic to NULL
   *
   *  @todo
   */
  void FreeMusic();

  /**
   *  free memory m_gMusic
   *  set m_gMusic to NULL
   *
   *  @todo
   */
  void PlayMusic();

  /**
   *  start playing m_gMusic
   *
   *  @todo
   */
  void FreeSfx();

  /**
   *  switch using m_SfxNum
   *	case play Sfx
   *
   *  @todo
   */
  void PlaySfx();

  Mix_Music *m_gMusic;

  Mix_Chunk *m_gSfxJump;
  Mix_Chunk *m_gSfxRestart;
  Mix_Chunk *m_gSfxStar;
  Mix_Chunk *m_gSfxOxygen;
  Mix_Chunk *m_gSfxGoal;
  Mix_Chunk *m_gSfxFall;
  Mix_Chunk *m_gSfxBye;
  Mix_Chunk *m_gSfxDeath;

  int m_CurrentMusicNum;
  int m_MinMusicNum;
  int m_MaxMusicNum;
  int m_SfxNum;
  bool m_SpeedMode;
  bool m_ToggleSfx;

};

#endif // SEA_JSOUND_H
