//JSound.cpp

#include <iostream>

#include "JSound.h"
#include "engine/Defines.h"


JSound::JSound()
{
	m_gMusic = nullptr;
	m_gSfxJump = nullptr;
	m_gSfxRestart = nullptr;
	m_gSfxStar = nullptr;
	m_gSfxOxygen = nullptr;
	m_gSfxGoal = nullptr;
	m_gSfxFall = nullptr;
	m_CurrentMusicNum = 0;
	m_MinMusicNum = 0;
	m_MaxMusicNum = 6;
	m_SfxNum = 0;

}

void JSound::ShuffleMusic()
{
	FreeMusic();
	m_CurrentMusicNum = rand() % 7; // get range for music random (% 10 = range 0-9)
	//printf("\n SHUFFLE MUSIC m_CurrentMusicNum = %d", m_CurrentMusicNum);
			
	LoadMusic();
}

void JSound::NextMusic()
{
	FreeMusic();
	m_CurrentMusicNum++; //to go to next song
	if(m_CurrentMusicNum > m_MaxMusicNum)
	{
		m_CurrentMusicNum = m_MinMusicNum;
	}
	//printf("\n NEXT MUSIC m_CurrentMusicNum = %d", m_CurrentMusicNum);
		
	LoadMusic();

}

void JSound::PreviousMusic()
{
	FreeMusic();
	m_CurrentMusicNum--; //to go to previous song
	if(m_CurrentMusicNum < m_MinMusicNum)
	{
		m_CurrentMusicNum = m_MaxMusicNum;
	}
	
	//printf("\n PREVIOUS MUSIC m_CurrentMusicNum = %d", m_CurrentMusicNum);
	
	LoadMusic();
	
}

void JSound::PlayMusic()
{
	Mix_PlayMusic(m_gMusic, 1);
	//printf("\n\n PLAYING MUSIC \n");
	
}
/*
	new music here
*/
void JSound::LoadMusic()
{
	switch (m_CurrentMusicNum)
	{
	case 0:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus0.wav");
		break;

	case 1:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus1.wav");
		break;

	case 2:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus2.wav");
		break;

	case 3:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus3.wav");
		break;

	case 4:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus4.wav");
		break;

	case 5:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus5.wav");
		break;

	case 6:
		m_gMusic = Mix_LoadMUS(PATH_TO_ASSETS "sound/mus6.wav");
		break;

	default:
		printf("\n\n PLAYMUSIC BROKEN \n");

	}
	
	PlayMusic();
	
}

void JSound::PlaySfxP(int num)
{
	m_SfxNum = num;
	PlaySfx();
	
}

/*
	any new sfx must be added here
	0 - jump
	1 - restart
	2 - star
	3 - oxygen
	4 - goal
	5 - fall
*/
void JSound::PlaySfx()
{
	switch(m_SfxNum){
		case 0:
			Mix_PlayChannel(-1, m_gSfxJump, 0);
			break;
		case 1:
			Mix_PlayChannel(-1, m_gSfxRestart, 0);
			break;
		case 2:
			Mix_PlayChannel(-1, m_gSfxStar, 0);
			break;
		case 3:
			Mix_PlayChannel(-1, m_gSfxOxygen, 0);
			break;
		case 4:
			Mix_PlayChannel(-1, m_gSfxGoal, 0);
			break;
		case 5:
			Mix_PlayChannel(-1, m_gSfxBye, 0);
			break;
		case 6:
			Mix_PlayChannel(-1, m_gSfxDeath, 0);
			break;
		default:
			printf("\n\n PlaySfx failed - m_SfxNum didn't match case\n\n");
	}
	
}

void JSound::PauseResumeMusic()
{
	//music paused
	if (Mix_PausedMusic() == 1)
	{
		//Resume music
		Mix_ResumeMusic();
		//m_MusicPaused = false;

		//printf("\n\n RESUMING MUSIC \n");

	}
	else//music playing

	{
		//Pause music
		//m_MusicPaused = true;
		Mix_PauseMusic();
		//printf("\n\n PAUSING MUSIC \n");

	}
	
}

void JSound::FreeMusic()
{
    if (m_gMusic)
    {
        Mix_FreeMusic(m_gMusic);
        m_gMusic = nullptr;
    }

	if (m_gMusic != nullptr)
	{
		printf("Music failed to free! SDL_mixer Error: %s\n", Mix_GetError());

	}

}

void JSound::FreeMusicP()
{
    if (m_gMusic)
    {
        Mix_FreeMusic(m_gMusic);
        m_gMusic = nullptr;
    }

	if (m_gMusic != nullptr)
	{
		printf("Music failed to free! SDL_mixer Error: %s\n", Mix_GetError());

	}

}

/*
	//must add sfx to load here
	0 - jump
	1 - restart
	2 - star
	3 - oxygen
	4 - goal
	5 - fall
*/
void JSound::LoadSfx()
{
	if (m_gSfxJump != nullptr || m_gSfxRestart != nullptr)
	{
		FreeSfx();
	}
	
	m_gSfxRestart = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxRespawn.wav");
	m_gSfxJump = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxJump.wav");
	m_gSfxStar = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxStarPickup.wav");
	m_gSfxOxygen = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxAirPickup.wav");
	m_gSfxGoal = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxFinish.wav");
	m_gSfxFall = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxBelowMinY.wav");
	m_gSfxBye = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxBye.wav");
	m_gSfxDeath = Mix_LoadWAV(PATH_TO_ASSETS "sound/sfxDeath.wav");
	
	if (m_gSfxJump == nullptr || m_gSfxRestart == nullptr ||m_gSfxStar == nullptr || m_gSfxOxygen == nullptr ||m_gSfxGoal == nullptr || m_gSfxFall == nullptr )
	{
		printf("Sound effect failed to load! SDL_mixer Error: %s\n", Mix_GetError());

	}
	else
	{
		//printf("\n\n SOUND EFFECTS LOADED \n\n");
		
	}
	
}

/*
	Be sure to add any sound effects that are loaded here so they are free too
*/
void JSound::FreeSfx()
{
    if (m_gSfxJump)
    {
        Mix_FreeChunk(m_gSfxJump);
        m_gSfxJump = nullptr;
    }

    if (m_gSfxRestart)
    {
        Mix_FreeChunk(m_gSfxRestart);
        m_gSfxRestart = nullptr;
    }
	
	if (m_gSfxJump != nullptr || m_gSfxRestart != nullptr)
	{
		printf("Sound effects failed to free! SDL_mixer Error: %s\n", Mix_GetError());

	}
	
}

void JSound::ToggleSfx()
{
	printf("\n%s\n", m_ToggleSfx ? "true" : "false");
	
	switch(m_ToggleSfx)
	{
		case 0:
			printf("\nCASE 0 toggle sfx ");

			m_ToggleSfx = true;
			LoadSfx();
			break;
		case 1:
			printf("\nCASE 1 toggle sfx ");

			m_ToggleSfx = false;
			FreeSfx();
			break;
		
	}
	
	
}


void JSound::FreeAll()
{
	FreeMusic();
	FreeSfx();
}

void JSound::VolumeDown(int num)
{
	Mix_VolumeMusic((Mix_VolumeMusic(-1) - num));

	
}

void JSound::VolumeUp(int num)
{
	Mix_VolumeMusic((Mix_VolumeMusic(-1) + num));
	
}

void JSound::SetVolume(int num)
{	
	Mix_VolumeMusic(num);
	
}

int JSound::GetVolume()
{
	return (Mix_VolumeMusic(-1));
}

void JSound::ToggleSpeedMode()
{
	printf("\n%s\n", m_SpeedMode ? "true" : "false");
	
	switch(m_SpeedMode)
	{
		case 0:
			m_SpeedMode = true;
			break;
		case 1:
			m_SpeedMode = false;
			break;
		
	}
	
}

void JSound::InitSpeedMode(bool on)
{
	m_SpeedMode = on;
	
}

bool JSound::GetSpeedMode()
{

		return(m_SpeedMode);
}

void JSound::SpeedMode(float speed)
{
	if(GetSpeedMode())
	{
		SetVolume((int)speed + 24);

	}

}

void JSound::PrintControls()
{
	printf("\nSound Controls: ------------- \n");
	printf("Volume Up:                  = \n");
	printf("Volume Up:                  = \n");
	printf("Toggle Music on/off:        0 \n");
	printf("Next Music:                 9 \n");
	printf("Previous Music:             8 \n");
	printf("Shuffle Music:              7 \n");
	printf("Toggle Speed Mode on/off:   6 \n");
	printf("Toggle Sfx on/off:          5 \n");
	
	
	
}


