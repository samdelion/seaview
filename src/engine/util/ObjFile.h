#ifndef SEA_OBJ_FILE_H
#define SEA_OBJ_FILE_H

#include <fstream>
#include <sstream>
#include <vector>

#include "math/Vec4.h"

namespace sea
{
    /**
     *  Class that loads Wavefront obj files.
     *
     *  @todo Fix limitation: every vertex must have a position, texture coordinate and normal.
     */
    class ObjFile
    {
    public:
        /**
         *  Initialize OBJ class with an .obj file, essentially
         *  parses an .obj file and loads it's data into memory.
         *
         *  @param  inStream    std::ifstream &, in file stream, the .obj file.
         *  @return             bool, returns TRUE if initialization was successful, FALSE otherwise.
         */
        bool Init(std::ifstream &inStream);

        /**
         *  Get the vertices in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std::vector<sea_math::Vec4> &, vertices found in OBJ file.
         */
        const std::vector<sea_math::Vec4> &GetVertices();
        /**
         *  Get the normals in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std::vector<sea_math::Vec3> &, normals found in OBJ file.
         */
        const std::vector<sea_math::Vec3> &GetNormals();
        /**
         *  Get the texture coordinates in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std::vector<sea_math::Vec3> &, texture co-ordinates found in OBJ file.
         */
        const std::vector<sea_math::Vec3> &GetTexCoords();
        /**
         *  Get the vertex indices in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std::vector<unsigned int> &, vertex indices found in OBJ file.
         */
        const std::vector< std::vector<unsigned int> > &GetVertexIndices();
        /**
         *  Get the texture co-ordinate indices in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std::vector<unsigned int> &, texture co-ordinate indices found in OBJ file.
         */
        const std::vector< std::vector<unsigned int> > &GetTexCoordIndices();
        /**
         *  Get the normal indices in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std::vector<unsigned int> &, normal indices found in OBJ file.
         */
        const std::vector< std::vector<unsigned int> > &GetNormalIndices();

        /**
         *  Get a packed array of OBJ file data.
         *
         *  Data is in the form:
         *      3 x float   position
         *      3 x float   normal
         *      2 x float   textureCoord
         *
         *  The data is indexed, use GetPackedArrayIndices to draw data.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const void *const, const void pointer to const data.
         *  @todo       Refactor this into a helper class.
         */
        const std::vector<float> &GetPackedArray() const;

        /**
         *  Get a packed array of index data.
         *
         *  These indices properly index the data obtained from the "GetPackedArray" function.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     const std
         *  @todo       Refactor this into a helper class.
         */
        const std::vector< std::vector<unsigned int> > &GetPackedArrayIndices() const;

        /**
         *  Get a contiguous, packed array of index data.
         *
         *  These indices properly index the data obtained from the "GetPackedArray" function.
         *  Contiguous data is necessary to pass to OpenGL.
         *
         *  @pre    Init has been called successfully.
         *
         *  @todo       Refactor this into a helper class.
         */
        const std::vector<unsigned int> &GetPackedArrayIndicesContiguous() const;

        /**
         *  Get the number of sub-meshes (objects) in this Wavefront obj file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     unsigned int, number of sub-meshes (objects) in this Wavefront obj file.
         */
        unsigned int GetNumSubMeshes() const;

        /**
         * Get the names of all the sub meshes in the obj file.
         *
         * The names appear in the same order they appear in the obj file.
         *
         * @return const reference to std::vector<std::string>, names of submeshes in obj file.
         */
        const std::vector<std::string> &GetSubMeshNames() const;

        /** @todo Does this need to be public? */
        struct FaceEntry;
    private:
        /**
         *  Read an OBJ vertex line and return a Vec4 containing the x, y, z and
         *  w co-ordinates of that vertex. Note that the w co-ordinate may
         *  be unused. Note also that the cursor in the stringstream
         *  will be advanced by this method.
         *
         *  @param  lineStream  std::stringstream &, line to read.
         *  @return             sea_math::Vec4, vertex co-ordinates.
         */
        sea_math::Vec4  ReadVertexLine(std::stringstream &lineStream);
        /**
         *  Read an OBJ texture co-ordinate line and return a Vec3
         *  containing the u, v and w components of that texture co-ordinate.
         *  Note that the w co-ordinate may be unused.
         *  Note also that the cursor in the stringstream
         *  will be advanced by this method.
         *
         *  @param  lineStream  std::stringstream &, line to read.
         *  @return             sea_math::Vec3, texture co-ordinates.
         */
        sea_math::Vec3  ReadTexCoordLine(std::stringstream &lineStream);
        /**
         *  Read an OBJ vertex normal line and return a Vec3
         *  containing the x, y and z components of that normal.
         *  Note that the cursor in the stringstream
         *  will be advanced by this method.
         *
         *  @param  lineStream  std::stringstream &, line to read.
         *  @return             sea_math::Vec3, normal.
         */
        sea_math::Vec3  ReadNormalLine(std::stringstream &lineStream);
        /**
         *  Read a OBJ face line and return a structure containing
         *  the vertex, texture co-ordinate and normal indices of that
         *  face.
         *  Note that the cursor in the stringstream will be advanced
         *  by this method.
         *
         *  @param  lineStream  std::stringstream &, line to read.
         *  @return             FaceEntry, single face entry.
         */
        FaceEntry ReadFaceEntry(std::stringstream &lineStream);

        /**
         *  Generate a packed, indexed array of data in the OBJ file.
         *
         *  @pre    Init has been called successfully.
         *
         *  @return     bool, TRUE if successful, FALSE otherwise.
         */
        bool GeneratePackedData();

        std::vector<sea_math::Vec4>                     m_vertices;
        std::vector<sea_math::Vec3>                     m_texCoords;
        std::vector<sea_math::Vec3>                     m_normals;
        std::vector< std::vector<unsigned int> >        m_vertexIndices;
        std::vector< std::vector<unsigned int> >        m_texCoordIndices;
        std::vector< std::vector<unsigned int> >        m_normalIndices;

        std::vector<float>								m_packedData;
        std::vector< std::vector<unsigned int> >		m_packedDataIndices;
        std::vector<unsigned int>						m_packedDataIndicesContiguous;

        std::vector<std::string> m_subMeshNames;
    };
}

#endif  // SEA_OBJ_FILE_H
