#include "engine/util/TextureManager.h"

namespace sea
{
    std::map<std::string, Texture> TextureManager::m_textureMap;

    const Texture &TextureManager::GetTexture(const std::string &texturePath)
    {
        std::map<std::string, Texture>::const_iterator it =
            m_textureMap.find(texturePath);

        // If texture found
        if (it != m_textureMap.end())
        {
            // Return texture
            return it->second;
        }
        // If texture not found
        else
        {
            // Create texture from bmp file
            unsigned int imgWidth, imgHeight;

            GLuint textureGL = Texture::CreateTextureObject(
                texturePath,
                imgWidth, imgHeight);

            Texture texture = Texture(textureGL, imgWidth, imgHeight);

            std::pair<std::map<std::string, Texture>::iterator, bool> ret;

            // Insert texture into map
            ret = m_textureMap.insert(std::pair<std::string, Texture>(texturePath, texture));

            // Return created mesh
            return ret.first->second;
        }
    }
}
