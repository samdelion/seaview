#ifndef SEA_TEXTURE_MANAGER_H
#define SEA_TEXTURE_MANAGER_H

#include <map>
#include <string>

#include "engine/render/Texture.h"

namespace sea
{
    /**
     * Texture manager class is used to keep track of textures that are already
     * loaded in the game so that we don't waste time/memory loading assets
     * we already have loaded.
     */
    class TextureManager
    {
    public:
        /**
         * Load the texture at the given path or return it if it has already
         * been loaded.
         *
         * The path is relative to the "assets" directory and must refer to a
         * 32-bit RGBA BMP file (safest to export from GIMP).
         *
         * @param texturePath const std::string &, path to texture BMP file.
         * @return const Texture &, const reference to loaded Texture.
         */
        const Texture &GetTexture(const std::string &texturePath);
    private:
        // Use texture path as key to map
        static std::map<std::string, Texture> m_textureMap;
    };
}

#endif // SEA_TEXTURE_MANAGER_H
