/**
 * Credit for this code belongs to Sergiu Craitoiu and
 * can be found at http://in2gpu.com/2015/02/25/create-a-game-engine-part-i-shader-manager/
 * @author Sergiu Craitoiu
 * @modified Samuel Evans-Powell
 * @date 27/09/2015
 */
#ifndef SEA_SHADER_MANAGER_H
#define SEA_SHADER_MANAGER_H

#include <string>
#include <map>

#include <GL/glew.h>

namespace sea
{
    /**
     * Handles the compilation, linkage and uniform binding
     * of shaders in the engine.
     */
    class ShaderManager
    {
    public:
        /**
         * Default constructor.
         */
        ShaderManager();
        /**
         * Destructor, free any memory associated with the class.
         */
        ~ShaderManager();
        /**
         * Create a program from a vertex shader and a fragment shader.
         *
         * The shader program is added to a list and referenced by the given
         * shader name.
         *
         * @param programName const std::string &, name (key) to give the shader program.
         * @param vertexShaderFilename const std::string &, path to vertex shader.

         * @param fragmentShaderFilename const std::string &, path to fragment shader.
         */
        void CreateProgram(const std::string &programName,
                           const std::string &vertexShaderFilename,
                           const std::string &fragmentShaderFilename);
        /**
         * Create a program from a vertex shader, geometry shader and
         * fragment shader.
         *
         * The shader program is added to a list and referenced by the given
         * shader name.
         *
         * @param programName const std::string &, name (key) to give the shader program.
         * @param vertexShaderFilename const std::string &, path to vertex shader.

         * @param fragmentShaderFilename const std::string &, path to fragment shader.
         */
        void CreateProgram(const std::string &programName,
                           const std::string &vertexShaderFilename,
                           const std::string &geometryShaderFilename,
                           const std::string &fragmentShaderFilename);
        /**
         * Get the shader program associated with the given name.
         *
         * @param programName const std::string &, name (key) of the
         *                   shader program.
         * @return GLuint, the shader object with the name given.
         */
        static const GLuint GetProgram(const std::string &programName);
    private:
        /**
         * Read a shader file and return the contents
         * of that file in a string.
         *
         * @param  filename const std::string &, path to shader file to load.
         * @return          std::string, contents of shader file.
         */
        std::string ReadShader(const std::string &filename);
        /**
         * Create a shader object of the given type from the
         * shader source.
         *
         * @param shaderType   GLenum, shader type
         *                     (GL_VERTEX_SHADER, GL_FRAGMENT_SHADER etc.)
         * @param shaderSource std::string, raw, uncompiled shader source.
         * @param shaderName   const std::string &, file path of the shader.
         * @return             GLuint, OpenGL shader object identifier.
         */
        GLuint CreateShader(GLenum shaderType,
                            std::string shaderSource,
                            const std::string &shaderName);

        static std::map<std::string, GLuint> programs;
    };
}

#endif // SEA_SHADER_MANAGER_H
