#include <iostream>

#include <GL/glew.h>

#include "engine/util/MeshManager.h"
#include "engine/util/ObjFile.h"
#include "engine/Defines.h"

namespace sea
{
    std::map<std::string, MeshNew> MeshManager::m_meshMap;

    const MeshNew &MeshManager::GetMesh(const std::string &meshPath)
    {
        std::map<std::string, MeshNew>::const_iterator it =
            m_meshMap.find(meshPath);
        // If mesh found
        if (it != m_meshMap.end())
        {
            // Return mesh
            return it->second;
        }
        // Else if mesh not found
        else
        {
            // Create mesh from obj file
            std::ifstream objStream(meshPath);
            ObjFile objFile;
            if (objFile.Init(objStream) == false)
            {
                std::cerr << "Could not load Wavefront OBJ file." << std::endl;
            }

            GLuint vbo = 0;
            glGenBuffers(1, &vbo);
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBufferData(GL_ARRAY_BUFFER,  objFile.GetPackedArray().size() * sizeof(float), &objFile.GetPackedArray()[0], GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            GLuint ibo = 0;
            glGenBuffers(1, &ibo);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, objFile.GetPackedArrayIndicesContiguous().size() * sizeof(unsigned int), &objFile.GetPackedArrayIndicesContiguous()[0], GL_STATIC_DRAW);

            GLuint vao;
            glGenVertexArrays(1, &vao);
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(2);
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), NULL);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (char *)NULL + 6 * sizeof(float));
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
            glBindVertexArray(0);

            sea::MeshNew newMesh = sea::MeshNew(vao);
            // For each submesh
            // Keep track of offset into index buffer to begin pulling indices from
            unsigned int cumulativeOffset = 0;
            for (unsigned int i = 0; i < objFile.GetPackedArrayIndices().size(); ++i)
            {
                sea::SubMesh subMesh = sea::SubMesh(
                    cumulativeOffset,
                    objFile.GetPackedArrayIndices()[i].size()
                );

                newMesh.AddSubMesh(objFile.GetSubMeshNames()[i], sea::SubMesh(subMesh));
                cumulativeOffset += objFile.GetPackedArrayIndices()[i].size();
            }

            std::pair<std::map<std::string, MeshNew>::iterator, bool> ret;
            // Insert mesh into map
            ret = m_meshMap.insert(std::pair<std::string, MeshNew>(meshPath, newMesh));

            // Return created mesh
            return ret.first->second;
        }
    }
}
