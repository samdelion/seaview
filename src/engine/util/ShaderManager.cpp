/**
 * Credit for this code belongs to Sergiu Craitoiu and
 * can be found at http://in2gpu.com/2015/02/25/create-a-game-engine-part-i-shader-manager/
 * @author
 * @modified Samuel Evans-Powell
 * @date 27/09/2015
 */
#include <fstream>
#include <iostream>
#include <vector>

#include "engine/util/ShaderManager.h"

namespace sea
{
    std::map<std::string, GLuint> ShaderManager::programs;

    ShaderManager::ShaderManager()
    {
    }

    ShaderManager::~ShaderManager()
    {
    }

    void ShaderManager::CreateProgram(const std::string &programName,
                                      const std::string &vertexShaderFilename,
                                      const std::string &fragmentShaderFilename)
    {
        // Get shader source
        std::string vsSource = ReadShader(vertexShaderFilename);
        std::string fsSource = ReadShader(fragmentShaderFilename);

        GLuint vs = CreateShader(GL_VERTEX_SHADER,
                                 vsSource,
                                 vertexShaderFilename);
        GLuint fs = CreateShader(GL_FRAGMENT_SHADER,
                                 fsSource,
                                 fragmentShaderFilename);

        // Link shaders to make program
        GLuint program = glCreateProgram();
        glAttachShader(program, vs);
        glAttachShader(program, fs);
        glLinkProgram(program);

        // Check for linker errors
        int linkResult = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &linkResult);

        if (linkResult == GL_FALSE)
        {
            // If there was an error, get information
            int logLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
            std::vector<char> programLog(logLength);
            glGetProgramInfoLog(program, logLength, NULL, &programLog[0]);
            std::cout << "ShaderManager: LINK ERROR" << std::endl
                      << "\t" << &programLog[0];
            // Ensure we return NULL program
            program = 0;
        }

        programs[programName] = program;
    }

    void ShaderManager::CreateProgram(const std::string &programName,
                       const std::string &vertexShaderFilename,
                       const std::string &geometryShaderFilename,
                       const std::string &fragmentShaderFilename)
    {
        // Get shader source
        std::string vsSource = ReadShader(vertexShaderFilename);
        std::string gsSource = ReadShader(geometryShaderFilename);
        std::string fsSource = ReadShader(fragmentShaderFilename);

        GLuint vs = CreateShader(GL_VERTEX_SHADER,
                                 vsSource,
                                 vertexShaderFilename);
        GLuint gs = CreateShader(GL_GEOMETRY_SHADER,
                                 gsSource,
                                 geometryShaderFilename);
        GLuint fs = CreateShader(GL_FRAGMENT_SHADER,
                                 fsSource,
                                 fragmentShaderFilename);

        // Link shaders to make program
        GLuint program = glCreateProgram();
        glAttachShader(program, vs);
        glAttachShader(program, gs);
        glAttachShader(program, fs);
        glLinkProgram(program);

        // Check for linker errors
        int linkResult = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &linkResult);

        if (linkResult == GL_FALSE)
        {
            // If there was an error, get information
            int logLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
            std::vector<char> programLog(logLength);
            glGetProgramInfoLog(program, logLength, NULL, &programLog[0]);
            std::cout << "ShaderManager: LINK ERROR" << std::endl
                      << "\t" << &programLog[0];
            // Ensure we return NULL program
            program = 0;
        }

        programs[programName] = program;
    }

    const GLuint ShaderManager::GetProgram(const std::string &programName)
    {
        return programs.at(programName);
    }

    std::string ShaderManager::ReadShader(const std::string &filename)
    {
        std::string shaderSource;
        std::ifstream file(filename, std::ios::in);

        if (file.good())
        {
            // Move to the end of the file
            file.seekg(0, std::ios::end);
            // Get the current position at end of file (size of file)
            // and size string object to fit
            shaderSource.resize((unsigned int)file.tellg());
            // Move back to start of file
            file.seekg(0, std::ios::beg);
            // Read file
            file.read(&shaderSource[0], shaderSource.size());
            // Close file
            file.close();
        }
        else
        {
            std::cout << "Can't read shader file: " << filename << std::endl;
        }

        return shaderSource;
    }

    GLuint ShaderManager::CreateShader(GLenum shaderType,
                                       std::string shaderSource,
                                       const std::string &shaderName)
    {
        int result = 0;

        GLuint shader = glCreateShader(shaderType);
        const char *shaderCode = shaderSource.c_str();
        const int shaderCodeSize = shaderSource.size();

        // Compile shader
        glShaderSource(shader, 1, &shaderCode, &shaderCodeSize);
        glCompileShader(shader);

        // Check for errors
        glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

        if (result == GL_FALSE)
        {
            // If there is an error, get information.
            int logLength = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
            std::vector<char> shaderLog(logLength);
            glGetShaderInfoLog(shader, logLength, NULL, &shaderLog[0]);
            std::cout << "ERROR compiling shader: " << shaderName << std::endl;

            std::cout << "ShaderManager: COMPILE ERROR" << std::endl
                      << "\t" << &shaderLog[0];
        }

        return shader;
    }
}
