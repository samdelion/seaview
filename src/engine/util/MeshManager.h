#ifndef SEA_MESH_MANAGER_H
#define SEA_MESH_MANAGER_H

#include <map>
#include <string>

#include "engine/render/MeshNew.h"

namespace sea
{
    /**
     * Mesh manager class is used to keep track of meshes that are already
     * loaded in the game so that we don't waste time/memory loading assets
     * we already have loaded.
     */
    class MeshManager
    {
    public:
        /**
         * Load the mesh at the given path or return it if it has already been
         * loaded.
         *
         * The path is relative to the "assets" directory and must refer to a
         * Wavefront obj file.
         *
         * @param meshPath const std::string &, path to mesh obj file.
         * @return const MeshNew &, const reference to loaded Mesh.
         */
        const MeshNew &GetMesh(const std::string &meshPath);
    private:
        // Uses mesh path as key to map
        static std::map<std::string, MeshNew> m_meshMap;
    };
}

#endif // SEA_MESH_MANAGER_H
