#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <map>

#include "math/Vec3.h"
#include "engine/Defines.h"

#include "ObjFile.h"

namespace sea
{
    // Face structure, holds indices to
    // data in vertex, texcoord and normal
    // arrays.
    struct ObjFile::FaceEntry
    {
        // Vertex index
        unsigned int    v;
        // Vertex texcoord index
        unsigned int    vt;
        // Vertex normal index
        unsigned int    vn;

        bool operator==(const ObjFile::FaceEntry &other) const
        {
            return (other.v == v && other.vt == vt && other.vn == vn);
        }
    };

    bool ObjFile::Init(std::ifstream &inStream)
    {
        bool result = false;

        std::string     line, key;

        // Some obj files are made up of smaller sub meshes.
        // Every time we encounter a line declaring a new submesh,
        // we append further indices to a new array, containing the indices
        // for just that sub mesh
        unsigned int    currentSubMesh = 0;

        // Only attempt to parse file while it is still good
        // (i.e. none of the error flags are set, such as eof)
        while (inStream.good())
        {
            // The key is what is used to identify the type of each
            // line. In an .obj file:
            //      v - vertex
            //      vn - normal
            //      # - comment, etc...
            //  Make sure to clear key for each line we read.
            key = "";

            // Read one line of file
            std::getline(inStream, line);

            // Create string stream out of line
            std::stringstream   lineStream(line);

            // Read key into key variable.
            lineStream >> key;

            // If we find a vertex line
            if (key == "v")
            {
                m_vertices.push_back(ReadVertexLine(lineStream));
            }
            // If we find a texcoord line
            else if (key == "vt")
            {
                m_texCoords.push_back(ReadTexCoordLine(lineStream));
            }
            // If we find a normal line
            else if (key == "vn")
            {
                m_normals.push_back(Normalize(ReadNormalLine(lineStream)));
            }
            // If we find a sub mesh line
            else if (key == "o")
            {
                std::string subMeshName;
                lineStream >> subMeshName;

                m_subMeshNames.push_back(subMeshName);

                // Only increase current sub mesh after the first mesh
                if (m_vertexIndices.size() > 0)
                {
                    ++currentSubMesh;
                }

                // Append new sub mesh array to index arrays
                m_vertexIndices.push_back(std::vector<unsigned int>());
                m_texCoordIndices.push_back(std::vector<unsigned int>());
                m_normalIndices.push_back(std::vector<unsigned int>());
            }
            // If we find a face line
            else if (key == "f")
            {
                // Read all face entries on a line
                while (lineStream.good())
                {
                    FaceEntry face = ReadFaceEntry(lineStream);

                    // Read face line sets member to 0 if there are no
                    // indices for that attribute, hence check.
                    if (face.v != 0)
                    {
                        // .obj files index starting from 1, hence decrement index.
                        // (we start from 0)
                        m_vertexIndices[currentSubMesh].push_back(face.v - 1);
                    }

                    if (face.vt != 0)
                    {
                        m_texCoordIndices[currentSubMesh].push_back(face.vt - 1);
                    }

                    if (face.vn != 0)
                    {
                        m_normalIndices[currentSubMesh].push_back(face.vn - 1);
                    }
                }
            }
        }

        // Make sure we read whole file
        result = !inStream.bad() && inStream.eof();
        result = result && GeneratePackedData();

        return result;
    }

    const std::vector<sea_math::Vec4> &ObjFile::GetVertices()
    {
        return m_vertices;
    }

    const std::vector<sea_math::Vec3> &ObjFile::GetTexCoords()
    {
        return m_texCoords;
    }

    const std::vector<sea_math::Vec3> &ObjFile::GetNormals()
    {
        return m_normals;
    }

    const std::vector< std::vector<unsigned int> > &ObjFile::GetVertexIndices()
    {
        return m_vertexIndices;
    }

    const std::vector< std::vector<unsigned int> > &ObjFile::GetTexCoordIndices()
    {
        return m_texCoordIndices;
    }

    const std::vector< std::vector<unsigned int> > &ObjFile::GetNormalIndices()
    {
        return m_normalIndices;
    }

    const std::vector<float> &ObjFile::GetPackedArray() const
    {
        return m_packedData;
    }

    const std::vector< std::vector<unsigned int> > &ObjFile::GetPackedArrayIndices() const
    {
        return m_packedDataIndices;
    }

    const std::vector<unsigned int> &ObjFile::GetPackedArrayIndicesContiguous() const
    {
        return m_packedDataIndicesContiguous;
    }

    unsigned int ObjFile::GetNumSubMeshes() const
    {
        return m_vertexIndices.size();
    }

    sea_math::Vec4 ObjFile::ReadVertexLine(std::stringstream &lineStream)
    {
        sea_math::Vec4  vertex;

        // Loop thru each vertex co-ordinate and read it in
        for (int i = 0; i < 4 && lineStream.good(); ++i)
        {
            // Make sure to advance past any white space (ws)
            lineStream >> std::ws >> vertex[i];
        }

        return vertex;
    }

    sea_math::Vec3 ObjFile::ReadTexCoordLine(std::stringstream &lineStream)
    {
        sea_math::Vec3  texCoord;

        // Loop thru each vertex texture co-ordinate and read it in
        for (int i = 0; i < 3 && lineStream.good(); ++i)
        {
            lineStream >> std::ws >> texCoord[i];
        }

        return texCoord;
    }

    sea_math::Vec3 ObjFile::ReadNormalLine(std::stringstream &lineStream)
    {
        sea_math::Vec3  normal;

        for (int i = 0; i < 3 && lineStream.good(); ++i)
        {
            lineStream >> std::ws >> normal[i];
        }

        return normal;
    }

    ObjFile::FaceEntry ObjFile::ReadFaceEntry(std::stringstream &lineStream)
    {
        FaceEntry face;
        face.v = 0;
        face.vt = 0;
        face.vn = 0;

        if (lineStream.good())
        {
            lineStream >> std::ws >> face.v;

            // Is next character a "/"?
            if (lineStream.peek() == '/')
            {
                // If it is, remove it from stream
                lineStream.get();

                // If immediately followed by another "/"
                if (lineStream.peek() == '/')
                {
                    // If it is, remove it from stream
                    lineStream.get();

                    // Texture coordinate index does not exist
                    // Read face entry normal
                    lineStream >> face.vn;
                }
                // If not immediately followed by another "/"
                else
                {
                    // Must be a texture co-ordinate index
                    lineStream >> face.vt;

                    // Is there a normal index after this?
                    if (lineStream.peek() == '/')
                    {
                        lineStream.get();

                        lineStream >> face.vn;
                    }
                }
            }
        }

        return face;
    }

    bool ObjFile::GeneratePackedData()
    {
        bool result = true;

        // Map unique composition of vertex, texcoord and normal index to unique index we'll use for drawing
        std::vector< std::pair<FaceEntry, unsigned int> > faceIndexExisting;

        // Data is split by submesh, loop thru all submeshes and regroup data
        for (unsigned int subMesh = 0; subMesh < m_vertexIndices.size(); ++subMesh)
        {
            m_packedDataIndices.push_back(std::vector<unsigned int>());

            // Loop thru indices (number of vertex indices should be the same
            // as the number of texCoord and normal indices)
            for (unsigned int i = 0; i < m_vertexIndices[subMesh].size(); ++i)
            {
                // For each entry
                FaceEntry entry;

                entry.v = m_vertexIndices[subMesh][i];
                // Only load texture co-ordinates if they exist
                if (m_texCoordIndices[subMesh].size() != 0)
                {
                    entry.vt = m_texCoordIndices[subMesh][i];
                }
                else
                {
                    entry.vt = 0;
                }
                entry.vn = m_normalIndices[subMesh][i];

                // Attempt to find identical vertex data in the array
                std::vector< std::pair<FaceEntry, unsigned int> >::const_iterator it;

                bool found = false;
                for (it = faceIndexExisting.begin(); it != faceIndexExisting.end() && found == false; ++it)
                {
                    if (entry == it->first)
                    {
                        found = true;
                        // Break to stop for loop advancing the iterator
                        break;
                    }
                }

                // If identical entry not found, must be new unique set of indices
                if (!found)
                {
                    // Add new index
                    m_packedDataIndices[subMesh].push_back(faceIndexExisting.size());

                    // Add new index to our record of faces
                    faceIndexExisting.push_back(std::pair<FaceEntry, unsigned int>(entry, faceIndexExisting.size()));

                    // Add new data to packed array
                    m_packedData.push_back(m_vertices[entry.v].x);
                    m_packedData.push_back(m_vertices[entry.v].y);
                    m_packedData.push_back(m_vertices[entry.v].z);
                    m_packedData.push_back(m_normals[entry.vn].x);
                    m_packedData.push_back(m_normals[entry.vn].y);
                    m_packedData.push_back(m_normals[entry.vn].z);
                    if (m_texCoords.size() != 0)
                    {
                        m_packedData.push_back(m_texCoords[entry.vt].x);
                        m_packedData.push_back(m_texCoords[entry.vt].y);
                    }
                    else
                    {
                        m_packedData.push_back(0);
                        m_packedData.push_back(0);
                    }
                }
                // Else data already exists in packed array
                else
                {
                    // Add index of existing data to indices
                    m_packedDataIndices[subMesh].push_back(it->second);
                }
            }
        }

        // pack index data into one contiguous array for OpenGL's sake
        for (unsigned int i = 0; i < m_packedDataIndices.size(); ++i)
        {
            for (unsigned int j = 0; j < m_packedDataIndices[i].size(); ++j)
            {
                m_packedDataIndicesContiguous.push_back(m_packedDataIndices[i][j]);
            }
        }

        return result;
    }

    const std::vector<std::string> &ObjFile::GetSubMeshNames() const
    {
        return m_subMeshNames;
    }
}
