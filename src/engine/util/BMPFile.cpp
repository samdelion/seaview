/**
 *  @author  Samuel Evans-Powell
 *  @date    09/08/2015
 *  
 *  Created using information from: https://web.archive.org/web/20080612225149/http://www.fortunecity.com/skyscraper/windows/364/bmpffrmt.html#bmih. All information regarding the structure of the BMP file is Copyright 1998 Stefan Hetzl.
 *  Additional information was gathered from: https://en.wikipedia.org/wiki/BMP_file_format.
 *  
 *  @todo   Currently does not support OS/2 bitmaps, only Windows bitmaps.
 */
#include "util/BMPFile.h"

namespace sea
{
    /* Stop compiler from padding these structures
     * so that they align to byte boundaries.
     * This is super important!
     */
    #pragma pack(1)

    struct BMPFile::BitmapFileHeader
    {
        // Type of file. Always set to 'BM'.
        uint16_t    type;
        // Size of file in bytes.
        uint32_t    size;
        // Depends on the application that creates the image.
        uint16_t    reserved1;
        // Depends on the application that creates the image.
        uint16_t    reserved2;
        // Offset to bitmap data.
        uint32_t    offset;
    };

    struct BMPFile::BitmapInfoHeader
    {
        // Size of the BitmapInfoHeader (bytes).
        uint32_t    size;
        // Width of the image in pixels.
        uint32_t    imgWidth;
        // Height of the image in pixels.
        uint32_t    imgHeight;
        // Number of planes of target device, must be 1.
        uint16_t    planes;
        // Number bits per pixel.
        uint16_t    bpp;
        // Type of compression, usually 0 (i.e. no compression).
        uint32_t    compressionMethod;
        // Uncompressed size of image data (bytes), if no compression may be set to 0.
        uint32_t    uncompressedSize;
        // Horizontal resolution of the image in pixels per meter.
        uint32_t    ppmHorizontal;
        // Vertical resolution of the image in pixels per meter.
        uint32_t    ppmVertical;
        // Num of colours used in the image, if 0, number of colours is calculated using the bpp field.
        uint32_t    numColours;
        // Num of colours that are important to the bitmap, if 0, all colours are important.
        uint32_t    numImportant;
    };

    // Extra data included in V3 Header
    struct BMPFile::BitmapInfoV3Header
    {
        // Masks should be contiguous and non-overlapping
        // Which pixel bits are red
        uint32_t    redMask;
        // Which pixel bits are green
        uint32_t    greenMask;
        // Which pixel bits are blue
        uint32_t    blueMask;
        // Which pixel bits are alpha
        uint32_t    alphaMask; 
    };
    
    #pragma pack()

    typedef enum
    {
        BITMAP_CORE_HEADER = 12,
        BITMAP_OS2_HEADER = 64,
        BITMAP_INFO_HEADER = 40,
        BITMAP_V2_INFO_HEADER = 52,
        BITMAP_V3_INFO_HEADER = 56,
        BITMAP_V4_HEADER = 108,
        BITMAP_V5_HEADER = 124
    } BitmapInfoHeaderVersion;

    typedef enum
    {
        BI_RGB = 0,
        BI_RLE8 = 1,
        BI_RLE4 = 2,
        BI_BITFIELDS = 3,
        BI_JPEG = 4,
        BI_PNG = 5,
        BI_ALPHABITFIELDS = 6,
        BI_CMYK = 11,
        BI_CMYKRLE8 = 12,
        BI_CMKYRLE4 = 13 
    } BitmapCompressionMethod;
    
    struct BMPFile::Mask
    {
        // Full mask
        uint32_t    mask;
        /*  When we apply the mask, the remaining bits
         *  will exist at an arbitrary point in the uint.
         *  We need to shift these bits down to the least
         *  significant bits, the shift value records how
         *  many places we need to shift down.
         */
        uint8_t    shift;
    };
 
    BMPFile::BMPFile()
    {
        m_pixels = NULL;
        m_imgWidth = 0;
        m_imgHeight = 0;
    }

    BMPFile::~BMPFile()
    {
        if (m_pixels != NULL)
        {
            delete[] m_pixels;
        }
    }

    bool BMPFile::Init(FILE *stream)
    {
        bool result = false;

        BitmapFileHeader *fileHeader = new struct BitmapFileHeader;
        BitmapInfoHeader *infoHeader = new struct BitmapInfoHeader;

        if (stream != NULL)
        {
            if (ReadFileHeader(stream, fileHeader))
            {
                if (ReadInfoHeader(stream, infoHeader))
                {
                    switch (infoHeader->size)
                    {
                        case BitmapInfoHeaderVersion::BITMAP_V3_INFO_HEADER:
                        case BitmapInfoHeaderVersion::BITMAP_V4_HEADER:
                        case BitmapInfoHeaderVersion::BITMAP_V5_HEADER:
                        {
                            BitmapInfoV3Header *v3InfoHeader = new BitmapInfoV3Header();
                            // Read in mask information
                            ReadInfoV3Header(stream, v3InfoHeader);

                            Mask masks[4]; 
                            PopulateMasks(masks, v3InfoHeader);

                            delete v3InfoHeader;

                            result = PopulatePixels(&m_pixels, stream, fileHeader, infoHeader, masks);
                            break; 
                        }
                        default:
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
        }

        m_imgWidth = infoHeader->imgWidth;
        m_imgHeight = infoHeader->imgHeight;

        delete fileHeader;
        delete infoHeader;

        return result;
    }

   bool BMPFile::PopulatePixels(Pixel **pixels, FILE *stream, const BitmapFileHeader *const fileHeader, const BitmapInfoHeader *const infoHeader, const Mask *const masks)
    {
        bool result = false;
        
        if (stream && masks && fileHeader && infoHeader)
        {
            // Go to start of image data
            fseek(stream, fileHeader->offset, SEEK_SET);
            // Size in bytes of a single row of pixels
            uint64_t rowSize = infoHeader->bpp * infoHeader->imgWidth; 
            // Size in bytes of a single row of raw data
            uint64_t rowRawSize = ((infoHeader->bpp * infoHeader->imgWidth + 31) / 32) * 4;
            // Size in bytes of the raw pixel array
            uint64_t rawDataSize = rowRawSize * infoHeader->imgHeight;
            uint32_t *rawData = new uint32_t[rawDataSize];
            // Read all raw image data (includes padding)
            fread((void *)rawData, rawDataSize, 1, stream);

            *pixels = new Pixel[infoHeader->imgWidth * infoHeader->imgHeight];
            uint32_t *itr = rawData;
            for (uint32_t row = 0; row < infoHeader->imgHeight; ++row)
            {
                for (uint32_t col = 0; col < infoHeader->imgWidth; ++col)
                {
                    uint64_t pos = row * infoHeader->imgWidth + col;
                    (*pixels)[pos].r = (*itr & masks[0].mask) >> masks[0].shift;
                    (*pixels)[pos].g = (*itr & masks[1].mask) >> masks[1].shift;
                    (*pixels)[pos].b = (*itr & masks[2].mask) >> masks[2].shift;
                    (*pixels)[pos].a = (*itr & masks[3].mask) >> masks[3].shift;
                    ++itr;
                }
            }
            
            delete[] rawData;

            result = true;
        }

        return result;
    }

    void BMPFile::PopulateMasks(Mask *const masks, const BitmapInfoV3Header *const v3InfoHeader)
    {
        // Create red, green, blue and alpha masks
        masks[0].mask = v3InfoHeader->redMask;
        masks[0].shift = CalculateMaskShift(masks[0].mask);
        masks[1].mask = v3InfoHeader->greenMask;
        masks[1].shift = CalculateMaskShift(masks[1].mask);
        masks[2].mask = v3InfoHeader->blueMask;
        masks[2].shift = CalculateMaskShift(masks[2].mask);
        masks[3].mask = v3InfoHeader->alphaMask;
        masks[3].shift = CalculateMaskShift(masks[3].mask);
    }

    uint8_t BMPFile::CalculateMaskShift(uint32_t mask)
    {
        uint8_t shift = 0;
       
        // Ensure we don't have an empty mask (mask with no set (1) bits)
        // otherwise the while loop will loop indefinitely.
        if (mask != 0)
        {
            // Bitwise AND mask with 1, if result is 0, we need to continue shifting 
            while (!(mask & 1))
            {
                // Right shift mask
                mask = mask >> 1; 
                ++shift;
            } 
        }

        return shift;
    }

    bool BMPFile::ReadInfoV3Header(FILE *stream, BitmapInfoV3Header *v3InfoHeader)
    {
        bool result = false;

        fseek(stream, sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader), SEEK_SET);

        size_t numToRead = 1;
        size_t numRead = fread((void *)v3InfoHeader, sizeof(BitmapInfoV3Header), numToRead, stream);
        
        if (numToRead == numRead)
        {
            result = true;
        }
        
        return result;
    }

    bool BMPFile::ReadFileHeader(FILE *stream, BitmapFileHeader *fileHeader)
    {
        bool result = false;
   
        if (fileHeader != NULL)
        { 
            // Navigate to beginning of stream
            fseek(stream, 0, SEEK_SET);
            
            // Read in Bitmap File Header from stream
            size_t numToRead = 1;
            size_t numRead = fread((void *)fileHeader, sizeof(BitmapFileHeader), numToRead, stream);
      
            // If fread didn't read as many file headers as we asked it to,
            // we know an error occurred. 
            if (numRead == numToRead) 
            {
                result = true;
            } 
        }
        
        return result;
    } 

    bool BMPFile::ReadInfoHeader(FILE *stream, BitmapInfoHeader *infoHeader)
    {
        bool result = false;
        
        if (infoHeader != NULL)
        {
            // Navigate to the start of the BitmapInfoHeader
            fseek(stream, sizeof(BMPFile::BitmapFileHeader), SEEK_SET);

            // Read the beginning of the BitmapInfoHeader, to determine it's size.
            size_t numToRead = 1;
            size_t numRead = fread((void *)infoHeader, sizeof(BMPFile::BitmapInfoHeader), numToRead, stream);
           
            if (numRead == numToRead)
            {
                result = true;
            }
        }
            
        return result;
    }

    unsigned int BMPFile::GetImageWidth() const
    {
        return m_imgWidth;
    } 
    
    unsigned int BMPFile::GetImageHeight() const
    {
        return m_imgHeight;
    }

    const BMPFile::Pixel *BMPFile::GetImageData() const
    {
        return m_pixels;
    }
}
