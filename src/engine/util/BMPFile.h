/**
 *  @author Samuel Evans-Powell
 *  @date   09/08/2015
 */
#ifndef SEA_BMP_FILE_H
#define SEA_BMP_FILE_H

#include <cstdio>
#include <fstream>
#include <cstdint>
#include <iostream>

#include <GL/glew.h>

namespace sea
{
    /**
     *  Class for loading BMP files.
     */
    class BMPFile
    {
    public:
        /**
         *  Struct used to contain pixel data.
         */
        struct Pixel
        {
            // Red component of pixel
            uint32_t    r;
            // Green component of pixel
            uint32_t    g;
            // Blue component of pixel
            uint32_t    b;
            // Alpha component of pixel
            uint32_t    a;
        };

        /**
         *  Construct BMP file and initialize memory.
         *  Does not actually load any image.
         */
        BMPFile();
        /**
         *  Free all memory associated with the BMPFile.
         */
        ~BMPFile();

        /**
         *  Initialize BMP file.
         *
         *  BMP file must be initalized with a stream (an open file).
         *
         *  @param  stream  FILE *, pointer to stream.
         *  @return         bool, returns TRUE if initialization was succesful, FALSE otherwise.
         */
        bool Init(FILE *stream);
        //bool Init(std::fstream &stream);

        /**
         *  Get the width of the image in pixels.
         *
         *  @pre    Init function has been called and returned TRUE.
         *
         *  @return     unsigned int, width of the image in pixels.
         */
        unsigned int GetImageWidth() const;
        /**
         *  Get the height of the image in pixels.
         *
         *  @pre    Init function has been called and returned TRUE.
         *
         *  @return     unsigned int, height of the image in pixels.
         */
        unsigned int GetImageHeight() const;
        /**
         *  Get the raw image data of the BMP file.
         *  BMP files store image data 'upside down' so raw image data obtained from
         *  this function will also be upside down.
         *
         *  @pre    Init function has been called and returned TRUE.
         *  @post   returned pointer is only valid as long as the BMPFile object exists.
         *
         *  @return     pointer to const Pixel, pointer to an array of Pixel values, the
         *              raw bmp image data.
         */
        const Pixel *GetImageData() const;

    private:
        struct BitmapFileHeader;
        struct BitmapInfoHeader;
        struct BitmapInfoV3Header;
        struct Mask;

        /**
         *  Read the BMP file header into the given BitmapFileHeader structure.
         *  Returns TRUE if the read was successful and FALSE otherwise.
         *  Stream cursor may move after this operation.
         *
         *  @pre    Valid and open BMP file in stream.
         *  @pre    fileHeader contains pointer to BitmapFileHeader with enough
         *          memory allocated to hold a BitmapFileHeader.
         *
         *  @param  stream      pointer to FILE, BMP file stream.
         *  @param  fileHeader  pointer to BitmapFileHeader structure, to fill
         *                      with data from BMP file.
         *  @return     bool, TRUE if the read was successful and FALSE otherwise.
         */
        bool ReadFileHeader(FILE *stream, BitmapFileHeader *fileHeader);
        /**
         *  Read the BMP info header into the given BitmapInfoHeader structure.
         *  Returns TRUE if the read was successful and FALSE otherwise.
         *  Stream cursor may move after this operation.
         *
         *  @pre    Valid and open BMP file in stream.
         *  @pre    infoHeader contains pointer to BitmapInfoHeader with enough
         *          memory allocated to hold a BitmapInfoHeader.
         *
         *  @param  stream      pointer to FILE, BMP file stream.
         *  @param  infoHeader  pointer to BitmapInfoHeader structure, to fill
         *                      with data from the BMP file.
         *  @return     bool, TRUE if the read was successful, FALSE otherwise.
         */
        bool ReadInfoHeader(FILE *stream, BitmapInfoHeader *infoHeader);
        /**
         *  Read the BMP V3 Info header into the given BitmapInfoV3Header structure.
         *  Returns TRUE if the read was successful and FALSE otherwise.
         *  Strean cursor may move after this operation.
         *
         *  @pre    Valid and open BMP file in stream.
         *  @pre    v3InfoHeader contains pointer to BitmapInfoV3Header with enough
         *          memory allocated to hold a BitmapFileHeader.
         *
         *  @param  stream          pointer to FILE, BMP file stream.
         *  @param  v3InfoHeader    pointer to BitmapInfoV3Header structure, to fill
         *                          with data from BMP file.
         *  @return     bool, TRUE if the read was successful, FALSE otherwise.
         */
        bool ReadInfoV3Header(FILE *stream, BitmapInfoV3Header *v3InfoHeader);
        /**
         *  Calculate and return the number of unset (0) bits between the lower end of
         *  a 32 bit mask and the first set (1) bit.
         *  Or the size of the bitwise shift that needs to occur once the mask is applied.
         *
         *  @param  mask    uint32_t, 32-bit mask.
         *  @return         uint8_t, number of unset bits between the lower end of the
         *                  mask and the first set bit.
         */
        uint8_t CalculateMaskShift(uint32_t mask);
        /**
         *  Populate the red, green, blue and alpha masks from the given BitmapInfoV3Header
         *  structure.
         *
         *  @pre    masks points to an array of 4 Mask structures (one for each channel).
         *
         *  @param  masks           const pointer to Mask, array of masks to populate with data.
         *  @param  v3InfoHeader    const pointer to const BitmapInfoV3Header, structure
         *                          to get mask information from.
         */
        void PopulateMasks(Mask *const masks, const BitmapInfoV3Header *const v3InfoHeader);
        /**
         *  Populate the pixels paramter with the raw image data from the BMP file.
         *  BMP files store image data 'upside down' so raw image data obtained from
         *  this function will also be upside down.
         *
         *  @pre    pixels parameter should not already be pointing to any data as this
         *          function will overwrite the pointer.
         *  @post   pixels parameter points to an array of Pixel structures of size
         *          imgWidth * imgHeight.
         *
         *  @param      pixels      pointer to pointer to Pixel to fill with raw image data.
         *  @param      stream      pointer to FILE, BMP image file stream.
         *  @param      fileHeader  const pointer to const BitmapFileHeader.
         *  @param      infoHeader  const pointer to const BitmapInfoHeader.
         *  @param      masks       const pointer to const Mask, array of 4 masks describing
         *                          any bitmasks used.
         *  @return                 bool, TRUE if the function was successful, false otherwise.
         */
        bool PopulatePixels(Pixel **pixels, FILE *stream, const BitmapFileHeader *const fileHeader, const BitmapInfoHeader *const infoHeader, const Mask *const masks);

        Pixel                       *m_pixels;
        unsigned int                m_imgWidth, m_imgHeight;
    };
}

#endif  // SEA_BMP_FILE_H
