#ifndef WGT_QUAT
#define WGT_QUAT

#include "Precision.h"
#include "Mat4.h"

namespace sea_math
{
    class Mat4;
    /**
     *  Quaternion class.
     */
	class Quat
    {
	public:
        /**
         *  Initialize quaternion with given values.
         *
         *  @param  _x  scalar, first component of quaternion.
         *  @param  _y  scalar, second component of quaternion.
         *  @param  _z  scalar, third component of quaternion.
         *  @param  _w  scalar, fourth component of quaternion.
         */
		Quat(scalar _x = 0.0f, scalar _y = 0.0f, scalar _z = 0.0f, scalar _w = 1.0f);
        /**
         *  Initialize quaternion with an axis and a fourth component.
         *
         *  @param  axis    Vec3, axis to initialize quaternion with.
         *  @param  _w      scalar, fourth component of quaternion.
         */
		Quat(Vec3 axis, scalar _w);
		
        /**
         *  Subscript operator, access components of quaternion.
         *
         *  @pre    index < 4
         *
         *  @param  index   unsigned int, index of component to access.
         *  @return         scalar, component of quaternion at given index.
         */
		scalar &operator[] (unsigned int index);
        /**
         *  Const subscript operator, access components of quaternion.
         *
         *  @pre    index < 4
         *
         *  @param  index   unsigned int, index of component to access.
         *  @return         scalar, component of quaternion at given index.
         */
		const scalar &operator[] (unsigned int index) const;
		/**
		 *	Equivalence operator.k
		 *
		 * 	Compare two quaternions and return TRUE if they are equal or FALSE otherwise.
		 *
		 * 	@param 	other 	const Quat reference, Quat to compare to this Quat.
		 * 	@return 		bool, TRUE if two quaternions are equal, FALSE otherwise.
		 */
        bool operator==(const Quat &other) const;

        /**
         *  Rotate quaternion about an axis by a given angle.
         *
         *  @post   Quaternion is normalized.
         *
         *  @param  axis        const Vec3 &, axis to rotate quaternion about.
         *  @param  angleRad    scalar, radian angle to rotate quaternion by.
         */
		void Offset(const Vec3 &axis, scalar angleRad);
        /**
         *  Rotate quaternion about an axis by a given angle.
         *
         *  @post   Quaternion is normalized.
         *
         *  @param  xAxis       scalar, xaxis to rotate quaternion about.
         *  @param  yAxis       scalar, yaxis to rotate quaternion about.
         *  @param  zAxis       scalar, zaxis to rotate quaternion about.
         *  @param  angleRad    scalar, radian angle to rotate quaternion by.
         */
		void Offset(scalar xAxis, scalar yAxis, scalar zAxis, scalar angleRad);

		scalar x, y, z, w;
	};

	// Binary operators
    /**
     *  Multiply two quaternions.
     *
     *  @param  q1  const Quat &, quaternion to multiply.
     *  @param  q2  const Quat &, quaternion to multiply by.
     *  @return     Quat, resulting quaternion.
     */
	Quat operator*(const Quat &q1, const Quat &q2);

	// Unary operators
    /**
     *  Invert a quaternion.
     *
     *  @post   Does not modify quaternion argument.
     *
     *  @param  q1  const Quat &, quaternion to invert.
     *  @return     Quat, inverted quaternion.
     */
	Quat operator-(const Quat &q);

    /**
     *  Return the magnitude of the quaternion.
     *
     *  @param  q   const Quat &, quaternion to get magnitude of.
     *  @return     scalar, magnitude of quaternion.
     */
	scalar 	Magnitude(const Quat &q);
    /**
     *  Return a normalized version of the given quaternion.
     *
     *  @post   The given quaternion is not changed.
     *  
     *  @param      q   const Quat &, quaternion to normalize.
     *  @return         Quat, normalized quaternion.
     */
	Quat 	Normalize(const Quat &q);
    /**
     *  Return the quaternion dot product of two given quaternions.
     *
     *  @param  q1  const Quat &, quaternion argument.
     *  @param  q2  const Quat &, quaternion argument.
     *  @return     scalar, dot product of two given quaternions.
     */
	scalar 	Dot(const Quat &q1, const Quat &q2);
    /** @todo   Implement Quaternion slerp */
	// Quat 	slerp(const Quat &q1, const Quat &q2, scalar t);

	// Casts
    /**
     *  Convert the quaternion into a rotation transformation matrix.
     *
     *  @param  q   const Quat &, quaternion to convert to transformation matrix.
     *  @return     Mat4, returned transformation matrix.
     */
	Mat4 CastMat4(const Quat &q);
}

#endif // WGT_QUAT
