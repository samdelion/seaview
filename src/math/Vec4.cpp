#include <cmath>
#include <cassert>

#include "Vec4.h"

namespace sea_math
{
	Vec4::Vec4(scalar _x, scalar _y, scalar _z, scalar _w) : 
		x(_x), y(_y), z(_z), w(_w)
	{
	}

	Vec4::Vec4(const Vec3 &vec, scalar _w) : 
		x(vec.x), y(vec.y), z(vec.z), w(_w)
	{
	}

	Vec4::Vec4(const Vec4 &other) : 
		x(other.x), y(other.y), z(other.z), w(other.w)
	{
	}

	Vec4 Vec4::operator=(const Vec4 &other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
		w = other.w;

		return (*this);
	}

	scalar &Vec4::operator[](unsigned int index)
	{
		assert(index < 4 && "Attempt to access outside Vec4 bounds.\n");

		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		case 3:
			return w;
			break;
		default:
			return x;
			break;
		}
	}

	const scalar &Vec4::operator[](unsigned int index) const
	{
		assert(index < 4 && "Attempt to access outside Vec4 bounds.\n");

		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		case 3:
			return w;
			break;
		default:
			return x;
			break;
		}
	}


	Vec4 Vec4::operator+=(const Vec4 &other)
	{
		x += other.x;
		y += other.y;
		z += other.z;
		w += other.w;

		return (*this);
	}

	Vec4 Vec4::operator-=(const Vec4 &other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;
		w -= other.w;

		return (*this);
	}

	bool Vec4::operator==(const Vec4 &other) const
	{
		return (fabs(x - other.x) < FLOAT_ACCURACY &&
				fabs(y - other.y) < FLOAT_ACCURACY &&
				fabs(z - other.z) < FLOAT_ACCURACY &&
				fabs(w - other.w) < FLOAT_ACCURACY);
	}

	Vec4 operator+(const Vec4 &v1, const Vec4 &v2)
	{
		return (
			Vec4(
				v1.x + v2.x,
				v1.y + v2.y,
				v1.z + v2.z,
				v1.w + v2.w
			)
		);
	}

	Vec4 operator-(const Vec4 &v1, const Vec4 &v2)
	{
		return (
			Vec4(
				v1.x - v2.x,
				v1.y - v2.y,
				v1.z - v2.z,
				v1.w - v2.w
			)
		);
	}

	Vec4 operator*(scalar factor, const Vec4 &vec)
	{
		return (
			Vec4(
				factor * vec.x,
				factor * vec.y,
				factor * vec.z,
				factor * vec.w
			)
		);
	}

	Vec4 operator*(const Vec4 &vec, scalar factor)
	{
		return (
			Vec4(
				factor * vec.x,
				factor * vec.y,
				factor * vec.z,
				factor * vec.w
			)
		);
	}

	scalar Dot(const Vec4 &v1, const Vec4 &v2)
	{
		return (
			v1.x * v2.x +
			v1.y * v2.y +
			v1.z * v2.z +
			v1.w * v2.w
		);
	}

	scalar Magnitude(const Vec4 &vec)
	{
		return (sqrt(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2) + pow(vec.w, 2)));
	}

	Vec4 Normalize(const Vec4 &vec)
	{
		scalar mag = Magnitude(vec);

		if (mag == 0)
		{
			return vec;
		}
		else
		{
			return (Vec4(vec.x/mag, vec.y/mag, vec.z/mag, vec.w/mag));
		}
	}
}
