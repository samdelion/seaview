#ifndef SEA_FRUSTUM_H
#define SEA_FRUSTUM_H

#include "math/Vec3.h"
#include "math/Plane.h"

namespace sea_math
{
	class Frustum
	{
	public:
		enum 	FrustumSide {Near, Far, Top, Bottom, Left, Right, NumPlanes};

		/**
		 *	Construct a Frustum with some default values.
		 *
		 * 	Object should be initialized before use (see Init() methods).
		 */
		Frustum();
		/**
		 *	Initializes the Frustum with the given parameters.
		 *
		 * 	Returns	true if initialization is successful and 
		 * 	false otherwise.
		 *
		 * 	@param 	fov 			scalar, field of view of frustum.
		 * 	@param 	aspectRatio 	scalar, aspect ratio of frustum.
		 * 	@param 	nearClip 		scalar, near clipping plane distance (distance from the camera).
		 * 	@param 	farClip 		scalar, far clipping plane distance (distance from the camera).
		 *	@return 				bool, true if initialization was successful, false otherwise.
		 */
		bool Init(scalar fov, scalar aspectRatio, scalar nearClip, scalar farClip);

		/**
		 *	Get the field of view of the Frustum.
		 *
		 * 	@return 	scalar, field of view of the Frustum.
		 */
		scalar GetFieldOfView() const;
		/**
		 *	Get the near clipping plane distance of the Frustum.
		 *
		 * 	@return 	scalar, near clipping plane distance of the Frustum.
		 */
		scalar GetNearClippingDistance() const;
		/**
		 *	Get the far clipping plane distance of the Frustum.
		 *
		 * 	@return 	scalar, far clipping plane distance of the Frustum.
		 */
		scalar GetFarClippingDistance() const;
		/**
		 *	Get the aspect ratio of the Frustum (width / height).
		 *
		 * 	@return 	scalar, aspect ratio of the Frustum.
		 */
		scalar GetAspectRatio() const;
		/**
		 *	Get one of the planes of the Frustum.
		 *
		 * 	@pre 		side must not be "NumPlanes"
		 *
		 * 	@param 		side, enumerated side of the Frustum to get plane of.
		 *	@return 	const Plane &, const reference to plane of the given
		 * 				side of the Frustum.
		 */
		const Plane &GetPlane(FrustumSide side) const;
		/**
		 *	Get a vertice of the near clipping plane.
		 * 	
		 * 	From the camera's perspective, 
		 * 		vertice 0 is the upper left of the near clipping plane
		 * 		vertice 1 is the upper right of the near clipping plane
		 * 		vertice 2 is the lower right of the near clipping plane
		 * 		vertice 3 is the lower left of the near clipping plane
		 *
		 * 	@pre 	index must be between 0 and 3 inclusive.
		 *
		 * 	@param 	index 	unsigned int, vertice of the near clipping plane
		 * 					to retrieve.
		 * 	@return 		const Vec3 &, requested vertice of the near clipping
		 * 					plane.
		 */
		const Vec3 &GetNearClippingPlaneVert(unsigned int index) const;
		/**
		 *	Get a vertice of the far clipping plane.
		 * 	
		 * 	From the camera's perspective, 
		 * 		vertice 0 is the upper left of the far clipping plane
		 * 		vertice 1 is the upper right of the far clipping plane
		 * 		vertice 2 is the lower right of the far clipping plane
		 * 		vertice 3 is the lower left of the far clipping plane
		 *
		 * 	@pre 	index must be between 0 and 3 inclusive.
		 *
		 * 	@param 	index 	unsigned int, vertice of the far clipping plane
		 * 					to retrieve.
		 * 	@return 		const Vec3 &, requested vertice of the far clipping
		 * 					plane.
		 */
		const Vec3 &GetFarClippingPlaneVert(unsigned int index) const;

		/**
		 * 	Checks if a point is inside the Frustum.
		 *
		 * 	Returns true if the point is inside the Frustum and false
		 * 	otherwise.
		 *
		 * 	@param 	point 	const Vec3 &, point to check.
		 * 	@return 		bool, true if the point is inside the Frustum, false otherwise.
		 */	
		bool IsInside(const Vec3 &point) const;
		/**
		 *	Check to see if a sphere is fully contained within the Frustum.
		 *
		 * 	The sphere is specified by an origin point and a radius.
		 * 	
		 * 	Returns true if the sphere is fully contained within in the Frustum,
		 * 	false otherwise.
		 *
		 * 	@param 	point 	const Vec3 &, origin of sphere.
		 * 	@param 	radius 	float, radius of sphere.
		 * 	@return 		bool, true if the sphere is fully contained within the
		 * 					Frustum, false otherwise.
		 */
		bool IsInside(const Vec3 &point, float radius) const;
		
	private:
		/** Planes of the frustum, relative to the camera. */
		Plane 	m_planes[NumPlanes];
		/** Verts of the near clipping plane, relative to the camera. */
		Vec3 	m_nearClip[4];
		/** Verts of the far clipping plane, relative to the camera. */
		Vec3 	m_farClip[4];

		/** Field of view, radians. */
		scalar	m_fov;
		/** Near clipping distance. */
		scalar	m_near;
		/** Far clipping distance. */
		scalar	m_far;
		/** Aspect ratio (width / height) */
		scalar	m_aspectRatio;
	};
}

#endif 	// WGT_FRUSTUM_H
