#include <cassert>

#include "Mat4.h"

namespace sea_math
{
	Mat4::Mat4()
	{
		data[0] = Vec4(1.0f, 0.0f, 0.0f, 0.0f);
		data[1] = Vec4(0.0f, 1.0f, 0.0f, 0.0f);
		data[2] = Vec4(0.0f, 0.0f, 1.0f, 0.0f);
		data[3] = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	Mat4::Mat4(scalar leading)
	{
		data[0] = Vec4(leading, 0.0f, 0.0f, 0.0f);
		data[1] = Vec4(0.0f, leading, 0.0f, 0.0f);
		data[2] = Vec4(0.0f, 0.0f, leading, 0.0f);
		data[3] = Vec4(0.0f, 0.0f, 0.0f, leading);
	}

	Mat4::Mat4(const Mat4 &other)
	{
		data[0] = other.data[0];
		data[1] = other.data[1];
		data[2] = other.data[2];
		data[3] = other.data[3];
	}

	Mat4::Mat4(const Vec4 &v0, const Vec4 &v1, const Vec4 &v2, const Vec4 &v3)
	{
		data[0] = v0;
		data[1] = v1;
		data[2] = v2;
		data[3] = v3;	
	}

	Mat4::Mat4(	scalar el00, scalar el01, scalar el02, scalar el03,
	    		scalar el10, scalar el11, scalar el12, scalar el13,
	        	scalar el20, scalar el21, scalar el22, scalar el23,
	        	scalar el30, scalar el31, scalar el32, scalar el33)
	{
		data[0] = Vec4(el00, el10, el20, el30);
		data[1] = Vec4(el01, el11, el21, el31);
		data[2] = Vec4(el02, el12, el22, el32);
		data[3] = Vec4(el03, el13, el23, el33);
	}

	Mat4 Mat4::operator=(const Mat4 &other)
	{
		data[0] = other.data[0];
		data[1] = other.data[1];
		data[2] = other.data[2];
		data[3] = other.data[3];

		return (*this);
	}

	Vec4 &Mat4::operator[](unsigned int index)
	{
		assert(index < 4 && "Attempt to access outside Vec4 bounds.\n");

		switch (index)
		{
		case 0:
			return data[0];
			break;
		case 1:
			return data[1];
			break;
		case 2:
			return data[2];
			break;
		case 3:
			return data[3];
			break;
		default:
			return data[0];
			break;
		}
	}

	const Vec4 &Mat4::operator[](unsigned int index) const
	{
		assert(index < 4 && "Attempt to access outside Vec4 bounds.\n");

		switch (index)
		{
		case 0:
			return data[0];
			break;
		case 1:
			return data[1];
			break;
		case 2:
			return data[2];
			break;
		case 3:
			return data[3];
			break;
		default:
			return data[0];
			break;
		}
	}

	bool Mat4::operator==(const Mat4 &other) const
	{
		return ((*this)[0] == other[0] && (*this)[1] == other[1] && (*this)[2] == other[2] && (*this)[3] == other[3]);	
	}

    Mat4 Mat4::GetTranslationTransform(const Vec3 &translation) 
    {
        Mat4 mat = Mat4(1.0f);

        // Translation transform is just x, y and z components in
        // the fourth column of the matrix.
        mat[3] = Vec4(translation, 1.0f);

        return mat;
    }

    Mat4 Mat4::GetScaleTransform(const Vec3 &scale)
    {
        Mat4 mat = Mat4(1.0f);

        // Scale transform is x, y and z components down
        // the diagonal of the matrix.
        mat[0][0] = scale.x;
        mat[1][1] = scale.y;
        mat[2][2] = scale.z;

        return mat;
    }

    Mat4 Mat4::GetRotationTransform(const Quat &rotation)
    {
        return CastMat4(rotation);
    }

	Mat4 operator+(const Mat4 &m1, const Mat4 &m2)
	{
		return (
			Mat4(
				m1.data[0] + m2.data[0],
				m1.data[1] + m2.data[1],
				m1.data[2] + m2.data[2],
				m1.data[3] + m2.data[3]
			)
		);
	}

	Mat4 operator-(const Mat4 &m1, const Mat4 &m2)
	{
		return (
			Mat4(
				m1.data[0] - m2.data[0],
				m1.data[1] - m2.data[1],
				m1.data[2] - m2.data[2],
				m1.data[3] - m2.data[3]
			)
		);
	}

	Mat4 operator*(const Mat4 &m1, const Mat4 &m2)
	{
		Vec4 row0(m1[0].x, m1[1].x, m1[2].x, m1[3].x);
		Vec4 row1(m1[0].y, m1[1].y, m1[2].y, m1[3].y);
		Vec4 row2(m1[0].z, m1[1].z, m1[2].z, m1[3].z);
		Vec4 row3(m1[0].w, m1[1].w, m1[2].w, m1[3].w);

		Vec4 v0(Dot(row0, m2[0]), Dot(row1, m2[0]), Dot(row2, m2[0]), Dot(row3, m2[0]));
		Vec4 v1(Dot(row0, m2[1]), Dot(row1, m2[1]), Dot(row2, m2[1]), Dot(row3, m2[1]));
		Vec4 v2(Dot(row0, m2[2]), Dot(row1, m2[2]), Dot(row2, m2[2]), Dot(row3, m2[2]));
		Vec4 v3(Dot(row0, m2[3]), Dot(row1, m2[3]), Dot(row2, m2[3]), Dot(row3, m2[3]));
		
		return (Mat4(v0, v1, v2, v3));
	}

	Vec4 operator*(const Mat4 &mat, const Vec4 &column)
	{
		Vec4 row0(mat[0].x, mat[1].x, mat[2].x, mat[3].x);
	    Vec4 row1(mat[0].y, mat[1].y, mat[2].y, mat[3].y);
	    Vec4 row2(mat[0].z, mat[1].z, mat[2].z, mat[3].z);
	    Vec4 row3(mat[0].w, mat[1].w, mat[2].w, mat[3].w);

	    return ( Vec4(Dot(row0, column), Dot(row1, column), Dot(row2, column), Dot(row3, column)) );
	}

	Vec4 operator*(const Vec4 &row, const Mat4 &mat)
	{
		Vec4 col0(mat[0].x, mat[0].y, mat[0].z, mat[0].w);
		Vec4 col1(mat[1].x, mat[1].y, mat[1].z, mat[1].w);
		Vec4 col2(mat[2].x, mat[2].y, mat[2].z, mat[2].w);
		Vec4 col3(mat[3].x, mat[3].y, mat[3].z, mat[3].w);

		return ( Vec4(Dot(row, col0), Dot(row, col1), Dot(row, col2), Dot(row, col3)) );
	}

	Mat4 operator*(scalar factor, const Mat4 &mat)
	{
		return (
			Mat4(
				mat.data[0] * factor,
				mat.data[1] * factor,
				mat.data[2] * factor,
				mat.data[3] * factor
			)
		);
	}

	Mat4 operator*(const Mat4 &mat, scalar factor)
	{
		return (
			Mat4(
				mat.data[0] * factor,
				mat.data[1] * factor,
				mat.data[2] * factor,
				mat.data[3] * factor
			)
		);
	}

	Mat4 Inverse(const Mat4 &mat)
	{
	    // Uses Laplace expansion to find determinant and inverse of matrix
	    // Find sub-factors (col, row).
	    /* Sub-factor is determinant of 2x2 matrix left when:
	     *  -you eliminate row and col of 4x4 co-factor 
	     *  -you eliminate row and col of 3x3 co-factor
	     */
	    scalar sub00 = mat[2].z*mat[3].w - mat[3].z*mat[2].w;
	    scalar sub01 = mat[1].z*mat[3].w - mat[3].z*mat[1].w;
	    scalar sub02 = mat[1].z*mat[2].w - mat[2].z*mat[1].w;
	    scalar sub03 = mat[0].z*mat[3].w - mat[3].z*mat[0].w;
	    scalar sub04 = mat[0].z*mat[2].w - mat[2].z*mat[0].w;
	    scalar sub05 = mat[0].z*mat[1].w - mat[1].z*mat[0].w;
	    scalar sub06 = mat[1].y*mat[2].w - mat[2].y*mat[1].w;
	    scalar sub07 = mat[0].y*mat[3].w - mat[3].y*mat[0].w;
	    scalar sub08 = mat[0].y*mat[1].w - mat[1].y*mat[0].w;
	    scalar sub10 = mat[0].y*mat[1].z - mat[1].y*mat[0].z;
	    scalar sub11 = mat[0].y*mat[2].z - mat[2].y*mat[0].z;
	    scalar sub12 = mat[2].y*mat[3].w - mat[3].y*mat[2].w;
	    scalar sub13 = mat[1].y*mat[3].w - mat[3].y*mat[1].w;
	    scalar sub14 = mat[1].y*mat[2].w - mat[2].y*mat[1].w;
	    scalar sub15 = mat[0].y*mat[2].w - mat[2].y*mat[0].w;
	    scalar sub16 = mat[2].y*mat[3].z - mat[3].y*mat[2].z;
	    scalar sub17 = mat[1].y*mat[3].z - mat[3].y*mat[1].z;
	    scalar sub18 = mat[1].y*mat[2].z - mat[2].y*mat[1].z;
	    scalar sub19 = mat[0].y*mat[3].z - mat[3].y*mat[0].z;

	    // Find co-factor matrix (col, row)
	    scalar cof00 =     mat[1].y*(sub00) - mat[2].y*(sub01) + mat[3].y*(sub02);
	    scalar cof10 =   -(mat[0].y*(sub00) - mat[2].y*(sub03) + mat[3].y*(sub04));
	    scalar cof20 =     mat[0].y*(sub01) - mat[1].y*(sub03) + mat[3].y*(sub05);
	    scalar cof30 =   -(mat[0].y*(sub02) - mat[1].y*(sub04) + mat[2].y*(sub05));

	    scalar cof01 =   -(mat[1].x*(sub00) - mat[2].x*(sub01) + mat[3].x*(sub02));
	    scalar cof11 =     mat[0].x*(sub00) - mat[2].x*(sub03) + mat[3].x*(sub04);
	    scalar cof21 =   -(mat[0].x*(sub01) - mat[1].x*(sub03) + mat[3].x*(sub05));
	    scalar cof31 =     mat[0].x*(sub02) - mat[1].x*(sub04) + mat[2].x*(sub05);

	    scalar cof02 =     mat[1].x*(sub12) - mat[2].x*(sub13) + mat[3].x*(sub14);
	    scalar cof12 =   -(mat[0].x*(sub12) - mat[2].x*(sub07) + mat[3].x*(sub15));
	    scalar cof22 =     mat[0].x*(sub13) - mat[1].x*(sub07) + mat[3].x*(sub08);
	    scalar cof32 =   -(mat[0].x*(sub06) - mat[1].x*(sub15) + mat[2].x*(sub08));

	    scalar cof03 =   -(mat[1].x*(sub16) - mat[2].x*(sub17) + mat[3].x*(sub18));
	    scalar cof13 =     mat[0].x*(sub16) - mat[2].x*(sub19) + mat[3].x*(sub11);

	    scalar cof23 =   -(mat[0].x*(sub17) - mat[1].x*(sub19) + mat[3].x*(sub10));
	    scalar cof33 =     mat[0].x*(sub18) - mat[1].x*(sub11) + mat[2].x*(sub10);

	    // Transpose co-factor matrix to get adjoint matrix (note row-major order of constructor)
	    Mat4 adj = Mat4( 	cof00, cof01, cof02, cof03,
	                    	cof10, cof11, cof12, cof13,
	                    	cof20, cof21, cof22, cof23,
	                    	cof30, cof31, cof32, cof33
	    );

	    scalar invDet = 1 / (mat[0].x*cof00 + mat[1].x*cof10 + mat[2].x*cof20 + mat[3].x*cof30);
	    
	    // Multiply adjoint matrix by inverse of determinant to get inverse matrix
	    Mat4 inv = adj * invDet;

	    return (inv);
	}
}
