#ifndef WGT_MATRIX_STACK
#define WGT_MATRIX_STACK

#include <list>

#include "math/Mat4.h"

namespace sea_math
{
	/**
	 *	Matrix Stack class.
	 *
	 *	Provides a convenient method of keeping track of a series
	 * 	of transformations. When a matrix is pushed on top of the stack,
	 * 	the current top of the stack is preserved and the pushed matrix
	 * 	becomes the new top of the stack. Popping removes a matrix from
	 * 	the top of the stack.
	 */
	class MatrixStack
	{
	public:
		/**
		 *	Constructs an empty MatrixStack object.
		 */
		MatrixStack();
		/**
		 *	Push a matrix onto the top of the stack, the current
		 * 	top of the stack matrix is preserved.
		 *	
		 * 	If no memory is available, returns false, else returns true.
		 *
		 * 	@param	mat 	const Mat4 &, matrix to push onto the top of the stack.
		 *	@return 		bool, false if no memory is available, else true.
		 */
		bool Push(const Mat4 &mat);
		/**
		 *	Pop the matrix currently at the top of the stack off the stack.
		 * 	The matrix currently at the top of the stack is returned 
		 * 	via the given parameter (if one is given).
		 *
		 * 	Method returns false if the MatrixStack is empty.
		 *
		 * 	@param 	mat 	const pointer to Mat4, pointer to matrix to fill
		 * 					with the data of the matrix at the top of the stack.
		 * 					May be NULL.
		 * 	@return 		bool, returns false if the MatrixStack is empty, true otherwise.
		 */
		bool Pop(Mat4 *const mat = NULL);
		/**
		 *	Returns the matrix at the top of the MatrixStack, or the identity
		 * 	matrix if the MatrixStack is empty.
		 *
		 *	@return 	const Mat4 &, Matrix currently at the top of the MatrixStack
		 * 				or the identity matrix if the MatrixStack is empty.
		 */
		const Mat4 &Peek() const;
		/**
		 *	Returns true if the MatrixStack is empty and false otherwise.
		 *
		 * 	@return 	bool, true if the MatrixStack is empty, false otherwise.
		 */
		bool Empty();
	private:
		std::list<Mat4>	stack;
	};
}

#endif 	// WGT_MATRIX_STACK