#include "math/Plane.h"

namespace sea_math
{
	Plane::Plane()
	{
		m_a = 0.0f;
		m_b = 0.0f;
		m_c = 0.0f;
		m_d = 0.0f;
	}

	bool Plane::Init(const Vec3 &a, const Vec3 &b, const Vec3 &c)
	{
		bool result = false;

		Vec3 ab = b - a;
		Vec3 ac = c - a;

		// Construct plane normal from cross product
		// Note normal will face towards you if points are specified in clockwise order.
		Vec3 normal = Normalize(Cross(ac, ab));

		if (normal != Vec3(0, 0, 0))
		{
			// http://tutorial.math.lamar.edu/Classes/CalcIII/EqnsOfPlanes.aspx
			m_a = normal.x;
			m_b = normal.y;
			m_c = normal.z;
			m_d = -((normal.x * a.x) + (normal.y * a.y) + (normal.z * a.z));

			result = true;
		}

		return result;
	}

	bool Plane::Init(scalar a, scalar b, scalar c, scalar d)
	{
		bool result = false;

		// Atleast one of the co-efficients must be non-zero
		if (a != 0 || b != 0 || c != 0)
		{
			m_a = a;
			m_b = b;
			m_c = c;
			m_d = d;
			result = true;
		}

		return result;
	}

	bool Plane::operator==(const Plane &other)
	{
		return (Normal() == other.Normal());
	}

	scalar Plane::DistanceToPoint(const Vec3 &point) const
	{
		return ((m_a * point.x + m_b * point.y + m_c * point.z + m_d) / Magnitude(Vec3(m_a, m_b, m_c)));
	}

	scalar Plane::DistToPlaneAlongLine(const Vec3 &point, const Vec3 &lineDir) const
	{
		Vec3 line = Normalize(lineDir);
		return (-m_d - m_a * point.x - m_b * point.y - m_c * point.z) / (m_a * line.x + m_b * line.y + m_c * line.z);
	}

	Vec3 Plane::Normal() const
	{
		return Normalize(Vec3(m_a, m_b, m_c));
	}
}