#ifndef WGT_MAT4
#define WGT_MAT4

#include "Vec4.h"
#include "Quat.h"

namespace sea_math
{
    class Quat;

	class Mat4
	{
	public:
		/**
		 *	Default constructor.
		 *
		 * 	Creates a 4x4 identity matrix.
		 */
		Mat4();
		/**
		 *	Construct a matrix with the given leading value.
		 *
		 * 	Constructs a 4x4 matrix filled with zeros, other than the
		 * 	leading diagonal, which is filled with the given parameter.
		 *
		 * 	@param 	leading 	scalar, value of leading diagonal numbers in matrix.
		 */
		Mat4(scalar leading);
		/**
		 *	Copy constructor.
		 *
		 * 	Create a Mat4 from another Mat4, other Mat4 is cloned.
		 *
		 * 	@param 	other 	const Mat4 reference, matrix to clone.
		 */
		Mat4(const Mat4 &other);
		/**
		 *	Create a Mat4 with the given column vectors.
		 *
		 * 	v0 is column 0, v1 is column 1, etc.
		 *
		 * 	@param 	v0 	const Vec4 reference, column 0 of matrix.
		 * 	@param 	v1 	const Vec4 reference, column 1 of matrix.
		 * 	@param 	v2 	const Vec4 reference, column 2 of matrix.
		 * 	@param 	v3 	const Vec4 reference, column 3 of matrix.
		 */
		Mat4(const Vec4 &v0, const Vec4 &v1, const Vec4 &v2, const Vec4 &v3);
		/**
		 *	Create a Mat4 using the given element parameters.
		 *
		 * 	Note that element ordering is (row, col), so:
		 * 		el00 is the first column, first row of the matrix and
		 * 		el10 is the first column, second row of the matrix.
		 *
		 * 	@param 	el00 	scalar, first column first row element.
		 * 	@param 	el01 	scalar, second column first row element.
		 * 	@param 	el02 	scalar, third column first row element.
		 * 	@param 	el03 	scalar, fourth column first row element.
	 	 * 	@param 	el10 	scalar, first column second row element.
		 * 	@param 	el11 	scalar, second column second row element.
		 * 	@param 	el12 	scalar, third column second row element.
		 * 	@param 	el13 	scalar, fourth column second row element.
	 	 * 	@param 	el20 	scalar, first column third row element.
		 * 	@param 	el21 	scalar, second column third row element.
		 * 	@param 	el22 	scalar, third column third row element.
		 * 	@param 	el23 	scalar, fourth column third row element.
	 	 * 	@param 	el30 	scalar, first column fourth row element.
		 * 	@param 	el31 	scalar, second column fourth row element.
		 * 	@param 	el32 	scalar, third column fourth row element.
		 * 	@param 	el33 	scalar, fourth column fourth row element.
		 */
		Mat4(	scalar el00, scalar el01, scalar el02, scalar el03,
	        	scalar el10, scalar el11, scalar el12, scalar el13,
	            scalar el20, scalar el21, scalar el22, scalar el23,
	            scalar el30, scalar el31, scalar el32, scalar el33);

		/**
		 *	Copy assignment operator.
		 *
		 * 	Assign value of other Mat4 to this Mat4.
		 *
		 * 	@param 	other 	const Mat4 reference, Mat4 to assign to this Mat4.
		 * 	@return 		Mat4, resulting value of expression (ie. other Mat4).
		 */
		Mat4 operator=(const Mat4 &other);
		/**
		 *	Indexed member access operator.
		 *
		 * 	[0] = first column.
		 * 	[1] = second column.
		 * 	[2] = third column.
		 * 	[3] = fourth column.
		 *
		 * 	@pre 	index must be less than 4.
		 *
		 * 	@param 	index 	unsigned int, index of member to access.
		 * 	@return 		Vec4 reference, appropriate column vector.
		 */
		Vec4 &operator[](unsigned int index);
		/**
		 *	Const indexed member access operator.
		 *
		 * 	[0] = first column.
		 * 	[1] = second column.
		 * 	[2] = third column.
		 * 	[3] = fourth column.
		 *
		 * 	@pre 	index must be less than 4.
		 *
		 * 	@param 	index 	unsigned int, index of member to access.
		 * 	@return 		const Vec4 reference, appropriate column vector.
		 */
		const Vec4 &operator[](unsigned int index) const;
		/**
		 *	Equivalence oerator.
		 *
		 * 	Returns TRUE if the two matrices are identical and
		 * 	FALSE otherwise.
		 *
		 * 	Two matrices are identical if each element at each position
		 * 	in the two matrices is identical.
		 *
		 * 	@param 	other 	const Mat4 reference, other matrix to compare with.
		 * 	@return 		bool, TRUE if matrices are identical, FALSE otherwise.
		 */
		bool operator==(const Mat4 &other) const;

        /**
         *  Get transform matrix with translation by the given Vec3.
         *
         *  @param  translation     const Vec3 &, translation x, y and z.
         *  @return                 Mat4, translation transform.
         */
        static Mat4 GetTranslationTransform(const Vec3 &translation);
        /**
         *  Get transform matrix with scale by the given Vec3.
         *
         *  @param  scale           const Vec3 &, scale x, y and z.
         *  @return                 Mat4, scale transform.
         */
        static Mat4 GetScaleTransform(const Vec3 &scale);
        /**
         *  Get transform matrix with rotation by the given Vec3.
         *
         *  @param  scale           const Vec3 &, rotation x, y and z.
         *  @return                 Mat4, scale transform.
         */
        static Mat4 GetRotationTransform(const Quat &rotation);

		Vec4 data[4];
	};

	// Binary operators
	/**
	 * 	Binary addition operator.
	 *
	 * 	Perform component-wise addition on two matrices.
	 *
	 * 	@param 	m1 	const Mat4 reference, Mat4 to sum.
	 * 	@param 	m2 	const Mat4 reference, Mat4 to sum.
	 * 	@return 	Mat4, component-wise sum of m1 and m2.
	 */
	Mat4 operator+(const Mat4 &m1, const Mat4 &m2);
	/**
	 *	Binary subtraction operator.
	 *
	 * 	Perform component-wise subtraction on two matrices.
	 * 	Result is m1 - m2.
	 *
	 * 	@param 	m1 	const Mat4 reference, Mat4 to subtract from.
	 * 	@param 	m2 	const Mat4 reference, Mat4 to subtract with.
	 * 	@return 	Mat4, result of the component-wise subtraction: m1 - m2
	 */
	Mat4 operator-(const Mat4 &m1, const Mat4 &m2);
	/**
	 *	Matrix multiplication.
	 *
	 * 	Multiply two matrices.
	 *
	 * 	@param 	m1 	const Mat4 reference, Mat4 to multiply.
	 * 	@param 	m2 	const Mat4 reference, Mat4 to multiply.
	 * 	@return		Mat4, result of matrix multiplication between m1 and m2.
	 */
	Mat4 operator*(const Mat4 &m1, const Mat4 &m2);
	/**
	 *	Matrix-vector multiplication.
	 *
	 * 	Multiply the matrix by the given column vector.
	 *
	 * 	@param 	mat 	const Mat4 reference, matrix to multiply.
	 * 	@param 	column 	const Vec4 reference, column vector to multiply.
	 * 	@return 		Vec4, resulting column vector.
	 */
	Vec4 operator*(const Mat4 &mat, const Vec4 &column);
	/**
	 *	Matrix-vector multiplication.
	 *
	 * 	Multiply the matrix by the given row vector.
	 *
	 * 	@param 	row 	const Vec4 reference, row vector to multiply.
	 * 	@param 	mat 	const Mat4 reference, matrix to multiply.
	 * 	@return 		Vec4, resulting row vector.
	 */
	Vec4 operator*(const Vec4 &row, const Mat4 &mat);
	/**
	 * 	Multiply each element in a matrix by a given factor.
	 *
	 * 	@param 	factor 	scalar, factor to multiply each element of the matrix with.
	 * 	@param 	mat 	const Mat4 reference, matrix to multiply.
	 * 	@return 		Mat4, result of scaling factor applied to each element in the matrix.
	 */
	Mat4 operator*(scalar factor, const Mat4 &mat);
	/**
	 * 	Multiply each element in a matrix by a given factor.
	 *
	 * 	@param 	mat 	const Mat4 reference, matrix to multiply.
	 * 	@param 	factor 	scalar, factor to multiply each element of the matrix with.
	 * 	@return 		Mat4, result of scaling factor applied to each element in the matrix.
	 */
	Mat4 operator*(const Mat4 &mat, scalar factor);

	/**
	 *	Return the inverse of the given matrix.
	 *
	 *	@param 		mat 	const Mat4 &, matrix to find inverse of.
	 * 	@return 			Mat4, matrix inverse.
	 */
	Mat4 Inverse(const Mat4 &mat);
}

#endif // WGT_MAT4
