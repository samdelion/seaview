#include <cassert>

#include "math/Frustum.h"
#include "math/Globals.h"

namespace sea_math
{
	Frustum::Frustum()
	{
		m_fov = sea_math::SEA_PI / 4.0f; 	// Default field of view = 90 degrees
		m_aspectRatio = 1.0f;
		m_near = 1.0f;
		m_far = 1000.0f;
	}

	bool Frustum::Init(scalar fov, scalar aspectRatio, scalar nearClip, scalar farClip)
	{
		m_fov = fov;
		m_aspectRatio = aspectRatio;
		m_near = nearClip;
		m_far = farClip;

		scalar tanFovDiv2 = (scalar)tan(m_fov/2.0f); 
		// Near clipping plane, vector from centre of plane to right edge of plane.
		Vec3 nearRight = m_near * tanFovDiv2 * m_aspectRatio * sea_math::g_right;
		// Far clipping plane, vector from centre of plane to right edge of plane.
		Vec3 farRight =	m_far * tanFovDiv2 * m_aspectRatio * sea_math::g_right;
		// Near clipping plane, vector from centre of plane to upper edge of plane.
		Vec3 nearUp = m_near * tanFovDiv2 * sea_math::g_up;
		// Far clipping plane, vector from centre of plane to upper edge of plane.
		Vec3 farUp = m_far * tanFovDiv2 * sea_math::g_up;

		// Now we use these vectors to determine vectors from the camera to each
		// of the vertices of the near and far clipping planes.
		// Vertices start in the upper left and go around clockwise (from the position of the camera).
		m_nearClip[0] = (m_near * sea_math::g_forward) - nearRight + nearUp;
		m_nearClip[1] = (m_near * sea_math::g_forward) + nearRight + nearUp;
		m_nearClip[2] = (m_near * sea_math::g_forward) + nearRight - nearUp;
		m_nearClip[3] = (m_near * sea_math::g_forward) - nearRight - nearUp;

		m_farClip[0] = (m_far * sea_math::g_forward) - farRight + farUp;
		m_farClip[1] = (m_far * sea_math::g_forward) + farRight + farUp;
		m_farClip[2] = (m_far * sea_math::g_forward) + farRight - farUp;
		m_farClip[3] = (m_far * sea_math::g_forward) - farRight - farUp;

		// Create planes of frustum from vertices.
		// Vertices are placed in an order to ensure that the normals of the plane
		// face outwards.
		// Note that only three vertices are required to construct a plane.
		Vec3 origin(0.0f, 0.0f, 0.0f);
		m_planes[Near].Init(m_nearClip[0], m_nearClip[1], m_nearClip[2]);
		m_planes[Far].Init(m_farClip[1], m_farClip[0], m_farClip[3]);
		m_planes[Top].Init(origin, m_farClip[0], m_farClip[1]);
		m_planes[Bottom].Init(origin, m_farClip[2], m_farClip[3]);
		m_planes[Left].Init(origin, m_farClip[3], m_farClip[0]);
		m_planes[Right].Init(origin, m_farClip[1], m_farClip[2]);

		return true;
	}

	bool Frustum::IsInside(const Vec3 &point) const
	{
		// Check for all planes
		for (int i = 0; i < NumPlanes; ++i)
		{
			// Ensure that the distance between the point and the plane
			// is never positive (remember, normals face outward).
			if (m_planes[i].DistanceToPoint(point) >= 0.0f)
			{
				return false;
			}
		}		

		return true;
	}

	bool Frustum::IsInside(const Vec3 &point, float radius) const
	{
		for (int i = 0; i < NumPlanes; ++i)
		{
			if (m_planes[i].DistanceToPoint(point) + radius >= 0.0f)
			{
				return false;
			}
		}

		return true;
	}

	scalar Frustum::GetFieldOfView() const
	{
		return m_fov;
	}

	scalar Frustum::GetNearClippingDistance() const
	{
		return m_near;
	}

	scalar Frustum::GetFarClippingDistance() const
	{
		return m_far;
	}

	scalar Frustum::GetAspectRatio() const
	{
		return m_aspectRatio;
	}

	const Plane &Frustum::GetPlane(FrustumSide side) const
	{
		return m_planes[side];
	}

	const Vec3 &Frustum::GetNearClippingPlaneVert(unsigned int index) const
	{
		assert(index < 4 && "Attempted to access invalid near clipping plane vert");

		return m_nearClip[index];
	}

	const Vec3 &Frustum::GetFarClippingPlaneVert(unsigned int index) const
	{
		assert(index < 4 && "Attempted to access invalid far clipping plane vert");

		return m_farClip[index];
	}
}
