#ifndef SEA_SPHERE_H
#define SEA_SPHERE_H

#include "math/Vec3.h"
#include "math/Precision.h"

namespace sea_math
{
    class Sphere
    {
    public:

        Sphere();

        /**
         * Construct a sphere with a position and radius. Position defined
         * by the Vec3 and radius defined using the scalar.
         *
         * Will set the sphere position to be at the specified location. This
         * location is defined by the Vec3 param. Also requires a radius for
         * collision checks.
         *
         * @param position Vec3, Sets the position of the sphere.
         * @param radius scalar, Sets the radius of the sphere.
         */
        Sphere(Vec3 position, scalar radius);


        /**
         * Returns the sphere position represented by a Vec3.
         *
         * Will return the position of the shere with a Vec3. This
         * will represent the center point of the sphere.
         *
         * @return Vec3 The center position of the sphere.
         */
        Vec3 getSpherePosition();


        /**
         * Set the sphere center position.
         *
         * Sets the sphere center point to a location specified by
         * a Vec3 passed in.
         *
         * @param newPosition Vec3, The new center position of the sphere.
         */
        void setSpherePosition(Vec3 newPosition);


        /** Return the sphere radius, represented by a scalar type.
         *
         * Will return the radius of the sphere from the designated center
         * point of the sphere.
         *
         * @return scalar, Radius of sphere from specified center point.
         */
        scalar getSphereRadius();


        /**
         * Set the sphere's radius from the center point location of
         * the sphere.
         *
         * Set the new radius from the specified center position of the
         * Sphere. Useful for the collision checking to see whether one
         * area intersects with another.
         *
         * @param newRadius scalar, The new radius from center-point of sphere.
         */
        void setSphereRadius(scalar newRadius);


        /**
         * Sets all required sphere values.
         *
         * Sets both the radius and the position of the sphere. The radius
         * extends from the specified center point.
         *
         * @param newPosition Vec3, The location of the center point.
         * @param newRadius scalar, The radius of the sphere.
         */
        void setSphereValues(Vec3 newPosition, scalar newRadius);


        /**
         * Checks to see whether two spheres are intersecting.
         *
         * This does a check to see whether this sphere and another sphere
         * are intersecting. Does a check with both center points and both
         * radius values.
         *
         * @param otherSphere Sphere, The other sphere to check for intersection
         * against.
         */
        bool isCollided(Sphere otherSphere);


    private:

        /** The position of the sphere. */
        Vec3 m_position;

        /** The radius of the sphere from the position. */
        scalar m_radius;

    };
}


#endif
