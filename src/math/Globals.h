#ifndef SEA_MATH_GLOBALS_H
#define SEA_MATH_GLOBALS_H

#define _USE_MATH_DEFINES
#include <math.h>

#include "math/Vec3.h"

namespace sea_math
{
	extern sea_math::Vec3 g_right;
	extern sea_math::Vec3 g_up;
	extern sea_math::Vec3 g_forward;

    /**
     *  Convert degrees angle to radians angle.
     *
     *  @param  angleDeg    float, degrees to convert to radians.
     *  @return             float, degrees argument converted to radians.
     */
    float fDegToRad(float angleDeg);

    /**
     *  Convert radians angle to degrees angle.
     *
     *  @param  angleRad    float, radians to convert to degrees.
     *  @return             float, radians argument converted to degrees.
     */
    float fRadToDeg(float angleRad);

	#ifndef _USE_MATH_DEFINES
	#define _USE_MATH_DEFINES	
	#endif

    /** Our pi constant */
	const double SEA_PI = M_PI;
}

#endif  // SEA_MATH_GLOBALS_H
