#include <cmath>
#include <cassert>

#include "Vec3.h"

namespace sea_math
{
	Vec3::Vec3(float _x, float _y, float _z) :
		x(_x), y(_y), z(_z)
	{
	}

	Vec3::Vec3(const Vec3 &other) :
		x(other.x), y(other.y), z(other.z)
	{
	}

	Vec3 Vec3::operator=(const Vec3 &other)
	{
		x = other.x;
		y = other.y;
		z = other.z;

		return (*this);
	}

	scalar &Vec3::operator[](unsigned int index)
	{
		assert(index < 3 && "Attempt to access outside Vec3 bounds.\n");
		
		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		default:
			return x;
			break;
		}	
	}

	const scalar &Vec3::operator[](unsigned int index) const
	{
		assert(index < 3 && "Attempt to access outside Vec3 bounds.\n");
		
		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		default:
			return x;
			break;
		}		
	}

	Vec3 Vec3::operator*=(const scalar factor)
	{
		x *= factor;
		y *= factor;
		z *= factor;
		return (*this);
	}

	Vec3 Vec3::operator+=(const Vec3 &other)
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return (*this);
	}

	Vec3 Vec3::operator-=(const Vec3 &other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return (*this);
	}

	bool Vec3::operator==(const Vec3 &other) const
	{
		return (fabs(x - other.x) < FLOAT_ACCURACY && 
				fabs(y - other.y) < FLOAT_ACCURACY && 
				fabs(z - other.z) < FLOAT_ACCURACY);
	}

	bool Vec3::operator!=(const Vec3 &other) const
	{
		return (!(*this == other));		
	}

	Vec3 operator+(const Vec3 &v1, const Vec3 &v2)
	{
		return (
			Vec3(
				v1.x + v2.x,
				v1.y + v2.y,
				v1.z + v2.z
			)
		);
	}

	Vec3 operator-(const Vec3 &v1, const Vec3 &v2)
	{
		return (
			Vec3(
				v1.x - v2.x,
				v1.y - v2.y,
				v1.z - v2.z
			)
		);
	}

	Vec3 operator*(scalar factor, const Vec3 &vec)
	{
		return (
			Vec3(
				factor * vec.x,
				factor * vec.y,
				factor * vec.z
			)
		);
	}

	Vec3 operator*(const Vec3 &vec, scalar factor)
	{
		return (
			Vec3(
				factor * vec.x,
				factor * vec.y,
				factor * vec.z
			)
		);
	}

	Vec3 operator-(const Vec3 &vec)
	{
		return (
			Vec3(
				-vec.x,
				-vec.y,
				-vec.z
			)
		);
	}

	scalar Dot(const Vec3 &v1, const Vec3 &v2)
	{
		return (
			v1.x * v2.x +
			v1.y * v2.y +
			v1.z * v2.z
		);
	}

	Vec3 Cross(const Vec3 &v1, const Vec3 &v2)
	{
		return (
			Vec3(v1.y * v2.z - v1.z * v2.y,
	        v1.z * v2.x - v1.x * v2.z,
	        v1.x * v2.y - v1.y * v2.x)
		);
	}

	scalar Magnitude(const Vec3 &vec)
	{
		return sqrt(Dot(vec, vec));
	}

	Vec3 Normalize(const Vec3 &vec)
	{
		float mag = Magnitude(vec);

		if (mag == 0)
		{
			return vec;
		}
		else
		{
			return (Vec3(vec.x/mag, vec.y/mag, vec.z/mag));
		}
	}

	Vec3 Invert(const Vec3 &vec)
	{
		return (Vec3(-vec.x, -vec.y, -vec.z));
	}

	float AngleBetween(const Vec3 &a, const Vec3 &b)
	{
		return acos(Dot(a, b) / (Magnitude(a) * Magnitude(b)));
	}

    Vec3 ProjectAontoB(const Vec3 &a, const Vec3 &b)
    {
        return (Dot(a,b) / Magnitude(b)) * Normalize(b);
    }
}
