#include "MatrixStack.h"

namespace sea_math
{
	MatrixStack::MatrixStack()
	{
		stack = std::list<Mat4>();
		stack.push_back(Mat4());
	}	

	bool MatrixStack::Push(const Mat4 &mat)
	{
		bool result = true;

		try 
		{
			stack.push_back(mat);
		} 
		catch (...)
		{
			result = false;
		}

		return result;
	}

	bool MatrixStack::Pop(Mat4 *const mat)
	{
		bool result = true;

		if (Empty() == false)
		{
			if (mat != NULL)
			{
				*mat = stack.back();
				stack.pop_back();
			}
		}
		else
		{
			result = false;
		}

		return result;
	}

	const Mat4 &MatrixStack::Peek() const
	{
		return(stack.back());
	}

	bool MatrixStack::Empty()
	{
		return (stack.size() <= 1);
	}
}