#ifndef WGT_VEC4
#define WGT_VEC4

#include "Precision.h"
#include "Vec3.h"

namespace sea_math
{
	class Vec4
	{
	public:
		/**
		 *	Construct a Vec4 from the given x, y, z and w values.
		 *
		 * 	@param 	_x 	scalar, x-value to give Vec4.
		 * 	@param 	_y 	scalar, y-value to give Vec4.
		 * 	@param 	_z 	scalar, z-value to give Vec4.
		 * 	@param 	_w 	scalar, w-value to give Vec4.
		 */
		Vec4(scalar _x = 0.0f, scalar _y = 0.0f, scalar _z = 0.0f, scalar _w = 1.0f);
		/**
		 *	Construct a Vec4 from the given Vec3 and additional w value.
		 *
		 * 	@param 	vec 	const Vec3 reference, Vec3 to use for first three values
		 * 					of a new Vec4.
		 * 	@param 	_w 	scalar, w-value to give Vec4.
		 */
		Vec4(const Vec3 &vec, scalar _w = 1.0f);
		/**
		 *	Copy constructor.
		 *
		 * 	Create a Vec4 that is a copy of the given Vec4.
		 *
		 * 	@param 	other 	const Vec4 reference, Vec4 to copy.
		 */
		Vec4(const Vec4 &other);

		/**
		 *	Copy assignment operator.
		 *
		 * 	Assign value of other Vec4 to this Vec4.
		 *
		 * 	@param 		other 	const Vec4 reference, Vec4 to copy.
		 * 	@return 			Vec4, resulting value of assignment operation (i.e. other Vec4).
		 */
		Vec4 operator=(const Vec4 &other);
		/**
		 *	Indexed member access operator.
		 *
		 *	[0] = x
		 * 	[1] = y
		 * 	[2] = z
		 * 	[3] = w
		 *
		 *	@pre 	index is greater than and equal to 0 and less then 4.
		 *
		 * 	@param 	index 	unsigned int, index of Vec4 element to access.
		 * 	@return 		reference to scalar, appropriate element.
		 */
		scalar &operator[](unsigned int index);
		/**
		 *	Const indexed member access operator.
		 *
		 *	[0] = x
		 * 	[1] = y
		 * 	[2] = z
		 * 	[3] = w
		 *
		 *	@pre 	index is greater than and equal to 0 and less then 4.
		 *
		 * 	@param 	index 	unsigned int, index of Vec4 element to access.
		 * 	@return 		const reference to scalar, appropriate element.
		 */	
		const scalar &operator[](unsigned int index) const;
		/**
		 *	Combo addition-assignment operator.
		 *
		 * 	Assign value of sum(other Vec4, this Vec4) to this Vec4.
		 *
		 *	@param 	other 	const Vec4 reference, Vec4 to sum with this Vec4.
		 * 	@return 		Vec4, resulting value of operation (i.e. this + other).
		 */
		Vec4 operator+=(const Vec4 &other);
		/**
		 *	Combo subtraction-assignment operator.
		 *
		 * 	Assign value of (other Vec4 - this Vec4) to this Vec4.
		 *
		 *	@param 	other 	const Vec4 reference, Vec4 to subtract from this Vec4.
		 * 	@return 		Vec4, resulting value of operation (i.e. this - other).
		 */
		Vec4 operator-=(const Vec4 &other);
		/**
		 *	Equivalence operator.
		 *
		 * 	Compare two Vec4s and return TRUE if they are equal or FALSE otherwise.
		 *
		 * 	@param 	other 	const Vec4 reference, Vec4 to compare to this Vec4.
		 * 	@return 		bool, TRUE if two Vec4s are equal, FALSE otherwise.
		 */
		bool operator==(const Vec4 &other) const;

		scalar x, y, z, w;
	};

	// Binary operators
	/**
	 * 	Binary addition operator.
	 *
	 * 	Sum two Vec4s.
	 *
	 * 	@param 	v1 	const Vec4 reference, Vec4 to sum.
	 * 	@param 	v2 	const Vec4 reference, Vec4 to sum.
	 * 	@return 	Vec4, result of v1 + v2.
	 */
	Vec4 operator+(const Vec4 &v1, const Vec4 &v2);
	/**
	 *	Binary subtraction operator.
	 *
	 * 	Subtract one Vec4 from another.
	 *
	 * 	@param 	v1 	const Vec4 reference, Vec4 to subtract from.
	 * 	@param 	v2 	const Vec4 reference, Vec4 to subtract with.
	 * 	@return 	Vec4, result of v1 - v2.
	 */
	Vec4 operator-(const Vec4 &v1, const Vec4 &v2);
	/**
	 * 	Scale vector by factor.
	 *
	 * 	@param 	factor 	scalar, scaling factor.
	 * 	@param 	vec 	const Vec4 reference, Vec4 to scale.
	 * 	@return 		Vec4, scaled vector.
	 */
	Vec4 operator*(scalar factor, const Vec4 &vec);
	/**
	 * 	Scale vector by factor.
	 *
	 * 	@param 	vec 	const Vec4 reference, Vec4 to scale.
	 * 	@param 	factor 	scalar, scaling factor.
	 * 	@return 		Vec4, scaled vector.
	 */
	Vec4 operator*(const Vec4 &vec, scalar factor);

	/**
	 *	Find the dot product of the two given vectors.
	 *
	 * 	@param 	v1 	const Vec4 reference, first vector argument.
	 * 	@param 	v2 	const Vec4 reference, second vector argument.
	 * 	@return 	scalar, dot product of the two given vectors.
	 */
	scalar Dot(const Vec4 &v1, const Vec4 &v2);
	/**
	 * 	Return the magnitude of the given vector,
	 *
	 * 	@param 	vec 	const Vec4 reference, vector to find the magnitude of.
	 * 	@return 		scalar, magnitude of given vector.
	 */
	scalar Magnitude(const Vec4 &vec);
	/**
	 * 	Return a vector in the same direction as the given vector
	 * 	but with a magnitude of 1.
	 *
	 *	@param 	vec 	const Vec4 reference, vector to normalize.
	 * 	@return 		Vec4, vector in same direction as given vector, but with a magnitude of 1. 	
	 */
	Vec4 Normalize(const Vec4 &vec);
}

#endif // WGT_VEC4
