#ifndef WGT_PRECISION_H
#define WGT_PRECISION_H

namespace sea_math
{
	/**
	 *  Used in math classes, allows us to easily change the precision
	 *  of our math and physics calculations.
	 */
	typedef float scalar;

	/**
	 *	How 'close' should two floats be to be considered equal.
	 */
	const float FLOAT_ACCURACY = 0.0000001f;
}

#endif	// WGT_PRECISION_H
