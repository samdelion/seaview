#include "Sphere.h"

namespace sea_math {
    
    Sphere::Sphere()
    {
        
    }


    Sphere::Sphere(Vec3 position, scalar radius)
    {
        m_position = position;
        m_radius = radius;
    }


    Vec3 Sphere::getSpherePosition()
    {
        return m_position;
    }



    void Sphere::setSpherePosition(Vec3 newPosition)
    {
        m_position = newPosition;
    }



    scalar Sphere::getSphereRadius()
    {
        return m_radius;
    }



    void Sphere::setSphereRadius(scalar newRadius)
    {
        m_radius = newRadius;
    }



    void Sphere::setSphereValues(Vec3 newPosition, scalar newRadius)
    {
        m_position = newPosition;
        m_radius = newRadius;
    }



    bool Sphere::isCollided(Sphere otherSphere)
    {
        Vec3 distanceBetween = m_position - otherSphere.getSpherePosition();
        scalar magnitudeBetween = sea_math::Magnitude(distanceBetween);
        scalar combinedRadius = m_radius + otherSphere.getSphereRadius();
        
        bool flag = false;
        
        if (magnitudeBetween <= combinedRadius)
        {
            flag = true;
        }
        
        
        return flag;
        
    }
    
}
