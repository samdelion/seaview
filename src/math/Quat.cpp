#include <cassert>
#include <cmath>

#include "Quat.h"

namespace sea_math
{
	Quat::Quat(scalar _x, scalar _y, scalar _z, scalar _w) : x(_x), y(_y), z(_z), w(_w)
	{
	}

	Quat::Quat(Vec3 axis, scalar _w) : x(axis.x), y(axis.y), z(axis.z), w(_w)
	{
	}

	scalar &Quat::operator[](unsigned int index)
	{
		assert(index < 4 && "Attempt to access outside Vec4 bounds.\n");

		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		case 3:
			return w;
			break;
		default:
			return x;
			break;
		}
	}

	const scalar &Quat::operator[](unsigned int index) const
	{
		assert(index < 4 && "Attempt to access outside Vec4 bounds.\n");

		switch (index)
		{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		case 3:
			return w;
			break;
		default:
			return x;
			break;
		}
	}

    bool Quat::operator==(const Quat &other) const
    {
		return (fabs(x - other.x) < FLOAT_ACCURACY && 
				fabs(y - other.y) < FLOAT_ACCURACY && 
				fabs(z - other.z) < FLOAT_ACCURACY &&
                fabs(w - other.w) < FLOAT_ACCURACY);
    }

	void Quat::Offset(const Vec3 &axis, scalar angleRad)
	{
	    Vec3 _axis = Normalize(axis);

	    _axis = _axis * sinf(angleRad / 2.0f);
	    float scal = cosf(angleRad / 2.0f);

	    Quat offset = Quat(_axis, scal);
		
		(*this) = (*this) * offset;

	    (*this) = Normalize(*this);
	}

	void Quat::Offset(scalar xAxis, scalar yAxis, scalar zAxis, scalar angleRad)
	{
	    Vec3 _axis = Normalize(Vec3(xAxis, yAxis, zAxis));

		_axis = _axis * sinf(angleRad / 2.0f);
	    float scal = cosf(angleRad / 2.0f);

	    Quat offset = Quat(_axis, scal);
		
		(*this) = (*this) * offset;

	    (*this) = Normalize(*this);
	}

	Quat operator*(const Quat &q1, const Quat &q2)
	{
		return (
	        Quat(   
				q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y,
	            q1.w * q2.y + q1.y * q2.w + q1.z * q2.x - q1.x * q2.z,
	            q1.w * q2.z + q1.z * q2.w + q1.x * q2.y - q1.y * q2.x,
	            q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z
			)
	    );
	}

	Quat operator-(const Quat &q) 
	{
		return ( Quat(q.x, q.y, q.z, -q.w) );
	}
    
	scalar Magnitude(const Quat &q)
	{
		return ( sqrt(pow(q.x, 2) + pow(q.y, 2) + pow(q.z, 2) + pow(q.w, 2)) );
	}

	Quat Normalize(const Quat &q)
	{
		scalar mag = Magnitude(q);

		if (mag == 0)
		{
			return q;
		}
		else
		{
			return ( Quat(q.x/mag, q.y/mag, q.z/mag, q.w/mag) );
		}
	}

	scalar Dot(const Quat &q1, const Quat &q2)
	{
		return (
			q1.x * q2.x + 
			q1.y * q2.y +
			q1.z * q2.z +
			q1.w * q2.w
		);
	}

	// Quat slerp(const Quat &q1, const Quat &q2, scalar t)
	// {

	// }

	Mat4 CastMat4(const Quat &q)
	{
		// Mat4 columns
	    Vec4 v0 = Vec4(1 - 2*q.y*q.y - 2*q.z*q.z, 2*q.x*q.y + 2*q.w*q.z, 2*q.x*q.z - 2*q.w*q.y, 0.0f);
	    Vec4 v1 = Vec4(2*q.x*q.y - 2*q.w*q.z, 1 - 2*q.x*q.x - 2*q.z*q.z, 2*q.y*q.z + 2*q.w*q.x, 0.0f);
	    Vec4 v2 = Vec4(2*q.x*q.z + 2*q.w*q.y, 2*q.y*q.z - 2*q.w*q.x, 1 - 2*q.x*q.x - 2*q.y*q.y, 0.0f);
	    Vec4 v3 = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
	        
	    return ( Mat4(v0, v1, v2, v3) );
	}
}
