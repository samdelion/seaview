#include "math/Globals.h"

namespace sea_math
{
	Vec3 g_right = sea_math::Vec3(1.0f, 0.0f, 0.0f);
	Vec3 g_up = sea_math::Vec3(0.0f, 1.0f, 0.0f);
	Vec3 g_forward = sea_math::Vec3(0.0f, 0.0f, -1.0f);

    float fDegToRad(float angleDeg)
    {
        return (SEA_PI * (angleDeg / 180.0f));    
    }

    float fRadToDeg(float angleRad)
    {
        return (180.0f * (angleRad / SEA_PI));    
    }
}
