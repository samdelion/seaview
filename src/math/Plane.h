#ifndef WGT_PLANE_H
#define WGT_PLANE_H

#include "math/Precision.h"
#include "math/Vec3.h"

namespace sea_math
{
	class Plane
	{
	public:
		/**
		 *	Default constructor.
		 *
		 * 	Object should be initialized before use (use the Init method).
		 */
		Plane();
		/**
 	 	 * 	Construct a Plane from three points.
 	 	 *
 	 	 *	Will return false and initialization will fail 
 	 	 * 	if all points are co-linear (exist on the same line).
 	 	 *
 	 	 * 	Normal for the plane will face towards you if co-ordinates are 
 	 	 * 	given in clock-wise order.
 	 	 *
 	 	 * 	@param 	a 	const Vec3 &, point to construct plane from.
 	 	 * 	@param 	b 	const Vec3 &, point to construct plane from.
 	 	 * 	@param 	c 	const Vec3 &, point to construct plane from.
 	 	 * 	@return 	bool, true if initialization was successful, false otherwise.
		 */
		bool Init(const Vec3 &a, const Vec3 &b, const Vec3 &c);
		/**
		 *	Construct a Plane from the general form equation: ax + by + cz + d = 0
		 *
		 *	Will return false and initialization will fail if co-efficients
		 * 	do not describe a valid plane (ie. atleast one of the co-efficients (a, b or c) must
		 * 	be non-zero to correctly describe a plane).
		 *
		 *	@param 	a 	scalar, a co-efficient in general form of equation for a plane.
		 *	@param 	b 	scalar, b co-efficient in general form of equation for a plane.
		 *	@param 	c 	scalar, c co-efficient in general form of equation for a plane.
		 *	@param 	d 	scalar, d co-efficient in general form of equation for a plane.
		 *	@return 	bool, true if initialization was successful, false otherwise.
		 */
		bool Init(scalar a, scalar b, scalar c, scalar d);

		/**
		 *	Compare this plane and another plane for equivalence.
		 *
		 * 	@param 	other 	const Plane &, other plane to compare to.
		 * 	@return 		bool, returns true if the two planes are equivalent, false otherwise.
		 */
		bool operator==(const Plane &other);

		/**
		 *	Return the normal of the plane.
		 *
		 * 	@return 	Vec3, normal of the plane.
		 */
		Vec3 Normal() const;
		/**
		 *	Find the distance between this plane and the given point.
		 *	
		 * 	Distance is signed, will be positive if point is in same
		 * 	direction as normal and negative otherwise.
		 *
		 * 	@param 	point 	const Vec3 &, point to find distance to.
		 * 	@return 		scalar, signed distance to point from this plane.
		 */
		scalar DistanceToPoint(const Vec3 &point) const;

		/**
		*	Find the distance between this plane and the given point
		*	along the given direction.
		*
		* 	Distance is signed, will be positive if point is in 
		*	opposite direction.
		*
		* 	@param 	point 	const Vec3 &, point to find distance to.
		*	@param	lineDir	const Vec3 &, direction of life to follow. 
		* 	@return 		scalar, signed distance to point from this plane.
		*/
		scalar DistToPlaneAlongLine(const Vec3 &point, const Vec3 &lineDir) const;


	private:
		/** Co-efficients in general form equation of a plane: ax + by + cz + d = 0.
		*	Note: Do not confuse with ax + by + cz = d.
		*/
		
		scalar 	m_a, m_b, m_c, m_d;
	};
}

#endif 	// WGT_PLANE_H