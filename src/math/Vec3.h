#ifndef WGT_VEC3
#define WGT_VEC3

#include "Precision.h"

namespace sea_math
{
	/**
	 *	Vector-3 class.
	 */	
	class Vec3 {
	public:
		/**
		 *	Construct a Vec3 from the given x, y and z values.
		 *
		 * 	@param 	_x 	float, x-value to give Vec3.
		 * 	@param 	_y 	float, y-value to give Vec3.
		 * 	@param 	_z 	float, z-value to give Vec3.
		 */
		Vec3(float _x = 0.0f, float _y = 0.0f, float _z = 0.0f);
		/**
		 *	Construct a Vec3 from another Vec3 (copy constructor).
		 *
		 * 	@param 	other 	const Vec3 reference, Vec3 to copy.
		 */
		Vec3(const Vec3 &other);

		/**
		 *	Copy assignment operator.
		 *
		 * 	Assign value of other Vec3 to this Vec3.
		 *
		 * 	@param 		other 	const Vec3 reference, Vec3 to copy.
		 * 	@return 			Vec3, resulting value of assignment operation (i.e. other Vec3).
		 */
		Vec3 operator=(const Vec3 &other);
		/**
		 *	Indexed member access operator.
		 *
		 *	[0] = x
		 * 	[1] = y
		 * 	[2] = z
		 *
		 *	@pre 	index is greater than and equal to 0 and less then 3.
		 *
		 * 	@param 	index 	unsigned int, index of Vec3 element to access.
		 * 	@return 		reference to scalar, appropriate element.
		 */
		scalar &operator[](unsigned int index);
		/**
		 *	Const indexed member access operator.
		 *
		 *	[0] = x
		 * 	[1] = y
		 * 	[2] = z
		 *
		 *	@pre 	index is greater than and equal to 0 and less then 3.
		 *
		 * 	@param 	index 	unsigned int, index of Vec3 element to access.
		 * 	@return 		reference to scalar, appropriate element.
		 */
		const scalar &operator[](unsigned int index) const;

		/**
		*	Scalar multiplication operator.
		*
		* 	Multiply all components of the vector by factor.
		*
		*	@param 	factor 	const scalar float
		* 	@return 		Vec3, resulting value of operation (i.e. this + other).
		*/
		Vec3 operator*=(const scalar factor);

		/**
		 *	Combo addition-assignment operator.
		 *
		 * 	Assign value of sum(other Vec3, this Vec3) to this Vec3.
		 *
		 *	@param 	other 	const Vec3 reference, Vec3 to sum with this Vec3.
		 * 	@return 		Vec3, resulting value of operation (i.e. this + other).
		 */
		Vec3 operator+=(const Vec3 &other);
		/**
		 *	Combo subtraction-assignment operator.
		 *
		 * 	Assign value of (other Vec3 - this Vec3) to this Vec3.
		 *
		 *	@param 	other 	const Vec3 reference, Vec3 to subtract from this Vec3.
		 * 	@return 		Vec3, resulting value of operation (i.e. this - other).
		 */
		Vec3 operator-=(const Vec3 &other);
		/**
		 *	Equivalence operator.
		 *
		 * 	Compare two Vec3s and return TRUE if they are equal or FALSE otherwise.
		 *
		 * 	@param 	other 	const Vec3 reference, Vec3 to compare to this Vec3.
		 * 	@return 		bool, TRUE if two Vec3s are equal, FALSE otherwise.
		 */
		bool operator==(const Vec3 &other) const;
		/**
		 *	Equivalence operator.
		 *
		 * 	Compare two Vec3s and return TRUE if they are equal or FALSE otherwise.
		 *
		 * 	@param 	other 	const Vec3 reference, Vec3 to compare to this Vec3.
		 * 	@return 		bool, TRUE if two Vec3s are equal, FALSE otherwise.
		 */
		bool operator!=(const Vec3 &other) const;

		scalar 	x, y, z;
	};

	// Binary operators
	/**
	 * 	Binary addition operator.
	 *
	 * 	Sum two Vec3s.
	 *
	 * 	@param 	v1 	const Vec3 reference, Vec3 to sum.
	 * 	@param 	v2 	const Vec3 reference, Vec3 to sum.
	 * 	@return 	Vec3, result of v1 + v2.
	 */
	Vec3 operator+(const Vec3 &v1, const Vec3 &v2);
	/**
	 *	Binary subtraction operator.
	 *
	 * 	Subtract one Vec3 from another.
	 *
	 * 	@param 	v1 	const Vec3 reference, Vec3 to subtract from.
	 * 	@param 	v2 	const Vec3 reference, Vec3 to subtract with.
	 * 	@return 	Vec3, result of v1 - v2.
	 */
	Vec3 operator-(const Vec3 &v1, const Vec3 &v2);
	/**
	 * 	Scale vector by factor.
	 *
	 * 	@param 	factor 	scalar, scaling factor.
	 * 	@param 	vec 	const Vec3 reference, Vec3 to scale.
	 * 	@return 		Vec3, scaled vector.
	 */
	Vec3 operator*(scalar factor, const Vec3 &vec);
	/**
	 * 	Scale vector by factor.
	 *
	 * 	@param 	vec 	const Vec3 reference, Vec3 to scale.
	 * 	@param 	factor 	scalar, scaling factor.
	 * 	@return 		Vec3, scaled vector.
	 */
	Vec3 operator*(const Vec3 &vec, scalar factor);
	/**
	 *	Invert vector.
	 *
	 * 	Change the sign of each of the vectors components.
	 *
	 * 	@param 	vec 	const Vec3 reference, vector to invert.
	 * 	@return 		Vec3, inverted vector.
	 */
	Vec3 operator-(const Vec3 &vec);

	/**
	 *	Find the dot product of the two given vectors.
	 *
	 * 	@param 	v1 	const Vec3 reference, first vector argument.
	 * 	@param 	v2 	const Vec3 reference, second vector argument.
	 * 	@return 	scalar, dot product of the two given vectors.
	 */
	scalar Dot(const Vec3 &v1, const Vec3 &v2);
	/**
	 *	Find the cross product of the two given vectors.
	 *
	 * 	@param 	v1 	const Vec3 reference, first vector argument.
	 * 	@param 	v2 	const Vec3 reference, second vector argument.
	 * 	@return 	Vec3, cross product of two vectors.
	 */
	Vec3 Cross(const Vec3 &v1, const Vec3 &v2);
	/**
	 * 	Return the magnitude of the given vector,
	 *
	 * 	@param 	vec 	const Vec3 reference, vector to find the magnitude of.
	 * 	@return 		scalar, magnitude of given vector.
	 */
	scalar Magnitude(const Vec3 &vec);
	/**
	 * 	Return a vector in the same direction as the given vector
	 * 	but with a magnitude of 1.
	 *
	 *	@param 	vec 	const Vec3 reference, vector to normalize.
	 * 	@return 		Vec3, vector in same direction as given vector, but with a magnitude of 1. 	
	 */
	Vec3 Normalize(const Vec3 &vec);
	/**
	 *	Return the inverse of the given vector.
	 *
	 * 	Change the sign of each component of the given vector.
	 *
	 * 	@param 	vec 	const Vec3 reference, vector to invert.
	 * 	@return 		Vec3, inverted vector.
	 */
	Vec3 Invert(const Vec3 &vec);
	/**
	*	Return the angle between two vectors.
	* 	Based on cosine rule. Note that order does not matter,
	*	and the result will always be less than pi rads.
	*
	* 	@param 	a 	const Vec3 reference, first vector
	* 	@param 	b 	const Vec3 reference, second vector
	* 	@return 	float, angle in radians between a and b
	*/
	float AngleBetween(const Vec3 &a, const Vec3 &b);
    /**
    *	Return the vector projection of A onto B. In other words,
    *   find the vector component of A in the B direction.
    *
    * 	@param 	a 	const Vec3 reference, vector to project
    * 	@param 	b 	const Vec3 reference, vector being projected onto
    * 	@return 	Vec3, projection of A onto B
    */
    Vec3 ProjectAontoB(const Vec3 &a, const Vec3 &b);
}

#endif // WGT_VEC3