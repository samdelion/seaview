# README #

### Description ###

ICT290 Project

### How to Get Setup ###

#### Windows ####

* Clone the repository
* Download one of the compressed external_dependencies files in the Downloads section of this repository
* Extract the contents of external_dependencies file
* Place the external_dependencies file such that the folder structure is as follows:

```
#!c++

 |
 - external_dependencies/
    - cmake../
    - GLEW/
 |
 - seaview/
```

* Run the "build.bat" executable found in the root directory of the repository
* Navigate to seaview/builds and build the ALL_BUILD project CMake has created for you.
* In VS, set the seaview project as the default startup project.
* Build and run the seaview project to play the game!

#### Mac/Linux ####

* Install the following dependencies: CMake, GLEW, SDL-1.2, Unittest++ and freeglut.
* Create a new folder in the root directory of seaview called "build"
* Execute "cmake .." to generate build files
* Execute "make" to build project
* Execute "bin/seaview" to play the game!
