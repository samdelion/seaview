# - Try to find UnitTest++
#
#   Author: Erwin Coumans
#   Source: https://github.com/erwincoumans/rbdl/blob/master/CMake/FindUnitTest%2B%2B.cmake
#

SET (UNITTEST++_FOUND FALSE)

FIND_PATH (UNITTEST++_INCLUDE_DIR UnitTest++.h
	/usr/include/unittest++ 
	/usr/local/include/unittest++ 
	/opt/local/include/unittest++
	$ENV{UNITTESTXX_PATH}/src 
	$ENV{UNITTESTXX_INCLUDE_PATH}
	${CMAKE_SOURCE_DIR}/../external_dependencies/unittest-cpp/include/unittest++
	)

FIND_LIBRARY (UNITTEST++_LIBRARY_DEBUG NAMES UnitTest++ PATHS 
	/usr/lib 
	/usr/local/lib 
	/opt/local/lib 
	$ENV{UNITTESTXX_PATH} 
	ENV{UNITTESTXX_LIBRARY_PATH}
	${CMAKE_SOURCE_DIR}/../external_dependencies/unittest-cpp/builds/Debug
	)
FIND_LIBRARY (UNITTEST++_LIBRARY NAMES UnitTest++ PATHS 
	/usr/lib 
	/usr/local/lib 
	/opt/local/lib 
	$ENV{UNITTESTXX_PATH} 
	ENV{UNITTESTXX_LIBRARY_PATH}
	${CMAKE_SOURCE_DIR}/../external_dependencies/unittest-cpp/builds/Release
	)

IF (UNITTEST++_INCLUDE_DIR AND UNITTEST++_LIBRARY AND UNITTEST++_LIBRARY_DEBUG)
	SET (UNITTEST++_FOUND TRUE)
	SET (UNITTEST++_LIBRARIES
		 ${UNITTEST++_LIBRARY}
		 ${UNITTEST++_LIBRARY_DEBUG}
		)
ENDIF (UNITTEST++_INCLUDE_DIR AND UNITTEST++_LIBRARY AND UNITTEST++_LIBRARY_DEBUG)

IF (UNITTEST++_FOUND)
   IF (NOT UnitTest++_FIND_QUIETLY)
      MESSAGE(STATUS "Found UnitTest++: ${UNITTEST++_LIBRARY}")
   ENDIF (NOT UnitTest++_FIND_QUIETLY)
ELSE (UNITTEST++_FOUND)
   IF (UnitTest++_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find UnitTest++")
   ENDIF (UnitTest++_FIND_REQUIRED)
ENDIF (UNITTEST++_FOUND)

MARK_AS_ADVANCED (
	UNITTEST++_INCLUDE_DIR
	UNITTEST++_LIBRARY
	UNITTEST++_LIBRARY_DEBUG
	)
