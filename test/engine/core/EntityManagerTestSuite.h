#include <UnitTest++.h>

#include "engine/core/EntityManager.h"
#include "engine/components/TransformComponentManager.h"

#define MINIMUM_INDICES_QUEUE 1024

SUITE(EntityManager)
{
    TEST(TestIsValidNonExistantEntity)
    {
        sea::EntityManager manager; 
        sea::Entity e;
        e.id = 0;

        CHECK_EQUAL(false, manager.IsValid(e));
    }

    TEST(TestIsValidCreatedEntity)
    {
        sea::EntityManager manager;
        sea::Entity e = manager.Create();

        CHECK_EQUAL(true, manager.IsValid(e));
    }

    TEST(TestIsValidDestroyedEntity)
    {
        sea::EntityManager manager;
        sea::Entity e = manager.Create();

        manager.Destroy(e);

        CHECK_EQUAL(false, manager.IsValid(e));
    }

    TEST(TestCreateLargeNumberOfEntities)
    {
        sea::EntityManager manager;
        sea::Entity entities[2048];

        for (int i = 0; i < 2048; ++i)
        {
            entities[i] = manager.Create();
        }

        for (int i = 0; i < 2048; ++i)
        {
            CHECK_EQUAL(true, manager.IsValid(entities[i]));
        }

        for (int i = 0; i < 2048; ++i)
        {
            manager.Destroy(entities[i]);
            CHECK_EQUAL(false, manager.IsValid(entities[i]));
        }
    }

    TEST(TestCreateEntityFromFreeList)
    {
        sea::EntityManager manager;
        sea::Entity entities[MINIMUM_INDICES_QUEUE];

        // Fill up free list by creating and destroying MINIMUM_INDICES_QUEUE entities
        for (int i = 0; i < MINIMUM_INDICES_QUEUE; ++i)
        {
            entities[i] = manager.Create();
        }
        for (int i = 0; i < MINIMUM_INDICES_QUEUE; ++i)
        {
            manager.Destroy(entities[i]);
        }

        // Create new id, will draw from free list
        sea::Entity e = manager.Create();
        // Check that id really pulled from free list
        CHECK(e.GetIndex() == entities[0].GetIndex());
        // Check generations are not the same
        CHECK(e.GetGeneration() != entities[0].GetGeneration());

        // Ensure old entity is valid
        CHECK_EQUAL(false, manager.IsValid(entities[0]));
        // Ensure new entity is invalid
        CHECK_EQUAL(true, manager.IsValid(e));
    }

    TEST(TestRemoveEntityComponentsFromManager)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        entityManager.RegisterComponentManager(&transformManager);

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        // Test instance is valid
        CHECK_EQUAL(true, i.index != -1);
        
        // Delete entity
        entityManager.Destroy(e);

        // Get instance
        i = transformManager.VGetInstanceForEntity(e);

        // Test instance is invalid
        CHECK_EQUAL(true, i.index == -1);
    }
}
