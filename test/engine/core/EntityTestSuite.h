#include <UnitTest++.h>

#include "engine/core/Entity.h"

SUITE(Entity)
{
    TEST(TestGetIndex0)
    {
        sea::Entity e;
        e.id = 0;

        CHECK_EQUAL(0, e.GetIndex());
    }

    TEST(TestGetGeneration0)
    {
        sea::Entity e;
        e.id = 0;

        CHECK_EQUAL(0, e.GetGeneration());
    }

    TEST(TestGetIndex1)
    {
        sea::Entity e;
        e.id = 1;

        CHECK_EQUAL(1, e.GetIndex());
    }

    TEST(TestGetGeneration1)
    {
        sea::Entity e;
        e.id = (1 << sea::Entity::ENTITY_INDEX_BITS);

        CHECK_EQUAL(1, e.GetGeneration());
    }

    TEST(TestGetComposite)
    {
        sea::Entity e;
        e.id = (1 << sea::Entity::ENTITY_INDEX_BITS) + 1;

        CHECK_EQUAL(1, e.GetIndex());
        CHECK_EQUAL(1, e.GetGeneration());
    }
}
