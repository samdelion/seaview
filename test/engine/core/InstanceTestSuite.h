#include <UnitTest++.h>

#include "engine/core/Instance.h"

SUITE(Instance)
{
    TEST(TestMakeInstance)
    {
        sea::Instance instance = sea::Instance::MakeInstance(1);

        CHECK_EQUAL(1, instance.index);
    }

    TEST(TestMakeInstance2)
    {
        sea::Instance instance = sea::Instance::MakeInstance(-1);

        CHECK_EQUAL(-1, instance.index);
    }
}
