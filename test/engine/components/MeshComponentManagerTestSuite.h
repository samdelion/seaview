#include <UnitTest++.h>

#include "engine/core/EntityManager.h"
#include "engine/components/MeshComponentManager.h"

SUITE(MeshComponentManager)
{
    TEST(TestAddDefaultComponent)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = meshManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(true, meshManager.GetVertexArrayID(i) == 0);
    }

    TEST(TestCreateLargeNumberOfComponents)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        for (int c = 0; c < 64; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = meshManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, meshManager.GetVertexArrayID(i) == 0); 
        }
    }

    TEST(TestCreateVeryLargeNumberOfComponents)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        for (int c = 0; c < 4096; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = meshManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, meshManager.GetVertexArrayID(i) == 0); 
        }
    }
    
    TEST(TestGetNumInstancesEmpty)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        CHECK_EQUAL(0, meshManager.VGetNumInstances());
    }

    TEST(TestGetNumInstancesOne)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = meshManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(1, meshManager.VGetNumInstances());
    }
    
    TEST(TestGetNumInstancesVeryLarge)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 4096;
        for (int c = 0; c < numInstances; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = meshManager.VCreateComponentForEntity(e);
        }

        CHECK_EQUAL(numInstances, meshManager.VGetNumInstances());
    }

    TEST(TestGetInstanceForEntity)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = meshManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(true, i == meshManager.VGetInstanceForEntity(e));
    }

    TEST(TestGetInstanceManyInstances)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;
    
        const unsigned int numInstances = 4096;
        sea::Entity e[numInstances];
        sea::Instance i[numInstances];

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = meshManager.VCreateComponentForEntity(e[c]);
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, i[c] == meshManager.VGetInstanceForEntity(e[c]));
        }
    }

    TEST(TestSetVertexArrayID)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;
       
        sea::Entity e = entityManager.Create();
        sea::Instance i = meshManager.VCreateComponentForEntity(e);

        meshManager.SetVertexArrayID(i, 2);

        CHECK_EQUAL(true, meshManager.GetVertexArrayID(i) == 2);
    }

    TEST(TestSetVertexArrayIDMany)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        unsigned int vaoID[numInstances];

        vaoID[0] = 0;
        vaoID[1] = 1; 
        vaoID[2] = 2; 

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = meshManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, meshManager.SetVertexArrayID(i[c], vaoID[c]));
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, meshManager.GetVertexArrayID(i[c]) == vaoID[c]);
        }
    }

    TEST(TestAddSubMesh)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = meshManager.VCreateComponentForEntity(e);

        sea::SubMesh subMesh = sea::SubMesh(0, 33);

        meshManager.AddSubMesh(i, subMesh);

        CHECK_EQUAL(true, meshManager.GetVertexArrayID(i) == 0); 
        CHECK_EQUAL(true, meshManager.GetSubMesh(i, 0) == subMesh); 
    }

    TEST(TestAddSubMeshMany)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        sea::SubMesh subMesh[numInstances];

        subMesh[0] = sea::SubMesh(0, 33);
        subMesh[1] = sea::SubMesh(33, 66); 
        subMesh[2] = sea::SubMesh(66, 99); 

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = meshManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, meshManager.AddSubMesh(i[c], subMesh[c]));
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, meshManager.GetSubMesh(i[c], 0) == subMesh[c]);
        }
    }

    TEST(TestRemoveInstanceProperlyReducesNumberOfActiveInstances)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = meshManager.VCreateComponentForEntity(e);
        
        CHECK_EQUAL(1, meshManager.VGetNumInstances());
        meshManager.VRemoveInstance(i);
        CHECK_EQUAL(0, meshManager.VGetNumInstances());
    }

    TEST(TestRemoveInstanceProperlyReducesNumberOfActiveInstancesManyEntities)
    {
        sea::MeshComponentManager       meshManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 4096;
        for (int c = 0; c < numInstances; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = meshManager.VCreateComponentForEntity(e);
        }

        CHECK_EQUAL(numInstances, meshManager.VGetNumInstances());

        meshManager.VRemoveInstance(sea::Instance::MakeInstance(1));

        CHECK_EQUAL(numInstances - 1, meshManager.VGetNumInstances());
    }

    TEST(TestRemoveInstanceDoesNotModifyOtherComponents)
    {
        sea::MeshComponentManager   meshManager;
        sea::EntityManager          entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        unsigned int vaoID[numInstances];

        vaoID[0] = 4;
        vaoID[1] = 1;
        vaoID[2] = 300;

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = meshManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, meshManager.SetVertexArrayID(i[c], vaoID[c]));
        }

        meshManager.VRemoveInstance(i[0]);

        CHECK_EQUAL(true, meshManager.GetVertexArrayID(meshManager.VGetInstanceForEntity(e[1])) == vaoID[1]);
        CHECK_EQUAL(true, meshManager.GetVertexArrayID(meshManager.VGetInstanceForEntity(e[2])) == vaoID[2]);
    }
}
