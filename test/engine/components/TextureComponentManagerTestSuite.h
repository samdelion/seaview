#include <UnitTest++.h>

#include "engine/core/EntityManager.h"
#include "engine/components/TextureComponentManager.h"

SUITE(TextureComponentManager)
{
    TEST(TestAddDefaultComponent)
    {
        sea::TextureComponentManager    textureManager;
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = textureManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(true, textureManager.GetTextureID(i) == 0);
    }

    TEST(TestCreateLargeNumberOfComponents)
    {
        sea::TextureComponentManager        textureManager; 
        sea::EntityManager                  entityManager;

        for (int c = 0; c < 64; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = textureManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, textureManager.GetTextureID(i) == 0); 
        }
    }

    TEST(TestCreateVeryLargeNumberOfComponents)
    {
        sea::TextureComponentManager        textureManager; 
        sea::EntityManager                  entityManager;

        for (int c = 0; c < 4096; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = textureManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, textureManager.GetTextureID(i) == 0); 
        }
    }

    TEST(TestGetNumInstancesEmpty)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;

        CHECK_EQUAL(0, textureManager.VGetNumInstances());
    }

    TEST(TestGetNumInstancesOne)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = textureManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(1, textureManager.VGetNumInstances());
    }
    
    TEST(TestGetNumInstancesVeryLarge)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 4096;
        for (int c = 0; c < numInstances; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = textureManager.VCreateComponentForEntity(e);
        }

        CHECK_EQUAL(numInstances, textureManager.VGetNumInstances());
    }

    TEST(TestGetInstanceForEntity)
    {
        sea::TextureComponentManager       textureManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = textureManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(true, i == textureManager.VGetInstanceForEntity(e));
    }

    TEST(TestGetInstanceManyInstances)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;
    
        const unsigned int numInstances = 4096;
        sea::Entity e[numInstances];
        sea::Instance i[numInstances];

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = textureManager.VCreateComponentForEntity(e[c]);
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, i[c] == textureManager.VGetInstanceForEntity(e[c]));
        }
    }

    TEST(TestSetTextureID)
    {
        sea::TextureComponentManager    textureManager;
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = textureManager.VCreateComponentForEntity(e);

        textureManager.SetTextureID(i, 2);

        CHECK_EQUAL(true, textureManager.GetTextureID(i) == 2);
    }

    TEST(TestSetTextureIDMany)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity     e[numInstances];
        sea::Instance   i[numInstances];
        unsigned int    textureID[numInstances];

        textureID[0] = 0;
        textureID[1] = 1; 
        textureID[2] = 2; 

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = textureManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, textureManager.SetTextureID(i[c], textureID[c]));
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, textureManager.GetTextureID(i[c]) == textureID[c]);
        }
    }

    TEST(TestRemoveInstanceProperlyReducesNumberOfActiveInstances)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = textureManager.VCreateComponentForEntity(e);
        
        CHECK_EQUAL(1, textureManager.VGetNumInstances());
        textureManager.VRemoveInstance(i);
        CHECK_EQUAL(0, textureManager.VGetNumInstances());
    }

    TEST(TestRemoveInstanceProperlyReducesNumberOfActiveInstancesManyEntities)
    {
        sea::TextureComponentManager    textureManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 4096;
        for (int c = 0; c < numInstances; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = textureManager.VCreateComponentForEntity(e);
        }

        CHECK_EQUAL(numInstances, textureManager.VGetNumInstances());

        textureManager.VRemoveInstance(sea::Instance::MakeInstance(1));

        CHECK_EQUAL(numInstances - 1, textureManager.VGetNumInstances());
    }

    TEST(TestRemoveInstanceDoesNotModifyOtherComponents)
    {
        sea::TextureComponentManager    textureManager;
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity     e[numInstances];
        sea::Instance   i[numInstances];
        unsigned int    textureID[numInstances];

        textureID[0] = 4;
        textureID[1] = 1;
        textureID[2] = 300;

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = textureManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, textureManager.SetTextureID(i[c], textureID[c]));
        }

        textureManager.VRemoveInstance(i[0]);

        CHECK_EQUAL(true, textureManager.GetTextureID(textureManager.VGetInstanceForEntity(e[1])) == textureID[1]);
        CHECK_EQUAL(true, textureManager.GetTextureID(textureManager.VGetInstanceForEntity(e[2])) == textureID[2]);
    }
}
