#include <iostream>

#include <UnitTest++.h>

#include "engine/core/EntityManager.h"
#include "engine/components/TransformComponentManager.h"
#include "math/Globals.h"

SUITE(TransformComponentManager)
{
    TEST(TestAddDefaultComponent)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3());
        CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
        CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
    }

    TEST(TestCreateLargeNumberOfComponents)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        for (int c = 0; c < 64; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = transformManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3());
            CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
            CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
        }
    }

    TEST(TestCreateVeryLargeNumberOfComponents)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        for (int c = 0; c < 4096; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = transformManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3());
            CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
            CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
        }
    }

    TEST(TestGetNumInstancesEmpty)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        CHECK_EQUAL(0, transformManager.VGetNumInstances());
    }

    TEST(TestGetNumInstancesOne)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(1, transformManager.VGetNumInstances());
    }
    
    TEST(TestGetNumInstancesVeryLarge)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 4096;
        for (int c = 0; c < numInstances; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = transformManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3());
            CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
            CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
        }

        CHECK_EQUAL(numInstances, transformManager.VGetNumInstances());
    }

    TEST(TestGetInstanceForEntity)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        CHECK_EQUAL(true, i == transformManager.VGetInstanceForEntity(e));
    }

    TEST(TestGetInstanceManyInstances)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;
    
        const unsigned int numInstances = 4096;
        sea::Entity e[numInstances];
        sea::Instance i[numInstances];

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = transformManager.VCreateComponentForEntity(e[c]);
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, i[c] == transformManager.VGetInstanceForEntity(e[c]));
        }
    }
    
    TEST(TestSetPosition)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        sea_math::Vec3 pos = sea_math::Vec3(3.0f, 3.0f, -3.0f);

        CHECK_EQUAL(true, transformManager.SetPosition(i, pos));
        CHECK_EQUAL(true, transformManager.GetPosition(i) == pos); 
        CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
        CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
    }

    TEST(TestSetPositionMany)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        sea_math::Vec3 pos[numInstances];

        pos[0] = sea_math::Vec3(3.0f, 3.0f, -3.0f);
        pos[1] = sea_math::Vec3(6.0f, 6.0f, -7.0f);
        pos[2] = sea_math::Vec3(300.0f, 32.0f, -75.0f);

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = transformManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, transformManager.SetPosition(i[c], pos[c]));
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, transformManager.GetPosition(i[c]) == pos[c]); 
            CHECK_EQUAL(true, transformManager.GetOrientation(i[c]) == sea_math::Quat());
            CHECK_EQUAL(true, transformManager.GetScale(i[c]) == sea_math::Vec3());
        }
    }

    TEST(TestSetOrientation)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        sea_math::Quat orientation = sea_math::Quat(1.0f);
        orientation.Offset(sea_math::Vec3(0.0f, 1.0f, 0.0f), sea_math::SEA_PI/6); 

        CHECK_EQUAL(true, transformManager.SetOrientation(i, orientation));
        CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3()); 
        CHECK_EQUAL(true, transformManager.GetOrientation(i) == orientation);
        CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
    }

    TEST(TestSetOrientationMany)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        sea_math::Quat rot[numInstances];

        rot[0] = sea_math::Quat(3.0f, 3.0f, -3.0f);
        rot[1] = sea_math::Quat(6.0f, 6.0f, -7.0f);
        rot[2] = sea_math::Quat(300.0f, 32.0f, -75.0f);

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = transformManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, transformManager.SetOrientation(i[c], rot[c]));
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, transformManager.GetPosition(i[c]) == sea_math::Vec3()); 
            CHECK_EQUAL(true, transformManager.GetOrientation(i[c]) == rot[c]);
            CHECK_EQUAL(true, transformManager.GetScale(i[c]) == sea_math::Vec3());
        }
    }

    TEST(TestSetScale)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);

        sea_math::Vec3 scale = sea_math::Vec3(3.0f, 3.0f, -3.0f);

        CHECK_EQUAL(true, transformManager.SetScale(i, scale));
        CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3()); 
        CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
        CHECK_EQUAL(true, transformManager.GetScale(i) == scale);
    }

    TEST(TestSetScaleMany)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        sea_math::Vec3 scale[numInstances];

        scale[0] = sea_math::Vec3(3.0f, 3.0f, -3.0f);
        scale[1] = sea_math::Vec3(6.0f, 6.0f, -7.0f);
        scale[2] = sea_math::Vec3(300.0f, 32.0f, -75.0f);

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = transformManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, transformManager.SetScale(i[c], scale[c]));
        }

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            CHECK_EQUAL(true, transformManager.GetPosition(i[c]) == sea_math::Vec3()); 
            CHECK_EQUAL(true, transformManager.GetOrientation(i[c]) == sea_math::Quat());
            CHECK_EQUAL(true, transformManager.GetScale(i[c]) == scale[c]);
        }
    }

    TEST(TestRemoveInstanceProperlyReducesNumberOfActiveInstances)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        sea::Entity e = entityManager.Create();
        sea::Instance i = transformManager.VCreateComponentForEntity(e);
        
        CHECK_EQUAL(1, transformManager.VGetNumInstances());
        transformManager.VRemoveInstance(i);
        CHECK_EQUAL(0, transformManager.VGetNumInstances());
    }

    TEST(TestRemoveInstanceProperlyReducesNumberOfActiveInstancesManyEntities)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 4096;
        for (int c = 0; c < numInstances; ++c)
        {
            sea::Entity e = entityManager.Create();
            sea::Instance i = transformManager.VCreateComponentForEntity(e);

            CHECK_EQUAL(true, transformManager.GetPosition(i) == sea_math::Vec3());
            CHECK_EQUAL(true, transformManager.GetOrientation(i) == sea_math::Quat());
            CHECK_EQUAL(true, transformManager.GetScale(i) == sea_math::Vec3());
        }

        CHECK_EQUAL(numInstances, transformManager.VGetNumInstances());

        transformManager.VRemoveInstance(sea::Instance::MakeInstance(1));

        CHECK_EQUAL(numInstances - 1, transformManager.VGetNumInstances());
    }

    TEST(TestRemoveInstanceDoesNotModifyOtherComponents)
    {
        sea::TransformComponentManager  transformManager; 
        sea::EntityManager              entityManager;

        const unsigned int numInstances = 3;

        sea::Entity e[numInstances];
        sea::Instance i[numInstances];
        sea_math::Vec3 pos[numInstances];

        pos[0] = sea_math::Vec3(3.0f, 3.0f, -3.0f);
        pos[1] = sea_math::Vec3(6.0f, 6.0f, -7.0f);
        pos[2] = sea_math::Vec3(300.0f, 32.0f, -75.0f);

        for (unsigned int c = 0; c < numInstances; ++c)
        {
            e[c] = entityManager.Create();
            i[c] = transformManager.VCreateComponentForEntity(e[c]);

            CHECK_EQUAL(true, transformManager.SetPosition(i[c], pos[c]));
        }
        
        transformManager.VRemoveInstance(i[1]); 

        CHECK_EQUAL(true, transformManager.GetPosition(transformManager.VGetInstanceForEntity(e[0])) == pos[0]);
        CHECK_EQUAL(true, transformManager.GetPosition(transformManager.VGetInstanceForEntity(e[2])) == pos[2]);
    }
}
