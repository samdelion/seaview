#include <cstdio>

#include <UnitTest++.h>

#include "engine/Defines.h"
#include "engine/util/BMPFile.h"

SUITE(BMPFile)
{
    TEST(TestInit32ARGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/green_32_ARGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        fclose(bmpStream);
    }
    
    TEST(TestImageWidth32ARGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/bushCourtBanner_32_ARGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        CHECK_EQUAL(1024, bmpFile.GetImageWidth());

        fclose(bmpStream);
    }
    
    TEST(TestImageHeight32ARGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/bushCourtBanner_32_ARGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        CHECK_EQUAL(512, bmpFile.GetImageHeight());

        fclose(bmpStream);
    }

    TEST(TestGetImageData32ARGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/green_32_ARGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        const sea::BMPFile::Pixel *pixels = bmpFile.GetImageData();

        CHECK(NULL != pixels);
        for (int row = 0; row < bmpFile.GetImageHeight(); ++row)
        {
            for (int col = 0; col < bmpFile.GetImageWidth(); ++col)
            {
                CHECK_EQUAL(0, pixels[(row * bmpFile.GetImageWidth()) + col].r);
                CHECK_EQUAL(255, pixels[(row * bmpFile.GetImageWidth()) + col].g);
                CHECK_EQUAL(0, pixels[(row * bmpFile.GetImageWidth()) + col].b);
                CHECK_EQUAL(255, pixels[(row * bmpFile.GetImageWidth()) + col].a);
            }
        }

        fclose(bmpStream);
    }
    
    TEST(TestInit32XRGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/bushCourtBanner_32_XRGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        fclose(bmpStream);
    }

    TEST(TestImageWidth32XRGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/bushCourtBanner_32_XRGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        CHECK_EQUAL(1024, bmpFile.GetImageWidth());

        fclose(bmpStream);
    }
    
    TEST(TestImageHeight32XRGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/bushCourtBanner_32_XRGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        CHECK_EQUAL(512, bmpFile.GetImageHeight());

        fclose(bmpStream);
    }

    TEST(TestGetImageData32XRGB)
    {
        FILE *bmpStream = fopen(PATH_TO_ASSETS "test/green_32_XRGB.bmp", "r");
        sea::BMPFile bmpFile = sea::BMPFile();

        CHECK_EQUAL(true, bmpFile.Init(bmpStream));

        const sea::BMPFile::Pixel *pixels = bmpFile.GetImageData();

        CHECK(NULL != pixels);
        for (int row = 0; row < bmpFile.GetImageHeight(); ++row)
        {
            for (int col = 0; col < bmpFile.GetImageWidth(); ++col)
            {
                CHECK_EQUAL(0, pixels[(row * bmpFile.GetImageWidth()) + col].r);
                CHECK_EQUAL(255, pixels[(row * bmpFile.GetImageWidth()) + col].g);
                CHECK_EQUAL(0, pixels[(row * bmpFile.GetImageWidth()) + col].b);
                CHECK_EQUAL(0, pixels[(row * bmpFile.GetImageWidth()) + col].a);
            }
        }

        fclose(bmpStream);
    }

    //TEST(TestExportAsRaw)
    //{
        //FILE *bmpStream = fopen(PATH_TO_ASSETS "test/bushCourtBanner.bmp", "r");
        //sea::BMPFile bmpFile = sea::BMPFile();

        //CHECK_EQUAL(true, bmpFile.Init(bmpStream));
        //CHECK_EQUAL(1000, bmpFile.GetImageWidth());
        //CHECK_EQUAL(400, bmpFile.GetImageHeight());

        //unsigned int imgWidth = bmpFile.GetImageWidth();
        //unsigned int imgHeight = bmpFile.GetImageHeight();

        //fclose(bmpStream);

        //FILE *fileOut = fopen(PATH_TO_ASSETS "test/bannerFinal.raw", "wb");

        //const sea::BMPFile::Pixel *pixels = bmpFile.GetImageData();
       
        //uint8_t *rawFileData = new uint8_t[imgWidth * imgHeight * 3];
        //uint64_t pos = 0;
        //for (uint32_t row = 0; row < bmpFile.GetImageHeight(); ++row)
        //{
            //for (uint32_t col = 0; col < bmpFile.GetImageWidth(); ++col)
            //{
                //rawFileData[pos] = (uint8_t)pixels[row*imgWidth + col].r;
                //++pos;
                //rawFileData[pos] = (uint8_t)pixels[row*imgWidth + col].g;
                //++pos;
                //rawFileData[pos] = (uint8_t)pixels[row*imgWidth + col].b;
                //++pos;
            //}
        //}

        //fwrite(rawFileData, sizeof(uint32_t), imgWidth * imgHeight, fileOut);

        //fseek(fileOut, 0, SEEK_SET);
        //delete[] rawFileData;
        //rawFileData = new uint8_t[imgWidth * imgHeight * 3];
        //fread((void *)rawFileData, imgWidth * imgHeight * 3, 1, fileOut);
        //for (int i = 0; i < imgWidth * imgHeight * 3; ++i)
        //{
            //printf("%u ", rawFileData[i]); 
            //(i + 1) % 3 == 0 ? printf("\n") : 0;
        //}

        //delete[] rawFileData;
        //fclose(fileOut);
    //}
}
