#include <UnitTest++.h>

#include "engine/Defines.h"
#include "engine/util/ObjFile.h"

SUITE(ObjFile)
{
    TEST(TestInit)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        CHECK_EQUAL(true, inFile.good());
        CHECK_EQUAL(true, obj.Init(inFile));

        inFile.close();
    }

    TEST(TestGetVertices)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        obj.Init(inFile);

        float vertices[8][4] = {
            {1.000000, -1.000000, -1.000000, 1.0f},
            {1.000000, -1.000000, 1.000000, 1.0f},
            {-1.000000, -1.000000, 1.000000, 1.0f},
            {-1.000000, -1.000000, -1.000000, 1.0f},
            {1.000000, 1.000000, -1.000000, 1.0f},
            {1.000000, 1.000000, 1.000000, 1.0f},
            {-1.000000, 1.000000, 1.000000, 1.0f},
            {-1.000000, 1.000000, -1.000000, 1.0f}
        };

        for (int i = 0; i < 8; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                CHECK_EQUAL(vertices[i][j], obj.GetVertices()[i][j]);
            }
        }

        inFile.close();
    }

    TEST(TestGetTexCoords)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        obj.Init(inFile);

        float texCoords[14][3] = {
            {0.500000, 0.749957, 0.0f},
            {0.250043, 0.749957, 0.0f},
            {0.250043, 0.500000, 0.0f},
            {0.250043, 0.250043, 0.0f},
            {0.250043, 0.000087, 0.0f},
            {0.500000, 0.000087, 0.0f},
            {0.749957, 0.500000, 0.0f},
            {0.749957, 0.749957, 0.0f},
            {0.500000, 0.999913, 0.0f},
            {0.250043, 0.999913, 0.0f},
            {0.000087, 0.749957, 0.0f},
            {0.000087, 0.500000, 0.0f},
            {0.500000, 0.500000, 0.0f},
            {0.500000, 0.250043, 0.0f}
        };

        for (int i = 0; i < 14; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                CHECK_EQUAL(texCoords[i][j], obj.GetTexCoords()[i][j]);
            }
        }

        inFile.close();
    }

    TEST(TestGetNormals)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        obj.Init(inFile);

        float normals[6][3] = {
            {0.000000, -1.000000, 0.000000},
            {0.000000, 1.000000, 0.000000},
            {1.000000, 0.000000, 0.000000},
            {-0.000000, 0.000000, 1.000000},
            {-1.000000, -0.000000, -0.000000},
            {0.000000, 0.000000, -1.000000},
        };

        for (int i = 0; i < 6; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                CHECK_EQUAL(normals[i][j], obj.GetNormals()[i][j]);
            }
        }

        inFile.close();
    }

    TEST(TestGetVertexIndices)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        obj.Init(inFile);

        size_t vertexIndices[36] = {
            2, 3, 4,
            8, 7, 6,
            5, 6, 2,
            6, 7, 3,
            3, 7, 8,
            1, 4, 8,
            1, 2, 4,
            5, 8, 6,
            1, 5, 2,
            2, 6, 3,
            4, 3, 8,
            5, 1, 8,
        };

        for (int i = 0; i < 36; ++i)
        {
            // Obj files index from 1, us from 0, hence -1
            CHECK_EQUAL(vertexIndices[i] - 1, obj.GetVertexIndices()[0][i]);
        }

        inFile.close();
    }

    TEST(TestGetNormalIndices)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        obj.Init(inFile);

        size_t normalIndices[36] = {
            1, 1, 1,
            2, 2, 2,
            3, 3, 3,
            4, 4, 4,
            5, 5, 5,
            6, 6, 6,
            1, 1, 1,
            2, 2, 2,
            3, 3, 3,
            4, 4, 4,
            5, 5, 5,
            6, 6, 6,
        };

        for (int i = 0; i < 36; ++i)
        {
            // Obj files index from 1, us from 0, hence -1
            CHECK_EQUAL(normalIndices[i] - 1, obj.GetNormalIndices()[0][i]);
        }

        inFile.close();
    }

    TEST(TestGetTexCoordIndices)
    {
        sea::ObjFile    obj;
        std::ifstream   inFile(PATH_TO_ASSETS "test/cube.obj");

        obj.Init(inFile);

        size_t texCoordIndices[36] = {
            1, 2, 3,
            4, 5, 6,
            7, 8, 1,
            9, 10, 2,
            2, 11, 12,
            13, 3, 4,
            13, 1, 3,
            14, 4, 6,
            13, 7, 1,
            1, 9, 2,
            3, 2, 12,
            14, 13, 4,
        };

        for (int i = 0; i < 36; ++i)
        {
            CHECK_EQUAL(texCoordIndices[i] - 1, obj.GetTexCoordIndices()[0][i]);
        }

        inFile.close();
    }

}
