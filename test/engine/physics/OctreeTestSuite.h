#include <iostream>

#include <UnitTest++.h>

#include "engine/physics/Octree.h"

SUITE(Octree)
{
    /* TEST(TestConstructor) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     unsigned int a = 4; */
    /*     sea::Octree<unsigned int *>::Object obj; */

    /*     obj.contents = &a; */
    /*     obj.position = sea_math::Vec3(1.0f, 1.0f, 1.0f); */
    /* } */

    /* TEST(TestGetNodeToInsertInto) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     unsigned int a = 4; */
    /*     sea::Octree<unsigned int *>::Object obj; */

    /*     obj.contents = &a; */
    /*     obj.position = sea_math::Vec3(-1.0f, -1.0f, 1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1000); */

    /*     obj.position = sea_math::Vec3(1.0f, -1.0f, 1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1001); */

    /*     obj.position = sea_math::Vec3(-1.0f, -1.0f, -1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1010); */

    /*     obj.position = sea_math::Vec3(1.0f, -1.0f, -1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1011); */

    /*     obj.position = sea_math::Vec3(-1.0f, 1.0f, 1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1100); */

    /*     obj.position = sea_math::Vec3(1.0f, 1.0f, 1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1101); */

    /*     obj.position = sea_math::Vec3(-1.0f, 1.0f, -1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1110); */

    /*     obj.position = sea_math::Vec3(1.0f, 1.0f, -1.0f); */
    /*     CHECK_EQUAL(octree.GetNodeToInsertInto(octree.m_nodes[1], obj), 0b1111); */
    /* } */

    /* TEST(TestLookupNodeRoot) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     CHECK(octree.LookupNode(1) != nullptr); */

    /*     /\* for (uint32_t locCode = 0b100; locCode < UINT32_MAX; ++locCode) *\/ */
    /*     /\* { *\/ */
    /*     /\*     CHECK(octree.LookupNode(locCode) == nullptr); *\/ */
    /*     /\* } *\/ */
    /* } */

    /* TEST(TestInsertRoot) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     unsigned int array[8] = {0, 1, 2, 3, 4, 5, 6, 7}; */

    /*     sea::Octree<unsigned int *>::Object obj0(&array[0], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj0); */

    /*     CHECK(octree.LookupNode(0b1)->objects[0] == obj0); */

    /*     sea::Octree<unsigned int *>:: Object obj1(&array[1], sea_math::Vec3(1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj1); */

    /*     sea::Octree<unsigned int *>:: Object obj2(&array[2], sea_math::Vec3(-1.0f, -1.0f, -1.0f)); */
    /*     octree.Insert(obj2); */

    /*     sea::Octree<unsigned int *>:: Object obj3(&array[3], sea_math::Vec3(1.0f, -1.0f, -1.0f)); */
    /*     octree.Insert(obj3); */

    /*     sea::Octree<unsigned int *>:: Object obj4(&array[4], sea_math::Vec3(-1.0f, 1.0f, 1.0f)); */
    /*     octree.Insert(obj4); */

    /*     sea::Octree<unsigned int *>:: Object obj5(&array[5], sea_math::Vec3(1.0f, 1.0f, 1.0f)); */
    /*     octree.Insert(obj5); */

    /*     sea::Octree<unsigned int *>:: Object obj6(&array[6], sea_math::Vec3(-1.0f, 1.0f, -1.0f)); */
    /*     octree.Insert(obj6); */

    /*     sea::Octree<unsigned int *>:: Object obj7(&array[7], sea_math::Vec3(1.0f, 1.0f, -1.0f)); */
    /*     octree.Insert(obj7); */

    /*     CHECK(octree.LookupNode(0b1000)->objects[0] == obj0); */
    /*     CHECK(octree.LookupNode(0b1001)->objects[0] == obj1); */
    /*     CHECK(octree.LookupNode(0b1010)->objects[0] == obj2); */
    /*     CHECK(octree.LookupNode(0b1011)->objects[0] == obj3); */
    /*     CHECK(octree.LookupNode(0b1100)->objects[0] == obj4); */
    /*     CHECK(octree.LookupNode(0b1101)->objects[0] == obj5); */
    /*     CHECK(octree.LookupNode(0b1111)->objects[0] == obj7); */
    /* } */

    /* TEST(TestInsertStraddleBoundaryNoChildren) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     unsigned int array[8] = {0, 1, 2, 3, 4, 5, 6, 7}; */

    /*     sea::Octree<unsigned int *>::Object obj0(&array[0], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj0); */

    /*     CHECK(octree.LookupNode(0b1)->objects.size() == 1); */

    /*     sea::Octree<unsigned int *>::Object obj1(&array[1], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj1); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 2); */

    /*     sea::Octree<unsigned int *>::Object obj2(&array[2], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj2); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 3); */

    /*     sea::Octree<unsigned int *>::Object obj3(&array[3], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj3); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 4); */

    /*     sea::Octree<unsigned int *>::Object obj4(&array[4], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj4); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 5); */

    /*     sea::Octree<unsigned int *>::Object obj5(&array[5], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj5); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 6); */

    /*     sea::Octree<unsigned int *>::Object obj6(&array[6], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj6); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 7); */

    /*     sea::Octree<unsigned int *>::Object obj7(&array[7], sea_math::Vec3(-1.0f, -1.0f, 1.0f)); */
    /*     octree.Insert(obj7); */

    /*     CHECK(octree.LookupNode(0b1000111111111111111)->objects.size() == 8); */

    /*     CHECK(octree.LookupNode(0b1)->objects.size() == 0); */
    /*     CHECK(octree.LookupNode(0b1000)->objects.size() == 0); */
    /*     CHECK(octree.LookupNode(0b1000111)->objects.size() == 0); */
    /*     CHECK(octree.LookupNode(0b1000111111)->objects.size() == 0); */
    /*     CHECK(octree.LookupNode(0b1000111111111)->objects.size() == 0); */
    /*     CHECK(octree.LookupNode(0b1000111111111111)->objects.size() == 0); */
    /* } */

    /* TEST(TestInsertLowerLevel) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     unsigned int array[8] = {0, 1, 2, 3, 4, 5, 6, 7}; */

    /*     sea::Octree<unsigned int *>::Object obj0(&array[0], sea_math::Vec3(-63.0f, -63.0f, 63.0f)); */
    /*     octree.Insert(obj0); */

    /*     CHECK(octree.LookupNode(0b1)->objects.size() == 1); */

    /*     sea::Octree<unsigned int *>::Object obj1(&array[1], sea_math::Vec3(-1.0f, -63.0f, 63.0f)); */
    /*     octree.Insert(obj1); */

    /*     CHECK(octree.LookupNode(0b1)->objects.size() == 0); */
    /*     CHECK(octree.LookupNode(0b1000) != nullptr); */
    /*     CHECK(octree.LookupNode(0b1000000)->objects.size() == 1); */
    /*     CHECK(octree.LookupNode(0b1000001)->objects.size() == 1); */
    /* } */

    /* TEST(TestInsertStraddleBoundaryWithChildren) */
    /* { */
    /*     sea::Octree<unsigned int *> octree(sea_math::Vec3(0.0f, 0.0f, 0.0f), 64.0f); */

    /*     unsigned int array[8] = {0, 1, 2, 3, 4, 5, 6, 7}; */

    /*     sea::Octree<unsigned int *>::Object obj0(&array[0], sea_math::Vec3(-63.0f, -63.0f, 63.0f)); */
    /*     octree.Insert(obj0); */

    /*     sea::Octree<unsigned int *>::Object obj1(&array[1], sea_math::Vec3(-1.0f, -63.0f, 63.0f)); */
    /*     octree.Insert(obj1); */

    /*     sea::Octree<unsigned int *>::Object obj2(&array[2], sea_math::Vec3(-32.0f, -32.0f, 32.0f)); */
    /*     octree.Insert(obj2); */

    /*     CHECK(octree.LookupNode(0b1000)->objects.size() == 1); */
    /*     CHECK(octree.LookupNode(0b1000000)->objects.size() == 1); */
    /*     CHECK(octree.LookupNode(0b1000001)->objects.size() == 1); */
    /* } */
}
