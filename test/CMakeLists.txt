add_definitions(-DCMAKE_TEST_ASSET_DIR="${CMAKE_SOURCE_DIR}/test/assets/")

include_directories(${CMAKE_SOURCE_DIR}/test/)

add_executable(${PROJECT_NAME}_test main.cpp)

target_link_libraries(${PROJECT_NAME}_test ${LIBS} seaview_engine seaview_math)

install(TARGETS ${PROJECT_NAME}_test DESTINATION bin)
