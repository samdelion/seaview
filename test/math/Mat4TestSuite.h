#include <iostream>

#include <UnitTest++.h>

#include "math/Mat4.h"
#include "math/Globals.h"

SUITE(Mat4)
{
	TEST(TestDefaultConstructor)
	{
		sea_math::Mat4 mat = sea_math::Mat4();

		CHECK_EQUAL(true, sea_math::Vec4(1.0f, 0.0f, 0.0f, 0.0f) == mat[0]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 1.0f, 0.0f, 0.0f) == mat[1]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 0.0f, 1.0f, 0.0f) == mat[2]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 0.0f, 0.0f, 1.0f) == mat[3]);
	}

	TEST(TestLeadingConstructor)
	{
		sea_math::scalar leading = 1.0f;

		sea_math::Mat4 mat = sea_math::Mat4(leading);

		CHECK_EQUAL(true, sea_math::Vec4(1.0f, 0.0f, 0.0f, 0.0f) == mat[0]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 1.0f, 0.0f, 0.0f) == mat[1]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 0.0f, 1.0f, 0.0f) == mat[2]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 0.0f, 0.0f, 1.0f) == mat[3]);		

		leading = -2.5f;

		mat = sea_math::Mat4(leading);

		CHECK_EQUAL(true, sea_math::Vec4(leading, 0.0f, 0.0f, 0.0f) == mat[0]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, leading, 0.0f, 0.0f) == mat[1]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 0.0f, leading, 0.0f) == mat[2]);
		CHECK_EQUAL(true, sea_math::Vec4(0.0f, 0.0f, 0.0f, leading) == mat[3]);		
	}

	TEST(TestEquivalenceOperatorEqual)
	{
		sea_math::Mat4 mat1 = sea_math::Mat4(1.0f);
		sea_math::Mat4 mat2 = sea_math::Mat4(1.0f);

		CHECK_EQUAL(true, mat1 == mat2);
	}

	TEST(TestEquivalenceOperatorNotEqual)
	{
		sea_math::Mat4 mat1 = sea_math::Mat4(1.0f);
		sea_math::Mat4 mat2 = sea_math::Mat4(1.0f);

		mat2[0].y = 2.0f;

		CHECK_EQUAL(false, mat1 == mat2);
	}

	TEST(TestCopyConstructor)
	{
		sea_math::scalar leading = 3.4f;

		sea_math::Mat4 mat1 = sea_math::Mat4(leading);
		sea_math::Mat4 mat2 = sea_math::Mat4(mat1);

		CHECK_EQUAL(true, mat1 == mat2);
	}

	TEST(TestVectorConstructor)
	{
		sea_math::Vec4 v0 = sea_math::Vec4(1.0f, 0.0f, 0.0f, 0.0f);
		sea_math::Vec4 v1 = sea_math::Vec4(0.0f, 1.0f, 0.0f, 0.0f);
		sea_math::Vec4 v2 = sea_math::Vec4(0.0f, 0.0f, 1.0f, 0.0f);
		sea_math::Vec4 v3 = sea_math::Vec4(0.0f, 0.0f, 0.0f, 1.0f);

		sea_math::Mat4 mat = sea_math::Mat4(v0, v1, v2, v3);

		CHECK_EQUAL(true, sea_math::Mat4() == mat);
	}

	TEST(TestElementConstructor)
	{
		sea_math::Mat4 mat = sea_math::Mat4(1.0f, 0.0f, 0.0f, 0.0f,
						0.0f, 1.0f, 0.0f, 0.0f,
						0.0f, 0.0f, 1.0f, 0.0f,
						0.0f, 0.0f, 0.0f, 1.0f);

		CHECK_EQUAL(true, sea_math::Mat4() == mat);
	}

	TEST(TestIndexedMemberAccessOperator)
	{
		sea_math::Mat4 mat = sea_math::Mat4();

		CHECK_EQUAL(true, mat[0] == mat.data[0]);
		CHECK_EQUAL(true, mat[1] == mat.data[1]);
		CHECK_EQUAL(true, mat[2] == mat.data[2]);
		CHECK_EQUAL(true, mat[3] == mat.data[3]);
	}

	TEST(TestBinaryAdditionOperator)
	{
		sea_math::Mat4 mat1 = sea_math::Mat4();
		sea_math::Mat4 mat2 = sea_math::Mat4();
		sea_math::Mat4 mat3 = sea_math::Mat4(2.0f);

		CHECK_EQUAL(true, mat3 == mat1 + mat2);
	}

	TEST(TestBinarySubtractionOperator)
	{
		sea_math::Mat4 mat1 = sea_math::Mat4();
		sea_math::Mat4 mat2 = sea_math::Mat4();
		sea_math::Mat4 mat3 = sea_math::Mat4(0.0f);

		CHECK_EQUAL(true, mat3 == mat1 - mat2);
	}

	TEST(TestMatrixMultiplication)
	{
		sea_math::Mat4 identity = sea_math::Mat4();

		CHECK_EQUAL(true, sea_math::Mat4() == identity * identity);

		sea_math::Mat4 mat1 = sea_math::Mat4(3.0f);

		CHECK_EQUAL(true, mat1 == identity * mat1);

		sea_math::Mat4 mat2 = sea_math::Mat4(2.0f);

		CHECK_EQUAL(true, sea_math::Mat4(6.0f) == mat1 * mat2);

		sea_math::Mat4 mat3 = sea_math::Mat4(1.0f);
		mat3[3].x = 34.0f;
		mat3[3].y = -5.0f;
		mat3[3].z = 0.0f;

		CHECK_EQUAL(true, mat3 == identity * mat3);
	}

	TEST(TestFactorMultiplication)
	{
		sea_math::Mat4 identity = sea_math::Mat4();
		sea_math::scalar factor = 3.0f;

		CHECK_EQUAL(true, sea_math::Mat4(3.0f) == factor * identity);
		CHECK_EQUAL(true, sea_math::Mat4(3.0f) == identity * factor);
	}

	TEST(TestMatrixInverse)
	{
		sea_math::Mat4 identity = sea_math::Mat4();

		CHECK_EQUAL(true, identity == sea_math::Inverse(identity));

		sea_math::Mat4 mat = sea_math::Mat4(
			122.0f, 1.0f, 3.0f, 23.0f,
			32.0f, 3.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 4.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 5.0f
		);

		sea_math::Mat4 result = sea_math::Mat4(
			3/334.0f, -1/334.0f, -9/1336.0f, -69/1670.0f,
			-16/167.0f, 61/167.0f, 12/167.0f, 368/835.0f,
			0.0f, 0.0f, 1/4.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1/5.0f
		);

		sea_math::Mat4 inv = sea_math::Inverse(mat);

		CHECK_EQUAL(true, result == inv);
	}

	TEST(TestRowMatrixMultiplication)
	{
		sea_math::scalar scale = 2.0f;

		sea_math::Mat4 mat = sea_math::Mat4(scale);
		mat[3].w = 1.0f;

		sea_math::Vec4 point = sea_math::Vec4(1.0f, 3.0f, 10.0f);

		sea_math::Vec4 result = point * mat;

		CHECK_EQUAL(point.x * scale, result.x);
		CHECK_EQUAL(point.y * scale, result.y);
		CHECK_EQUAL(point.z * scale, result.z);
	}

	TEST(TestColMatrixMultiplication)
	{
		sea_math::Vec4 translate = sea_math::Vec4(10.0f, 3.04f, -4.3f);

		sea_math::Mat4 mat = sea_math::Mat4();
		mat[3] = translate;

		sea_math::Vec4 point = sea_math::Vec4(1.0f, 3.0f, 10.0f);

		sea_math::Vec4 result = mat * point;

		CHECK_EQUAL(point.x + translate.x, result.x);
		CHECK_EQUAL(point.y + translate.y, result.y);
		CHECK_EQUAL(point.z + translate.z, result.z);
	}

    TEST(TestGetTranslationTransform)
    {
        sea_math::Vec3 translation = sea_math::Vec3(1.0f, 2.0f, 3.0f);
        sea_math::Mat4 mat = sea_math::Mat4(1.0f);
        mat[3] = sea_math::Vec4(translation, 1.0f);

        CHECK_EQUAL(true, mat == sea_math::Mat4::GetTranslationTransform(translation));
    }

    TEST(TestGetScaleTransform)
    {
        sea_math::Vec3 scale = sea_math::Vec3(1.0f, 2.0f, 3.0f);
        sea_math::Mat4 mat = sea_math::Mat4(1.0f);
        mat[0][0] = scale.x;
        mat[1][1] = scale.y;
        mat[2][2] = scale.z;

        CHECK_EQUAL(true, mat == sea_math::Mat4::GetScaleTransform(scale));
    }

    TEST(TestGetRotationTransform)
    {
        sea_math::Quat rotation = sea_math::Quat();
        sea_math::Vec3 axis = sea_math::Vec3(0.0f, 1.0f, 0.0f);
        sea_math::scalar angle = sea_math::SEA_PI/2;
        rotation.Offset(axis, angle);

        sea_math::Mat4 mat = sea_math::CastMat4(rotation); 

        CHECK_EQUAL(true, mat == sea_math::Mat4::GetRotationTransform(rotation));
    }
};

