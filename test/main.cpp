#include <UnitTest++.h>

#include "engine/util/BMPFileTestSuite.h"
#include "engine/util/ObjFileTestSuite.h"
#include "engine/core/EntityManagerTestSuite.h"
#include "engine/core/EntityTestSuite.h"
#include "engine/core/InstanceTestSuite.h"
#include "engine/components/TransformComponentManagerTestSuite.h"
#include "engine/components/MeshComponentManagerTestSuite.h"
#include "engine/components/TextureComponentManagerTestSuite.h"
#include "engine/physics/OctreeTestSuite.h"

int main(void)
{
    return UnitTest::RunAllTests();
}
